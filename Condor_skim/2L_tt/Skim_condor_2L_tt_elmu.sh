#!/bin/bash

# Before submitting condor jobs, make sure you ran these commands already
# cmsenv
# voms-proxy-init --voms cms

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
#export MYPROXYPATH="$(voms-proxy-info -path)"
#export TARGETPROXYPATH="/afs/cern.ch/user/h/hboucham/private/x509up_file"
#echo ">>> ${BASH_SOURCE[0]}: Found proxy at: ${MYPROXYPATH}, copying to ${TARGETPROXYPATH}"

#cp ${MYPROXYPATH} /afs/cern.ch/user/h/hboucham/private/x509up

cd /afs/cern.ch/user/h/hboucham/work/H4B/CMSSW_13_1_0_pre4/src/demo-luna-framework/skim
cmsenv

# running on Condor instead of locally is about 5 times faster
bash runSkim_2L_tt.sh el mu unskimmed nom
