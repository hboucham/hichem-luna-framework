#include "ROOT/RDataFrame.hxx"
#include "ROOT/RSnapshotOptions.hxx"
#include "ROOT/RVec.hxx"

#include "Math/Vector4D.h"
#include "TChain.h"
#include "TROOT.h"
#include "TStopwatch.h"

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <stdio.h>
#include <unordered_map>
#include <chrono>
#include <TFile.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH3.h>
#include <algorithm> // std::sort
#include <TH1.h>
#include <TF1.h>
#include <TVector2.h>
#include <TRandom3.h>
#include <sstream>

#include "helpers/helpers.h"
#include "helpers/selection_2L_Z.h"
#include "helpers/lumiMask.h"
#include "helpers/JetEnergyCorrections.h"
//#include "helpers/LeptonSF_IDISO.h"
//#include "helpers/LeptonSF_trig.h"
//#include "helpers/PU_reweight.h"
//#include "helpers/Btag.h"
#include "correction.h"

using correction::CorrectionSet;

/*
 * Main function of the skimming step of the analysis.
 */

//template <typename T> std::string type_name();


//using namespace boost::placeholders;

//template <typename T>;

int main(int argc, char **argv) {

    // Can add command-line arguments for bells and whistles, for now ignore them 
    //(void) argc;
    //(void) argv;
    
/********************************************************************************************/ 
/************************** MANUAL ADJUSTMENT: Specify channel  *****************************/
   //std::string chan = "mu";
   //std::string chan = "el";
/********************************************************************************************/ 
/************************** MANUAL ADJUSTMENT: Specify file type  ***************************/
    //std::string type_file = "skim";
    //std::string type_file = "unskim";
/********************************************************************************************/ 
/********************************************************************************************/ 

  // Command line arguments
    int nArgc = 4;
    if (argc != nArgc) {
        std::cout << "[Error:] Incorrect number of arguments -- Use executable with following arguments: ./runSkim_2L_Z.sh channel(mu/el) samples(unskimmed/skims) sys(nom/JERup/JERdn/JESup/JESdn)" << std::endl;
        std::cout << "[Error:] Found " << argc << " arguments instead of " << nArgc << std::endl;
        return -1;
    }
    std::cout << ">>> ROOT Version " << gROOT->GetVersion() << std::endl;
    std::string chan = argv[1];
    std::cout << ">>> Running Dilepton category ZH, with both leptons = " << chan << std::endl;
    std::string type_file = argv[2];
    std::cout << ">>> The samples used are: " << type_file << std::endl;
    std::string syst = argv[3];
    std::cout << ">>> The systematics variation is: " << syst << std::endl;
    
    ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel
   
    TStopwatch time;  // track how long it took our script to run
    time.Start();

   // Run on one file only, can use xroottd or /eos/ path 
   /******************************************************************************************
   // std::string line = "root://cmsxrootd.fnal.gov///store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v1_2023_10_06/SUSY_ZH_ZToAll_HToAATo4B_Pt150_M-15_TuneCP5_13TeV_madgraph_pythia8/r1/PNet_v1.root";
   //  std::string line = "/eos/cms/store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v1_2023_10_06/SUSY_ZH_ZToAll_HToAATo4B_Pt150_M-All_TuneCP5_13TeV_madgraph_pythia8/r1/PNet_v1.root";
   //  const char *filename = line.c_str(); 
   //  TChain *ch = new TChain("Events");  // NanoAOD contains a TTree called "Events"
   //  int result = ch->Add(filename);
   ******************************************************************************************/

    // Specify input directory
    const char *input_dir_skim = "fileList/skims/";
    const char *input_dir_unskim = "fileList/unskimmed/";
    auto input_dir = (type_file == "skim") ? input_dir_skim : input_dir_unskim;
 
    // Specify output directory 
    std::string output_dir_lep = "/eos/user/h/hboucham/Haa4b/skim/2L_Z_" + chan +"_" + syst + "/";
    const char *output_dir = output_dir_lep.c_str();
    //const char *output_dir_mu = "/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu/";
    //const char *output_dir_el = "/eos/user/h/hboucham/Haa4b/skim/2L_Z_el/";
    //auto *output_dir = (chan == "mu") ? output_dir_mu : output_dir_el;
    
    // Specify test files
    const char *test_in = "SUSY_ZH_M-all.txt";
    //const char *test_in = "Postprocessor_SUSY_yield.txt";
    //const char *test_in = "SUSY_ZH_M-30_postprocessor_test.txt";
    //const char *test_in = "data_SingleMuonA.txt";
    //const char *test_in = "data_EGammaA.txt";
    const char *test_out_mu = "test_mu_skim.root";
    const char *test_out_el = "test_el_skim.root";
  
   // Number of samples
  const int nSamples = 19; 
  
  // List of text files with input samples
  const char *f_input_mu[] = {test_in, 
    // ZH HSS4b signal
    //"SUSYSS_ZH_M-15_0.txt", "SUSYSS_ZH_M-15_0p01.txt", "SUSYSS_ZH_M-15_0p1.txt",
    // ZH Signal
    "SUSY_ZH_M-15.txt", "SUSY_ZH_M-30.txt", "SUSY_ZH_M-55.txt",
   // DY jets M10-50 amcnlo and HT bins M-50 
 "DY_10-50.txt", "DY_50_HT70-100.txt", "DY_50_HT100-200.txt", "DY_50_HT200-400.txt", "DY_50_HT400-600.txt", "DY_50_HT600-800.txt","DY_50_HT800-1200.txt", "DY_50_HT1200-2500.txt", "DY_50_HT2500-inf.txt",
    // TTbar (2lep) and ZZ (2lep) NLO
    "TT_2L2Nu.txt", "ZZTo2Q2L.txt",
    // Data SingleMuon 2018
    "data_SingleMuonA.txt", "data_SingleMuonB.txt", "data_SingleMuonC.txt", "data_SingleMuonD.txt"};

  const char *f_input_el[] = {test_in, 
    // ZH HSS4b signal
    //"SUSYSS_ZH_M-15_0.txt", "SUSYSS_ZH_M-15_0p01.txt", "SUSYSS_ZH_M-15_0p1.txt",
    // ZH Signal
    "SUSY_ZH_M-15.txt", "SUSY_ZH_M-30.txt", "SUSY_ZH_M-55.txt",
   // DY jets M10-50 amcnlo and HT bins M-50 
 "DY_10-50.txt", "DY_50_HT70-100.txt", "DY_50_HT100-200.txt", "DY_50_HT200-400.txt", "DY_50_HT400-600.txt", "DY_50_HT600-800.txt","DY_50_HT800-1200.txt", "DY_50_HT1200-2500.txt", "DY_50_HT2500-inf.txt",
    // TTbar (2lep) and ZZ (2lep) NLO
    "TT_2L2Nu.txt", "ZZTo2Q2L.txt",
    // Data EGamma 2018
    "data_EGammaA.txt", "data_EGammaB.txt", "data_EGammaC.txt", "data_EGammaD.txt"};
  
  // Cannot use conditional to define const char, so this is the best way to do it currently
  auto f_input = (chan == "mu") ? f_input_mu : f_input_el;

  // List of output samples
  const char *f_output_mu[] = {test_out_mu, 
    // ZH Signal
    "SUSY_ZH_M-15_skim.root", "SUSY_ZH_M-30_skim.root", "SUSY_ZH_M-55_skim.root", 
   // DY jets M10-50 amcnlo and HT bins M-50
     "DY_10-50_skim.root", "DY_50_HT70-100_skim.root", "DY_50_HT100-200_skim.root", "DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root", "DY_50_HT600-800_skim.root","DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root",
    // TTbar (2lep) and ZZ (2lep) NLO
    "TT_2L2Nu_skim.root", "ZZTo2Q2L_skim.root",
    // Data SingleMuon 2018
    "data_SingleMuonA_skim.root", "data_SingleMuonB_skim.root", "data_SingleMuonC_skim.root", "data_SingleMuonD_skim.root"};
  
  const char *f_output_el[] = {test_out_el,
    // ZH Signal
    "SUSY_ZH_M-15_skim.root", "SUSY_ZH_M-30_skim.root", "SUSY_ZH_M-55_skim.root", 
   // DY jets M10-50 amcnlo and HT bins M-50
     "DY_10-50_skim.root", "DY_50_HT70-100_skim.root", "DY_50_HT100-200_skim.root", "DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root", "DY_50_HT600-800_skim.root","DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root",
    // TTbar (2lep) and ZZ (2lep) NLO
    "TT_2L2Nu_skim.root", "ZZTo2Q2L_skim.root",
    // Data EGamma 2018
    "data_EGammaA_skim.root", "data_EGammaB_skim.root", "data_EGammaC_skim.root", "data_EGammaD_skim.root"};
  auto f_output = (chan == "mu") ? f_output_mu : f_output_el;


  // Loading Golden JSON for lumi mask
  std::string jsonfile = "../commonFiles/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt"; 
  const auto myLumiMask = lumiMask::fromJSON(jsonfile);
  //std::cout << "Testing the JSON! Known good run/lumi returns: " << myLumiMask.accept(315257, 10) << ", and known bad run returns: " << myLumiMask.accept(315257, 90) << std::endl;
  auto goldenjson = [myLumiMask](unsigned int &run, unsigned int &luminosityBlock){return myLumiMask.accept(run, luminosityBlock);};

  // Looping over all samples (Need to be updated to be compatible with Condor)
  for (int i = 0; i < nSamples; i++){
    // Break statement for testing on 1 single sample
    if (i > 3){ break; }
    //if (i < 15){ continue; }
    //if (i < 10){ continue; }
   // if (i != 12){ continue; }

    // Loading sample from sample list
    std::cout << ">>> skim_2L_Z.cxx: Starting new sample! >>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Input file
    char f_input_full[200]; // hard coded size is a bad habit, sorry!
    // Concat directory and file name
    strcpy(f_input_full, input_dir);
    strcat(f_input_full, f_input[i]);
    FILE* input_files = fopen(f_input_full, "r");  
    std::cout << ">>> skim_2L_Z.cxx: Input Text file name: " << f_input_full << std::endl;
    
    // Output file
    char f_output_full[200]; // hard coded size is a bad habit, sorry!
    // Concat directory and file name
    strcpy(f_output_full, output_dir);
    strcat(f_output_full, f_output[i]);
    const std::string output = f_output_full;
    std::cout << ">>> skim_2L_Z.cxx: Output Skim file name: " << output << std::endl;
    if (input_files == NULL) {
        printf("The file is not found. The program will now exit.");
        exit(1);
    }

    TChain *ch = new TChain("Events");  // NanoAOD contains a TTree called "Events"
    char *line = NULL;
    size_t len = 0;
    char file_name[1024] = "";
    while(getline(&line, &len, input_files) != -1) {
	    if (len > 0 && line[len-1] == '\n'){
    	  line[len-1] = 0;
	    }
	    strncpy(file_name, line + 0, len - 3);
	    file_name[strlen(file_name)-1] ='\0';
	    if (file_name[strlen(file_name)-1]=='\n'){ // Added to deal with txt files with only 1 line
		    file_name[strlen(file_name)-1]='\0';
	    } 
	    ch->Add(file_name);
	    strcpy(file_name, "");
    }        
    fclose(input_files);

    // Create the RDataFrame object 
    ROOT::RDataFrame df(*ch);
    std::cout << ">>> skim_2L_Z.cxx: Starting!" << std::endl;
    // Applying Lumi Mask
    ROOT::RDF::RNode df1 = df;
    if (Helper::containsSubstring(f_input[i],"data")){
      df1 = df1.Define("passesJSON", goldenjson, {"run","luminosityBlock"});
      df1 = df1.Filter("passesJSON == 1", "Data passes Golden JSON");
    }
    std::cout << ">>> Step 1 (Data): Lumi Mask Applied" << std::endl;


    
    // Applying different selections and defining relevant branches
    auto df2 = ApplyNoiseFilter(df1);
    std::cout << ">>> Step 2: Noise Filters Applied" << std::endl;
    auto df3 = ApplyTrigger(df2, chan);
    std::cout << ">>> Step 3: Lepton Triggers Done" << std::endl;
    auto df4 = ApplyLooseSelection(df3, chan); 
    std::cout << ">>> Step 4: Lepton Loose Selection Done" << std::endl;
    auto df5 = ApplyJEC(df4, f_input[i], syst);
    std::cout << ">>> Step 5: JEC applied" << std::endl;
    auto df6 = SelectFatJet(df5);
    std::cout << ">>> Step 6: Leading AK8-Jet Selection Done" << std::endl; 
    auto df7 = SelectLeadingPair(df6, chan);
    std::cout << ">>> Step 7: Lepton Pair Selection Done" << std::endl;
    auto df8 = Lepton_vars(df7, chan);
    std::cout << ">>> Step 8: Adding Lepton branches Done" << std::endl;
    auto df9 = FatJet_vars(df8);
    std::cout << ">>> Step 9: Adding AK8 branches Done" << std::endl;
    auto df10 = Lepton_mother(df9, chan, f_input[i]);
    std::cout << ">>> Step 10: Saving Lepton Mother Done" << std::endl;
    auto df11 = JetInfo(df10);
    std::cout << ">>> Step 11: Saving AK4 Jet Information Done" << std::endl;
    auto df12 = SignalMC_GenMatch(df11, f_input[i]);
    std::cout << ">>> Step 12 (Signal MC): AK8-Jet Gen-match Done" << std::endl;
    auto df13 = QCD_stitch(df12, f_input[i]);
    std::cout << ">>> Step 13 (QCD MC): QCD Samples Stitching Done" << std::endl;
    auto df14 = Gen_info(df13, f_input[i]);
    std::cout << ">>> Step 14 (DY MC): Gen Z Information Saved" << std::endl;
    auto dfFinal = df14;
    
    // Save the branches below in output n-tuple
    std::vector<std::string> finalVariables = {
    			 // AK8 candidate branches
           "pt_fatjet", "eta_fatjet", "phi_fatjet", "m_fatjet", "msoftdrop_fatjet",
           "msoftscaled_fatjet", "mtopo_fatjet", "msofttopo_fatjet",
           "mH_avg", "mA_avg", "score_Haa4b_vs_QCD", "score_Haa4b_vs_QCD_v1", "score_Haa4b_vs_QCD_34",
           "score_Haa4b_vs_QCD_v2a", "score_Haa4b_vs_QCD_v2b","mA_34a", "mA_34b", "mA_34d",
           
           // lepton branches
           "pt_l1", "eta_l1", "phi_l1", "m_l1", "pt_l2", "eta_l2", "phi_l2", "m_l2",
           "m_ll", "pt_ll", "eta_ll", "phi_ll", "ngoodMuons", "ngoodElectrons",
           "dxy_l1", "dz_l1", "ip3d_l1", "jetPtRelv2_l1", "jetRelIso_l1", 
           "dxy_l2", "dz_l2", "ip3d_l2", "jetPtRelv2_l2", "jetRelIso_l2",
           "Mother_l1", "Mother_l2", "trigger_IDbit", /*"triglep_l1", "triglep_l2",*/
           /*"Double_trigger_bit",*/
           
           // Muon Iso and ID branches
           "Muon_pfRelIso04_all_l1", "Muon_miniIsoId_l1", "Muon_pfIsoId_l1", "Muon_miniPFRelIso_all_l1",
           "Muon_pfRelIso04_all_l2", "Muon_miniIsoId_l2", "Muon_pfIsoId_l2", "Muon_miniPFRelIso_all_l2",
           "Muon_pfRelIso04_all_max", "Muon_miniIsoId_min", "Muon_pfIsoId_min", "Muon_miniPFRelIso_all_max",
           "Idbit_l1", "Idbit_l2", "highpTId_l1", "highpTId_l2",
           
           // Electron ID branches
           "Idbit_l1_mva", "Idbit_l2_mva", "Idbit_l1_cut", "Idbit_l2_cut", 
           
           // MET and dphi branches
           "pt_MET", "phi_MET", "dphi_MET_fatjet", "mt_MET_ll","mt_MET_l1", "mt_MET_l2", 
           "dphi_fatjet_ll", "deta_fatjet_ll", "dphi_MET_ll", "dphi_MET_l1", "dphi_MET_l2",
           "dphi_l1_j1", "dphi_l1_b1", "dphi_l1_l2", "dr_l1_l2",
           
           // AK4 vars
           "pt_jet1", "pt_jet2", "eta_jet1", "eta_jet2", "phi_jet1", "phi_jet2",
           "pt_bjet1", "pt_bjet2", "eta_bjet1", "eta_bjet2", "phi_bjet1", "phi_bjet2",
           "pt_SSum", "pt_VSum", "nJet_add", "nJetCent_add", "nBJetM_add",
           "mindR_fatjet_jet", "nJet_add1", "nJet_add2", "nJet_add3", "nJet_add4",
           
           // Postprocessing branches (SF, Gen info..)
           "gen_pt_Z", "gen_pt_H", "gen_mass_Z", "gen_weight", "npu", "HT_LHE", 
           "GENnBhadron_fatjet", "Run_number", "LHEPdfWeight", "PSWeight",
           "TrigObj_id", "TrigObj_eta", "TrigObj_phi", "Pileup_nTrueInt",
           "event",
           
           // Btag SF branches 
           "Jet_btagSF_deepjet_M", "Jet_btagSF_deepjet_M_up", "Jet_btagSF_deepjet_M_down", 
           "Jet_btagSF_deepjet_M_up_correlated", "Jet_btagSF_deepjet_M_down_correlated",
           "Jet_btagSF_deepjet_M_up_uncorrelated", "Jet_btagSF_deepjet_M_down_uncorrelated",
            
    }; 

    // name of the event tree is set here
    dfFinal.Snapshot("event_tree", output, finalVariables);
    std::cout << ">>> skim_2L_Z.cxx: Output ntuple saved" << std::endl;
    // Print the cutflow report
    auto report = dfFinal.Report();
    report->Print();
  } 
  time.Stop();
  time.Print();
  return 0;
}
