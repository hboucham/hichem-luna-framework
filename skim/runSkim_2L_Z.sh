# Usage:
# bash runSkim_2L_Z.sh mu/el unskimmed/skims nom/JERup/JERdn/JECup/JECdn

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi


#--------------------------------------------------------
# Compile
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Compiling skim_2L_Z.cxx executable ..."
TARGET_FILES="helpers/lumiMask.cc"
COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
#FLAGS=$(root-config correction config --cflags --ldflags root-config)
#FLAGS=$(root-config correction config --cflags --libs --ldflags --rpath)
FLAGSS=$(correction config --cflags --ldflags --rpath)
time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o Skim_2L_Z skim_2L_Z.cxx ${TARGET_FILES} $FLAGS $FLAGSS

if [[ $? -ne 0 ]]; then
    echo ">>> Compile failed, exit"
    exit 1
fi


#--------------------------------------------------------
# Execute
#--------------------------------------------------------
./Skim_2L_Z $1 $2 $3
