# Usage:
# bash runSkim_1L.sh mu/el unskimmed/skims

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi


#--------------------------------------------------------
# Compile
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Compiling skim_1L.cxx executable ..."
TARGET_FILES="helpers/lumiMask.cc"
COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
FLAGSS=$(correction config --cflags --ldflags --rpath)
time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o Skim_1L skim_1L.cxx ${TARGET_FILES} $FLAGS $FLAGSS

if [[ $? -ne 0 ]]; then
    echo ">>> Compile failed, exit"
    exit 1
fi


#--------------------------------------------------------
# Execute
#--------------------------------------------------------
./Skim_1L $1 $2 $3
