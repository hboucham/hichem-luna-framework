#ifndef LEADING_TAU_TAU_H_INCL
#define LEADING_TAU_TAU_H_INCL

#include "helpers.h"
#include <string>
#include "lumiMask.h"
#include <algorithm>
#include <cmath>

static constexpr auto zeroval = 0.f; // define const in header for example
static constexpr auto Z_gen_mass = 91.25f; // Z gen mass

/**********************************************************/

template <typename T>
auto ApplyNoiseFilter(T &df) {
// Filtering events that fail Noise Filters
  return df.Filter("Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonFilter && Flag_BadPFMuonDzFilter && Flag_eeBadScFilter && Flag_ecalBadCalibFilter", "ApplyNoiseFilter: Has events passing Noise Filters");
}

/**********************************************************/

template <typename T>
auto ApplyTrigger(T &df, std::string channel) {
// Filtering events that fail lepton triggers
  
  if (channel == "mu"){ // Muon Channel Triggers
    return df.Filter("(L1_SingleMu22 || L1_SingleMu25) && (HLT_IsoMu24 || HLT_Mu50)", "ApplyTrigger: Has events passing Muon triggers");
  
  }else if (channel == "el"){ // Electron Channel Triggers
    return df.Filter("HLT_Ele32_WPTight_Gsf || HLT_Ele35_WPTight_Gsf_L1EGMT || HLT_Ele115_CaloIdVT_GsfTrkIdT || HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165", "ApplyTrigger: Has events passing Electron triggers");

  }else{
    return df;
  }
}

/**********************************************************/

template <typename T>
auto ApplyLooseSelection(T &df, std::string lep1, std::string lep2) {
  // baseline (good) leptons
  auto df2 = df.Define("goodMuons", "Muon_pt > 10 && abs(Muon_eta) < 2.4  && Muon_miniPFRelIso_all < 0.10 && (Muon_mediumPromptId || ( Muon_pt > 200 && Muon_highPtId > 0)) && (abs(Muon_dz) < 0.1) && (abs(Muon_dxy) < 0.02)")
        // Muon_miniPFRelIso_all < 0.20 (medium WP/ Muon_miniIsoId = 2) or  Muon_miniPFRelIso_all < 0.10 (tight WP/ Muon_miniIsoId = 3)
               .Define("ngoodMuons", "Sum(goodMuons)")
               .Define("goodElectrons", "Electron_pt > 10 && abs(Electron_eta) < 2.5 && (abs(Electron_eta) < 1.44 || abs(Electron_eta) > 1.57) && Electron_mvaFall17V2Iso_WPL && (Electron_mvaFall17V2Iso_WP90 || (Electron_pt > 35 && Electron_cutBased_HEEP))")
               .Define("ngoodElectrons", "Sum(goodElectrons)")
                
               // trigger leptons
               .Define("triggerMuons", "goodMuons && Muon_pt > 26")
               .Define("triggerElectrons", "goodElectrons && Electron_pt > 35 && abs(Electron_dxy) < 0.02 && abs(Electron_dz) < 0.10 && Electron_mvaFall17V2Iso_WP90 && (Electron_mvaFall17V2Iso_WP80 || (Electron_pt > 35 && Electron_cutBased_HEEP))")
               .Define("ntriggerMuons", "Sum(triggerMuons)")
               .Define("ntriggerElectrons", "Sum(triggerElectrons)");
  
  //Muon-muon Loose Selection
  if (lep1 == "mu" && lep2 == "mu"){ 
    return df2.Filter("ngoodMuons > 1", "ApplyLooseSelection: event with exactly 2 good Muons and 0 good Electrons")
              .Filter("ntriggerMuons > 0", "ApplyLooseSelection: event has at least 1 trigger muon");
 
  //Electron-electron Loose Selection
  }else if (lep1 == "el" && lep2 == "el"){ 
    return df2.Filter("ngoodElectrons > 1", "ApplyLooseSelection: event with exactly 2 good Electrons and 0 good Muons")
              .Filter("ntriggerElectrons > 0", "ApplyLooseSelection: event has at least 1 trigger electron");
  
  //Muon-electron Loose Selection
  }else if (lep1 == "mu" && lep2 == "el"){ 
    return df2.Filter("ngoodMuons > 0 && ngoodElectrons > 0", "ApplyLooseSelection: event with exactly 1 good Muon and 1 good Electron")
              .Filter("ntriggerMuons > 0", "ApplyLooseSelection: event has 1 trigger muon");
 
  //Electron-muon Loose Selection
  }else{ // (lep1 == "el" && lep2 == "mu")
    return df2.Filter("ngoodMuons > 0 && ngoodElectrons > 0", "ApplyLooseSelection: event with exactly 1 good Electron and 1 good Muon")
              .Filter("ntriggerElectrons > 0 && ntriggerMuons == 0", "ApplyLooseSelection: event has 1 trigger electron and 0 trigger Muons");
  }
}

/**********************************************************/

template <typename T>
auto SelectFatJet(T &df) {
   // recasting as int to avoid error processing skims vas unskimmed
    return df.Define("nfatjet", "(int) nFatJet") 
             .Define("njet", "(int) nJet") 
             
             // FatJet H candidate selection
             .Define("FatJet_particleNetMD_Xbb_vs_QCD", "FatJet_particleNetMD_Xbb/(FatJet_particleNetMD_Xbb + FatJet_particleNetMD_QCD)")
             .Define("goodFatJet", "cFatJet_pt > 170 &&  FatJet_particleNetMD_Xbb_vs_QCD > 0.75 && FatJet_jetId >= 6 && cFatJet_msoftdrop > 20 && FatJet_eta < 2.4")
             // v2 tagger AK8(3b) + AK4(b) tagger used to rank AK8 H cand
             .Define("FatJet_PNet_v2max", "FatJet_PNet_X4b_v2a_Haa34b_score + FatJet_PNet_X4b_v2b_Haa34b_score")
             .Define("idx_fatjet", Helper::Leading_FatJet_2L, {"goodFatJet", "FatJet_PNet_v2max"}) 
             .Filter("idx_fatjet != -1", "SelectFatJet: event has at least 1 AK8 H candidate")
             
             // Save H candidate information
             .Define("pt_fatjet", "cFatJet_pt[idx_fatjet]")
             .Define("eta_fatjet", "FatJet_eta[idx_fatjet]")
             .Define("phi_fatjet", "FatJet_phi[idx_fatjet]")
             .Define("m_fatjet",   "cFatJet_mass[idx_fatjet]")
             .Define("msoftdrop_fatjet",   "cFatJet_msoftdrop[idx_fatjet]")
            
             // ParticleNet branches
             // v2 tagger AK8(4b)
             .Define("score_Haa4b_vs_QCD",   "0.5*(FatJet_PNet_X4b_v2a_Haa4b_score[idx_fatjet] + FatJet_PNet_X4b_v2b_Haa4b_score[idx_fatjet])")
             // v1 tagger AK8(4b)
             .Define("score_Haa4b_vs_QCD_v1",   "FatJet_PNet_X4b_v1_Haa4b_vs_QCD[idx_fatjet]")
             // Other v2 taggers
             .Define("score_Haa4b_vs_QCD_34",   "0.5*FatJet_PNet_v2max[idx_fatjet]")
             .Define("score_Haa4b_vs_QCD_v2a",   "FatJet_PNet_X4b_v2a_Haa4b_score[idx_fatjet]")
             .Define("score_Haa4b_vs_QCD_v2b",   "FatJet_PNet_X4b_v2b_Haa4b_score[idx_fatjet]")
             
             //.Filter("score_Haa4b_vs_QCD > 0.8", "SelectFatJet: Applying ParticleNet tagger cut")
             // PNET regressed H mass
             .Define("mH_avg",   "FatJet_PNet_massH_v2b[idx_fatjet]")
             // PNET regressed a mass
             .Define("mA_avg",   "FatJet_PNet_massAa[idx_fatjet]")
             .Define("mA_34a",   "FatJet_PNet_34massAa[idx_fatjet]")
             .Define("mA_34b",   "FatJet_PNet_34massAb[idx_fatjet]")
             .Define("mA_34d",   "FatJet_PNet_34massAd[idx_fatjet]")
             //.Define("mH_avg",   "FatJet_PNet_massH_avg[idx_fatjet]")
             //.Define("mA_avg",   "FatJet_PNet_massA_avg[idx_fatjet]")
             
             // Save MET branches
             .Define("pt_MET", "MET_pt")
             .Define("phi_MET", "MET_phi")
             .Define("pt_PUPPIMET", "PuppiMET_pt")
             .Define("phi_PUPPIMET", "PuppiMET_phi");

}

/**********************************************************/

template <typename T>
auto SelectLeptonPair(T &df, std::string lep1, std::string lep2) {
                // Check H cand overlap with leptons in the event
  auto df2 = df.Define("dR_fatjet_mu", Helper::VDeltaR, {"eta_fatjet","Muon_eta", "phi_fatjet", "Muon_phi"})
               .Define("dR_fatjet_el", Helper::VDeltaR, {"eta_fatjet","Electron_eta", "phi_fatjet", "Electron_phi"})
               
               // check if any trigger leptons overlap with H candidate and
               // discard events where that happens
               .Define("triggerMuons_OverlapH", "triggerMuons && dR_fatjet_mu < 0.8")
               .Define("ntriggerMuons_OverlapH", "Sum(triggerMuons_OverlapH)")
               .Define("triggerElectrons_OverlapH", "triggerElectrons && dR_fatjet_el < 0.8")
               .Define("ntriggerElectrons_OverlapH", "Sum(triggerElectrons_OverlapH)")
               .Filter("ntriggerElectrons_OverlapH == 0 && ntriggerMuons_OverlapH == 0", "SelectLeadingPair: Discard event with a trigger lepton overlapping with H candidate")
               
               // Check how many good leptons do NOT overlap with H cnadidate,
               // that number determines 1L or 2L category
               .Define("goodMuons_NoOverlapH", "goodMuons && dR_fatjet_mu > 0.8")
               .Define("goodElectrons_NoOverlapH", "goodElectrons && dR_fatjet_el > 0.8")
               .Define("ngoodMuons_NoOverlapH", "Sum(goodMuons_NoOverlapH)")
               .Define("ngoodElectrons_NoOverlapH", "Sum(goodElectrons_NoOverlapH)");

  if (lep1 == "mu" && lep2 == "mu"){ 
   return df2.Filter("ngoodMuons_NoOverlapH == 2 && ngoodElectrons_NoOverlapH == 0", " SelectLeadingPair: event has exactly 2 non overlapping muons with H candidate and 0 non-overlapping electrons with H candidate")
            
     // Find Lepton pair: idx_1 is always a trigger muon. If there are
            // 2 trigger muons, idx_1 will have higher pT
             .Define("pairIdx", Helper::FindPairIndices, {"goodMuons_NoOverlapH", "triggerMuons", "Muon_pt"})
             .Define("idx_1", "pairIdx[0]")
             .Define("idx_2", "pairIdx[1]")
             
             // Save Muon Pair Information
             // Muon 1
             .Define("m_l1",   "Muon_mass[idx_1]")
             .Define("pt_l1",  "Muon_pt[idx_1]")
             .Define("eta_l1", "Muon_eta[idx_1]")
             .Define("phi_l1", "Muon_phi[idx_1]")
             .Define("charge_l1", "Muon_charge[idx_1]")
             //.Define("triglep_l1",   "triggerMuons[idx_1]")

             // Muon 2
             .Define("m_l2",   "Muon_mass[idx_2]")
             .Define("pt_l2", "Muon_pt[idx_2]")
             .Define("eta_l2", "Muon_eta[idx_2]")
             .Define("phi_l2", "Muon_phi[idx_2]") 
             .Define("charge_l2", "Muon_charge[idx_2]");
             //.Define("triglep_l2",   "triggerMuons[idx_2]");
             
  }else if (lep1 == "el" && lep2 == "el"){ 
   return df2.Filter("ngoodMuons_NoOverlapH == 0 && ngoodElectrons_NoOverlapH == 2", " SelectLeadingPair: event has exactly 0 non overlapping muons with H candidate and 2 non-overlapping electrons with H candidate")
             
            // Find Lepton pair: idx_1 is always a trigger electron. If there are
            // 2 trigger electrons, idx_1 will have higher pT
             .Define("pairIdx", Helper::FindPairIndices, {"goodElectrons_NoOverlapH", "triggerElectrons", "Electron_pt"})
             .Define("idx_1", "pairIdx[0]")
             .Define("idx_2", "pairIdx[1]")
             
             // Save Electron Pair Information
             // Electron 1
             .Define("m_l1",   "Electron_mass[idx_1]")
             .Define("pt_l1",  "Electron_pt[idx_1]")
             .Define("eta_l1", "Electron_eta[idx_1]")
             .Define("phi_l1", "Electron_phi[idx_1]")
             .Define("charge_l1", "Electron_charge[idx_1]")
             //.Define("triglep_l1",   "triggerElectrons[idx_1]")
             
             // Electron 2
             .Define("m_l2",   "Electron_mass[idx_2]")
             .Define("pt_l2", "Electron_pt[idx_2]")
             .Define("eta_l2", "Electron_eta[idx_2]")
             .Define("phi_l2", "Electron_phi[idx_2]") 
             .Define("charge_l2", "Electron_charge[idx_2]");
             //.Define("triglep_l2",   "triggerElectrons[idx_2]");

  }else if (lep1 == "mu" && lep2 == "el"){ 
   return df2.Filter("ngoodMuons_NoOverlapH == 1 && ngoodElectrons_NoOverlapH == 1", " SelectLeadingPair: event has exactly 1 non overlapping muons with H candidate and 1 non-overlapping electrons with H candidate")
             // For Muon-electron, require exactly 1 trigger muon nor
             // overlapping with H candidate
             .Define("triggerMuons_NoOverlapH", "goodMuons_NoOverlapH == 1 && triggerMuons == 1") 
             .Define("ntriggerMuons_NoOverlapH", "Sum(triggerMuons_NoOverlapH)") 
             .Filter("ntriggerMuons_NoOverlapH == 1", "SelectLeadingPair: event has exactly one trigger muon not overlapping with H")
             .Define("idx_1", Helper::Find_index, {"triggerMuons_NoOverlapH"})
             .Define("idx_2", Helper::Find_index, {"goodElectrons_NoOverlapH"})
             
             // Save Muon-Electron Pair Information
             // Muon 1
             .Define("m_l1",   "Muon_mass[idx_1]")
             .Define("pt_l1",  "Muon_pt[idx_1]")
             .Define("eta_l1", "Muon_eta[idx_1]")
             .Define("phi_l1", "Muon_phi[idx_1]")
             .Define("charge_l1", "Muon_charge[idx_1]")
             //.Define("triglep_l1",   "triggerMuons[idx_1]")
             
             // Electron 2
             .Define("m_l2",   "Electron_mass[idx_2]")
             .Define("pt_l2", "Electron_pt[idx_2]")
             .Define("eta_l2", "Electron_eta[idx_2]")
             .Define("phi_l2", "Electron_phi[idx_2]") 
             .Define("charge_l2", "Electron_charge[idx_2]");
             //.Define("triglep_l2",   "triggerElectrons[idx_2]");

  }else{ // (lep1 == "el" && lep2 == "mu")
   return df2.Filter("ngoodMuons_NoOverlapH == 1 && ngoodElectrons_NoOverlapH == 1", " SelectLeadingPair: event has exactly 1 non overlapping muons with H candidate and 1 non-overlapping electrons with H candidate")
             
             // For Electron-muon, require exactly 1 trigger electron not
             // overlapping with H candidate AND require muon to not be trigger muon
             // Note that the second requirement was specified in el-mu the Loose selection
             .Define("triggerElectrons_NoOverlapH", "goodElectrons_NoOverlapH == 1 && triggerElectrons == 1") 
             .Define("ntriggerElectrons_NoOverlapH", "Sum(triggerElectrons_NoOverlapH)") 
             .Filter("ntriggerElectrons_NoOverlapH == 1", "SelectLeadingPair: event has exactly one trigger electron not overlapping with H")
             .Define("idx_1", Helper::Find_index, {"triggerElectrons_NoOverlapH"})
             .Define("idx_2", Helper::Find_index, {"goodMuons_NoOverlapH"})
             
             // Save Electron-Muon Pair Information
             // Electron 1
             .Define("m_l1",   "Electron_mass[idx_1]")
             .Define("pt_l1",  "Electron_pt[idx_1]")
             .Define("eta_l1", "Electron_eta[idx_1]")
             .Define("phi_l1", "Electron_phi[idx_1]")
             .Define("charge_l1", "Electron_charge[idx_1]")
             //.Define("triglep_l1",   "triggerElectrons[idx_1]")
             
             // Muon 2
             .Define("m_l2",   "Muon_mass[idx_2]")
             .Define("pt_l2", "Muon_pt[idx_2]")
             .Define("eta_l2", "Muon_eta[idx_2]")
             .Define("phi_l2", "Muon_phi[idx_2]") 
             .Define("charge_l2", "Muon_charge[idx_2]");
             //.Define("triglep_l2",   "triggerMuons[idx_2]");
  }
}

/**********************************************************/
template <typename T>
auto Lepton_vars(T &df, std::string lep1, std::string lep2) {
             // Defining lep1+lep2 object and branches 
    auto df2 = df.Define("p4_l1", Helper::add_p4, {"pt_l1", "eta_l1", "phi_l1", "m_l1"})
                 .Define("p4_l2", Helper::add_p4, {"pt_l2", "eta_l2", "phi_l2", "m_l2"}) 
                 .Define("m_ll",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  }, {"p4_l1", "p4_l2"})
                 .Define("pt_ll", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_l1", "p4_l2"})
                 .Define("eta_ll", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Eta(); }, {"p4_l1", "p4_l2"})
                 .Define("phi_ll", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Phi(); }, {"p4_l1", "p4_l2"});

  if (lep1 == "mu" && lep2 == "mu"){ 
    // Require m_ll outside Z mass window, above 12 GeV and 0 charge
    return df2.Filter("(charge_l1 + charge_l2) == 0 && abs(m_ll - 90.0) >  10.0 && m_ll > 12", "SelectLeadingPair: Require same flavor lepton pair to be outside Z mass window, on top of opposite sign and m_ll > 12 GeV.")

      // Saving relevant Muon 1 branches
             .Define("Muon_pfRelIso04_all_l1",   "Muon_pfRelIso04_all[idx_1]")
             .Define("Muon_pfIsoId_l1",   "(int) Muon_pfIsoId[idx_1]")
             .Define("Muon_miniPFRelIso_all_l1",   "Muon_miniPFRelIso_all[idx_1]")
             .Define("Muon_miniIsoId_l1",   Helper::MiniIsoID_corrected, {"Muon_miniPFRelIso_all_l1"})
             .Define("Idbit_l1", "1*Muon_mediumId[idx_1] + 2*Muon_mediumPromptId[idx_1] + 4*Muon_tightId[idx_1] + 8*(Muon_highPtId[idx_1] > 0)")
    
      // Saving relevant Muon 2 branches
             .Define("Muon_pfRelIso04_all_l2",   "Muon_pfRelIso04_all[idx_2]")
             .Define("Muon_pfIsoId_l2",   "(int) Muon_pfIsoId[idx_2]")
             .Define("Muon_miniPFRelIso_all_l2", "Muon_miniPFRelIso_all[idx_2]")
             .Define("Muon_miniIsoId_l2",   Helper::MiniIsoID_corrected, {"Muon_miniPFRelIso_all_l2"})
             .Define("Idbit_l2", "1*Muon_mediumId[idx_2] + 2*Muon_mediumPromptId[idx_2] + 4*Muon_tightId[idx_2] + 8*(Muon_highPtId[idx_2] > 0)")
       // Saving Muon 1/2 branches and trigger bit
             .Define("Muon_miniIsoId_min",  [](int l1, int l2) { return (int) std::min(l1, l2);  }, {"Muon_miniIsoId_l1", "Muon_miniIsoId_l2"})
             .Define("Muon_pfIsoId_min",  [](int l1, int l2) { return (int) std::min(l1, l2);  }, {"Muon_pfIsoId_l1", "Muon_pfIsoId_l2"})
             .Define("Muon_miniPFRelIso_all_max",  [](float l1, float l2) { return (float) std::max(l1, l2);  }, {"Muon_miniPFRelIso_all_l1", "Muon_miniPFRelIso_all_l2"})
             .Define("Muon_pfRelIso04_all_max",  [](float l1, float l2) { return (float) std::max(l1, l2);  }, {"Muon_pfRelIso04_all_l1", "Muon_pfRelIso04_all_l2"})
             .Define("trigger_IDbit", "1*HLT_IsoMu24 + 2*HLT_Mu50")
    
       // Set electron branches to 0
             .Define("Idbit_l1_mva",  [=]() { return (int) 0; })
             .Define("Idbit_l2_mva",  [=]() { return (int) 0; })
             .Define("Idbit_l1_cut",  [=]() { return (int) 0; })
             .Define("Idbit_l2_cut",  [=]() { return (int) 0; });
  
  }else if (lep1 == "el" && lep2 == "el"){ 
    // Require m_ll outside Z mass window, above 12 GeV and 0 charge
   return df2.Filter("(charge_l1 + charge_l2) == 0 && abs(m_ll - 90.0) >  10.0 && m_ll > 12", "SelectLeadingPair: Require same flavor lepton pair to be outside Z mass window, on top of opposite sign and m_ll > 12 GeV.")
  
      // Saving relevant Electron 1 branches   
             .Define("Idbit_l1_mva",   "1*Electron_mvaFall17V2Iso_WPL[idx_1] + 2*Electron_mvaFall17V2Iso_WP90[idx_1] + 4*Electron_mvaFall17V2Iso_WP80[idx_1] + 8*Electron_cutBased_HEEP[idx_1]")
             .Define("Idbit_l1_cut",   "1*(Electron_cutBased[idx_1] > 1) + 2*(Electron_cutBased[idx_1] > 2) + 4*(Electron_cutBased[idx_1] > 3) + 8*Electron_cutBased_HEEP[idx_1]") 
      
      // Saving relevant Electron 2 branches   
             .Define("Idbit_l2_mva",   "1*Electron_mvaFall17V2Iso_WPL[idx_2] + 2*Electron_mvaFall17V2Iso_WP90[idx_2] + 4*Electron_mvaFall17V2Iso_WP80[idx_2] + 8*Electron_cutBased_HEEP[idx_2]")
             .Define("Idbit_l2_cut",   "1*(Electron_cutBased[idx_2] > 1) + 2*(Electron_cutBased[idx_2] > 2) + 4*(Electron_cutBased[idx_2] > 3) + 8*Electron_cutBased_HEEP[idx_2]") 
       
      // Saving Electron trigger bit
             .Define("trigger_IDbit", "1*HLT_Ele32_WPTight_Gsf + 2*HLT_Ele35_WPTight_Gsf_L1EGMT +4*HLT_Ele115_CaloIdVT_GsfTrkIdT + 8*HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165")

    // Set Muon branches to 0
             .Define("Muon_pfRelIso04_all_l1",  [=]() { return (float) 0.0; })
             .Define("Muon_pfRelIso04_all_l2",  [=]() { return (float) 0.0; })
             .Define("Muon_pfIsoId_l1",  [=]() { return (int) 0; })
             .Define("Muon_pfIsoId_l2",  [=]() { return (int) 0; })
             .Define("Muon_miniPFRelIso_all_l1",  [=]() { return (float) 0.0; })
             .Define("Muon_miniPFRelIso_all_l2",  [=]() { return (float) 0.0; })
             .Define("Muon_miniIsoId_l1",  [=]() { return (int) 0; })
             .Define("Muon_miniIsoId_l2",  [=]() { return (int) 0; })
             .Define("Idbit_l1",  [=]() { return (int) 0; })
             .Define("Idbit_l2",  [=]() { return (int) 0; })
             .Define("Muon_miniIsoId_min",  [=]() { return (int) 0; })
             .Define("Muon_pfIsoId_min",  [=]() { return (int) 0; })
             .Define("Muon_miniPFRelIso_all_max",  [=]() { return (float) 0; })
             .Define("Muon_pfRelIso04_all_max",  [=]() { return (float) 0; });

  }else if (lep1 == "mu" && lep2 == "el"){ 
    // Require m_ll above 12 GeV and 0 charge
   return df2.Filter("(charge_l1 + charge_l2) == 0 && m_ll > 12", "SelectLeadingPair: Require different flavor lepton pair to have opposite sign and m_ll > 12 GeV.")
    
     // Saving relevant Muon 1 branches and trigger bit
             .Define("Muon_pfRelIso04_all_l1",   "Muon_pfRelIso04_all[idx_1]")
             .Define("Muon_pfIsoId_l1",   "(int) Muon_pfIsoId[idx_1]")
             .Define("Muon_miniPFRelIso_all_l1",   "Muon_miniPFRelIso_all[idx_1]")
             .Define("Muon_miniIsoId_l1",   Helper::MiniIsoID_corrected, {"Muon_miniPFRelIso_all_l1"})
             .Define("Idbit_l1", "1*Muon_mediumId[idx_1] + 2*Muon_mediumPromptId[idx_1] + 4*Muon_tightId[idx_1] + 8*(Muon_highPtId[idx_1] > 0)")
             .Define("trigger_IDbit", "1*HLT_IsoMu24 + 2*HLT_Mu50")
    
     // Set Muon 2 branches to 0
             .Define("Muon_pfRelIso04_all_l2",  [=]() { return (float) 0.0; })
             .Define("Muon_pfIsoId_l2",  [=]() { return (int) 0; })
             .Define("Muon_miniPFRelIso_all_l2",  [=]() { return (float) 0.0; })
             .Define("Muon_miniIsoId_l2",  [=]() { return (int) 0; })
             .Define("Idbit_l2",  [=]() { return (int) 0; })
    
     // Set Muon min/max branches to 0
             .Define("Muon_miniIsoId_min",  [=]() { return (int) 0; })
             .Define("Muon_pfIsoId_min",  [=]() { return (int) 0; })
             .Define("Muon_miniPFRelIso_all_max",  [=]() { return (float) 0; })
             .Define("Muon_pfRelIso04_all_max",  [=]() { return (float) 0; })
   
     // Saving relevant Electron 2 branches 
             .Define("Idbit_l2_mva",   "1*Electron_mvaFall17V2Iso_WPL[idx_2] + 2*Electron_mvaFall17V2Iso_WP90[idx_2] + 4*Electron_mvaFall17V2Iso_WP80[idx_2] + 8*Electron_cutBased_HEEP[idx_2]")
             .Define("Idbit_l2_cut",   "1*(Electron_cutBased[idx_2] > 1) + 2*(Electron_cutBased[idx_2] > 2) + 4*(Electron_cutBased[idx_2] > 3) + 8*Electron_cutBased_HEEP[idx_2]") 
    
    // Set Electron 1 branches to 0
             .Define("Idbit_l1_mva",  [=]() { return (int) 0; })
             .Define("Idbit_l1_cut",  [=]() { return (int) 0; });

  }else{ // (lep1 == "el" && lep2 == "mu")
    // Require m_ll above 12 GeV and 0 charge
   return df2.Filter("(charge_l1 + charge_l2) == 0 && m_ll > 12", "SelectLeadingPair: Require different flavor lepton pair to have opposite sign and m_ll > 12 GeV.")
   
     // Saving relevant Electron 1 branches and trigger bit 
             .Define("Idbit_l1_mva",   "1*Electron_mvaFall17V2Iso_WPL[idx_1] + 2*Electron_mvaFall17V2Iso_WP90[idx_1] + 4*Electron_mvaFall17V2Iso_WP80[idx_1] + 8*Electron_cutBased_HEEP[idx_1]")
             .Define("Idbit_l1_cut",   "1*(Electron_cutBased[idx_1] > 1) + 2*(Electron_cutBased[idx_1] > 2) + 4*(Electron_cutBased[idx_1] > 3) + 8*Electron_cutBased_HEEP[idx_1]") 
             .Define("trigger_IDbit", "1*HLT_Ele32_WPTight_Gsf + 2*HLT_Ele35_WPTight_Gsf_L1EGMT +4*HLT_Ele115_CaloIdVT_GsfTrkIdT + 8*HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165")

    // Set Electron 2 branches to 0
             .Define("Idbit_l2_mva",  [=]() { return (int) 0; })
             .Define("Idbit_l2_cut",  [=]() { return (int) 0; })
     
    // Saving relevant Muon 2 branches 
             .Define("Muon_pfRelIso04_all_l2",   "Muon_pfRelIso04_all[idx_2]")
             .Define("Muon_pfIsoId_l2",   "(int) Muon_pfIsoId[idx_2]")
             .Define("Muon_miniPFRelIso_all_l2", "Muon_miniPFRelIso_all[idx_2]")
             .Define("Muon_miniIsoId_l2",   Helper::MiniIsoID_corrected, {"Muon_miniPFRelIso_all_l2"})
             .Define("Idbit_l2", "1*Muon_mediumId[idx_2] + 2*Muon_mediumPromptId[idx_2] + 4*Muon_tightId[idx_2] + 8*(Muon_highPtId[idx_2] > 0)")
    
    // Set Muon 1 branches to 0
             .Define("Muon_pfRelIso04_all_l1",  [=]() { return (float) 0.0; })
             .Define("Muon_pfIsoId_l1",  [=]() { return (int) 0; })
             .Define("Muon_miniPFRelIso_all_l1",  [=]() { return (float) 0.0; })
             .Define("Muon_miniIsoId_l1",  [=]() { return (int) 0; })
             .Define("Idbit_l1",  [=]() { return (int) 0; })
    
    // Set Muon min/max  branches to 0
             .Define("Muon_miniIsoId_min",  [=]() { return (int) 0; })
             .Define("Muon_pfIsoId_min",  [=]() { return (int) 0; })
             .Define("Muon_miniPFRelIso_all_max",  [=]() { return (float) 0; })
             .Define("Muon_pfRelIso04_all_max",  [=]() { return (float) 0; });
  }
}

/**********************************************************/

template <typename T>
auto FatJet_vars(T &df) {
            // Add objects made using AK8_cand, leptons and MET here

             // Example Printout 
  return  df.Define("p4_MET", [/* &zeroval no necessary since static */](float met, float metphi){ 
                 return Helper::add_p4(met, 0.f, metphi, zeroval);},{"pt_MET", "phi_MET"})
             /*.Display<ROOT::Math::PtEtaPhiMVector>({"p4_MET"})->Print() // Print out column
             .Display("p4_MET") // Print out column
             .Display({"p4_MET"})->Print() // Print out column*/
             
             // H mass corrections
             .Define("msoftscaled_fatjet",   Helper::M_softdrop_ptScaled, {"msoftdrop_fatjet", "pt_fatjet"});
}

/**********************************************************/
template <typename T>
auto JetInfo(T &df) {
  
              // Check that AK4 jets don't overlap with h cand and lepton pair 
      return df.Define("dR_fatjet_jet", Helper::VDeltaR, {"eta_fatjet","Jet_eta", "phi_fatjet", "Jet_phi"})
               .Define("dR_l1_jet", Helper::VDeltaR, {"eta_l1","Jet_eta", "phi_l1", "Jet_phi"})
               .Define("dR_l2_jet", Helper::VDeltaR, {"eta_l2","Jet_eta", "phi_l2", "Jet_phi"})
               // For every AK4 jet, checks dR with all good leptons
               .Define("dR_jet_lep_overlaps", Helper::OverlapdR, {"Jet_eta", "Jet_phi", "goodMuons", "Muon_eta", "Muon_phi", "goodElectrons", "Electron_eta", "Electron_phi"})

               // Define Additional jets quantities
               .Define("Jet_add", " cJet_pt > 30 && Jet_jetId >= 6 && (cJet_pt > 50 || Jet_puId >= 4) && dR_fatjet_jet > 0.8 && dR_jet_lep_overlaps == 0")
               .Define("nJet_add", "Sum(Jet_add)")
               .Define("JetCent_add", "Jet_add && abs(Jet_eta) < 2.4")
               .Define("nJetCent_add", "Sum(JetCent_add)")

               // Filter by number of additional b-jets 
               .Define("BJetM_add", "Jet_add && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783")
               // Medium B-tag UL 2018 WP: https://btv-wiki.docs.cern.ch/ScaleFactors/UL2018/
               .Define("nBJetM_add", "Sum(BJetM_add)")
               .Filter("nBJetM_add > 0", "JetInfo: Require at least 1 additional B-tagged (M) AK4 jet")

                // Finding leading 2 non-b-jets and leading 2 b-jets if they exist
               .Define("pairIdx_jet", Helper::Leading2jets, {"njet", "Jet_add", "BJetM_add", "cJet_pt"})
               .Define("idx_j1", "pairIdx_jet[0]")
               .Define("idx_j2", "pairIdx_jet[1]")
               .Define("pt_jet1", Helper::assign_safely, {"idx_j1", "cJet_pt"})
               .Define("pt_jet2", Helper::assign_safely, {"idx_j2", "cJet_pt"})
               .Define("eta_jet1", Helper::assign_safely, {"idx_j1", "Jet_eta"})
               .Define("eta_jet2", Helper::assign_safely, {"idx_j2", "Jet_eta"})
               .Define("phi_jet1", Helper::assign_safely, {"idx_j1", "Jet_phi"})
               .Define("phi_jet2", Helper::assign_safely, {"idx_j2", "Jet_phi"})
               .Define("pairIdx_Bjet", Helper::Leading2bjets, {"njet", "BJetM_add", "cJet_pt"})
               .Define("idx_b1", "pairIdx_Bjet[0]")
               .Define("idx_b2", "pairIdx_Bjet[1]")
               .Define("pt_bjet1", Helper::assign_safely, {"idx_b1", "cJet_pt"})
               .Define("pt_bjet2", Helper::assign_safely, {"idx_b2", "cJet_pt"})
               .Define("eta_bjet1", Helper::assign_safely, {"idx_b1", "Jet_eta"})
               .Define("eta_bjet2", Helper::assign_safely, {"idx_b2", "Jet_eta"})
               .Define("phi_bjet1", Helper::assign_safely, {"idx_b1", "Jet_phi"})
               .Define("phi_bjet2", Helper::assign_safely, {"idx_b2", "Jet_phi"})
               .Define("dphi_l1_j1",Helper::DeltaPhi_abs, {"phi_l1", "phi_jet1"})
               .Define("dphi_l1_b1",Helper::DeltaPhi_abs, {"phi_l1", "phi_bjet1"})  

               // Compute scalar and vector sum of all additional AK4 jets pT
               .Define("pt_SSum", "Sum(cJet_pt*Jet_add)")
               .Define("pt_VSum", Helper::Vector_sum, {"njet", "Jet_add", "cJet_pt", "Jet_eta", "Jet_phi", "cJet_mass"});      
}

/*********************************************************************************************/

template <typename T>
auto SignalMC_GenMatch(T &df, std::string sample) {
  // Select events with Leading Fat Jet that is Gen Matched to AK8 Jet contains4 or more b hadrons
  if (Helper::containsSubstring(sample,"SUSY")){ 
    //return df.Define("GENnBhadron_fatjet",   "FatJet_nBHadrons[idx_fatjet]");
    auto df2 = df.Define("GENnBhadron_fatjet",   "FatJet_nBHadrons[idx_fatjet]");
    return df2.Filter("GENnBhadron_fatjet > 3", "SignalMC_GenMatch: Selected AK8 Jet has 4+ b hadrons");
  
  // Save nGenB for background MC samples
  }else if (Helper::HasExactBranch(df, "FatJet_nBHadrons")){ 
    return df.Define("GENnBhadron_fatjet",   "FatJet_nBHadrons[idx_fatjet]");

  // if data or doesn't have that branch, save dummy
  }else{ 
    return df.Define("GENnBhadron_fatjet",  [=]() { return (unsigned char) 0; });
  }
}


/**********************************************************/

template <typename T>
auto QCD_stitch(T &df, std::string sample) {
  
  if (Helper::containsSubstring(sample, "bGenFilter")){ 
    auto df2 = df.Define("bad_QCD", "abs(GenPart_pdgId) == 5 && GenPart_status == 23");
    return df2.Define("nbad_QCD", "Sum(bad_QCD)")
              .Filter("nbad_QCD > 1", "QCD_stitch:D Dropping events in QCD bGenFilter samples with 1 or more GEN particles with abs(pdgId)=5 and pythia status=23");
  
  }else if (Helper::containsSubstring(sample, "PSWeights")){
    auto df2 = df.Define("Bhadron_ID", Helper::isBHadron, {"GenPart_pdgId"});
    return df2.Define("bad_QCD", "LHE_HT > 100 && ((abs(GenPart_pdgId) == 5 && GenPart_status == 23) || (Bhadron_ID && GenPart_status == 2))")
              .Define("nbad_QCD", "Sum(bad_QCD)")
              .Filter("nbad_QCD > 1", "QCD_stitch: Dropping events in QCD Inclusive samples with 1 or more GEN particles with LHE_HT > 100 and either a B quark (abs(pdgId)=5) with pythia status=23 or a hadron containing a B quark with pythia status=2");
  
  }else{
    return df;
  }
}

/**********************************************************/
template <typename T>
auto Gen_info(T &df, std::string sample) {
  
  // Save dummy branch for all gen quantities in data
  if (Helper::containsSubstring(sample, "data")){  
    return df.Define("gen_weight",  [=]() { return (float) 1.0; })
             .Define("gen_mass_Z",  [=]() { return (float) 0.0; })
             .Define("npu",  [=]() { return (int) 1; })
             .Define("HT_LHE",  [=]() { return (float) -99.0; })
             .Define("Run_number","run")
             .Define("Pileup_nTrueInt",[=]() { return (int) 0.0; })
             .Define("PSWeight",  [=]() { return (float) 0.0; })
             .Define("LHEPdfWeight",  [=]() { return (float) 0.0; })
             .Define("gen_pt_Z",  [=]() { return (float) 0.0; })
             
             // define dummy for Btag SF branch
             .Define("Jet_btagSF_deepjet_M", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_up", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_down", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_up_correlated", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_down_correlated", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_up_uncorrelated", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_down_uncorrelated", [=]() { return (float) 1.0; });

    // DY samples
  }else if (Helper::containsSubstring(sample, "DY")){ 
    auto df2 = df.Define("genZ_idx", Helper::GenDr_match_Z, {/*"eta_mumu", "phi_mumu","GenPart_eta", "GenPart_phi",*/ "GenPart_pdgId"});
    return df2.Define("gen_weight","genWeight")
              .Define("gen_pt_Z", "GenPart_pt[genZ_idx]")
              .Define("npu","Pileup_nPU")
              .Define("HT_LHE","LHE_HT")
              .Define("Run_number",  [=]() { return (unsigned int) 0; })
              .Define("gen_mass_Z", []() { return Z_gen_mass; });
    
    // Samples that don't include LHE_HT (diboson?)
  }else if (!Helper::HasExactBranch(df, "LHE_HT")){ 
    std::cout << ">>> ATTENTION: This sample has no LHE_HT branch. Using fallback value -99.0!" << std::endl;
    return df.Define("gen_weight","genWeight")
             .Define("gen_mass_Z",  [=]() { return (float) 0.0; })
             .Define("npu","Pileup_nPU")
             .Define("LHEPdfWeight",  [=]() { return (float) 1.0; })
             .Define("HT_LHE",  [=]() { return (float) -99.0; })
             .Define("Run_number",  [=]() { return (unsigned int) 0; })
             .Define("gen_pt_Z",  [=]() { return (float) 0.0; });
 
    // Other samples
  }else{ 
    return df.Define("gen_weight","genWeight")
             .Define("gen_mass_Z",  [=]() { return (float) 0.0; })
             .Define("npu","Pileup_nPU")
             .Define("HT_LHE","LHE_HT")
             .Define("Run_number",  [=]() { return (unsigned int) 0; })
             .Define("gen_pt_Z",  [=]() { return (float) 0.0; });
  } 
}

/*********************************************************************************************/



/********************************* Example of testing correction ******************/
/*
template <typename T>
auto ZH_pT_mfit(T &df, std::string sample) {
  if (sample == "MC_Signal" || sample == "MC_DY" ){ // Select events with Leading Fat Jet that is Gen Matched to AK8 Jet contains 4 or more b hadrons
    auto df2 = df.Define("m_cor1_fatjet",   Helper::ZH_corr1, {"m_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"});
    return df2.Define("msoftdrop_cor1_fatjet",   Helper::ZH_corr1, {"msoftdrop_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"})
              .Define("mtopo_fatjet",   Helper::Hmass_btbCorrection, {"m_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"})
              .Define("msofttopo_fatjet",   Helper::Hmass_btbCorrection, {"msoftdrop_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"});
  
  }else{ 
    return df.Define("m_cor1_fatjet",  [=]() { return (float) 0; })
             .Define("m_cor2_fatjet",  [=]() { return (float) 0; })
             .Define("msoftdrop_cor1_fatjet", [=]() { return (float) 0; })
             .Define("msoftdrop_cor2_fatjet", [=]() { return (float) 0; });
  }
}
*/
/********************************************************************************************************/
/**********************************************************/

/*
template <typename T>
auto Iso_ID_vars(T &df, std::string channel) {
  if (channel == "mu"){ // Muon channel
    // Lepton (mu/el) branches
    return df.Define("mvaTTH_lep", "Muon_mvaTTH[idx]")
             .Define("dxy_lep", "Muon_dxy[idx]")
             .Define("dz_lep", "Muon_dz[idx]")
             .Define("ip3d_lep", "Muon_ip3d[idx]")
             .Define("miniPFRelIso_all_lep", "Muon_miniPFRelIso_all[idx]")
             .Define("jetPtRelv2_lep", "Muon_jetPtRelv2[idx]")
             .Define("jetRelIso_lep", "Muon_jetRelIso[idx]")
    // Muon only branches
             .Define("Mu_pfIsoId",   "(int) Muon_pfIsoId[idx]")
             .Define("Mu_miniIsoId",   Helper::MiniIsoID_corrected, {"miniPFRelIso_all_lep"})
             .Define("Mu_pfRelIso04_all",   "Muon_pfRelIso04_all[idx]")
             .Define("Mu_Idbit", "1*Muon_mediumId[idx] + 2*Muon_mediumPromptId[idx] + 4*Muon_tightId[idx] + 8*(Muon_highPtId[idx] > 0)")
    // Set electron branches to 0
             .Define("El_Idbit_mva",  [=]() { return (int) 0; })
             .Define("El_Idbit_cut",  [=]() { return (int) 0; })
             .Define("El_pfRelIso03_all", [=]() { return (float) 0.0; }) 
             .Define("El_convVeto", [=]() { return (int) 0; })
             .Define("El_lostHits", [=]() { return (int) 0; })
             .Define("El_eInvMinusPInv", [=]() { return (float) 0.0; });
  }else if (channel == "el"){ // Electron channel
    // Lepton (mu/el) branches
    return df.Define("mvaTTH_lep", "Electron_mvaTTH[idx]")
             .Define("dxy_lep", "Electron_dxy[idx]")
             .Define("dz_lep", "Electron_dz[idx]")
             .Define("ip3d_lep", "Electron_ip3d[idx]")
             .Define("miniPFRelIso_all_lep", "Electron_miniPFRelIso_all[idx]")
             .Define("jetPtRelv2_lep", "Electron_jetPtRelv2[idx]")
             .Define("jetRelIso_lep", "Electron_jetRelIso[idx]")
    // Electron only branches
             .Define("El_Idbit_mva",   "1*Electron_mvaFall17V2Iso_WPL[idx] + 2*Electron_mvaFall17V2Iso_WP90[idx] + 4*Electron_mvaFall17V2Iso_WP80[idx] + 8*Electron_cutBased_HEEP[idx]")
             .Define("El_Idbit_cut",   "1*(Electron_cutBased[idx] > 1) + 2*(Electron_cutBased[idx] > 2) + 4*(Electron_cutBased[idx] > 3) + 8*Electron_cutBased_HEEP[idx]")
             .Define("El_pfRelIso03_all", "Electron_pfRelIso03_all[idx]")
             .Define("El_convVeto", "(int) Electron_convVeto[idx]")
             .Define("El_lostHits", "(int) Electron_lostHits[idx]")
             .Define("El_eInvMinusPInv", "Electron_eInvMinusPInv[idx]")
    // Set Muon branches to 0
             .Define("Mu_pfIsoId",  [=]() { return (int) 0; })
             .Define("Mu_miniIsoId",  [=]() { return (int) 0; })
             .Define("Mu_pfRelIso04_all",  [=]() { return (float) 0.0; })
             .Define("Mu_Idbit",  [=]() { return (int) 0; });
  }else{
    return df;
  }
}
*/
/**********************************************************/
/*
template <typename T>
auto SelectFatJet_old(T &df) {
    return df.Define("nfatjet", "(int) nFatJet") // recasting as int to avoid error processing skims vas unskimmed
             .Define("njet", "(int) nJet") // recasting as int to avoid error processing skims vas unskimmed
             // Standard CMS X->bb PNET tagger
             .Define("FatJet_particleNetMD_Xbb_vs_QCD", "FatJet_particleNetMD_Xbb/(FatJet_particleNetMD_Xbb + FatJet_particleNetMD_QCD)")
             // Baseline FatJet H candidate selection
             .Define("goodFatJet", "cleanFatJet_pt > 170 &&  FatJet_particleNetMD_Xbb_vs_QCD > 0.75 && FatJet_jetId >= 6 && cleanFatJet_Softdropmass > 20 && abs(cleanFatJet_eta) < 2.4")
             // Define v1 tagger score (previously was incorrectly using "FatJet_particleNetMD_Hto4b_binary_Haa4b")                            
             .Define("FatJet_Hto4b_tagger_v1",   "FatJet_particleNetMD_Hto4b_Haa4b/(FatJet_particleNetMD_Hto4b_Haa4b+ FatJet_particleNetMD_Hto4b_QCD0b + FatJet_particleNetMD_Hto4b_QCD1b + FatJet_particleNetMD_Hto4b_QCD2b + FatJet_particleNetMD_Hto4b_QCD3b + FatJet_particleNetMD_Hto4b_QCD4b)")
             //.Define("idx_fatjet", Helper::Leading_FatJet_2Ltt, {"nfatjet", "FatJet_jetId", "cleanFatJet_eta", "cleanFatJet_phi", 
             //                                               "FatJet_particleNetMD_Hto4b_binary_Haa4b", "cleanFatJet_Softdropmass", 
             //                                               "eta_l1", "phi_l1", "eta_l2", "phi_l2"}) 
             .Filter("idx_fatjet != -1", "SelectFatJet: Has leading Fat Jet passing selection with highest PNET_Haa4b_vs_QCD_score")
             // Save Leading Fat Jet information
             //.Define("pt_fatjet", "FatJet_pt[idx_fatjet]")
             //.Define("eta_fatjet", "FatJet_eta[idx_fatjet]")
             //.Define("phi_fatjet", "FatJet_phi[idx_fatjet]")
             //.Define("m_fatjet",   "FatJet_mass[idx_fatjet]")
             //.Define("msoftdrop_fatjet", "FatJet_msoftdrop[idx_fatjet]")
             .Define("pt_fatjet", "cleanFatJet_pt[idx_fatjet]")
             .Define("eta_fatjet", "cleanFatJet_eta[idx_fatjet]")
             .Define("phi_fatjet", "cleanFatJet_phi[idx_fatjet]")
             .Define("m_fatjet",   "cleanFatJet_mass[idx_fatjet]")
             .Define("msoftdrop_fatjet", "cleanFatJet_Softdropmass[idx_fatjet]")
             .Define("mH_avg",   "(FatJet_particleNet_massH_Hto4b_v0[idx_fatjet]*1.01 + FatJet_particleNet_massH_Hto4b_v1[idx_fatjet] + FatJet_particleNet_massH_Hto4b_v2[idx_fatjet] + FatJet_particleNet_massH_Hto4b_v3[idx_fatjet])/4")
             .Define("mA_avg",   "(FatJet_particleNet_massA_Hto4b_v0[idx_fatjet] + FatJet_particleNet_massA_Hto4b_v1[idx_fatjet] + FatJet_particleNet_massA_Hto4b_v3[idx_fatjet])/3")
             .Define("score_Haa4b_vs_QCD",   "FatJet_Hto4b_tagger_v1[idx_fatjet]")
             //.Filter("score_Haa4b_vs_QCD > 0.5", "SelectFatJet: Applying PNET_score > 0.5 cut")
             // Save MET and PUPPI MET information
             .Define("pt_MET", "MET_pt")
             .Define("phi_MET", "MET_phi")

             // Compute Dphi/Deta for several objects
             //.Define("deta_fatjet_lep",Helper::DeltaEta_abs, {"eta_fatjet", "eta_lep"})
             //.Define("dphi_fatjet_lep",Helper::DeltaPhi_abs, {"phi_fatjet", "phi_lep"})
             //.Define("dR_fatjet_lep",Helper::DeltaR, {"eta_fatjet", "eta_lep", "phi_fatjet", "phi_lep"})
             //.Define("dphi_MET_lep",Helper::DeltaPhi_abs, {"phi_MET", "phi_lep"})
             //.Define("phi_METlep", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Phi(); }, {"p4_MET", "p4_lep"})
             //.Define("pt_METlep", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_MET", "p4_lep"})
             //.Define("dphi_fatjet_METlep",Helper::DeltaPhi_abs, {"phi_fatjet", "phi_METlep"})
             //.Define("mt_MET_lep", Helper::Mt_MET_X, {"pt_MET", "phi_MET", "pt_lep", "phi_lep"})
             // H mass corrections
             .Define("msoftscaled_fatjet",   Helper::M_softdrop_ptScaled, {"msoftdrop_fatjet", "pt_fatjet"});
             //.Filter("pt_MET > 40", "SelectFatJet: Applying MET cut: pT(MET) > 40 GeV");
}
*/
/**********************************************************/
#endif
