#ifndef LEADING_TAU_TAU_H_INCL
#define LEADING_TAU_TAU_H_INCL

#include "helpers.h"
#include <string>
#include "lumiMask.h"
#include <algorithm>
#include <cmath>

static constexpr auto zeroval = 0.f; // define const in header for example
static constexpr auto Z_gen_mass = 91.25f; // Z gen mass

/**********************************************************/

template <typename T>
auto ApplyNoiseFilter(T &df) {
  // Filtering events that fail Noise Filters
  return df.Filter("Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonFilter && Flag_BadPFMuonDzFilter && Flag_eeBadScFilter && Flag_ecalBadCalibFilter", "ApplyNoiseFilter: Has events passing Noise Filters");
}

/**********************************************************/

template <typename T>
auto ApplyTrigger(T &df, std::string channel) {
   // Filtering events that fail lepton triggers
   
  if (channel == "mu"){ // Muon Channel Triggers
    return df.Filter("(L1_SingleMu22 || L1_SingleMu25) && (HLT_IsoMu24 || HLT_Mu50)", "ApplyTrigger: Has events passing Muon triggers");
  
  }else if (channel == "el"){ // Electron Channel Triggers
    return df.Filter("HLT_Ele32_WPTight_Gsf || HLT_Ele35_WPTight_Gsf_L1EGMT || HLT_Ele115_CaloIdVT_GsfTrkIdT || HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165", "ApplyTrigger: Has events passing Electron triggers");
  
  }else{
    return df;
  }
}

/**********************************************************/

template <typename T>
auto ApplyLooseSelection(T &df, std::string channel) {
  // baseline (good) leptons
  auto df2 = df.Define("goodMuons", "Muon_pt > 10 && abs(Muon_eta) < 2.4  && Muon_miniPFRelIso_all < 0.10 && (Muon_mediumPromptId || ( Muon_pt > 200 && Muon_highPtId > 0)) && (abs(Muon_dz) < 0.1) && (abs(Muon_dxy) < 0.02)")
        // Muon_miniPFRelIso_all < 0.20 (medium WP/ Muon_miniIsoId = 2) or  Muon_miniPFRelIso_all < 0.10 (tight WP/ Muon_miniIsoId = 3)
               .Define("ngoodMuons", "Sum(goodMuons)")
               .Define("goodElectrons", "Electron_pt > 10 && abs(Electron_eta) < 2.5 && (abs(Electron_eta) < 1.44 || abs(Electron_eta) > 1.57) && Electron_mvaFall17V2Iso_WPL && (Electron_mvaFall17V2Iso_WP90 || (Electron_pt > 35 && Electron_cutBased_HEEP))")
               .Define("ngoodElectrons", "Sum(goodElectrons)")

               // trigger leptons
               .Define("triggerMuons", "goodMuons && Muon_pt > 26")
               .Define("triggerElectrons", "goodElectrons && Electron_pt > 35 && abs(Electron_dxy) < 0.02 && abs(Electron_dz) < 0.10 && Electron_mvaFall17V2Iso_WP90 && (Electron_mvaFall17V2Iso_WP80 || (Electron_pt > 35 && Electron_cutBased_HEEP))")
               .Define("ntriggerMuons", "Sum(triggerMuons)")
               .Define("ntriggerElectrons", "Sum(triggerElectrons)");
 
  if (channel == "mu"){ // Muon Channel Loose Selection
      return df2.Filter("ntriggerMuons == 1 && ntriggerElectrons == 0", "ApplyLooseSelection: event has exactly 1 trigger muon");

  }else{ // (channel == "el"){ // Electron Channel Loose Selection
      return df2.Filter("ntriggerElectrons == 1 && ntriggerMuons == 0", "ApplyLooseSelection: event has exactly 1 trigger electron");
  }
}

/**********************************************************/

// Since there is only one lepton in this category, we can select lepton before H cand
template <typename T>
auto SelectLepton(T &df, std::string channel) {

  if (channel == "mu"){ // Muon Selection
    return df.Define("idx", Helper::Find_index, {"triggerMuons"})
             .Define("m_lep",   "Muon_mass[idx]")
             .Define("pt_lep",  "Muon_pt[idx]")
             .Define("eta_lep", "Muon_eta[idx]")
             .Define("phi_lep", "Muon_phi[idx]")
             .Define("p4_lep", Helper::add_p4, {"pt_lep", "eta_lep", "phi_lep", "m_lep"});

  }else if (channel == "el"){ // Electron Selection
    return df.Define("idx", Helper::Find_index, {"triggerElectrons"})
             .Define("m_lep",   "Electron_mass[idx]")
             .Define("pt_lep",  "Electron_pt[idx]")
             .Define("eta_lep", "Electron_eta[idx]")
             .Define("phi_lep", "Electron_phi[idx]")
             .Define("p4_lep", Helper::add_p4, {"pt_lep", "eta_lep", "phi_lep", "m_lep"});
  
  }else{
    return df;
  }
}


/**********************************************************/

template <typename T>
auto SelectFatJet(T &df) {
    // recasting as int to avoid error processing skims vas unskimmed
    return df.Define("nfatjet", "(int) nFatJet") 
             .Define("njet", "(int) nJet") 
             
             // FatJet H candidate selection
             .Define("FatJet_particleNetMD_Xbb_vs_QCD", "FatJet_particleNetMD_Xbb/(FatJet_particleNetMD_Xbb + FatJet_particleNetMD_QCD)")
             .Define("goodFatJet", "cFatJet_pt > 170 &&  FatJet_particleNetMD_Xbb_vs_QCD > 0.75 && FatJet_jetId >= 6 && cFatJet_msoftdrop > 20 && FatJet_eta < 2.4")
             // v2 tagger AK8(3b) + AK4(b) tagger used to rank AK8 H cand
             .Define("FatJet_PNet_v2max", "FatJet_PNet_X4b_v2a_Haa34b_score + FatJet_PNet_X4b_v2b_Haa34b_score")
             .Define("idx_fatjet", Helper::Leading_FatJet_1L, {"nfatjet", "goodFatJet", "FatJet_eta", "FatJet_phi", 
                                                               "FatJet_PNet_v2max", "eta_lep", "phi_lep"}) 
             .Filter("idx_fatjet != -1", "SelectFatJet: event has at least 1 AK8 H candidate")

             // Save H candidate information
             // Save Leading Fat Jet information
             .Define("pt_fatjet", "cFatJet_pt[idx_fatjet]")
             .Define("eta_fatjet", "FatJet_eta[idx_fatjet]")
             .Define("phi_fatjet", "FatJet_phi[idx_fatjet]")
             .Define("m_fatjet",   "cFatJet_mass[idx_fatjet]")
             .Define("msoftdrop_fatjet", "cFatJet_msoftdrop[idx_fatjet]")

             // ParticleNet branches
             // v2 tagger AK8(4b)
             .Define("score_Haa4b_vs_QCD",   "0.5*(FatJet_PNet_X4b_v2a_Haa4b_score[idx_fatjet] + FatJet_PNet_X4b_v2b_Haa4b_score[idx_fatjet])")
             // v1 tagger AK8(4b)
             .Define("score_Haa4b_vs_QCD_v1",   "FatJet_PNet_X4b_v1_Haa4b_vs_QCD[idx_fatjet]")
             // Other v2 taggers
             .Define("score_Haa4b_vs_QCD_34",   "0.5*FatJet_PNet_v2max[idx_fatjet]")
             .Define("score_Haa4b_vs_QCD_v2a",   "FatJet_PNet_X4b_v2a_Haa4b_score[idx_fatjet]")
             .Define("score_Haa4b_vs_QCD_v2b",   "FatJet_PNet_X4b_v2b_Haa4b_score[idx_fatjet]")
             
             //.Filter("score_Haa4b_vs_QCD > 0.8", "SelectFatJet: Applying ParticleNet tagger cut")
             // PNET regressed H mass
             .Define("mH_avg",   "FatJet_PNet_massH_v2b[idx_fatjet]")
             // PNET regressed a mass
             .Define("mA_avg",   "FatJet_PNet_massAa[idx_fatjet]")
             .Define("mA_34a",   "FatJet_PNet_34massAa[idx_fatjet]")
             .Define("mA_34b",   "FatJet_PNet_34massAb[idx_fatjet]")
             .Define("mA_34d",   "FatJet_PNet_34massAd[idx_fatjet]")
             //.Define("mH_avg",   "FatJet_PNet_massH_avg[idx_fatjet]")
             //.Define("mA_avg",   "FatJet_PNet_massA_avg[idx_fatjet]")
             
             // Save MET branches
             .Define("pt_MET", "MET_pt")
             .Define("phi_MET", "MET_phi")
             .Define("pt_PUPPIMET", "PuppiMET_pt")
             .Define("phi_PUPPIMET", "PuppiMET_phi")
             
             // Compute H cand vars below since we already selected lepton
             // MET 4-vector
             .Define("p4_MET", [/* &zeroval no necessary since static */](float met, float metphi){ 
                 return Helper::add_p4(met, 0.f, metphi, zeroval);},{"pt_MET", "phi_MET"})

             // Compute Dphi/Deta for several objects
             .Define("deta_fatjet_lep",Helper::DeltaEta_abs, {"eta_fatjet", "eta_lep"})
             .Define("dphi_fatjet_lep",Helper::DeltaPhi_abs, {"phi_fatjet", "phi_lep"})
             .Define("dR_fatjet_lep",Helper::DeltaR, {"eta_fatjet", "eta_lep", "phi_fatjet", "phi_lep"})
             .Define("dphi_MET_lep",Helper::DeltaPhi_abs, {"phi_MET", "phi_lep"})
             
             // MET+lep 4-vector 
             .Define("phi_METlep", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) {
                 return (float) (p4_1+p4_2).Phi(); }, {"p4_MET", "p4_lep"})
             .Define("pt_METlep", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) {
                 return (float) (p4_1+p4_2).Pt(); }, {"p4_MET", "p4_lep"})
             .Define("dphi_fatjet_METlep",Helper::DeltaPhi_abs, {"phi_fatjet", "phi_METlep"})
             .Define("mt_MET_lep", Helper::Mt_MET_X, {"pt_MET", "phi_MET", "pt_lep", "phi_lep"})
             
             // H mass corrections
             .Define("msoftscaled_fatjet",   Helper::M_softdrop_ptScaled, {"msoftdrop_fatjet", "pt_fatjet"});
}

/**********************************************************/

template <typename T>
auto Lepton_vars(T &df, std::string channel) {
  
  auto df2 = df.Define("dR_fatjet_mu", Helper::VDeltaR, {"eta_fatjet","Muon_eta", "phi_fatjet", "Muon_phi"})
               .Define("dR_fatjet_el", Helper::VDeltaR, {"eta_fatjet","Electron_eta", "phi_fatjet", "Electron_phi"})
               .Define("goodMuons_NoOverlapH", "goodMuons && dR_fatjet_mu > 0.8")
               .Define("goodElectrons_NoOverlapH", "goodElectrons && dR_fatjet_el > 0.8")
               .Define("ngoodMuons_NoOverlapH", "Sum(goodMuons_NoOverlapH)")
               .Define("ngoodElectrons_NoOverlapH", "Sum(goodElectrons_NoOverlapH)");
 
  if (channel == "mu"){ // Muon channel
    
    // Discard events where there is more than one (non-Overlapping with H cand) good lepton
     return df2.Filter("ngoodMuons_NoOverlapH == 1 && ngoodElectrons_NoOverlapH == 0", " Lepton_vars: event has exactly 1 non overlapping muons with H candidate and 0 non-overlapping electrons with H candidate")

                // Save Lepton branches
               .Define("mvaTTH_lep", "Muon_mvaTTH[idx]")
               .Define("dxy_lep", "Muon_dxy[idx]")
               .Define("dz_lep", "Muon_dz[idx]")
               .Define("ip3d_lep", "Muon_ip3d[idx]")
               .Define("miniPFRelIso_all_lep", "Muon_miniPFRelIso_all[idx]")
               .Define("jetPtRelv2_lep", "Muon_jetPtRelv2[idx]")
               .Define("jetRelIso_lep", "Muon_jetRelIso[idx]")
  
               // Save Muon-specific branches and trigger bit
               .Define("Mu_pfIsoId",   "(int) Muon_pfIsoId[idx]")
               .Define("Mu_miniIsoId",   Helper::MiniIsoID_corrected, {"miniPFRelIso_all_lep"})
               .Define("Mu_pfRelIso04_all",   "Muon_pfRelIso04_all[idx]")
               .Define("Mu_Idbit", "1*Muon_mediumId[idx] + 2*Muon_mediumPromptId[idx] + 4*Muon_tightId[idx] + 8*(Muon_highPtId[idx] > 0)")
               .Define("trigger_IDbit", "1*HLT_IsoMu24 + 2*HLT_Mu50")
     
               // Set electron branches to 0
               .Define("El_Idbit_mva",  [=]() { return (int) 0; })
               .Define("El_Idbit_cut",  [=]() { return (int) 0; })
               .Define("El_pfRelIso03_all", [=]() { return (float) 0.0; }) 
               .Define("El_convVeto", [=]() { return (int) 0; })
               .Define("El_lostHits", [=]() { return (int) 0; })
               .Define("El_eInvMinusPInv", [=]() { return (float) 0.0; });

  }else{ // (channel == "el"){ // Electron channel
    // Discard events where there is more than one (non-Overlapping with H cand) good lepton
     return df2.Filter("ngoodMuons_NoOverlapH == 0 && ngoodElectrons_NoOverlapH == 1", "Lepton_vars: event has exactly 0 non-overlapping muons with H candidate and 1 non-overlapping electrons with H candidate")

                // Save Lepton branches
               .Define("mvaTTH_lep", "Electron_mvaTTH[idx]")
               .Define("dxy_lep", "Electron_dxy[idx]")
               .Define("dz_lep", "Electron_dz[idx]")
               .Define("ip3d_lep", "Electron_ip3d[idx]")
               .Define("miniPFRelIso_all_lep", "Electron_miniPFRelIso_all[idx]")
               .Define("jetPtRelv2_lep", "Electron_jetPtRelv2[idx]")
               .Define("jetRelIso_lep", "Electron_jetRelIso[idx]")
      
                // Save Electron-specific branches and trigger bit
               .Define("El_Idbit_mva",   "1*Electron_mvaFall17V2Iso_WPL[idx] + 2*Electron_mvaFall17V2Iso_WP90[idx] + 4*Electron_mvaFall17V2Iso_WP80[idx] + 8*Electron_cutBased_HEEP[idx]")
               .Define("El_Idbit_cut",   "1*(Electron_cutBased[idx] > 1) + 2*(Electron_cutBased[idx] > 2) + 4*(Electron_cutBased[idx] > 3) + 8*Electron_cutBased_HEEP[idx]")
               .Define("El_pfRelIso03_all", "Electron_pfRelIso03_all[idx]")
               .Define("El_convVeto", "(int) Electron_convVeto[idx]")
               .Define("El_lostHits", "(int) Electron_lostHits[idx]")
               .Define("El_eInvMinusPInv", "Electron_eInvMinusPInv[idx]")
               .Define("trigger_IDbit", "1*HLT_Ele32_WPTight_Gsf + 2*HLT_Ele35_WPTight_Gsf_L1EGMT +4*HLT_Ele115_CaloIdVT_GsfTrkIdT + 8*HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165")

               // Set Muon branches to 0
               .Define("Mu_pfIsoId",  [=]() { return (int) 0; })
               .Define("Mu_miniIsoId",  [=]() { return (int) 0; })
               .Define("Mu_pfRelIso04_all",  [=]() { return (float) 0.0; })
               .Define("Mu_Idbit",  [=]() { return (int) 0; });
  }
}

/**********************************************************/

template <typename T>
auto JetInfo(T &df) {
     // check that Ak4 jets don't overlap with h cand and selected lepton
      return df.Define("dR_fatjet_jet", Helper::VDeltaR, {"eta_fatjet","Jet_eta", "phi_fatjet", "Jet_phi"})
               .Define("dR_lep_jet", Helper::VDeltaR, {"eta_lep","Jet_eta", "phi_lep", "Jet_phi"})
                // For every AK4 jet, checks dR with all good leptons
                // Not needed here since there is one good lepton outside the H cand in the event
               //.Define("dR_jet_lep_overlaps", Helper::OverlapdR, {"Jet_eta", "Jet_phi", "goodMuons", "Muon_eta", "Muon_phi", "goodElectrons", "Electron_eta", "Electron_phi"})
               
               // Define Additional jets quantities
               .Define("Jet_add", " cJet_pt > 30 && Jet_jetId >= 6 && (cJet_pt > 50 || Jet_puId >= 4) && dR_fatjet_jet > 0.8 && dR_lep_jet > 0.4")
               .Define("nJet_add", "Sum(Jet_add)")
               .Define("JetCent_add", "Jet_add && abs(Jet_eta) < 2.4")
               .Define("nJetCent_add", "Sum(JetCent_add)")

               // Filter by number of additional b-jets 
               .Define("BJetM_add", "Jet_add && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783")
               // Medium B-tag UL 2018 WP: https://btv-wiki.docs.cern.ch/ScaleFactors/UL2018/
               .Define("nBJetM_add", "Sum(BJetM_add)")
               //.Filter("nBJetM_add == 0 && abs(dphi_fatjet_METlep) > 0.75*M_PI && pt_METlep <= 300", "WH : Require no additional B-tagged (M) AK4 jets and dphi(AK8, MET+lep) > 0.75*pi")

                // Finding leading 2 non-b-jets and leading 2 b-jets if they exist
               .Define("pairIdx_jet", Helper::Leading2jets, {"njet", "Jet_add", "BJetM_add", "cJet_pt"})
               .Define("idx_j1", "pairIdx_jet[0]")
               .Define("idx_j2", "pairIdx_jet[1]")
               .Define("pt_jet1", Helper::assign_safely, {"idx_j1", "cJet_pt"})
               .Define("pt_jet2", Helper::assign_safely, {"idx_j2", "cJet_pt"})
               .Define("eta_jet1", Helper::assign_safely, {"idx_j1", "Jet_eta"})
               .Define("eta_jet2", Helper::assign_safely, {"idx_j2", "Jet_eta"})
               .Define("phi_jet1", Helper::assign_safely, {"idx_j1", "Jet_phi"})
               .Define("phi_jet2", Helper::assign_safely, {"idx_j2", "Jet_phi"})
               .Define("pairIdx_Bjet", Helper::Leading2bjets, {"njet", "BJetM_add", "cJet_pt"})
               .Define("idx_b1", "pairIdx_Bjet[0]")
               .Define("idx_b2", "pairIdx_Bjet[1]")
               .Define("pt_bjet1", Helper::assign_safely, {"idx_b1", "cJet_pt"})
               .Define("pt_bjet2", Helper::assign_safely, {"idx_b2", "cJet_pt"})
               .Define("eta_bjet1", Helper::assign_safely, {"idx_b1", "Jet_eta"})
               .Define("eta_bjet2", Helper::assign_safely, {"idx_b2", "Jet_eta"})
               .Define("phi_bjet1", Helper::assign_safely, {"idx_b1", "Jet_phi"})
               .Define("phi_bjet2", Helper::assign_safely, {"idx_b2", "Jet_phi"})

               // Compute scalar and vector sum of all additional AK4 jets pT
               .Define("pt_SSum", "Sum(cJet_pt*Jet_add)")
               .Define("pt_VSum", Helper::Vector_sum, {"njet", "Jet_add", "cJet_pt", "Jet_eta", "Jet_phi", "cJet_mass"});      

}

/*********************************************************************************************/


template <typename T>
auto SignalMC_GenMatch(T &df, std::string sample) {
  // Select events with Leading Fat Jet that is Gen Matched to AK8 Jet contains4 or more b hadrons
  if (Helper::containsSubstring(sample,"SUSY")){ 
    //return df.Define("GENnBhadron_fatjet",   "FatJet_nBHadrons[idx_fatjet]");
    auto df2 = df.Define("GENnBhadron_fatjet",   "FatJet_nBHadrons[idx_fatjet]");
    return df2.Filter("GENnBhadron_fatjet > 3", "SignalMC_GenMatch: Selected AK8 Jet has 4+ b hadrons");
  
  // Save nGenB for background MC samples
  }else if (Helper::HasExactBranch(df, "FatJet_nBHadrons")){ 
    return df.Define("GENnBhadron_fatjet",   "FatJet_nBHadrons[idx_fatjet]");

  // if data or doesn't have that branch, save dummy
  }else{ 
    return df.Define("GENnBhadron_fatjet",  [=]() { return (unsigned char) 0; });
  }
}


/**********************************************************/

template <typename T>
auto QCD_stitch(T &df, std::string sample) {
  
  if (Helper::containsSubstring(sample, "bGenFilter")){ 
    auto df2 = df.Define("bad_QCD", "abs(GenPart_pdgId) == 5 && GenPart_status == 23");
    return df2.Define("nbad_QCD", "Sum(bad_QCD)")
              .Filter("nbad_QCD > 1", "QCD_stitch:D Dropping events in QCD bGenFilter samples with 1 or more GEN particles with abs(pdgId)=5 and pythia status=23");
  
  }else if (Helper::containsSubstring(sample, "PSWeights")){
    auto df2 = df.Define("Bhadron_ID", Helper::isBHadron, {"GenPart_pdgId"});
    return df2.Define("bad_QCD", "LHE_HT > 100 && ((abs(GenPart_pdgId) == 5 && GenPart_status == 23) || (Bhadron_ID && GenPart_status == 2))")
              .Define("nbad_QCD", "Sum(bad_QCD)")
              .Filter("nbad_QCD > 1", "QCD_stitch: Dropping events in QCD Inclusive samples with 1 or more GEN particles with LHE_HT > 100 and either a B quark (abs(pdgId)=5) with pythia status=23 or a hadron containing a B quark with pythia status=2");
  
  }else{
    return df;
  }
}


/**********************************************************/

template <typename T>
auto Gen_info(T &df, std::string sample) {
   // Save dummy branch for all gen quantities in data
  if (Helper::containsSubstring(sample, "data")){  
    return df.Define("gen_weight",  [=]() { return (float) 1.0; })
             .Define("gen_mass_Z",  [=]() { return (float) 0.0; })
             .Define("npu",  [=]() { return (int) 1; })
             .Define("HT_LHE",  [=]() { return (float) -99.0; })
             .Define("Run_number","run")
             .Define("gen_pt_Z",  [=]() { return (float) 0.0; })
             .Define("PSWeight",  [=]() { return (float) 0.0; }) 
             .Define("LHEPdfWeight",  [=]() { return (float) 0.0; }) 
             .Define("Pileup_nTrueInt",[=]() { return (int) 0.0; })
             .Define("gen_pt_W",  [=]() { return (float) 0.0; })
             .Define("gen_pt_H",  [=]() { return (float) 0.0; })

             // define dummy for Btag SF branch
             .Define("Jet_btagSF_deepjet_M", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_up", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_down", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_up_correlated", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_down_correlated", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_up_uncorrelated", [=]() { return (float) 1.0; })
             .Define("Jet_btagSF_deepjet_M_down_uncorrelated", [=]() { return (float) 1.0; });

  // Signal WH samples
  }else if (Helper::containsSubstring(sample, "SUSY_WH")){ 
    auto df2 = df.Define("genW_idx", Helper::GenDr_match_W, {"GenPart_pdgId"});
    return df2.Define("genH_idx", Helper::GenDr_match_H, {"GenPart_pdgId", "GenPart_status"} )
              .Define("gen_weight","genWeight")
              .Define("gen_pt_W", "GenPart_pt[genW_idx]")
              .Define("gen_pt_H", "GenPart_pt[genH_idx]")
              .Define("gen_pt_Z",  [=]() { return (float) 0.0; })
              .Define("npu","Pileup_nPU")
              .Define("HT_LHE","LHE_HT")
              .Define("Run_number",  [=]() { return (unsigned int) 0; })
              .Define("gen_mass_Z",  [=]() { return (float) 0.0; });

    // DY samples
  }else if (Helper::containsSubstring(sample, "DY")){ 
    auto df2 = df.Define("genZ_idx", Helper::GenDr_match_Z, {/*"eta_mumu", "phi_mumu","GenPart_eta", "GenPart_phi",*/ "GenPart_pdgId"});
    return df2.Define("gen_weight","genWeight")
              .Define("gen_pt_Z", "GenPart_pt[genZ_idx]")
              .Define("npu","Pileup_nPU")
              .Define("HT_LHE","LHE_HT")
              .Define("gen_pt_W",  [=]() { return (float) 0.0; })
              .Define("gen_pt_H",  [=]() { return (float) 0.0; })
              .Define("Run_number",  [=]() { return (unsigned int) 0; })
              .Define("gen_mass_Z", []() { return Z_gen_mass; });

     // samples that don't have LHE_HT branch (diboson)
  }else if (!Helper::HasExactBranch(df, "LHE_HT")){ 
    std::cout << ">>> ATTENTION: This sample has no LHE_HT branch. Using fallback value -99.0!" << std::endl;
    return df.Define("gen_weight","genWeight")
             .Define("gen_mass_Z",  [=]() { return (float) 0.0; })
             .Define("npu","Pileup_nPU")
             .Define("HT_LHE",  [=]() { return (float) -99.0; })
             .Define("LHEPdfWeight",  [=]() { return (float) 1.0; })
             .Define("gen_pt_W",  [=]() { return (float) 0.0; })
             .Define("gen_pt_H",  [=]() { return (float) 0.0; })
             .Define("Run_number",  [=]() { return (unsigned int) 0; })
             .Define("gen_pt_Z",  [=]() { return (float) 0.0; });

  }else{ // Other samples
    return df.Define("gen_weight","genWeight")
             .Define("gen_mass_Z",  [=]() { return (float) 0.0; })
             .Define("npu","Pileup_nPU")
             .Define("HT_LHE","LHE_HT")
             .Define("Run_number",  [=]() { return (unsigned int) 0; })
             .Define("gen_pt_W",  [=]() { return (float) 0.0; })
             .Define("gen_pt_H",  [=]() { return (float) 0.0; })
             .Define("gen_pt_Z",  [=]() { return (float) 0.0; });
  } 
}


/*********************************************************************************************/



/********************************* Example of testing correction ******************/
/*
template <typename T>
auto ZH_pT_mfit(T &df, std::string sample) {
  if (sample == "MC_Signal" || sample == "MC_DY" ){ // Select events with Leading Fat Jet that is Gen Matched to AK8 Jet contains 4 or more b hadrons
    auto df2 = df.Define("m_cor1_fatjet",   Helper::ZH_corr1, {"m_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"});
    return df2.Define("msoftdrop_cor1_fatjet",   Helper::ZH_corr1, {"msoftdrop_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"})
              .Define("mtopo_fatjet",   Helper::Hmass_btbCorrection, {"m_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"})
              .Define("msofttopo_fatjet",   Helper::Hmass_btbCorrection, {"msoftdrop_fatjet", "pt_fatjet", "pt_ll", "dphi_fatjet_ll"});
  
  }else{ 
    return df.Define("m_cor1_fatjet",  [=]() { return (float) 0; })
             .Define("m_cor2_fatjet",  [=]() { return (float) 0; })
             .Define("msoftdrop_cor1_fatjet", [=]() { return (float) 0; })
             .Define("msoftdrop_cor2_fatjet", [=]() { return (float) 0; });
  }
}
*/
/********************************************************************************************************/
#endif
