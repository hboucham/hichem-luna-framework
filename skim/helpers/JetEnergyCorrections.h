#ifndef JES_H
#define JES_H

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>
#include "helpers.h"
#include "correction.h"

using correction::CorrectionSet;
using namespace ROOT::VecOps;


// Function that applies JEC/JER. Not in use anymore since branches are added
// via NanoAOD Tools. Scroll down for the current function in use.

  // Load correction files AK4 jets
  auto ak4corrset = CorrectionSet::from_file("../commonFiles/JEC_files/POG/JME/2018_UL/jet_jerc.json");
  auto ak4corr = ak4corrset->compound().at("Summer19UL18_V5_MC_L1L2L3Res_AK4PFchs"); 
  auto ak4ptres = ak4corrset->at("Summer19UL18_JRV2_MC_PtResolution_AK4PFchs"); 
  auto ak4jer = ak4corrset->at("Summer19UL18_JRV2_MC_ScaleFactor_AK4PFchs"); 
  auto ak4corrUnc = ak4corrset->at("Summer19UL18_V5_MC_Total_AK4PFchs");
  // Load correction files AK8 jets
  auto ak8corrset = CorrectionSet::from_file("../commonFiles/JEC_files/POG/JME/2018_UL/fatJet_jerc.json");
  auto ak8corr = ak8corrset->compound().at("Summer19UL18_V5_MC_L1L2L3Res_AK8PFPuppi");
  auto ak8ptres = ak8corrset->at("Summer19UL18_JRV2_MC_PtResolution_AK8PFPuppi"); 
  auto ak8jer = ak8corrset->at("Summer19UL18_JRV2_MC_ScaleFactor_AK8PFPuppi"); 
  auto ak8corrUnc = ak8corrset->at("Summer19UL18_V5_MC_Total_AK8PFPuppi");
  // MET correction
  //auto ak4corrL1 = ak4corrset->at("Summer19UL18_V5_MC_L1FastJet_AK4PFchs");

// Note correction files are for Summer19UL, not Summer20UL (the nanoAOD I'm
// using). The Summer20UL corrections are not out yet but JERC says it's OK to
// use Summer19UL files.

// variables inside [] are gloabal vars made available in the scope of lambda function
// eventually you should include good jet requirements to lower run time
// eventually you should do MET corrections here too
auto cleanAKJets( const RVec<float> jet_pt, const RVec<float> jet_m, const RVec<float> jet_SDm, const RVec<float> jet_eta, 
    const RVec<float> jet_phi, const RVec<float> gjet_pt, const RVec<float> gjet_eta, const RVec<float> gjet_phi, 
    const RVec<float> &jet_rf, const RVec<float> &jet_area, const RVec<int> &jet_genidx, const float &rho, std::string jesvar){
    
    // init empty vectors that are returned by this function
    RVec<float> cleanAKJetPt(jet_pt.size());
    RVec<float> cleanAKJetEta(jet_pt.size());
    RVec<float> cleanAKJetPhi(jet_pt.size());
    RVec<float> cleanAKJetMass(jet_pt.size());
    RVec<float> cleanAKJetSoftdropMass(jet_pt.size());
    RVec<float> newRawFact(jet_pt.size());
    RVec<float> cleanAKJetJES(jet_pt.size());
    RVec<float> cleanAKJetJER(jet_pt.size());

    // init these variables depending on the corrections systematics
    std::string jervar = "nom";
    float jesuncmult = 0; 
    // Systematics shift
    if(jesvar == "JERup") jervar = "up";
    if(jesvar == "JERdn") jervar = "down";
    if(jesvar == "JESup") jesuncmult = 1.0;
    if(jesvar == "JESdn") jesuncmult = -1.0;

    // Correctionlib tools
    correction::CompoundCorrection::Ref jescorr; // AK4/AK8 JES
    correction::Correction::Ref jescorrUnc; // AK4/AK8 JES syst
    correction::Correction::Ref akptres; // AK4/AK8 resolution
    correction::Correction::Ref akjer; // AK4/AK8 JER

    // init vars for AK8
    //float drmax = 0.4;
    //jescorr = ak8corr;  //  use AK8 JES json for scale factor
    //jescorrUnc = ak8corrUnc; // use AK8 JES json for systematics

    // If you were applying JEC for AK4 and AK8
    float drmax = 0.2;
    if(ROOT::VecOps::Mean(jet_area) < 1.0){ //AK4 jet has mean area less than 1
      jescorr = ak4corr;  //  use AK4 JES json for scale factor
      jescorrUnc = ak4corrUnc; // use AK4 JES json for systematics 
      akptres = ak4ptres;
      akjer = ak4jer;
   // focus on AK4 for now, uncomment this later
    }else{ //AK8 jet
      drmax = 0.4; // init drmax for AK8
      jescorr = ak8corr;  //  use AK8 JES json for scale factor
      jescorrUnc = ak8corrUnc; // use AK8 JES json for systematics
      akptres = ak8ptres;
      akjer = ak8jer;
    }    

    // loop over all jets in teh event
    for(unsigned int ijet = 0; ijet < jet_pt.size(); ijet++){
      //init vars you need (could save memory space and remove this later)
      float ijet_pt = jet_pt[ijet];
      float ijet_m = jet_m[ijet];
      float ijet_SDm = jet_SDm[ijet];
      float ijet_eta = jet_eta[ijet];
      float ijet_phi = jet_phi[ijet];
      float ijet_rawFactor = jet_rf[ijet];
      float ijet_area = jet_area[ijet];
      
      // init scale factors
      float jes = 1.0; 
      //float jesL1 = 1.0; 
      float jer = 1.0; 
      float unc = 1.0;
     
      // JEC Part 1 ----> Jet Energy Scale (JES)
      // JES is applied to data and MC, but Run2 UL data already includes it (?)
      // JES adjusts mean of the momentum response distribution (scaling) and
      // is applied to the raw (uncorrected) pT
    
      // Get jet raw pt and mass
      float ijet_rawpt = (1 - ijet_rawFactor) * ijet_pt;
      float ijet_rawm = (1 - ijet_rawFactor) * ijet_m;
      float ijet_rawSDm = (1 - ijet_rawFactor) * ijet_SDm;
      // Obtained JES scale factor 
      jes = jescorr->evaluate({ijet_area,ijet_eta,ijet_rawpt,rho});                
      // Obtain JES corrected pt, which will be used for JER
      float ijet_rawptjes = ijet_rawpt*jes;


      // JEC Part 2 ----> Jet Energy Resolution (JER)
      // JER is applied to MC only, and using the JES corrected pT
      // JER adjusts the width of the momentum response distribution
      // JER is typically smaller in MC than data --> scale factor large than 1
      // There are two methods depending on gen matching:
      // 1) IF there is a well matched gen jet:
      //  Adjust ratio of reco/gen pT using a scale factor
      // 2) IF there is NO matched gen jet: 
      // Randomly smear the momentum using a Gaussian distribution based on the resolution and a scale factor

      // Init resolution and scale factor (this is NOT the final JER scale factor)
      float res = akptres->evaluate({ijet_eta,ijet_rawpt,rho}); 
      float sf = akjer->evaluate({ijet_eta,jervar}); 
      bool smeared = false;    // init smearing not done 


      // if the jet is matched to a gen jet AND the gen jet has non-zero pt 
      if(jet_genidx[ijet] > -1 ){	// check if jet is matched to a gen jet 
        // init matched gen jet vars
        float igjet_pt = gjet_pt[jet_genidx[ijet]];
        float igjet_eta = gjet_eta[jet_genidx[ijet]];
        float igjet_phi = gjet_phi[jet_genidx[ijet]];
        if (igjet_pt > 0){ // if gen jet has non-zero pt
          double dPt = fabs(igjet_pt - ijet_rawptjes);
          const auto dR = Helper::DeltaR(ijet_eta, igjet_eta, ijet_phi, igjet_phi); 
          // if dR less than 0.2 (0.4) for AK4 (AK8) and dPt less than 3*resolution*(JES corrected jet pt)
          if(dR < drmax && dPt < 3*res*ijet_rawptjes){ 
            // All conditions are met, start scenario 1
            // max used to make sure JER scale factor is positive
            jer = std::max(0.0, 1.0 + (sf - 1.0)*((ijet_rawptjes - igjet_pt)/ijet_rawptjes));
            smeared = true; // JER smearing is done
          }
        }
      }
      if(!smeared){ // if smearing is not done, start scenario 2
        // Pick a seed so smearing is reproducible
        TRandom3 rand(abs(static_cast<int>(ijet_phi*1e4)));
        jer = std::max(0.0, 1.0 + rand.Gaus(0, res)*sqrt(std::max(0.0, sf*sf - 1.0)));
      }
      
      // pt after JES and JER
      float ijet_ptjer = ijet_rawptjes*jer;
      // JER uncertainty (nominal/up/down) gets the following value
      // note that if we are just evaluating nominal JEC, unc = 1 since jesuncmult = 0
      // This means that the jet after JER is unchanged (no up/down shifts)
      // ignore systematics for now, uncomment line below later
      unc = 1.0 + jesuncmult*(jescorrUnc->evaluate({ijet_eta, ijet_ptjer})); 

      // New raw factor that you use to get raw jet pt and m in the future	
      float rf = 1.0 - 1.0/(jes*jer*unc); 
      // New jet pt and m is corrected or the same (if jes*jer*unc = 1)
      cleanAKJetPt[ijet] = ijet_rawpt*jes*jer*unc;
      cleanAKJetMass[ijet] = ijet_rawm*jes*jer*unc;
      cleanAKJetSoftdropMass[ijet] = ijet_rawSDm*jes*jer*unc;
      cleanAKJetEta[ijet] = ijet_eta;
      cleanAKJetPhi[ijet] = ijet_phi;
      newRawFact[ijet] = rf;
      cleanAKJetJES[ijet] = jes;
      cleanAKJetJER[ijet] = jer;
      //std::cout << jes << " " << jer << " " << unc << std::endl;
    }
    RVec<RVec<float>> output = {cleanAKJetPt, cleanAKJetEta, cleanAKJetPhi, cleanAKJetMass, cleanAKJetSoftdropMass, newRawFact, cleanAKJetJES, cleanAKJetJER};
    return output;
  }
   

/**********************************************************/
// Current JEC/JER function 

template <typename T>
auto ApplyJEC(T &df, std::string sample, std::string JEC_val) { // Applied to AK8 jets only since selection does not depend ofn AK4 jets
  // JEC already applied to data
  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("cFatJet_pt", "FatJet_pt_nom")
             .Define("cFatJet_mass", "FatJet_mass_nom")
             .Define("cFatJet_msoftdrop", "FatJet_msoftdrop_nom")
             .Define("cJet_pt", "Jet_pt_nom")
             .Define("cJet_mass", "Jet_mass_nom");
  }else{
    if (JEC_val == "nom"){
      return df.Define("cFatJet_pt", "FatJet_pt_nom")
               .Define("cFatJet_mass", "FatJet_mass_nom")
               .Define("cFatJet_msoftdrop", "FatJet_msoftdrop_nom")
               .Define("cJet_pt", "Jet_pt_nom")
               .Define("cJet_mass", "Jet_mass_nom");
    }else if  (JEC_val == "JESup"){
      return df.Define("cFatJet_pt", "FatJet_pt_jesTotalUp")
               .Define("cFatJet_mass", "FatJet_mass_jesTotalUp")
               .Define("cFatJet_msoftdrop", "FatJet_msoftdrop_jesTotalUp")
               .Define("cJet_pt", "Jet_pt_jesTotalUp")
               .Define("cJet_mass", "Jet_mass_jesTotalUp");
    }else if  (JEC_val == "JESdn"){
      return df.Define("cFatJet_pt", "FatJet_pt_jesTotalDown")
               .Define("cFatJet_mass", "FatJet_mass_jesTotalDown")
               .Define("cFatJet_msoftdrop", "FatJet_msoftdrop_jesTotalDown")
               .Define("cJet_pt", "Jet_pt_jesTotalDown")
               .Define("cJet_mass", "Jet_mass_jesTotalDown");
    }else if  (JEC_val == "JERup"){
      return df.Define("cFatJet_pt", "FatJet_pt_jerUp")
               .Define("cFatJet_mass", "FatJet_mass_jerUp")
               .Define("cFatJet_msoftdrop", "FatJet_msoftdrop_jerUp")
               .Define("cJet_pt", "Jet_pt_jerUp")
               .Define("cJet_mass", "Jet_mass_jerUp");
    }else{ //if  (JEC_val == "JERup"){
      return df.Define("cFatJet_pt", "FatJet_pt_jerDown")
               .Define("cFatJet_mass", "FatJet_mass_jerDown")
               .Define("cFatJet_msoftdrop", "FatJet_msoftdrop_jerDown")
               .Define("cJet_pt", "Jet_pt_jerDown")
               .Define("cJet_mass", "Jet_mass_jerDown");
    }
  }
}

/**********************************************************/
// Old JEC/JER function

template <typename T>
auto ApplyJEC_old(T &df, std::string sample, std::string JEC_val) { // Applied to AK8 jets only since selection does not depend ofn AK4 jets
  // JEC already applied to data
  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("cleanFatJet_pt", "FatJet_pt")
              .Define("cleanFatJet_eta", "FatJet_eta")
              .Define("cleanFatJet_phi", "FatJet_phi")
              .Define("cleanFatJet_mass", "FatJet_mass")
              .Define("cleanFatJet_Softdropmass", "FatJet_msoftdrop")
              .Define("cleanFatJet_rawFactor", "FatJet_rawFactor")
              // Clean AK4 jets
              .Define("cleanJet_pt", "Jet_pt")
              .Define("cleanJet_eta", "Jet_eta")
              .Define("cleanJet_phi", "Jet_phi")
              .Define("cleanJet_mass", "Jet_mass")
              .Define("cleanJet_rawFactor", "Jet_rawFactor");
  }else{
    // For AK8
    auto df2 = df.Define("clean_FatJet", [JEC_val](const RVec<float>& FatJet_pt, const RVec<float>& FatJet_mass, 
                                          const RVec<float>& FatJet_Softdropmass, const RVec<float>& FatJet_eta,
                                          const RVec<float>& FatJet_phi, const RVec<float>& GenJetAK8_pt, const RVec<float>& GenJetAK8_eta,
                                          const RVec<float>& GenJetAK8_phi, const RVec<float>& FatJet_rawFactor, const RVec<float>& FatJet_area,
                                          const RVec<int>& FatJet_genJetAK8Idx, float fixedGridRhoFastjetAll) {
    return cleanAKJets(FatJet_pt, FatJet_mass, FatJet_Softdropmass, FatJet_eta, FatJet_phi, GenJetAK8_pt, GenJetAK8_eta, GenJetAK8_phi,
                    FatJet_rawFactor, FatJet_area, FatJet_genJetAK8Idx, fixedGridRhoFastjetAll, JEC_val);
    }, {"FatJet_pt", "FatJet_mass", "FatJet_msoftdrop", "FatJet_eta", "FatJet_phi", "GenJetAK8_pt", "GenJetAK8_eta", "GenJetAK8_phi", 
     "FatJet_rawFactor", "FatJet_area", "FatJet_genJetAK8Idx", "fixedGridRhoFastjetAll"});
    //auto df2 = df.Define("clean_Jets", cleanFatJets, {"FatJet_pt", "FatJet_mass", "FatJet_eta", "FatJet_phi", "GenJetAK8_pt", "GenJetAK8_eta", "GenJetAK8_phi", "FatJet_rawFactor", "FatJet_area", "FatJet_genJetAK8Idx", "fixedGridRhoFastjetAll"});
    return df2.Define("cleanFatJet_pt", "clean_FatJet[0]")
              .Define("cleanFatJet_eta", "clean_FatJet[1]")
              .Define("cleanFatJet_phi", "clean_FatJet[2]")
              .Define("cleanFatJet_mass", "clean_FatJet[3]")
              .Define("cleanFatJet_Softdropmass", "clean_FatJet[4]")
              .Define("cleanFatJet_rawFactor", "clean_FatJet[5]")
              .Define("cleanFatJet_JES", "clean_FatJet[6]")
              .Define("cleanFatJet_JER", "clean_FatJet[7]")
    // For AK4
              .Define("clean_Jet", [JEC_val](const RVec<float>& Jet_pt, const RVec<float>& Jet_mass, 
                                          const RVec<float>& Jet_massdummy, const RVec<float>& Jet_eta,
                                          const RVec<float>& Jet_phi, const RVec<float>& GenJet_pt, const RVec<float>& GenJet_eta,
                                          const RVec<float>& GenJet_phi, const RVec<float>& Jet_rawFactor, const RVec<float>& Jet_area,
                                          const RVec<int>& Jet_genJetIdx, float fixedGridRhoFastjetAll) {
    return cleanAKJets(Jet_pt, Jet_mass, Jet_massdummy, Jet_eta, Jet_phi, GenJet_pt, GenJet_eta, GenJet_phi,
                    Jet_rawFactor, Jet_area, Jet_genJetIdx, fixedGridRhoFastjetAll, JEC_val);
    }, {"Jet_pt", "Jet_mass", "Jet_mass", "Jet_eta", "Jet_phi", "GenJet_pt", "GenJet_eta", "GenJet_phi", 
     "Jet_rawFactor", "Jet_area", "Jet_genJetIdx", "fixedGridRhoFastjetAll"})
              .Define("cleanJet_pt", "clean_Jet[0]")
              .Define("cleanJet_eta", "clean_Jet[1]")
              .Define("cleanJet_phi", "clean_Jet[2]")
              .Define("cleanJet_mass", "clean_Jet[3]")
              .Define("cleanJet_JES", "clean_Jet[6]")
              .Define("cleanJet_JER", "clean_Jet[7]")
              .Define("cleanJet_rawFactor", "clean_Jet[5]");

  }
}

/*************************************************************/
#endif
