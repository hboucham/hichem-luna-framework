
#ifndef HELPERS_H
#define HELPERS_H

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>
/**********************************************************/

using namespace ROOT::VecOps;

/* 
 * Helper namespace for functions.
 */

std::vector<std::string> Bquark_hadrons = { "511", "513", "515", "521", "523", "525", "531", "533", "535", "541", "553", "555", "5101", "5103", "5112", "5114", "5122", "5132", "5201", "5203", "5222", "5224", "5232", "5301", "5303", "5332", "5401", "5403", "5503", "10551", "10553", "20553", "20555", "100553", "100555", "110551", "120553", "200553", "300553", "9000553", "9010553"};

/////// This file includes all the useful operations like deltaR between two objects..etc
namespace Helper {

/**************   String Helpers   ***************************************************/

  // Returns whether a substring is in a string.
  bool containsSubstring(std::string string, std::string substring) {
    return (string.find(substring) != std::string::npos);
  }

  // Returns whether any substrings in the provided vector, are in the string.
  bool containsAnyOfTheseSubstrings(std::string str, std::vector<std::string> vSubstring) {
    for (std::string substring : vSubstring) {
      if (str.find(substring) != std::string::npos) {
        return true;
      }
    }
    return false;
  }

/**************   Physics Equations Helpers   ***************************************************/

  // Returns 4-vector of an object
  ROOT::Math::PtEtaPhiMVector add_p4(float pt, float eta, float phi, float mass) {
    return ROOT::Math::PtEtaPhiMVector(pt, eta, phi, mass);
  }

  // Returns Dphi 
  float DeltaPhi(float phi1, float phi2) {
    const double c = M_PI;
    auto r = std::fmod(phi2 - phi1, 2.0 * c);
    if (r < -c) {
      r += 2.0 * c;
    }
    else if (r > c) {
      r -= 2.0 * c;
    }
    return r;
  }
  
  // Returns Dr
  float DeltaR(float eta1, float eta2, float phi1, float phi2) {
    auto deltaR = sqrt(pow(eta1 - eta2, 2) + pow(DeltaPhi(phi1, phi2), 2));
    return deltaR;
  }

  // Returns Dr Vector
  RVec<float> VDeltaR(float eta1, RVec<float>& eta2, float phi1, RVec<float>& phi2) {
    RVec<float> dR(eta2.size());
    for (size_t i = 0; i < eta2.size(); i++){
      dR[i] = sqrt(pow(eta1 - eta2[i], 2) + pow(DeltaPhi(phi1, phi2[i]), 2));
    }
    return dR;
  }

  // Returns abs(Dphi)
  float DeltaPhi_abs(float phi1, float phi2) {
    const double c = M_PI;
    auto r = std::fmod(phi2 - phi1, 2.0 * c);
    if (phi1 == -99 || phi2 == -99){
      return -99;
    }else{
      if (r < -c) {
        r += 2.0 * c;
      }
      else if (r > c) {
        r -= 2.0 * c;
      }
      return abs(r);
    }
  }

  // Returns abs(Deta)
  float DeltaEta_abs(float eta1, float eta2) {
    auto r = abs(eta1 - eta2);
    return r;
  }

  // Returns Transverse Mass of MET and object X
  float Mt_MET_X(float pt_M, float phi_M, float pt_X, float phi_X) { 
    auto mt = sqrt(2*pt_M*pt_X*(1-cos(DeltaPhi(phi_M, phi_X))));
    return mt;
  }

  // returns vector of 4-vec
  RVec<ROOT::Math::PtEtaPhiMVector> fVectorConstructor(RVec<float> &pt, RVec<float> &eta, RVec<float> &phi, RVec<float> &mass)
    {
      RVec<ROOT::Math::PtEtaPhiMVector> lv;
      //ROOT::Math::PtEtaPhiMVector tlv;
      for (size_t i = 0; i < pt.size(); i++)
      {
          //tlv = ROOT::Math::PtEtaPhiMVector(pt[i], eta[i], phi[i], mass[i]);
          lv.push_back(ROOT::Math::PtEtaPhiMVector(pt[i], eta[i], phi[i], mass[i]));
      }
      return lv;
    };


  //returns minimum of a vector
  float minV(RVec<float>& val){
    //float min = val[0];
    float min = 3;
    for (size_t i = 0; i < val.size(); i++){
      if (val[i] > 0.8){
        if (val[i] < min){
          min = val[i];
        }
      }
    }
    return min;
  }


 RVec<float> maxVec(RVec<float> &val1, RVec<float> &val2){
   RVec<float> vec;
   for(size_t i = 0; i < val1.size(); i++){
     if (val1[i] > val2[i]){
       vec.push_back(val1[i]);
     }else{
        vec.push_back(val2[i]);
     }
   }
   return vec;
 }

  // Overlap Check
  RVec<int> OverlapdR(RVec<float>& etajet, RVec<float>& phijet, RVec<int>& goodmu, RVec<float>& etamu,RVec<float>& phimu, RVec<int>& goodel, RVec<float>& etael,RVec<float>& phiel) {
    RVec<int> overlaps(etajet.size());
    for (size_t i = 0; i < etajet.size(); i++){
      overlaps[i] = 0;
      for (size_t j = 0; j < etamu.size(); j++){
        if (goodmu[j] == 1){
          if (sqrt(pow(etajet[i] - etamu[j], 2) + pow(DeltaPhi(phijet[i], phimu[j]), 2)) < 0.4){
            overlaps[i] = overlaps[i] + 1;
          }
        }
      }
      for (size_t k = 0; k < etael.size(); k++){
        if (goodel[k] == 1){
          if (sqrt(pow(etajet[i] - etael[k], 2) + pow(DeltaPhi(phijet[i], phiel[k]), 2)) < 0.4){
            overlaps[i] = overlaps[i] + 1;
          }
        }
      }
    }
    return overlaps;
  }

template<typename T>
auto reorder(const RVec<T> &v, const RVec<int> &idx){
  RVec<T> vnew(v.size());
  for(unsigned int i = 0; i < idx.size(); i++){
    vnew[i] = v[idx[i]];
  }
  return vnew;
}
/******************************************************************************/

/*
 * Return true if the dataframe has a branch "branch" and false otherwise.
 * The branch name must match exactly.
 */

template <typename T>
bool HasExactBranch(T &df, std::string branch) {

  bool hasBranch = false;

  // Get df column names (branches in the NanoAOD)
  auto colNames = df.GetColumnNames();
  for (auto &&colName : colNames) {
    if (colName == branch) {
      hasBranch = true;
      break;
    }
  }

  return hasBranch;
}

/****************************** 2L_Z Catgeory *****************************************/
/************** Miscellaneous Helpers: FindPairIndices   ***************************************************/

  // Returns the a pair of indices that refers to the leading lepton pair in the event or -1 if not found
  std::vector<int> FindPairIndices(RVec<int>& noHOverlapLeps, RVec<int>& triggerLeps,  RVec<float>& pt_lep) {
    
      int idx_1 = -1;
      int idx_2 = -1;
      float maxPt = -2;
      float secondMaxPt = -3;

      for (size_t i = 0; i < pt_lep.size(); i++) {
        if (noHOverlapLeps[i] == 1) {
          if (pt_lep[i]*triggerLeps[i] > maxPt){
            idx_2 = idx_1;
            idx_1 = i;
            secondMaxPt = maxPt;
            maxPt = pt_lep[i]*triggerLeps[i];
          }else if (pt_lep[i]*triggerLeps[i] > secondMaxPt && pt_lep[i]*triggerLeps[i] <= maxPt){
            idx_2 = i;
            secondMaxPt = pt_lep[i]*triggerLeps[i];
          }
        }
      }
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_ll_2LZ   ***************************************************/

  // Returns the a pair of indices that refers to the leading lepton pair in the event or -1 if not found
  std::vector<int> build_ll_2LZ(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, RVec<int>& trigObjects_1,
       RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& mass_1,RVec<int>& charge_1,
       RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& mass_2,RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
        const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
          if (deltar > 0) { // Exclude identical pairs
            validPair[i] = 1;
          }
        }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
          // Adding invariant mass requirement so they are within Z mass window
		      ROOT::Math::PtEtaPhiMVector p4_1 = Helper::add_p4(pt_1[tmp1], eta_1[tmp1], phi_1[tmp1], mass_1[tmp1]);
		      ROOT::Math::PtEtaPhiMVector p4_2 = Helper::add_p4(pt_2[tmp2], eta_2[tmp2], phi_2[tmp2], mass_2[tmp2]);
		      float M_vis = (p4_1+p4_2).M();
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) && abs(M_vis - 91) < 10){ 
            if (trigObjects_1[tmp1]){ // Require one of the leptons to be a "trigger lepton"
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Select pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_mu_pair   ***************************************************/

  // Returns the a pair of indices that refers to the leading muon pair in the event or -1 if not found
  std::vector<int> build_mu_pair(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, 
       RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& mass_1,RVec<int>& charge_1,
       RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& mass_2,RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
        const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
          if (deltar > 0) { // Exclude identical pairs
            validPair[i] = 1;
          }
        }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
          // Adding invariant mass requirement so they are within Z mass window
		      ROOT::Math::PtEtaPhiMVector p4_1 = Helper::add_p4(pt_1[tmp1], eta_1[tmp1], phi_1[tmp1], mass_1[tmp1]);
		      ROOT::Math::PtEtaPhiMVector p4_2 = Helper::add_p4(pt_2[tmp2], eta_2[tmp2], phi_2[tmp2], mass_2[tmp2]);
		      float M_vis = (p4_1+p4_2).M();
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) && (M_vis> 80) && (M_vis < 100) ){ 
            if (pt_1[tmp1]> 26){ // Require one of muons to have pt > 26 GeV
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Prefer the pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_el_pair   ***************************************************/

  // Returns the a pair of indices that refers to the leading electron pair in the event or -1 if not found
  std::vector<int> build_el_pair(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, 
       RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& mass_1,RVec<int>& charge_1,
       RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& mass_2,RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
        const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
          if (deltar > 0) { // Exclude identical pairs
            validPair[i] = 1;
          }
        }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
          // Adding invariant mass requirement so they are within Z mass window
		      ROOT::Math::PtEtaPhiMVector p4_1 = Helper::add_p4(pt_1[tmp1], eta_1[tmp1], phi_1[tmp1], mass_1[tmp1]);
		      ROOT::Math::PtEtaPhiMVector p4_2 = Helper::add_p4(pt_2[tmp2], eta_2[tmp2], phi_2[tmp2], mass_2[tmp2]);
		      float M_vis = (p4_1+p4_2).M();
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) && (M_vis> 80) && (M_vis < 100)){ 
            if (pt_1[tmp1]> 35 || pt_2[tmp2] > 35){ // Require one of lepton to have pt > 35 GeV
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Prefer the pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: Leading_FatJet   ***************************************************/
  
  // Returns index of the leading Fat Jet in the event
  int Leading_FatJet_2L( RVec<int>& goodjet, RVec<float>& jet_score){
	
    float maxPNET = 0;
	  int idx = -1;

    for (size_t i = 0; i < goodjet.size(); i++) {
      if (goodjet[i] == 1){
        if (jet_score[i] > maxPNET){
          idx = i;
          maxPNET = jet_score[i];
        }
      }
    }
	  return idx;
  }

/************** Miscellaneous Helpers: Leading_FatJet   ***************************************************/
  
  // Returns index of the leading Fat Jet in the event
/*  int Leading_FatJet_2LZ( int nJet, RVec<int>& jetId, RVec<float>& jet_eta, RVec<float>& jet_phi, 
      RVec<float>& jet_score, RVec<float>& jet_msoftdrop, float lep1_eta, float lep1_phi ,float lep2_eta ,float lep2_phi)  {
	
    float maxPNET = 0;
	  int idx = -1;
    //float PNET_score_cut = -1;

    for (int i = 0; i < int(nJet); i++) {
      // Requiring AK8 Jet isolated from selected lepton pair + PNET_score cut
      const auto deltar1 = Helper::DeltaR(jet_eta[i], lep1_eta, jet_phi[i], lep1_phi);
      const auto deltar2 = Helper::DeltaR(jet_eta[i], lep2_eta, jet_phi[i], lep2_phi);
	  	if (jetId[i] == 6 && jet_msoftdrop[i] > 20 && abs(jet_eta[i]) < 2.4 && deltar1 > 0.8 && deltar2 > 0.8){
		    if (jet_score[i] > maxPNET){ // Prefer AK8 jet with highest PNET score
		   // if (jet_pt[i] > maxPNET){ // Prefer AK8 jet with highest pT
				  idx = i;
			  	//maxPNET = jet_pt[i];
			  	maxPNET = jet_score[i];
		  	}
	  	}
  	}
	  return idx;
  }
*/
/************** Miscellaneous Helpers: nJet_iso   ***************************************************/

  // Compute number of AK4 Jet with dR > 0.8 from selected AK8 Jet && dR> 0.4 from selected lepton pair
  int nJet_iso(int nJet, RVec<float>& jet_eta, RVec<float>& jet_pt, RVec<int>& jetId, RVec<int>& jet_puId, 
      RVec<float>& jet_phi, float fatjet_eta, float fatjet_phi, float lep1_eta, float lep1_phi , float lep2_eta , float lep2_phi)  {
	  
    int num = 0;
   	
    for (int i = 0; i < int(nJet); i++) { // Same as above, probably need to consolidate functions 
      if (jet_pt[i] > 30 && jetId[i] == 6 && ( jet_pt[i] > 50 || jet_puId[i] > 3)){ 
        const auto deltar1 = Helper::DeltaR(jet_eta[i], lep1_eta, jet_phi[i], lep1_phi);
        const auto deltar2 = Helper::DeltaR(jet_eta[i], lep2_eta, jet_phi[i], lep2_phi);
        const auto deltarfat = Helper::DeltaR(jet_eta[i], fatjet_eta, jet_phi[i], fatjet_phi);
		    if (deltarfat> 0.8 && deltar1 > 0.4 && deltar2 > 0.4){
				  num=num+1;
		 	  }
      }
    }
    return num;
  }

/************** Miscellaneous Helpers: nJetCent_iso   ***************************************************/

  // Compute number of AK4 Jet with dR > 0.8 from selected AK8 Jet && dR> 0.4 from selected lepton pair
  int nJetCent_iso(int nJet, RVec<float>& jet_eta, RVec<float>& jet_pt, RVec<int>& jetId, RVec<int>& jet_puId, 
      RVec<float>& jet_phi, float fatjet_eta, float fatjet_phi, float lep1_eta, float lep1_phi , float lep2_eta , float lep2_phi)  {
	  
    int num = 0;
   	
    for (int i = 0; i < int(nJet); i++) { // Same as above, probably need to consolidate functions 
      if (jet_pt[i] > 30 && jetId[i] == 6 && ( jet_pt[i] > 50 || jet_puId[i] > 3) && abs(jet_eta[i]) < 2.4){ 
        const auto deltar1 = Helper::DeltaR(jet_eta[i], lep1_eta, jet_phi[i], lep1_phi);
        const auto deltar2 = Helper::DeltaR(jet_eta[i], lep2_eta, jet_phi[i], lep2_phi);
        const auto deltarfat = Helper::DeltaR(jet_eta[i], fatjet_eta, jet_phi[i], fatjet_phi);
		    if (deltarfat> 0.8 && deltar1 > 0.4 && deltar2 > 0.4){
				  num=num+1;
		 	  }
      }
    }
    return num;
  }

/************** Miscellaneous Helpers: nBtagMJet_iso   ***************************************************/

  // Compute number of AK4 Jet that are B tagged with WP Medium && dR > 0.8 from selected AK8 Jet && dR> 0.4 from selected lepton pair
  int nBtagMJet_iso(int nJet, RVec<float>& Btag,  RVec<float>& jet_eta, RVec<float>& jet_pt, RVec<int>& jetId, RVec<int>& jet_puId, 
      RVec<float>& jet_phi, float fatjet_eta, float fatjet_phi, float lep1_eta, float lep1_phi , float lep2_eta , float lep2_phi)  {
	  
    int num = 0;
    float WP = 0.2783; // DeepJet Medium WP for 2018UL: https://btv-wiki.docs.cern.ch/ScaleFactors/UL2018/
   	    
    for (int i = 0; i < int(nJet); i++) {
      // Select good AK4 Jets (standard quality cuts)
      if (jet_pt[i] > 30 && jetId[i] == 6 && ( jet_pt[i] > 50 || jet_puId[i] > 3) && abs(jet_eta[i]) < 2.4){ 
        if (Btag[i] > WP){ // Check it's B-tagged depending on WP (L/M/T)
          const auto deltar1 = Helper::DeltaR(jet_eta[i], lep1_eta, jet_phi[i], lep1_phi);
          const auto deltar2 = Helper::DeltaR(jet_eta[i], lep2_eta, jet_phi[i], lep2_phi);
          const auto deltarfat = Helper::DeltaR(jet_eta[i], fatjet_eta, jet_phi[i], fatjet_phi);
		      if (deltarfat> 0.8 && deltar1 > 0.4 && deltar2 > 0.4){ // Check jets isolated from lepton pair and selected AK8 Jet
				    num=num+1;
		 	    }
	      } 
      }
    }
    return num;
  }

/************** Miscellaneous Helpers: M_softdrop_ptScaled   ***************************************************/
    
  // Compute the pt scaled Softdrop Mass following a given formula 
  float M_softdrop_ptScaled(float m_jet, float pt_jet)  {
    
    float m_jet_scaled = m_jet;
	
    if (log2(pt_jet) < 8.644){ // pt-scaled softdrop mass is the same as softdrop mass above this threshold
      float a = 8.644 - log2(pt_jet);
      float b = - pow(a, 2);
      float c = pow(2, b);
      float scale =  1 + (8.644 / 15) * (1 - c);
      m_jet_scaled = m_jet_scaled* scale;
    }
    return m_jet_scaled;
  }

/************** Miscellaneous Helpers: GenDr_match_Z   ***************************************************/

  // Select gen Z that matches reco Z (for now only selecting 1 gen Z)
  int GenDr_match_Z(/*float eta_mumu, float phi_mumu, RVec<float>& eta_gen, RVec<float>& phi_gen,*/ RVec<int>& pdgid_gen)  {
	
    int idx = -1;
   	for (int i = 0; i < int(pdgid_gen.size()); i++) {
		  if (abs(pdgid_gen[i]) == 23){
        idx = i;
			  break;
		  } 
    
      /*const auto deltar = Helper::DeltaR(eta_muon, eta_gen[i], phi_muon, phi_gen[i]);
		  if (deltar < 0.4){
		    idx = i;
			  break;
		  }*/
	  }
	  return idx;
  }

/************** Miscellaneous Helpers: GenDr_match_H   ***************************************************/

  // Select gen H 
  int GenDr_match_H(RVec<int>& pdgid_gen, RVec<int>& pdgstatus_gen)  {
	
    int idx = -1;
   	for (int i = 0; i < int(pdgid_gen.size()); i++) {
		  if (pdgid_gen[i] == 25 &&  pdgstatus_gen[i] == 62){
        idx = i;
			  break;
		  }
	  }
	  return idx;
  }

/************** Miscellaneous Helpers: GenDr_match_W   ***************************************************/

  // Select gen W that matches reco W (for now only selecting 1 gen Z)
  int GenDr_match_W(/*float eta_mumu, float phi_mumu, RVec<float>& eta_gen, RVec<float>& phi_gen,*/ RVec<int>& pdgid_gen)  {
	
    int idx = -1;
   	for (int i = 0; i < int(pdgid_gen.size()); i++) {
		  if (abs(pdgid_gen[i]) == 24){
        idx = i;
			  break;
		  } 
    
      /*const auto deltar = Helper::DeltaR(eta_muon, eta_gen[i], phi_muon, phi_gen[i]);
		  if (deltar < 0.4){
		    idx = i;
			  break;
		  }*/
	  }
	  return idx;
  }
/************** Miscellaneous Helpers: GenDr_match_muon   ***************************************************/
  // Need to consolidate the 2 functions below
  int GenDr_match_muon(float eta_muon, float phi_muon, RVec<float>& eta_gen, RVec<float>& phi_gen, RVec<int>& pdgid_gen)  {
	
    int idx = -1;
   	for (int i = 0; i < int(pdgid_gen.size()); i++) {
		  if (abs(pdgid_gen[i]) != 13){
			  continue;
	  	}
		  const auto deltar = Helper::DeltaR(eta_muon, eta_gen[i], phi_muon, phi_gen[i]);
		  if (deltar < 0.4){
			  idx = i;
			  break;
	  	}
  	}
	  return idx;
  }

/************** Miscellaneous Helpers: GenDr_match_electron   ***************************************************/

  int GenDr_match_electron(float eta_electron, float phi_electron, RVec<float>& eta_gen, RVec<float>& phi_gen, RVec<int>& pdgid_gen)  {
	  
    int idx = -1;
   	for (int i = 0; i < int(pdgid_gen.size()); i++) {
		  if (abs(pdgid_gen[i]) != 11){
			  continue;
	  	}
      const auto deltar = Helper::DeltaR(eta_electron, eta_gen[i], phi_electron, phi_gen[i]);
		  if (deltar < 0.4){
			  idx = i;
			  break;
	  	}
  	}
	  return idx;
  }

/************** Miscellaneous Helpers: H mass ZH topology correction   ***************************************/

  float Hmass_btbCorrection(float mass_1, float pt_1, float pt_2, float dphi) {
    
    const double c = M_PI;
    float mass_corr2 = mass_1;

    if ((dphi > 7*c/8) && (pt_2/pt_1 > 1.0)){
      mass_corr2 = mass_1 * (pt_2/pt_1);
    }
    return mass_corr2;
  }

/************** Miscellaneous Helpers: MiniIsoID_corrected   ***************************************************/
    
  // Compute miniIsoID from Muon_miniPFRelIso_all as recommended here: https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideMuonSelection#Muon_Isolation
  // WP values found here: https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/DataFormats/MuonReco/interface/Muon.h#L207C7-L210C56

  int MiniIsoID_corrected(float RelIso)  {
    
    int IdIso = 0;
	
    if ( RelIso < 0.40){ // Loose WP
      IdIso = 1;
    }
    if ( RelIso < 0.20){ // Medium WP
      IdIso = 2;
    }
    if ( RelIso < 0.10){ // Tight WP
      IdIso = 3;
    }
    if ( RelIso < 0.05){ // Very Tight WP
      IdIso = 4;
    }
    return IdIso;
  }

/****************** ZH (Z->ll) END HERE *************************************************************************/
/****************** WH (W->lNu) STARTS HERE *************************************************************************/

/************** Miscellaneous Helpers: Find_index   ***************************************************/

  // Find index of good object
  int Find_index(RVec<int>& goodObject)  {
    
    int idx = -1;
    for (size_t i= 0; i < goodObject.size(); i++){
      if (goodObject[i]){
        idx = int(i);
        break;
      }
    }
    return idx;
  }

/************** Miscellaneous Helpers: Leading_FatJet_1L   ***************************************************/
  
  // Returns index of the leading Fat Jet in the event
  int Leading_FatJet_1L( int nJet, RVec<int>& goodjet, RVec<float>& jet_eta, RVec<float>& jet_phi, 
      RVec<float>& jet_score, float lep1_eta, float lep1_phi)  {
	
    float maxPNET = 0;
	  int idx = -1;

    for (int i = 0; i < int(nJet); i++) {
      // Requiring AK8 Jet isolated from selected lepton 
      const auto deltar1 = Helper::DeltaR(jet_eta[i], lep1_eta, jet_phi[i], lep1_phi);
	  	if (goodjet[i] && deltar1 > 0.8){
		    if (jet_score[i] > maxPNET){ // Prefer AK8 jet with highest score
				  idx = i;
			  	maxPNET = jet_score[i];
		  	}
	  	}
  	}
	  return idx;
  }

/************** Miscellaneous Helpers: Leading2jets   ***************************************************/

  std::vector<int> Leading2jets(int nJet, RVec<int>& goodObject, RVec<int>& badObject, RVec<float>& jet_pt) {
    
    int idx_1 = -1;
    int idx_2 = -1;
    float maxpt = -99;
    for (int i = 0; i < nJet; i++) {
      if (goodObject[i] && !(badObject[i])){
        if (jet_pt[i] > maxpt){
          idx_2 = idx_1;
          idx_1 = i;
        }
      }
    }
    return std::vector<int>({idx_1, idx_2});
  }

/************** Miscellaneous Helpers: Leading2Bjets   ***************************************************/

  std::vector<int> Leading2bjets(int nJet, RVec<int>& goodObject, RVec<float>& jet_pt) {
    
    int idx_1 = -1;
    int idx_2 = -1;
    float maxpt = -99;
    for (int i = 0; i < nJet; i++) {
      if (goodObject[i]){
        if (jet_pt[i] > maxpt){
          idx_2 = idx_1;
          idx_1 = i;
        }
      }
    }
    return std::vector<int>({idx_1, idx_2});
  }

/************** Miscellaneous Helpers: assign_safely   ***************************************************/

  float assign_safely(int idx, RVec<float>& branch) {
   
    float value= -99;
    if (idx == -1){
      return value;
    }else{
      return branch[idx];
    }
  }

/************** Miscellaneous Helpers: Leading2jets   ***************************************************/

  float Vector_sum(int nJet, RVec<int>& goodObject, RVec<float>& jet_pt, RVec<float>& jet_eta, RVec<float>& jet_phi, RVec<float>& jet_mass) {
   
    ROOT::Math::PtEtaPhiMVector Vec_sum = ROOT::Math::PtEtaPhiMVector(0, 0, 0, 0);
    float Var_sum = 0;
    for (int i = 0; i < nJet; i++) {
      if (goodObject[i]){
        Vec_sum = Vec_sum + ROOT::Math::PtEtaPhiMVector(jet_pt[i], jet_eta[i], jet_phi[i], jet_mass[i]);
      }
    }
    Var_sum = Vec_sum.Pt();
    return Var_sum;
  }

/************** Miscellaneous Helpers: Leading2jets   ***************************************************/
  // Returns 1 if GenPart is a B-hadron (hadron containing B quark) and 0 otherwise

  RVec<int> isBHadron(RVec<int>& pdg_id) {
    RVec<int> BHad(pdg_id.size());
    for (size_t i = 0; i < pdg_id.size(); i++){
      BHad[i] = 0;
      std::string str_pdgID = std::to_string(abs(pdg_id[i]));
      if (containsAnyOfTheseSubstrings(str_pdgID, Bquark_hadrons)){
        BHad[i] = 1;
      }
    }
    return BHad;
  }

/****************** 1L (WH/ttH)  END HERE *************************************************************************/

/****************** Dilepton ttH STARTS HERE *************************************************************************/

/********** Dilepton ttH ***********************/
/************** Miscellaneous Helpers: build_ll_2LZ   ***************************************************/

  // Returns the a pair of indices that refers to the leading lepton pair in the event or -1 if not found
  std::vector<int> build_ll_2Ltt(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, RVec<int>& trigObjects_1, int ngoodObjects_1,
       RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& mass_1,RVec<int>& charge_1,
       RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& mass_2,RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
        const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
          if (deltar > 0) { // Exclude identical pairs
            validPair[i] = 1;
          }
        }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
          // Adding invariant mass requirement so they are within Z mass window
		      ROOT::Math::PtEtaPhiMVector p4_1 = Helper::add_p4(pt_1[tmp1], eta_1[tmp1], phi_1[tmp1], mass_1[tmp1]);
		      ROOT::Math::PtEtaPhiMVector p4_2 = Helper::add_p4(pt_2[tmp2], eta_2[tmp2], phi_2[tmp2], mass_2[tmp2]);
		      float M_vis = (p4_1+p4_2).M();
          // ngoodObjects_1 == 1 means there is exactly 1 good muon or electron, which means the lepton pair has different flavor 
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) && M_vis > 12 && (ngoodObjects_1 == 1 || abs(M_vis - 91) > 10)){ 
            if (trigObjects_1[tmp1]){ // Require one of the leptons to be a "trigger lepton"
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Select pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_pair_mumu   ***************************************************/

  // Returns the a pair of indices that refers to the leading muon pair in the event or -1 if not found
  std::vector<int> build_pair_mumu(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, 
       RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& mass_1,RVec<int>& charge_1,
       RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& mass_2,RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
        const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
          if (deltar > 0) { // Exclude identical pairs
            validPair[i] = 1;
          }
        }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
          // Adding invariant mass requirement so they are outside Z mass window
		      ROOT::Math::PtEtaPhiMVector p4_1 = Helper::add_p4(pt_1[tmp1], eta_1[tmp1], phi_1[tmp1], mass_1[tmp1]);
		      ROOT::Math::PtEtaPhiMVector p4_2 = Helper::add_p4(pt_2[tmp2], eta_2[tmp2], phi_2[tmp2], mass_2[tmp2]);
		      float M_vis = (p4_1+p4_2).M();
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) && ((M_vis < 80) || (M_vis > 100)) ){ 
            if (pt_1[tmp1]> 26 || pt_2[tmp2] > 26){ // Require one of muons to have pt > 26 GeV
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Prefer the pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_pair_elel   ***************************************************/

  // Returns the a pair of indices that refers to the leading muon pair in the event or -1 if not found
  std::vector<int> build_pair_elel(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, 
       RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& mass_1,RVec<int>& charge_1,
       RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& mass_2,RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
        const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
          if (deltar > 0) { // Exclude identical pairs
            validPair[i] = 1;
          }
        }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
          // Adding invariant mass requirement so they are outside Z mass window
		      ROOT::Math::PtEtaPhiMVector p4_1 = Helper::add_p4(pt_1[tmp1], eta_1[tmp1], phi_1[tmp1], mass_1[tmp1]);
		      ROOT::Math::PtEtaPhiMVector p4_2 = Helper::add_p4(pt_2[tmp2], eta_2[tmp2], phi_2[tmp2], mass_2[tmp2]);
		      float M_vis = (p4_1+p4_2).M();
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) && ((M_vis < 80) || (M_vis > 100)) ){ 
            if (pt_1[tmp1]> 35 || pt_2[tmp2] > 35){ // Require one of electrons to have pt > 35 GeV
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Prefer the pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_pair_muel   ***************************************************/

  // Returns the a pair of indices that refers to the leading muon pair in the event or -1 if not found
  std::vector<int> build_pair_muel(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, 
       RVec<float>& pt_1, RVec<int>& charge_1, RVec<float>& pt_2, RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
            validPair[i] = 1;
          }
      }

    int idx_1 = -1;
    int idx_2 = -1;
    float bestSum = -99;

    for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) ){ 
            if (pt_1[tmp1]> 26 ){ // Require one of muon to have pt > 26 GeV
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Prefer the pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: build_pair_elmu   ***************************************************/

  // Returns the a pair of indices that refers to the leading muon pair in the event or -1 if not found
  std::vector<int> build_pair_elmu(RVec<int>& goodObjects_1, RVec<int>& goodObjects_2, 
       RVec<float>& pt_1, RVec<int>& charge_1, RVec<float>& pt_2, RVec<int>& charge_2)  {
    
    // Get indices of all possible combinations
    auto comb = Combinations(pt_1, pt_2);
    const auto numComb = comb[0].size();

    // Tag which pairs are valid based on delta r
    std::vector<int> validPair(numComb, 0);
    for (size_t i = 0; i < numComb; i++) {
      const auto i1 = comb[0][i];
      const auto i2 = comb[1][i];
      if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
            validPair[i] = 1;
          }
      }

      int idx_1 = -1;
      int idx_2 = -1;
      float bestSum = -99;

      for (size_t i = 0; i < numComb; i++) {
        if (validPair[i] == 1) {
          const int tmp1 = comb[0][i];
          const int tmp2 = comb[1][i];
		          
		      if ((charge_1[tmp1] == -1*charge_2[tmp2]) ){ 
            if (pt_1[tmp1]> 35 && pt_2[tmp2] < 26 ){ // Require electron to have pt > 35 GeV and muon to have pt < 26 GeV to avoid overlap with muel sub-category
			        if ((pt_1[tmp1] + pt_2[tmp2]) > bestSum) { // Prefer the pair with the highest pT sum
				        bestSum = (pt_1[tmp1] + pt_2[tmp2]);
				        idx_1 = tmp1;
                idx_2 = tmp2;
			        } 
            }
          }
        }
      }  
      return std::vector<int>({idx_1, idx_2});
    }

/************** Miscellaneous Helpers: Leading_FatJet_2L_tt   ***************************************************/
  
  // Returns index of the leading Fat Jet in the event
  int Leading_FatJet_2Ltt( int nJet, RVec<int>& jetId, RVec<float>& jet_eta, RVec<float>& jet_phi, 
      RVec<float>& jet_score, RVec<float>& jet_msoftdrop, float lep1_eta, float lep1_phi ,float lep2_eta ,float lep2_phi)  {
	
    float maxPNET = 0;
	  int idx = -1;
    //float PNET_score_cut = -1;

    for (int i = 0; i < int(nJet); i++) {
      // Requiring AK8 Jet isolated from selected lepton pair + PNET_score cut
      const auto deltar1 = Helper::DeltaR(jet_eta[i], lep1_eta, jet_phi[i], lep1_phi);
      const auto deltar2 = Helper::DeltaR(jet_eta[i], lep2_eta, jet_phi[i], lep2_phi);
	  	if (jetId[i] == 6 && jet_msoftdrop[i] > 20 && abs(jet_eta[i]) < 2.4 && deltar1 > 0.8 && deltar2 > 0.8){
		    if (jet_score[i] > maxPNET){ // Prefer AK8 jet with highest PNET score
				  idx = i;
			  	maxPNET = jet_score[i];
		  	}
	  	}
  	}
	  return idx;
  }

/****************** Dilepton ttH ENDS HERE *************************************************************************/
}
#endif
