#include "ROOT/RDataFrame.hxx"
#include "ROOT/RSnapshotOptions.hxx"
#include "ROOT/RVec.hxx"

#include "Math/Vector4D.h"
#include "TChain.h"
#include "TROOT.h"
#include "TStopwatch.h"

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <stdio.h>
#include <unordered_map>
#include <chrono>
#include <TFile.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH3.h>
#include <algorithm> // std::sort
#include <TH1.h>
#include <TF1.h>
#include <TVector2.h>
#include <TRandom3.h>
#include <sstream>

#include "helpers/helpers.h"
#include "helpers/selection_2L_tt.h"
#include "helpers/lumiMask.h"
#include "helpers/JetEnergyCorrections.h"
#include "correction.h"

using correction::CorrectionSet;



/*
 * Main function of the skimming step of the analysis.
 */

//template <typename T> std::string type_name();


//using namespace boost::placeholders;

//template <typename T>;

int main(int argc, char **argv) {

    // Can add command-line arguments for bells and whistles, for now ignore them 
    //(void) argc;
    //(void) argv;
    
/********************************************************************************************/ 
/************************** MANUAL ADJUSTMENT: Specify channel  *****************************/
   //std::string chan = "mu";
   //std::string chan = "el";
/********************************************************************************************/ 
/************************** MANUAL ADJUSTMENT: Specify file type  ***************************/
    //std::string type_file = "skim";
    //std::string type_file = "unskim";
/********************************************************************************************/ 
/********************************************************************************************/ 

  
    // Command line arguments
    int nArgc = 5;
    if (argc != nArgc) {
        std::cout << "[Error:] Incorrect number of arguments -- Use executable with following arguments: ./runSkim_2L_tt.sh lepton1(mu/el) lepton2(mu/el) samples(unskimmed/skims) sys(nom/JERup/JERdn/JESup/JESdn)" << std::endl;
        std::cout << "[Error:] Found " << argc << " arguments instead of " << nArgc << std::endl;
        return -1;
    }
    std::cout << ">>> ROOT Version " << gROOT->GetVersion() << std::endl;
    std::string chan = argv[1];
    std::string chan2 = argv[2];
    std::cout << ">>> Running Dilepton category ttH, with first lepton = " << chan  << " and the second lepton = " << chan2 << std::endl;
    std::string type_file = argv[3];
    std::cout << ">>> The samples used are: " << type_file << std::endl;
    std::string syst = argv[4];
    std::cout << ">>> The systematics variation is: " << syst << std::endl;

    ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel
   
    TStopwatch time;  // track how long it took our script to run
    time.Start();

   // Run on one file only, can use xroottd or /eos/ path 
   /*********************************************************************************
   // std::string line = "root://cmsxrootd.fnal.gov///store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v1_2023_10_06/SUSY_ZH_ZToAll_HToAATo4B_Pt150_M-15_TuneCP5_13TeV_madgraph_pythia8/r1/PNet_v1.root";
   //  std::string line = "/eos/cms/store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v1_2023_10_06/SUSY_ZH_ZToAll_HToAATo4B_Pt150_M-All_TuneCP5_13TeV_madgraph_pythia8/r1/PNet_v1.root";
   //  const char *filename = line.c_str(); 
   //  TChain *ch = new TChain("Events");  // NanoAOD contains a TTree called "Events"
   //  int result = ch->Add(filename);
   ********************************************************************************/

    // Specify input directory
    const char *input_dir_skim = "fileList/skims/";
    const char *input_dir_unskim = "fileList/unskimmed/";
    auto input_dir = (type_file == "skim") ? input_dir_skim : input_dir_unskim;
 
    std::string output_dir_lep = "/eos/user/h/hboucham/Haa4b/skim/2L_tt_" + chan + chan2 + "_" + syst + "/";
    const char *output_dir = output_dir_lep.c_str();
    
    // Specify test files
    //const char *test_in = "Postprocessor_SUSY_yield.txt";
    const char *test_in = "SUSY_TTH_M-all.txt";
    const char *test_out_mu = "test_mu_skim.root";
    const char *test_out_el = "test_el_skim.root";
  
   // Number of samples
  const int nSamples = 34; 
  
  // List of text files with input samples
  const char *f_input_mu[] = {test_in,
   // DY jets M10-50 amcnlo and HT bins M-50 
 "DY_10-50.txt", "DY_50_HT70-100.txt", "DY_50_HT100-200.txt", "DY_50_HT200-400.txt", "DY_50_HT400-600.txt", "DY_50_HT600-800.txt","DY_50_HT800-1200.txt", "DY_50_HT1200-2500.txt", "DY_50_HT2500-inf.txt",
    // DY Jets M1-10 and G Jets
 "DY_1-10_HT70-100.txt", "DY_1-10_HT100-200.txt", "DY_1-10_HT200-400.txt", "DY_1-10_HT400-600.txt", "DY_1-10_HT600-inf.txt",
 "GJets_HT40-100.txt",  "GJets_HT100-200.txt", "GJets_HT200-400.txt", "GJets_HT400-600.txt", "GJets_HT600-inf.txt",
  // TTbar and Single Top
  "TT_2L2Nu.txt", "TTW.txt", "ST_TW_Top_NoFullyHadronic.txt", "ST_TW_AntiTop_NoFullyHadronic.txt",
  // Diboson
  "WW.txt", "WZ.txt", "ZZ.txt",
  // Signal TTH
    "SUSY_TTH_M-15.txt", "SUSY_TTH_M-30.txt", "SUSY_TTH_M-55.txt",
  // Data
    "data_SingleMuonA.txt", "data_SingleMuonB.txt", "data_SingleMuonC.txt", "data_SingleMuonD.txt"};
  
  const char *f_input_el[] = {test_in,
   // DY jets M10-50 amcnlo and HT bins M-50 
 "DY_10-50.txt", "DY_50_HT70-100.txt", "DY_50_HT100-200.txt", "DY_50_HT200-400.txt", "DY_50_HT400-600.txt", "DY_50_HT600-800.txt","DY_50_HT800-1200.txt", "DY_50_HT1200-2500.txt", "DY_50_HT2500-inf.txt",
    // DY Jets M1-10 and G Jets
 "DY_1-10_HT70-100.txt", "DY_1-10_HT100-200.txt", "DY_1-10_HT200-400.txt", "DY_1-10_HT400-600.txt", "DY_1-10_HT600-inf.txt",
 "GJets_HT40-100.txt",  "GJets_HT100-200.txt", "GJets_HT200-400.txt", "GJets_HT400-600.txt", "GJets_HT600-inf.txt",
  // TTbar and Single Top
  "TT_2L2Nu.txt", "TTW.txt", "ST_TW_Top_NoFullyHadronic.txt", "ST_TW_AntiTop_NoFullyHadronic.txt",
  // Diboson
  "WW.txt", "WZ.txt", "ZZ.txt",
  // Signal TTH
    "SUSY_TTH_M-15.txt", "SUSY_TTH_M-30.txt", "SUSY_TTH_M-55.txt",
  // Data
    "data_EGammaA.txt", "data_EGammaB.txt", "data_EGammaC.txt", "data_EGammaD.txt"};
  
  // Cannot use conditional to define const char, so this is the best way to do it currently
  auto f_input = (chan == "mu") ? f_input_mu : f_input_el;

  // List of output samples
  const char *f_output_mu[] = {test_out_mu, 
   // DY jets M10-50 amcnlo and HT bins M-50
     "DY_10-50_skim.root", "DY_50_HT70-100_skim.root", "DY_50_HT100-200_skim.root", "DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root", "DY_50_HT600-800_skim.root","DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root",
    // DY Jets M1-10 and G Jets
 "DY_1-10_HT70-100_skim.root", "DY_1-10_HT100-200_skim.root", "DY_1-10_HT200-400_skim.root", "DY_1-10_HT400-600_skim.root", "DY_1-10_HT600-inf_skim.root", "GJets_HT40-100_skim.root",  "GJets_HT100-200_skim.root", "GJets_HT200-400_skim.root", "GJets_HT400-600_skim.root", "GJets_HT600-inf_skim.root",
  // TTbar and Single Top
  "TT_2L2Nu_skim.root", "TTW_skim.root", "ST_TW_Top_NoFullyHadronic_skim.root", "ST_TW_AntiTop_NoFullyHadronic_skim.root", 
  // Diboson and WH
  "WW_skim.root", "WZ_skim.root", "ZZ_skim.root", 
  // Signali TTH
    "SUSY_TTH_M-15_skim.root", "SUSY_TTH_M-30_skim.root", "SUSY_TTH_M-55_skim.root",
  // Data
    "data_SingleMuonA_skim.root", "data_SingleMuonB_skim.root", "data_SingleMuonC_skim.root", "data_SingleMuonD_skim.root"};
  
  const char *f_output_el[] = {test_out_el, 
   // DY jets M10-50 amcnlo and HT bins M-50
     "DY_10-50_skim.root", "DY_50_HT70-100_skim.root", "DY_50_HT100-200_skim.root", "DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root", "DY_50_HT600-800_skim.root","DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root",
    // DY Jets M1-10 and G Jets
 "DY_1-10_HT70-100_skim.root", "DY_1-10_HT100-200_skim.root", "DY_1-10_HT200-400_skim.root", "DY_1-10_HT400-600_skim.root", "DY_1-10_HT600-inf_skim.root", "GJets_HT40-100_skim.root",  "GJets_HT100-200_skim.root", "GJets_HT200-400_skim.root", "GJets_HT400-600_skim.root", "GJets_HT600-inf_skim.root",
  // TTbar and Single Top
  "TT_2L2Nu_skim.root", "TTW_skim.root", "ST_TW_Top_NoFullyHadronic_skim.root", "ST_TW_AntiTop_NoFullyHadronic_skim.root", 
  // Diboson and WH
  "WW_skim.root", "WZ_skim.root", "ZZ_skim.root", 
  // Signali TTH
    "SUSY_TTH_M-15_skim.root", "SUSY_TTH_M-30_skim.root", "SUSY_TTH_M-55_skim.root",
  // Data
    "data_EGammaA_skim.root", "data_EGammaB_skim.root", "data_EGammaC_skim.root", "data_EGammaD_skim.root"};
 
  auto f_output = (chan == "mu") ? f_output_mu : f_output_el;

  // Loading Golden JSON for lumi mask
  std::string jsonfile = "../commonFiles/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt"; 
  const auto myLumiMask = lumiMask::fromJSON(jsonfile);
  //std::cout << "Testing the JSON! Known good run/lumi returns: " << myLumiMask.accept(315257, 10) << ", and known bad run returns: " << myLumiMask.accept(315257, 90) << std::endl;
  auto goldenjson = [myLumiMask](unsigned int &run, unsigned int &luminosityBlock){return myLumiMask.accept(run, luminosityBlock);};

  // Looping over all samples (Need to be updated to be compatible with Condor)
  for (int i = 0; i < nSamples; i++){
    // Break statement for testing on 1 single sample
    //if (i > 0){ break; }
    ///if (i < 30){ continue; }
   //if (i > 53){ continue; }

    // Loading sample from sample list
    std::cout << ">>> skim_2L_tt.cxx: Starting new sample! >>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Input file
    char f_input_full[200]; // hard coded size is a bad habit, sorry!
    // Concat directory and file name
    strcpy(f_input_full, input_dir);
    strcat(f_input_full, f_input[i]);
    FILE* input_files = fopen(f_input_full, "r");  
    std::cout << ">>> skim_2L_tt.cxx: Input Text file name: " << f_input_full << std::endl;
    
    // Output file
    char f_output_full[200]; // hard coded size is a bad habit, sorry!
    // Concat directory and file name
    strcpy(f_output_full, output_dir);
    strcat(f_output_full, f_output[i]);
    const std::string output = f_output_full;
    std::cout << ">>> skim_2L_tt.cxx: Output Skim file name: " << output << std::endl;
    if (input_files == NULL) {
        printf("The file is not found. The program will now exit.");
        exit(1);
    }

    TChain *ch = new TChain("Events");  // NanoAOD contains a TTree called "Events"
    char *line = NULL;
    size_t len = 0;
    char file_name[1024] = "";
    while(getline(&line, &len, input_files) != -1) {
	    if (len > 0 && line[len-1] == '\n'){
    	  line[len-1] = 0;
	    }
	    strncpy(file_name, line + 0, len - 3);
	    file_name[strlen(file_name)-1] ='\0';
	    if (file_name[strlen(file_name)-1]=='\n'){ // Added to deal with txt files with only 1 line
		    file_name[strlen(file_name)-1]='\0';
	    } 
	    ch->Add(file_name);
	    strcpy(file_name, "");
    }        
    fclose(input_files);
    
    // Create the RDataFrame object 
    ROOT::RDataFrame df(*ch);
    std::cout << ">>> skim_2L_tt.cxx: Starting!" << std::endl;
    // Applying Lumi Mask
    ROOT::RDF::RNode df1 = df;
    if (Helper::containsSubstring(f_input[i],"data")){
      df1 = df1.Define("passesJSON", goldenjson, {"run","luminosityBlock"});
      df1 = df1.Filter("passesJSON == 1", "Data passes Golden JSON");
    }
    std::cout << ">>> Step 1 (Data): Lumi Mask Applied" << std::endl;
    // Applying different selections and defining relevant branches
    auto df2 = ApplyNoiseFilter(df1);
    std::cout << ">>> Step 2: Noise Filters Applied" << std::endl;
    auto df3 = ApplyTrigger(df2, chan);
    std::cout << ">>> Step 3: Lepton Triggers Done" << std::endl;
    auto df4 = ApplyLooseSelection(df3, chan, chan2); 
    std::cout << ">>> Step 4: Lepton Loose Selection Done" << std::endl;
    auto df5 = ApplyJEC(df4, f_input[i], syst);
    std::cout << ">>> Step 5: JEC applied" << std::endl;
    auto df6 = SelectFatJet(df5);
    std::cout << ">>> Step 6: Leading AK8-Jet Selection Done" << std::endl; 
    auto df7 = SelectLeptonPair(df6, chan, chan2);
    std::cout << ">>> Step 7: Lepton Pair Selection Done" << std::endl;
    auto df8 = Lepton_vars(df7, chan, chan2);
    std::cout << ">>> Step 8: Adding Isolation and ID variables Done" << std::endl;
    auto df9 = FatJet_vars(df8);
    std::cout << ">>> Step 9: Adding AK8 branches Done" << std::endl;
    auto df10 = JetInfo(df9);
    std::cout << ">>> Step 10: AK4 Jet Information saved" << std::endl; 
    auto df11 = SignalMC_GenMatch(df10, f_input[i]);
    std::cout << ">>> Step 11 (Signal MC): AK8-Jet Gen-match Done" << std::endl;
    auto df12 = QCD_stitch(df11, f_input[i]);
    std::cout << ">>> Step 12 (QCD MC): QCD Samples Stitching Done" << std::endl;
    auto df13 = Gen_info(df12, f_input[i]);
    std::cout << ">>> Step 13 (DY MC): Gen Z Information Saved" << std::endl;
    auto dfFinal = df13;
    
    // Save the branches below in output n-tuple
    std::vector<std::string> finalVariables = {
           // AK8 jet vars
           "m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet", "pt_fatjet", "eta_fatjet", "phi_fatjet", 
           "mH_avg", "mA_avg", "score_Haa4b_vs_QCD", "score_Haa4b_vs_QCD_v1", "score_Haa4b_vs_QCD_34",
           "score_Haa4b_vs_QCD_v2a", "score_Haa4b_vs_QCD_v2b","mA_34a", "mA_34b", "mA_34d",
           
           // Lepton branches
           "pt_l1", "eta_l1", "phi_l1", "m_l1", "pt_l2", "eta_l2", "phi_l2", "m_l2",
           "ngoodMuons", "ngoodElectrons", "trigger_IDbit",

           // Muon Iso and ID branches
           "Muon_pfRelIso04_all_l1", "Muon_miniIsoId_l1", "Muon_pfIsoId_l1", "Muon_miniPFRelIso_all_l1",
           "Muon_pfRelIso04_all_l2", "Muon_miniIsoId_l2", "Muon_pfIsoId_l2", "Muon_miniPFRelIso_all_l2",
           "Muon_pfRelIso04_all_max", "Muon_miniIsoId_min", "Muon_pfIsoId_min", "Muon_miniPFRelIso_all_max",
           "Idbit_l1", "Idbit_l2", 
           
           // Electron ID branches
           "Idbit_l1_mva", "Idbit_l2_mva", "Idbit_l1_cut", "Idbit_l2_cut", 
           
           // MET branches
           "pt_MET", "phi_MET", "pt_PUPPIMET", "phi_PUPPIMET", 
           
           // AK4 vars
           "pt_jet1", "pt_jet2", "eta_jet1", "eta_jet2", "phi_jet1", "phi_jet2",
           "pt_bjet1", "pt_bjet2", "eta_bjet1", "eta_bjet2", "phi_bjet1", "phi_bjet2",
           "pt_SSum", "pt_VSum", "nJet_add", "nJetCent_add", "nBJetM_add",
           
           // Postprocessing branches (SF, Gen info..)
           "gen_pt_Z", "gen_mass_Z", "gen_weight", "npu", "HT_LHE", 
           "GENnBhadron_fatjet", "Run_number", "LHEPdfWeight", "PSWeight",
           "TrigObj_id", "TrigObj_eta", "TrigObj_phi", "Pileup_nTrueInt",
           "event",
           
           // Btag SF branches 
           "Jet_btagSF_deepjet_M", "Jet_btagSF_deepjet_M_up", "Jet_btagSF_deepjet_M_down", 
           "Jet_btagSF_deepjet_M_up_correlated", "Jet_btagSF_deepjet_M_down_correlated",
           "Jet_btagSF_deepjet_M_up_uncorrelated", "Jet_btagSF_deepjet_M_down_uncorrelated",
           
    }; 

    // name of the event tree is set here
    dfFinal.Snapshot("event_tree", output, finalVariables);
    std::cout << ">>> skim_2L_tt.cxx: Output ntuple saved" << std::endl;
    // Print the cutflow report
    auto report = dfFinal.Report();
    report->Print();
  } 
  time.Stop();
  time.Print();
  return 0;
}
