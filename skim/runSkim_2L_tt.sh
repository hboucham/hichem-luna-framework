# Usage:
# bash runSkim_2L_tt.sh mu/el mu/el skims/unskimmed

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi


#--------------------------------------------------------
# Compile
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Compiling skim_2L_tt.cxx executable ..."
TARGET_FILES="helpers/lumiMask.cc"
COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
FLAGSS=$(correction config --cflags --ldflags --rpath)
time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o Skim_2L_tt skim_2L_tt.cxx ${TARGET_FILES} $FLAGS $FLAGSS

if [[ $? -ne 0 ]]; then
    echo ">>> Compile failed, exit"
    exit 1
fi


#--------------------------------------------------------
# Execute
#--------------------------------------------------------
./Skim_2L_tt $1 $2 $3 $4
