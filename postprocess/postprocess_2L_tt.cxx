#include "ROOT/RDataFrame.hxx"
#include "ROOT/RSnapshotOptions.hxx"
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "ROOT/RVec.hxx"
#include <ROOT/RDF/RResultMap.hxx>
#include <ROOT/RDFHelpers.hxx>   // need this for VariationsFor()

#include "TGraphAsymmErrors.h"
#include "Math/Vector4D.h"
#include "TChain.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TObject.h"

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#include <stdio.h>

#include "helpers/helperFunctions.h"
#include "helpers/sampleConfig_class.h"
#include "helpers/zPtReweighing-experimental.h"
#include "helpers/Total_Weight.h" 
#include "helpers/HEM.h" 
#include "helpers/Gen_Weight.h" 
//#include "helpers/pileupReweighing.h"
#include "helpers/PU_reweight.h"
#include "helpers/VH_reweighting.h"
#include "helpers/SimpleSystematics.h"
#include "helpers/LeptonSF_IDISO.h"
#include "helpers/LeptonSF_trig.h"


int main(int argc, char **argv) {

  // Can add command-line arguments for bells and whistles, for now ignore them 
  //(void) argc;
  //(void) argv;
    
  // Command line arguments
  int nArgc = 4;
  if (argc != nArgc) {
      std::cout << "[Error:] Incorrect number of arguments -- Use executable with following arguments: ./runPostprocess_2L_tt.sh lepton1(mu/el) lepton2(mu/el)" << std::endl;
      std::cout << "[Error:] Found " << argc << " arguments instead of " << nArgc << std::endl;
      return -1;
  }
  std::cout << ">>> ROOT Version " << gROOT->GetVersion() << std::endl;
  std::string chan = argv[1];
  std::string chan2 = argv[2];
  std::cout << ">>> Running Dilepton category ttH, with first lepton = " << chan  << " and the second lepton = " << chan2 << std::endl;
  std::string syst = argv[3];
  std::cout << ">>> Running dilepton category ttH, with systematics = " << syst << std::endl;
  
  ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel
   
  TStopwatch time;  // track how long it took our script to run
  time.Start();


  // Specify input directory
  std::string input_dir_lep = "/eos/user/h/hboucham/Haa4b/skim/2L_tt_" + chan + chan2 + "_" + syst + "/";
  const char *input_dir = input_dir_lep.c_str();
  // Specify output directory 
  std::string output_dir_lep = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_tt_" + chan + chan2 + "_" + syst + "/";
  const char *output_dir = output_dir_lep.c_str();
  
  // Number of samples
  const int nSamples = 34; 
  
  // List of text files with input samples
  const char *f_input_mu[] = {"test_mu_skim.root",
    // DY jets M10-50 amcnlo and HT bins M-50
    "DY_10-50_skim.root", "DY_50_HT70-100_skim.root", "DY_50_HT100-200_skim.root", "DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root", 
    "DY_50_HT600-800_skim.root","DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root",
    // DY Jets M1-10 and G Jets
    "DY_1-10_HT70-100_skim.root", "DY_1-10_HT100-200_skim.root", "DY_1-10_HT200-400_skim.root", "DY_1-10_HT400-600_skim.root", 
    "DY_1-10_HT600-inf_skim.root", "GJets_HT40-100_skim.root",  "GJets_HT100-200_skim.root", "GJets_HT200-400_skim.root",
    "GJets_HT400-600_skim.root", "GJets_HT600-inf_skim.root",
    // TTbar and Single Top
    "TT_2L2Nu_skim.root", "TTW_skim.root", "ST_TW_Top_NoFullyHadronic_skim.root", "ST_TW_AntiTop_NoFullyHadronic_skim.root", 
    // Diboson 
    "WW_skim.root", "WZ_skim.root", "ZZ_skim.root", 
    // Signal TTH
    "SUSY_TTH_M-15_skim.root", "SUSY_TTH_M-30_skim.root", "SUSY_TTH_M-55_skim.root",
    // Data Single Muon 2018
    "data_SingleMuonA_skim.root", "data_SingleMuonB_skim.root", "data_SingleMuonC_skim.root", "data_SingleMuonD_skim.root"};

  const char *f_input_el[] = {"test_el_skim.root",
    // DY jets M10-50 amcnlo and HT bins M-50
    "DY_10-50_skim.root", "DY_50_HT70-100_skim.root", "DY_50_HT100-200_skim.root", "DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root",
    "DY_50_HT600-800_skim.root","DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root",
    // DY Jets M1-10 and G Jets
    "DY_1-10_HT70-100_skim.root", "DY_1-10_HT100-200_skim.root", "DY_1-10_HT200-400_skim.root", "DY_1-10_HT400-600_skim.root",
    "DY_1-10_HT600-inf_skim.root", "GJets_HT40-100_skim.root",  "GJets_HT100-200_skim.root", "GJets_HT200-400_skim.root", 
    "GJets_HT400-600_skim.root", "GJets_HT600-inf_skim.root",
    // TTbar and Single Top
    "TT_2L2Nu_skim.root", "TTW_skim.root", "ST_TW_Top_NoFullyHadronic_skim.root", "ST_TW_AntiTop_NoFullyHadronic_skim.root", 
    // Diboson 
    "WW_skim.root", "WZ_skim.root", "ZZ_skim.root", 
    // Signal TTH
    "SUSY_TTH_M-15_skim.root", "SUSY_TTH_M-30_skim.root", "SUSY_TTH_M-55_skim.root",
    // Data EGamma 2018
    "data_EGammaA_skim.root", "data_EGammaB_skim.root", "data_EGammaC_skim.root", "data_EGammaD_skim.root"};

  // Cannot use conditional to define const char, so this is the best way to do it currently
  auto f_input = (chan == "mu") ? f_input_mu : f_input_el;

  // List of output samples
  const char *f_output_mu[] = {"test_mu.root",
    // DY jets M10-50 amcnlo and HT bins M-50
    "DY_10-50.root", "DY_50_HT70-100.root", "DY_50_HT100-200.root", "DY_50_HT200-400.root", "DY_50_HT400-600.root", 
    "DY_50_HT600-800.root","DY_50_HT800-1200.root", "DY_50_HT1200-2500.root", "DY_50_HT2500-inf.root",
    // DY Jets M1-10 and G Jets
    "DY_1-10_HT70-100.root", "DY_1-10_HT100-200.root", "DY_1-10_HT200-400.root", "DY_1-10_HT400-600.root", 
    "DY_1-10_HT600-inf.root", "GJets_HT40-100.root",  "GJets_HT100-200.root", "GJets_HT200-400.root",
    "GJets_HT400-600.root", "GJets_HT600-inf.root",
    // TTbar and Single Top
    "TT_2L2Nu.root", "TTW.root", "ST_TW_Top_NoFullyHadronic.root", "ST_TW_AntiTop_NoFullyHadronic.root", 
    // Diboson 
    "WW.root", "WZ.root", "ZZ.root", 
    // Signal TTH
    "SUSY_TTH_M-15.root", "SUSY_TTH_M-30.root", "SUSY_TTH_M-55.root",
    // Data Single Muon 2018
    "data_SingleMuonA.root", "data_SingleMuonB.root", "data_SingleMuonC.root", "data_SingleMuonD.root"};
    
  const char *f_output_el[] = {"test_el.root",
    // DY jets M10-50 amcnlo and HT bins M-50
    "DY_10-50.root", "DY_50_HT70-100.root", "DY_50_HT100-200.root", "DY_50_HT200-400.root", "DY_50_HT400-600.root", 
    "DY_50_HT600-800.root","DY_50_HT800-1200.root", "DY_50_HT1200-2500.root", "DY_50_HT2500-inf.root",
    // DY Jets M1-10 and G Jets
    "DY_1-10_HT70-100.root", "DY_1-10_HT100-200.root", "DY_1-10_HT200-400.root", "DY_1-10_HT400-600.root", 
    "DY_1-10_HT600-inf.root", "GJets_HT40-100.root",  "GJets_HT100-200.root", "GJets_HT200-400.root",
    "GJets_HT400-600.root", "GJets_HT600-inf.root",
    // TTbar and Single Top
    "TT_2L2Nu.root", "TTW.root", "ST_TW_Top_NoFullyHadronic.root", "ST_TW_AntiTop_NoFullyHadronic.root", 
    // Diboson 
    "WW.root", "WZ.root", "ZZ.root", 
    // Signal TTH
    "SUSY_TTH_M-15.root", "SUSY_TTH_M-30.root", "SUSY_TTH_M-55.root",
    // Data EGamma 2018
    "data_EGammaA.root", "data_EGammaB.root", "data_EGammaC.root", "data_EGammaD.root"};
  
  auto f_output = (chan == "mu") ? f_output_mu : f_output_el;

  // HTT RooWorkspace
  //TString wsFileName = "/afs/cern.ch/user/h/hboucham/work/H4B/CMSSW_13_1_0_pre4/src/demo-luna-framework/commonFiles/htt_scalefactors_legacy_2018.root";
  TString wsFileName = "../commonFiles/htt_scalefactors_legacy_2018.root";
  TFile fwmc(wsFileName);
  RooWorkspace *wmc;
  fwmc.GetObject("w", wmc);
  if (wmc == 0) {
    std::cout << "[ERROR:] RooWorkspace not retrievable from " << wsFileName << "!" << std::endl;
    return 0;
  }

// Looping over all samples
  for (int i = 0; i < nSamples; i++){
   //if (i > 0){ break;}
      
    
    std::cout << ">>> postprocess_2L_tt.cxx: Starting new sample! >>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
    // Input file
    char f_input_full[200]; // hard coded size is a bad habit, sorry!
    // Concat directory and file name
    strcpy(f_input_full, input_dir);
    strcat(f_input_full, f_input[i]);
    FILE* input_files = fopen(f_input_full, "r");  

    // Flag in case file not found
    if (input_files == NULL) { 
      printf("******** The file is not found. The program will now exit.");
      exit(1);
    }
    std::cout << ">>> postprocess_2L_tt.cxx: Input file name: " << f_input_full << std::endl;
     
    // Flag in case file is empty
    TFile *rfile = TFile::Open(f_input_full);
    //rfile->GetListOfKeys()->Print();  // Print content of .ls in file
    if (rfile->GetListOfKeys()->Contains("event_tree") == false){ 
      printf("******** This file has 0 events. Going to the next file");
      continue;
    }

    // Output file
    char f_output_full[200]; // hard coded size is a bad habit, sorry!
    // Concat directory and file name
    strcpy(f_output_full, output_dir);
    strcat(f_output_full, f_output[i]);
    const std::string output = f_output_full;
    std::cout << ">>> postprocess_2L_tt.cxx: Output Skim file name: " << output << std::endl;
    
    TChain *ch = new TChain("event_tree");  // NanoAOD contains a TTree called "Events"
    ch->Add(f_input_full);
 
    // VH signal 2D reweighting
    TString ZH_weight_file = "../commonFiles/ZH_weights.root";
    TString WH_weight_file = "../commonFiles/WH_weights.root";
    TFile f_ZH2D(ZH_weight_file);
    TFile f_WH2D(WH_weight_file);
    const TH2D *ZHweight_nominal;
    const TH2D *WHweight_nominal;
    f_ZH2D.GetObject("ZH_weights_histo", ZHweight_nominal);
    f_WH2D.GetObject("WH_weights_histo", WHweight_nominal);
    if (ZHweight_nominal == 0) {
        std::cout << "[ERROR:] ZH Weights 2D histo not retrievable from " << ZH_weight_file << "!" << std::endl;
        return 0;
    }
    if (WHweight_nominal == 0) {
        std::cout << "[ERROR:] WH Weights 2D histo not retrievable from " << WH_weight_file << "!" << std::endl;
        return 0;
    }
    
    // Create the RDataFrame object 
    ROOT::RDataFrame df(*ch);
    std::cout << ">>> Postprocess: Starting!" << std::endl; 
    auto df1 = df.Define("wmcPtr", [=]() { return wmc; }) // capture pointer to RooWorkSpace
                 .Define("ZHweight_nominal_ptr", [=]() { return ZHweight_nominal; })
                 .Define("WHweight_nominal_ptr", [=]() { return WHweight_nominal; });
    std::cout << ">>> Postprocess 1: initializing Pointers " << std::endl;
    // Calculate SF here
    auto df2 = GetZPtReweighing_experimental(df1, f_input[i]);
    std::cout << ">>> Postprocess 2 (DY MC): Get Z pt reweighting SF " << std::endl;
    auto df3 = ApplyPU_Reweighting(df2, f_input[i]);
    std::cout << ">>> Postprocess 3: (MC) Applying PU reweighting Done" << std::endl;
    auto df4 = GetVHWeight(df3, f_input[i]);
    std::cout << ">>> Postprocess 4 (ZH/WH Signal): Getting VH 2D reweighting SF " << std::endl;
    auto df5 = ApplyHEM(df4, f_input[i]);
    std::cout << ">>> Postprocess 5: Apply HEM Veto (Data) and calculating HEM weight (MC)" << std::endl;
    auto df6 = ApplyGenWeight(df5, f_input[i]);
    std::cout << ">>> Postprocess 6: Getting Gen weight sign" << std::endl;
    auto df7 = ApplyLeptonSF_IDISO_2Ltt(df6, chan, chan2, f_input[i]);
    std::cout << ">>> Postprocess 7: Applying Lepton ID/ISO SF Done" << std::endl;
    auto df8 = ApplyLeptonSF_Trig_2Ltt(df7, chan, chan2, f_input[i]);
    std::cout << ">>> Postprocess 8: Applying Lepton Trigger SF Done" << std::endl;
    auto df9 = ApplySys(df8, f_input[i]);
    std::cout << ">>> Postprocess 9: Calculating other systematics" << std::endl;
    auto dfFinal_almost = Total_Weight(df9);
    std::cout << ">>> Postprocess Final-1: Compute Total Weight Branch" << std::endl;
    auto dfFinal = dfFinal_almost;
    
    
    // Save the branches below in output n-tuple
    std::vector<std::string> finalVariables = {
           // AK8 jet vars
           "m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet", "pt_fatjet", "eta_fatjet", "phi_fatjet", 
           "mH_avg", "mA_avg", "score_Haa4b_vs_QCD", "score_Haa4b_vs_QCD_v1", "score_Haa4b_vs_QCD_34",
           "score_Haa4b_vs_QCD_v2a", "score_Haa4b_vs_QCD_v2b","mA_34a", "mA_34b", "mA_34d",
           
           // Lepton branches
           "pt_l1", "eta_l1", "phi_l1", "m_l1", "pt_l2", "eta_l2", "phi_l2", "m_l2",
           "ngoodMuons", "ngoodElectrons", "trigger_IDbit",

           // Muon Iso and ID branches
           "Muon_pfRelIso04_all_l1", "Muon_miniIsoId_l1", "Muon_pfIsoId_l1", "Muon_miniPFRelIso_all_l1",
           "Muon_pfRelIso04_all_l2", "Muon_miniIsoId_l2", "Muon_pfIsoId_l2", "Muon_miniPFRelIso_all_l2",
           "Muon_pfRelIso04_all_max", "Muon_miniIsoId_min", "Muon_pfIsoId_min", "Muon_miniPFRelIso_all_max",
           "Idbit_l1", "Idbit_l2", 
           
           // Electron ID branches
           "Idbit_l1_mva", "Idbit_l2_mva", "Idbit_l1_cut", "Idbit_l2_cut", 
           
           // MET branches
           "pt_MET", "phi_MET", "pt_PUPPIMET", "phi_PUPPIMET", 
           
           // AK4 vars
           "pt_jet1", "pt_jet2", "eta_jet1", "eta_jet2", "phi_jet1", "phi_jet2",
           "pt_bjet1", "pt_bjet2", "eta_bjet1", "eta_bjet2", "phi_bjet1", "phi_bjet2",
           "pt_SSum", "pt_VSum", "nJet_add", "nJetCent_add", "nBJetM_add",
           
           // Postprocessing branches (SF, Gen info..)
           "gen_pt_Z", "gen_mass_Z", "gen_weight", "npu", "HT_LHE", 
           "GENnBhadron_fatjet", "Run_number", "event",
           
           // weights
           "weight_zPt_nominal", "weight_zPt_Up", "weight_zPt_Down",
           "weight_pu_nominal", "weight_pu_Up", "weight_pu_Down",
           "weight_VH_nominal", "weight_VH_Up", "weight_VH_Down",
           "weight_PDF_nominal", "weight_PDF_Up", "weight_PDF_Down",
           "weight_ISR_nominal", "weight_ISR_Up", "weight_ISR_Down",
           "weight_FSR_nominal", "weight_FSR_Up", "weight_FSR_Down",
           "weight_HEM_nominal", "weight_GEN_nominal",
           "weight_tot", "weight_tot_nominal", "weight_tot_Up", "weight_tot_Down",
           "weight_LeptonIDISO_nominal", "weight_LeptonIDISO_Up", "weight_LeptonIDISO_Down", 
           "weight_trigger_nominal", "weight_trigger_Up", "weight_trigger_Down",
    }; 

    // name of the event tree is set here
    dfFinal.Snapshot("event_tree", output, finalVariables);
    std::cout << ">>> postprocess_2L_tt.cxx: Output ntuple saved" << std::endl;
    // Print the cutflow report
    auto report = dfFinal.Report();
    //fwmc.Close();
    report->Print();
 }
  fwmc.Close();
  time.Stop();
  time.Print();
  return 0;
}
