#ifndef LEPTONSF_IDISO_H
#define LEPTONSF_IDISO_H

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>
#include "helperFunctions.h"
#include "correction.h"

using correction::CorrectionSet;
using namespace ROOT::VecOps;

  // Load electron corrections
  //auto muonIDISOcorrset = CorrectionSet::from_file("../commonFiles/MuonSF/2018UL/MuonIDISO_Z_v2_overflow.json");
  auto muonIDISOcorrset = CorrectionSet::from_file("../commonFiles/MuonSF/2018UL/MuonSF_IDISO.json");
  auto electronIDISOcorrset = CorrectionSet::from_file("../commonFiles/ElectronSF/2018UL/ElectronSF.json");
  //auto muonIDcorrset = CorrectionSet::from_file("../commonFiles/MuonSF/2018UL/MuonIDv1.json");
  auto muonIDcorr = muonIDISOcorrset->at("NUM_MediumPromptID_DEN_TrackerMuons"); 
  auto muonISOcorr = muonIDISOcorrset->at("NUM_TightMiniIso_DEN_MediumPromptID"); 
  auto electronIDISOcorr = electronIDISOcorrset->at("EGamma_SF2D"); 


/**********************************************************/
auto ElectronIDISO_SF (const float &pt, const float &eta){
    RVec<double> SF_val;
    SF_val = {electronIDISOcorr->evaluate({"2018UL", "sf", "looseID", eta, pt}), 
  	          (electronIDISOcorr->evaluate({"2018UL", "sf", "looseID", eta, pt}) + electronIDISOcorr->evaluate({"2018UL", "sf_err", "looseID", eta, pt})),
  	          (electronIDISOcorr->evaluate({"2018UL", "sf", "looseID", eta, pt}) - electronIDISOcorr->evaluate({"2018UL", "sf_err", "looseID", eta, pt}))};
    return SF_val;
  }; 
/**********************************************************/
auto MuonIDISO_SF (const float &pt, const float &eta){
    RVec<double> SF_val;
    SF_val = {(muonIDcorr->evaluate({abs(eta), pt,"nominal"}))*(muonISOcorr->evaluate({abs(eta), pt,"nominal"})), 
	          (muonIDcorr->evaluate({abs(eta), pt,"systup"}))*(muonISOcorr->evaluate({abs(eta), pt,"systup"})), 
	          (muonIDcorr->evaluate({abs(eta), pt,"systdown"}))*(muonISOcorr->evaluate({abs(eta), pt,"systdown"}))};
    return SF_val;
  }; 
/**********************************************************/
auto MuonISO_SF (const float &pt, const float &eta){
    RVec<double> SF_val;
    SF_val = {muonISOcorr->evaluate({abs(eta), pt,"nominal"}), 
  	          muonISOcorr->evaluate({abs(eta), pt,"systup"}), 
	            muonISOcorr->evaluate({abs(eta), pt,"systdown"})};
    return SF_val;
  }; 
/**********************************************************/
auto MuonID_SF (const float &pt, const float &eta){
    RVec<double> SF_val;
    SF_val = {muonIDcorr->evaluate({abs(eta), pt,"nominal"}), 
  	          muonIDcorr->evaluate({abs(eta), pt,"systup"}), 
	            muonIDcorr->evaluate({abs(eta), pt,"systdown"})};
    return SF_val;
  }; 

/**********************************************************/

template <typename T>
auto ApplyLeptonSF_IDISO_2LZ(T &df, std::string channel, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("MuonSF_ID_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_ID_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_ID_Down", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_Down", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_nominal", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_Up", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_Down", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_nominal", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_Up", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_Down", [=]() { return (double) 1.0; });
  }else{
    if (channel == "mu"){
      auto df2 = df.Define("MuonID_SF_l1", MuonID_SF, {"pt_l1", "eta_l1"});
      return df2.Define("MuonISO_SF_l1", MuonISO_SF, {"pt_l1", "eta_l1"})
                .Define("MuonID_SF_l2", MuonID_SF, {"pt_l2", "eta_l2"})
                .Define("MuonISO_SF_l2", MuonISO_SF, {"pt_l2", "eta_l2"})
                .Define("MuonSF_ID_nominal", "MuonID_SF_l1[0]*MuonID_SF_l2[0]")
                .Define("MuonSF_ID_Up", "MuonID_SF_l1[1]*MuonID_SF_l2[1]")
                .Define("MuonSF_ID_Down", "MuonID_SF_l1[2]*MuonID_SF_l2[2]")
                .Define("MuonSF_ISO_nominal", "MuonISO_SF_l1[0]*MuonISO_SF_l2[0]")
                .Define("MuonSF_ISO_Up", "MuonISO_SF_l1[1]*MuonISO_SF_l2[1]")
                .Define("MuonSF_ISO_Down", "MuonISO_SF_l1[2]*MuonISO_SF_l2[2]")
                .Define("ElectronSF_IDISO_nominal", [=]() { return (double) 1.0; })
                .Define("ElectronSF_IDISO_Up", [=]() { return (double) 1.0; })
                .Define("ElectronSF_IDISO_Down", [=]() { return (double) 1.0; })
                .Define("weight_LeptonIDISO_nominal", "MuonSF_ID_nominal*MuonSF_ISO_nominal")
                .Define("weight_LeptonIDISO_Up", "MuonSF_ID_Up*MuonSF_ISO_Up")
                .Define("weight_LeptonIDISO_Down", "MuonSF_ID_Down*MuonSF_ISO_Down");
    }else{
      auto df2 = df.Define("ElectronIDISO_SF_l1", ElectronIDISO_SF, {"pt_l1", "eta_l1"});
      return df2.Define("ElectronIDISO_SF_l2", ElectronIDISO_SF, {"pt_l2", "eta_l2"})
               .Define("ElectronSF_IDISO_nominal", "ElectronIDISO_SF_l1[0]*ElectronIDISO_SF_l2[0]")
               .Define("ElectronSF_IDISO_Up", "ElectronIDISO_SF_l1[1]*ElectronIDISO_SF_l2[1]")
               .Define("ElectronSF_IDISO_Down", "ElectronIDISO_SF_l1[2]*ElectronIDISO_SF_l2[2]")
               .Define("MuonSF_ID_nominal", [=]() { return (double) 1.0; })
               .Define("MuonSF_ID_Up", [=]() { return (double) 1.0; })
               .Define("MuonSF_ID_Down", [=]() { return (double) 1.0; })
               .Define("MuonSF_ISO_nominal", [=]() { return (double) 1.0; })
               .Define("MuonSF_ISO_Up", [=]() { return (double) 1.0; })
               .Define("MuonSF_ISO_Down", [=]() { return (double) 1.0; })
               .Define("weight_LeptonIDISO_nominal", "ElectronSF_IDISO_nominal")
               .Define("weight_LeptonIDISO_Up", "ElectronSF_IDISO_Up")
               .Define("weight_LeptonIDISO_Down", "ElectronSF_IDISO_Down");
    }
  }
}
   
/**********************************************************/

template <typename T>
auto ApplyLeptonSF_IDISO_1L(T &df, std::string channel, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("MuonSF_ID_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_ID_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_ID_Down", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_Down", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_nominal", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_Up", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_Down", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_nominal", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_Up", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_Down", [=]() { return (double) 1.0; });
  }else{
    if (channel == "mu"){
      auto df2 = df.Define("MuonID_SF_lep", MuonID_SF, {"pt_lep", "eta_lep"});
      return df2.Define("MuonISO_SF_lep", MuonISO_SF, {"pt_lep", "eta_lep"})
                .Define("MuonSF_ID_nominal", "MuonID_SF_lep[0]")
                .Define("MuonSF_ID_Up", "MuonID_SF_lep[1]")
                .Define("MuonSF_ID_Down", "MuonID_SF_lep[2]")
                .Define("MuonSF_ISO_nominal", "MuonISO_SF_lep[0]")
                .Define("MuonSF_ISO_Up", "MuonISO_SF_lep[1]")
                .Define("MuonSF_ISO_Down", "MuonISO_SF_lep[2]")
                .Define("ElectronSF_IDISO_nominal", [=]() { return (double) 1.0; })
                .Define("ElectronSF_IDISO_Up", [=]() { return (double) 1.0; })
                .Define("ElectronSF_IDISO_Down", [=]() { return (double) 1.0; })
                .Define("weight_LeptonIDISO_nominal", "MuonSF_ID_nominal*MuonSF_ISO_nominal")
                .Define("weight_LeptonIDISO_Up", "MuonSF_ID_Up*MuonSF_ISO_Up")
                .Define("weight_LeptonIDISO_Down", "MuonSF_ID_Down*MuonSF_ISO_Down");
    }else{
      auto df2 = df.Define("ElectronIDISO_SF_lep", ElectronIDISO_SF, {"pt_lep", "eta_lep"});
      return df2.Define("ElectronSF_IDISO_nominal", "ElectronIDISO_SF_lep[0]")
               .Define("ElectronSF_IDISO_Up", "ElectronIDISO_SF_lep[1]")
               .Define("ElectronSF_IDISO_Down", "ElectronIDISO_SF_lep[2]")
               .Define("MuonSF_ID_nominal", [=]() { return (double) 1.0; })
               .Define("MuonSF_ID_Up", [=]() { return (double) 1.0; })
               .Define("MuonSF_ID_Down", [=]() { return (double) 1.0; })
               .Define("MuonSF_ISO_nominal", [=]() { return (double) 1.0; })
               .Define("MuonSF_ISO_Up", [=]() { return (double) 1.0; })
               .Define("MuonSF_ISO_Down", [=]() { return (double) 1.0; })
               .Define("weight_LeptonIDISO_nominal", "ElectronSF_IDISO_nominal")
               .Define("weight_LeptonIDISO_Up", "ElectronSF_IDISO_Up")
               .Define("weight_LeptonIDISO_Down", "ElectronSF_IDISO_Down");
    }
  }
}

/**********************************************************/

template <typename T>
auto ApplyLeptonSF_IDISO_2Ltt(T &df, std::string chan1, std::string chan2, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("MuonSF_ID_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_ID_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_ID_Down", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_ISO_Down", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_nominal", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_Up", [=]() { return (double) 1.0; })
             .Define("ElectronSF_IDISO_Down", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_nominal", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_Up", [=]() { return (double) 1.0; })
             .Define("weight_LeptonIDISO_Down", [=]() { return (double) 1.0; });
  }else{
    if (chan1 == "mu" && chan2 == "mu"){
      auto df2 = df.Define("MuonID_SF_l1", MuonID_SF, {"pt_l1", "eta_l1"});
      return df2.Define("MuonISO_SF_l1", MuonISO_SF, {"pt_l1", "eta_l1"})
                .Define("MuonID_SF_l2", MuonID_SF, {"pt_l2", "eta_l2"})
                .Define("MuonISO_SF_l2", MuonISO_SF, {"pt_l2", "eta_l2"})
                .Define("MuonSF_ID_nominal", "MuonID_SF_l1[0]*MuonID_SF_l2[0]")
                .Define("MuonSF_ID_Up", "MuonID_SF_l1[1]*MuonID_SF_l2[1]")
                .Define("MuonSF_ID_Down", "MuonID_SF_l1[2]*MuonID_SF_l2[2]")
                .Define("MuonSF_ISO_nominal", "MuonISO_SF_l1[0]*MuonISO_SF_l2[0]")
                .Define("MuonSF_ISO_Up", "MuonISO_SF_l1[1]*MuonISO_SF_l2[1]")
                .Define("MuonSF_ISO_Down", "MuonISO_SF_l1[2]*MuonISO_SF_l2[2]")
                .Define("ElectronSF_IDISO_nominal", [=]() { return (double) 1.0; })
                .Define("ElectronSF_IDISO_Up", [=]() { return (double) 1.0; })
                .Define("ElectronSF_IDISO_Down", [=]() { return (double) 1.0; })
                .Define("weight_LeptonIDISO_nominal", "MuonSF_ID_nominal*MuonSF_ISO_nominal")
                .Define("weight_LeptonIDISO_Up", "MuonSF_ID_Up*MuonSF_ISO_Up")
                .Define("weight_LeptonIDISO_Down", "MuonSF_ID_Down*MuonSF_ISO_Down");
    }else if (chan1 == "el" && chan2 == "el"){
      auto df2 = df.Define("ElectronIDISO_SF_l1", ElectronIDISO_SF, {"pt_l1", "eta_l1"});
      return df2.Define("ElectronIDISO_SF_l2", ElectronIDISO_SF, {"pt_l2", "eta_l2"})
                .Define("ElectronSF_IDISO_nominal", "ElectronIDISO_SF_l1[0]*ElectronIDISO_SF_l2[0]")
                .Define("ElectronSF_IDISO_Up", "ElectronIDISO_SF_l1[1]*ElectronIDISO_SF_l2[1]")
                .Define("ElectronSF_IDISO_Down", "ElectronIDISO_SF_l1[2]*ElectronIDISO_SF_l2[2]")
                .Define("MuonSF_ID_nominal", [=]() { return (double) 1.0; })
                .Define("MuonSF_ID_Up", [=]() { return (double) 1.0; })
                .Define("MuonSF_ID_Down", [=]() { return (double) 1.0; })
                .Define("MuonSF_ISO_nominal", [=]() { return (double) 1.0; })
                .Define("MuonSF_ISO_Up", [=]() { return (double) 1.0; })
                .Define("MuonSF_ISO_Down", [=]() { return (double) 1.0; })
                .Define("weight_LeptonIDISO_nominal", "ElectronSF_IDISO_nominal")
                .Define("weight_LeptonIDISO_Up", "ElectronSF_IDISO_Up")
                .Define("weight_LeptonIDISO_Down", "ElectronSF_IDISO_Down");
    }else if (chan1 == "mu" && chan2 == "el"){
      auto df2 = df.Define("MuonID_SF_l1", MuonID_SF, {"pt_l1", "eta_l1"});
      return df2.Define("MuonISO_SF_l1", MuonISO_SF, {"pt_l1", "eta_l1"})
                .Define("ElectronIDISO_SF_l2", ElectronIDISO_SF, {"pt_l2", "eta_l2"})
                .Define("MuonSF_ID_nominal", "MuonID_SF_l1[0]")
                .Define("MuonSF_ID_Up", "MuonID_SF_l1[1]")
                .Define("MuonSF_ID_Down", "MuonID_SF_l1[2]")
                .Define("MuonSF_ISO_nominal", "MuonISO_SF_l1[0]")
                .Define("MuonSF_ISO_Up", "MuonISO_SF_l1[1]")
                .Define("MuonSF_ISO_Down", "MuonISO_SF_l1[2]")
                .Define("ElectronSF_IDISO_nominal", "ElectronIDISO_SF_l2[0]")
                .Define("ElectronSF_IDISO_Up", "ElectronIDISO_SF_l2[1]")
                .Define("ElectronSF_IDISO_Down", "ElectronIDISO_SF_l2[2]")
                .Define("weight_LeptonIDISO_nominal", "MuonSF_ID_nominal*ElectronSF_IDISO_nominal")
                .Define("weight_LeptonIDISO_Up", "MuonSF_ID_Up*ElectronSF_IDISO_Up")
                .Define("weight_LeptonIDISO_Down", "MuonSF_ID_Down*ElectronSF_IDISO_Down");
    }else{ //(chan1 == "el" && chan2 == "mu")
      auto df2 = df.Define("ElectronIDISO_SF_l1", ElectronIDISO_SF, {"pt_l1", "eta_l1"});
      return df2.Define("MuonID_SF_l2", MuonID_SF, {"pt_l2", "eta_l2"})
                .Define("MuonISO_SF_l2", MuonISO_SF, {"pt_l2", "eta_l2"})
                .Define("ElectronSF_IDISO_nominal", "ElectronIDISO_SF_l1[0]")
                .Define("ElectronSF_IDISO_Up", "ElectronIDISO_SF_l1[1]")
                .Define("ElectronSF_IDISO_Down", "ElectronIDISO_SF_l1[2]")
                .Define("MuonSF_ID_nominal", "MuonID_SF_l2[0]")
                .Define("MuonSF_ID_Up", "MuonID_SF_l2[1]")
                .Define("MuonSF_ID_Down", "MuonID_SF_l2[2]")
                .Define("MuonSF_ISO_nominal", "MuonISO_SF_l2[0]")
                .Define("MuonSF_ISO_Up", "MuonISO_SF_l2[1]")
                .Define("MuonSF_ISO_Down", "MuonISO_SF_l2[2]")
                .Define("weight_LeptonIDISO_nominal", "ElectronSF_IDISO_nominal*MuonSF_ID_nominal")
                .Define("weight_LeptonIDISO_Up", "ElectronSF_IDISO_Up*MuonSF_ID_Up")
                .Define("weight_LeptonIDISO_Down", "ElectronSF_IDISO_Down*MuonSF_ID_Down");

    }
  }
}
   
/**********************************************************/

#endif
