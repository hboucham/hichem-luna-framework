// Simple Systematics 

#ifndef SIMPLESYSTEMATICS_H_INCL
#define SIMPLESYSTEMATICS_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

template <typename T>
auto ApplySys(T &df, std::string sample) {
  if (Helper::containsSubstring(sample, "data")){
    // PDF uncertainty
    return df.Define("weight_PDF_nominal", [=]() { return (float) 1.0; })
             .Define("weight_PDF_Up", [=]() { return (float) 1.0; })
             .Define("weight_PDF_Down", [=]() { return (float) 1.0; })
    // ISR uncertainty
             .Define("weight_ISR_nominal", [=]() { return (float) 1.0; })
             .Define("weight_ISR_Up", [=]() { return (float) 1.0; })
             .Define("weight_ISR_Down", [=]() { return (float) 1.0; })
    // FSR uncertainty
             .Define("weight_FSR_nominal", [=]() { return (float) 1.0; })
             .Define("weight_FSR_Up", [=]() { return (float) 1.0; })
             .Define("weight_FSR_Down", [=]() { return (float) 1.0; });
  }else{
    // PDF uncertainty
//    return df.Define("PDF_STD", "Std(LHEPdfWeight)")
    return df.Define("PDF_STD", Helper::CalculateStdDev, {"LHEPdfWeight"} )
             .Define("weight_PDF_nominal", [=]() { return (float) 1.0; })
             .Define("weight_PDF_Up", "weight_PDF_nominal + PDF_STD")
             .Define("weight_PDF_Down", "weight_PDF_nominal - PDF_STD")
    // ISR uncertainty
             .Define("weight_ISR_nominal", [=]() { return (float) 1.0; })
             .Define("weight_ISR_Up", "PSWeight[0]")
             .Define("weight_ISR_Down", "PSWeight[2]")
    // FSR uncertainty
             .Define("weight_FSR_nominal", [=]() { return (float) 1.0; })
             .Define("weight_FSR_Up", "PSWeight[1]")
             .Define("weight_FSR_Down", "PSWeight[3]");
  }       
}

/*******************************************************************/


#endif
