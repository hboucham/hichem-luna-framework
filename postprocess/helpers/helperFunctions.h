// Functions in the Helper namespace.
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>


#ifndef HELPERFUNCTIONS_H_INCL
#define HELPERFUNCTIONS_H_INCL

/***************************************************************************/

using namespace ROOT::VecOps;

namespace Helper {

  const int mt = 0;
  const int et = 1;
  const int em  = 2;
  const int none = 3;

  const int pdgId_Z = 23;
  const int pdgId_W = 24;
  const int pdgId_H = 25;

  // For symmetric case h->a1a1 and asymmetric case h->a1a2, pdgId_a = 36 is the "h"
  const int pdgId_a = 36;

  const int pdgId_a1 = 25;
  const int pdgId_a2 = 35; // h->a2a1 cascade where a2->a1a1

  const float pi0mass = 0.1349768; // 134.97 MeV according to PDG ID. In NanoAOD it is 0.1395263 for tau dm == 0

    // Returns whether a substring is in a string.
    bool containsSubstring(std::string string, std::string substring);

    // Returns whether any substrings in the provided vector, are in the string.
    bool containsAnyOfTheseSubstrings(std::string str, std::vector<std::string> vSubstring);

  /***************************************************************************/

  float compute_deltaR(float eta_1, float eta_2, float phi_1, float phi_2);
  float compute_deltaPhi(float v1, float v2, const float c = M_PI);
  float getValueByChannelWithDefault(int channel, float val_default, float val_mt, float val_et, float val_em);
  float getValueByChannel(int channel, float val_mt, float val_et, float val_em);

  // added functions
//  float HEM_MC_weight(int bad_HEM);
  float CalculateStdDev(const RVec<float>& values); 
  // Returns Dr
  float DeltaR(float eta1, float eta2, float phi1, float phi2);

}



/*************************************************************************/


#endif
