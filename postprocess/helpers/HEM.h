//HEM Veto and weights

#ifndef HEM_H_INCL
#define HEM_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"
#include "fileIO.h"

/*******************************************************************/
// Study to determine eta-phi region here: https://indico.cern.ch/event/1424480/contributions/5991445/attachments/2870800/5027966/2024_06_07_HToAATo4B_HEM_geometry.pdf
      // HEM region check for AK8 jet candidate
   float HEM_region_AK8 (float eta_ak8, float phi_ak8){
      float eta_cut_AK8    = -1.1;  // Extra 0.4 buffer for AK8
      float phi_window_AK8 = 0.55;   // Extra 0.4 buffer for AK8
      float HEM_wgt = 0.35233;

      float weight = 1;
      if (eta_ak8 < eta_cut_AK8){ 
        if (abs(phi_ak8 + 1.22) < phi_window_AK8){
          weight = HEM_wgt;
        }
      }
      return weight;
    }

      // HEM region check for AK8 jet candidate AND single electron
      float HEM_region_el (float eta_ak8, float phi_ak8, float eta_lep, float phi_lep){
      float eta_cut_el    = -1.2;
      float phi_window_el = 0.45;   // i.e. -1.57 < phi < -0.87
      float eta_cut_AK8    = -1.1;  // Extra 0.4 buffer for AK8
      float phi_window_AK8 = 0.55;   // Extra 0.4 buffer for AK8
      float HEM_wgt = 0.352; //https://docs.google.com/spreadsheets/d/19ot4nFlhiJoD6v81qhgyKSYjE5PhgqgT2dz98PNIWg0/edit?gid=103960112#gid=103960112

      float weight = 1;
      if (eta_ak8 < eta_cut_AK8){ 
        if (abs(phi_ak8 + 1.22) < phi_window_AK8){
          weight = HEM_wgt;
        }
      }
      if (eta_lep < eta_cut_el){ 
        if (abs(phi_lep + 1.22) < phi_window_el){
          weight = HEM_wgt;
        }
      }
      return weight;
    }

/***************************************************************************/
/*template <typename T>
auto ApplyHEM(T &df, std::string sample) {
  auto df2 = df.Define("weight_HEM_nominal", HEM_region_WH, {"eta_fatjet", "phi_fatjet", "eta_lep", "phi_lep"}); 
  auto df2_RNode = ROOT::RDF::RNode(df2);
  if (Helper::containsSubstring(sample, "data")){
    df2_RNode = df2_RNode.Filter("weight_HEM_nominal == 1 || Run_number < 319077");
    return df2_RNode;
  }
  return df2;
*/
/************************ HEM module W->lv *******************************/
template <typename T>
auto ApplyHEM_W(T &df, std::string sample, std::string channel) {
  if (Helper::containsSubstring(sample, "data")){
    if (Helper::containsSubstring(channel, "el")){
     return df.Define("weight_HEM_nominal", HEM_region_el, {"eta_fatjet", "phi_fatjet", "eta_lep", "phi_lep"})
              .Filter("weight_HEM_nominal == 1 || Run_number < 319077", "HEM: Applying HEM Veto on data events in the affected runs and phi-eta region");
    }else{ 
     return df.Define("weight_HEM_nominal", HEM_region_AK8, {"eta_fatjet", "phi_fatjet"})
              .Filter("weight_HEM_nominal == 1 || Run_number < 319077", "HEM: Applying HEM Veto on data events in the affected runs and phi-eta region");
    }
  }else{
    if (Helper::containsSubstring(channel, "el")){
     return df.Define("weight_HEM_nominal", HEM_region_el, {"eta_fatjet", "phi_fatjet", "eta_lep", "phi_lep"})
              .Filter("Run_number == 0", "HEM: Dummy Filter on MC events, no events discarded");
    }else{ 
     return df.Define("weight_HEM_nominal", HEM_region_AK8, {"eta_fatjet", "phi_fatjet"})
              .Filter("Run_number == 0", "HEM: Dummy Filter on MC events, no events discarded");
    }
  }
}
/************************ HEM module Z->ll *******************************/
/***************************************************************************/
template <typename T>
auto ApplyHEM(T &df, std::string sample) {
  if (Helper::containsSubstring(sample, "data")){
     return df.Define("weight_HEM_nominal", HEM_region_AK8, {"eta_fatjet", "phi_fatjet"})
              .Filter("weight_HEM_nominal == 1 || Run_number < 319077", "HEM: Applying HEM Veto on data events in the affected runs and phi-eta region");
  }else{
     return df.Define("weight_HEM_nominal", HEM_region_AK8, {"eta_fatjet", "phi_fatjet"})
              .Filter("Run_number == 0", "HEM: Dummy Filter on MC events, no events discarded");
  }
}
/*template <typename T>
auto ApplyHEM(T &df, std::string sample) {

  if (Helper::containsSubstring(sample, "data")){
   //return df.Define("weight_HEM_nominal", "eta_fatjet + phi_fatjet")
        return df.Filter("Run_number < 319077", "HEM: Applying HEM Veto on data events in the affected runs and phi-eta region");
    }else{
  //  }
     return df.Filter("Run_number == 0", "HEM: Applying HEM Veto on data events in the affected runs and phi-    eta region");
  } 
}*/
/***************************************************************************/
/*template <typename T>
auto function(T &df, bool cond) {

  if (cond){
        return df.Filter("rdf_entry < 3", "Apply filter");
    }else{
     return df;
  } 
}*/
/*******************************************************************/

#endif
