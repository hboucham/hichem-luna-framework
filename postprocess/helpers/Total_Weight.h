//Total Weight

#ifndef TOTAL_WEIGHT_H_INCL
#define TOTAL_WEIGHT_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

template <typename T>
auto Total_Weight(T &df) {
  auto df2 = df.Define("weight_tot_nominal", "weight_zPt_nominal*weight_pu_nominal*weight_VH_nominal*weight_ISR_nominal*weight_FSR_nominal*weight_PDF_nominal*weight_LeptonIDISO_nominal*weight_trigger_nominal");
  return df2.Define("weight_tot_Up", "weight_zPt_Up*weight_pu_Up*weight_VH_Up*weight_ISR_Up*weight_FSR_Up*weight_PDF_Up*weight_LeptonIDISO_Up*weight_trigger_Up")
            .Define("weight_tot_Down", "weight_zPt_Down*weight_pu_Down*weight_VH_Down*weight_ISR_Down*weight_FSR_Down*weight_PDF_Down*weight_LeptonIDISO_Down*weight_trigger_Down")
            .Define("weight_tot", "weight_tot_nominal*weight_HEM_nominal*weight_GEN_nominal");
}

/*******************************************************************/


#endif
