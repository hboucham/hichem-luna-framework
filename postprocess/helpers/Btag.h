#ifndef BTAG_H
#define BTAG_H

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>
#include "helpers.h"
#include "correction.h"

using correction::CorrectionSet;
using namespace ROOT::VecOps;

  // Load correction file b-tagging
  auto btagcorrset = CorrectionSet::from_file("../commonFiles/JEC_files/POG/BTV/2018_UL/btagging.json");
  // Load correction b-tagging
  auto btagcorrBC = btagcorrset->at("deepJet_comb");
  auto btagcorrLight = btagcorrset->at("deepJet_incl");

auto BtagSF( const RVec<float> &jet_pt, const RVec<float> &jet_eta, const RVec<float> &jet_Btageff, const RVec<int> &jet_flav){

    RVec<float> BtagSF_central(jet_pt.size());
    RVec<float> BtagSF_cor_Up(jet_pt.size());
    RVec<float> BtagSF_cor_Down(jet_pt.size());
    RVec<float> BtagSF_uncor_Up(jet_pt.size());
    RVec<float> BtagSF_uncor_Down(jet_pt.size());
    RVec<float> BtagSF_Up(jet_pt.size());
    RVec<float> BtagSF_Down(jet_pt.size());
    float deepjetWP = 0.2783; // Medium B-tag UL 2018 WP: https://btv-wiki.docs.cern.ch/ScaleFactors/UL2018/ 

    for(unsigned int ijet = 0; ijet < jet_pt.size(); ijet++){
      correction::Correction::Ref btagcorr;
      //float ijet_pt = jet_pt[ijet];
      //float ijet_abseta = fabs(jet_eta[ijet]);
      //float ijet_Btageff = jet_Btageff[ijet];
      //int ijet_flav = jet_flav[ijet];

      if(jet_flav.at(ijet) < 4){
        btagcorr = btagcorrLight; 
      }else{
        btagcorr = btagcorrBC; 
      }
     
      if(jet_Btageff.at(ijet) > deepjetWP && abs(jet_eta.at(ijet)) < 2.4){
        BtagSF_central[ijet] = btagcorr->evaluate({"central","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
        BtagSF_Up[ijet] = btagcorr->evaluate({"up","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
        BtagSF_Down[ijet] = btagcorr->evaluate({"down","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
        BtagSF_cor_Up[ijet] = btagcorr->evaluate({"up_correlated","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
        BtagSF_cor_Down[ijet] = btagcorr->evaluate({"down_correlated","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
        BtagSF_uncor_Up[ijet] = btagcorr->evaluate({"up_uncorrelated","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
        BtagSF_uncor_Down[ijet] = btagcorr->evaluate({"down_uncorrelated","M", jet_flav.at(ijet), abs(jet_eta.at(ijet)), jet_pt.at(ijet)});
      /*if(ijet_Btageff > deepjetWP){
        BtagSF_central[ijet] = btagcorr->evaluate({"central","M", ijet_flav, ijet_abseta, ijet_pt});
        BtagSF_cor_Up[ijet] = btagcorr->evaluate({"up_correlated","M", ijet_flav, ijet_abseta, ijet_pt});
        BtagSF_cor_Down[ijet] = btagcorr->evaluate({"down_correlated","M", ijet_flav, ijet_abseta, ijet_pt});
        BtagSF_uncor_Up[ijet] = btagcorr->evaluate({"up_uncorrelated","M", ijet_flav, ijet_abseta, ijet_pt});
        BtagSF_uncor_Down[ijet] = btagcorr->evaluate({"down_uncorrelated","M", ijet_flav, ijet_abseta, ijet_pt});*/
      }else{ // We only care about Medium B-tagged jets, so ignore B-tag SF for non B-tagged jets for this test
        BtagSF_central[ijet] = -1;
        BtagSF_Up[ijet] = -1;
        BtagSF_Down[ijet] = -1;
        BtagSF_cor_Up[ijet] = -1;
        BtagSF_cor_Down[ijet] = -1;
        BtagSF_uncor_Up[ijet] = -1;
        BtagSF_uncor_Down[ijet] = -1;
      }
    }
    
    RVec<RVec<float>> output = {BtagSF_central, BtagSF_cor_Up, BtagSF_cor_Down, BtagSF_uncor_Up, BtagSF_uncor_Down, BtagSF_Up, BtagSF_Down};
    return output;
}
   

/**********************************************************/

template <typename T>
auto ApplyBtagSF(T &df, std::string sample) { 
  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("RDF_BtagSF_central",   []() { return (float) 1.0; })
             .Define("RDF_BtagSF_cor_Up",   []() { return (float) 1.0; })
             .Define("RDF_BtagSF_cor_Down",   []() { return (float) 1.0; })
             .Define("RDF_BtagSF_uncor_Up",   []() { return (float) 1.0; })
             .Define("RDF_BtagSF_uncor_Down",   []() { return (float) 1.0; })
             .Define("RDF_BtagSF_Up",   []() { return (float) 1.0; })
             .Define("RDF_BtagSF_Down",   []() { return (float) 1.0; });
  }else{
    auto df2 = df.Define("BtagSFs", BtagSF, {"Jet_pt", "Jet_eta", "Jet_btagDeepFlavB", "Jet_hadronFlavour"});
    return df2.Define("RDF_BtagSF_central", "BtagSFs[0]")
              .Define("RDF_BtagSF_cor_Up", "BtagSFs[1]")
              .Define("RDF_BtagSF_cor_Down", "BtagSFs[2]")
              .Define("RDF_BtagSF_uncor_Up", "BtagSFs[3]")
              .Define("RDF_BtagSF_uncor_Down", "BtagSFs[4]")
              .Define("RDF_BtagSF_Up", "BtagSFs[5]")
              .Define("RDF_BtagSF_Down", "BtagSFs[6]");
  }
}

/*************************************************************/
#endif
