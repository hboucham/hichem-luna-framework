#ifndef SAMPLE_CONFIG_CLASS_H_INCL
#define SAMPLE_CONFIG_CLASS_H_INCL

#include <string>

#include "helperFunctions.h"

/*************************************************************************/

namespace LUNA {

    class sampleConfig_t {

        private:
            std::string sampleName;
            int isSampleData;
            int isSampleEmbedded;
            int isSampleMC;
            int sampleYear;
            std::string runTag;

            // Do systematics at all?
            int doSys;
            int hasDNN;

            int isSampleTTbar;
            int isSampleDY;

            int isSampleDataEmbedMuTau;
            int isSampleDataEmbedETau;
            int isSampleDataEmbedEMu;

            // Do systematics for this dataset?
            int doSys_muonES;
            int doSys_tauES;
            int doSys_tauhID;
            int doSys_eleFakeTauh;
            int doSys_muFakeTauh;
            int doSys_JER;
            int doSys_btagEff;
            int doSys_recoilMET;
            int doSys_unclusteredMET;


        public:

            // Are the JER systematic jet pT and MET branches in the file?
            int hasJERsysInputs; // has "MET_T1Smear_pt_jerUp"
            int hasJERyearsysInputs; // has "MET_T1Smear_pt_jesAbsolute_2018Up"
            int hasMetUESInputs; // has "MET_T1Smear_pt_unclustEnUp"


            // To initialize a sample config
            sampleConfig_t(std::string processName, int doDefaultShifts, int hasDNNattached);

            // Setters
            void setYear(int year);
            void setDoSystematics(int performSys);

            // Getters
            std::string name();
            int doShifts();
            int isData();
            int isEmbedded();
            int isMC();
            int year();
            std::string yearStr();

            // Is the sample data or embed that is specific to a final state
            int isMuTauOnly();
            int isETauOnly();
            int isEMuOnly();

            int runSystematics();
            int isSignal();
            int isTTbar();
            int isDY();
            int isWJets();
            int isHiggsRecoil();
            int doRecoil();
            int recoilBosonPdgId();

            // Recoil type: 0 for none, 1 for W recoil boson, 2 for Z/H recoil boson (electrically neutral)
            int recoilType();

            // Is MC and not a Higgs sample (for contamination to Embedded from non-DYjets samples)
            int isMCnonHiggs();

            std::string getRunTag();
    };

}
/*************************************************************************/

#endif
