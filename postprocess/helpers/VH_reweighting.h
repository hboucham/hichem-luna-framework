#ifndef ZHREWEIGHTING_H_INCL
#define ZHREWEIGHTING_H_INCL

#include <mutex>
#include "helperFunctions.h"
#include "sampleConfig_class.h"
#include "fileIO.h"


float VHweight(float pt_genH, float pt_genV, const TH2D* VHptr) {

  float weight = 1.0;  // default value
  float VH_X = log2(2*pt_genH/(pt_genH + pt_genV));
  float VH_Y = log2(pt_genH);
  int globalBin = VHptr->FindFixBin(VH_X, VH_Y);
  weight = VHptr->GetBinContent(globalBin);
  
  return weight;
}


/*******************************************************************/


template <typename T>
auto GetVHWeight(T &df, std::string sample){

  if (Helper::containsSubstring(sample,"SUSY_ZH")) { //ZH signal samples
    auto df2 = df.Define("weight_VH_nominal", VHweight, {"gen_pt_H", "gen_pt_Z", "ZHweight_nominal_ptr"});
    return df2.Define("weight_VH_Down", [=]() { return 1.0f;})
              .Define("weight_VH_Up", [](float weight_nom) { return (float) 1 + 2*(weight_nom -1); }, {"weight_VH_nominal"});
  }else if (Helper::containsSubstring(sample,"SUSY_WH")) { //ZH signal samples
    auto df2 = df.Define("weight_VH_nominal", VHweight, {"gen_pt_H", "gen_pt_W", "WHweight_nominal_ptr"});
    return df2.Define("weight_VH_Down", [=]() { return 1.0f;})
              .Define("weight_VH_Up", [](float weight_nom) { return (float) 1 + 2*(weight_nom -1); }, {"weight_VH_nominal"});
  }else{ //everything else is not reweighted
    auto df2 = df.Define("weight_VH_nominal", [=]() { return 1.0f; });
    return df2.Define("weight_VH_Down", [=]() { return 1.0f;})
              .Define("weight_VH_Up", [=]() { return 1.0f;});
  }
}

/*******************************************************************/

#endif
