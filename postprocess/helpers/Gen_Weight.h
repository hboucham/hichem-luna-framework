//GEN Weight

#ifndef GEN_WEIGHT_H_INCL
#define GEN_WEIGHT_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

template <typename T>
auto ApplyGenWeight(T &df, std::string sample) {
  if (Helper::containsSubstring(sample, "data")){
    return df.Define("weight_GEN_nominal", [=]() { return (float) 1.0; });
  }else{
    return df.Define("dummy_one", [=]() {return (float) 1.0; })
             .Define("weight_GEN_nominal", "dummy_one - 2*(gen_weight < 0)");
  }       
}

/*******************************************************************/


#endif
