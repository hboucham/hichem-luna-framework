// Pile-up reweighing

#ifndef PILEUP_REWEIGHING_H
#define PILEUP_REWEIGHING_H

#include "fileIO.h"
#include "sampleConfig_class.h"

// Old PU reweighting module, uses ROOT files that need to be produced for each
// sample. This module is taken Stephanie Kwan's framework.
// However, the PU weights seem to be wrong, so we now use a more
// straightforward approach, using a json file from LUMI POG.

/*******************************************************************/

/*
 * Pileup_nPU: the number of pileup interactions that have been added to the event in the current bunch crossing
 */

float getPUWeight(int npu, const TH1F* pileup_ptr) {

  float weight = 1.0;  // default value
  weight = pileup_ptr->GetBinContent(pileup_ptr->GetBin(npu));

  // std::cout << "get PU weight: " << weight << std::endl;
  return weight;
}

/*******************************************************************/

/*
 * Get the pile-up distribution re-weighting as a branch.
 */

template <typename T>
auto GetPileupWeight(T &df, std::string sample){

  if (Helper::containsSubstring(sample,"data")) { //Data
    auto df2 = df.Define("weight_pu_nominal", [=]() { return 1.0f; });
    return df2.Define("weight_pu_Down",  [=]() { return 1.0f; })
              .Define("weight_pu_Up",    [=]() { return 1.0f; });
  }else{ //MC
    auto df2 = df.Define("weight_pu_nominal", getPUWeight, {"npu", "puweight_nominal_ptr"});
    return df2.Define("weight_pu_Down", getPUWeight, {"npu", "puweight_down_ptr"})
              .Define("weight_pu_Up",   getPUWeight, {"npu", "puweight_up_ptr"});
  }
}

/*******************************************************************/

#endif
