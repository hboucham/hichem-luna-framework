#ifndef PU_REWEIGHT_H
#define PU_REWEIGHT_H

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>
#include "helperFunctions.h"
#include "correction.h"

// Latest PU reweighting module, uses JSON file from POG instead of ROOT

using correction::CorrectionSet;
using namespace ROOT::VecOps;

  // Load PU corrections
  auto pucorrset = CorrectionSet::from_file("../commonFiles/JEC_files/POG/LUM/2018_UL/puWeights.json");
  auto pucorr = pucorrset->at("Collisions18_UltraLegacy_goldenJSON"); 


/**********************************************************/
auto PU_Reweighting (const float &numTrueInt){
    RVec<double> SF_val;
    SF_val = {pucorr->evaluate({numTrueInt, "nominal"}), 
              pucorr->evaluate({numTrueInt, "up"}), 
              pucorr->evaluate({numTrueInt, "down"})};
    return SF_val;
  }; 
/**********************************************************/
template <typename T>
auto ApplyPU_Reweighting(T &df, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("weight_pu_nominal", [=]() { return (double) 1.0; })
             .Define("weight_pu_Up", [=]() { return (double) 1.0; })
             .Define("weight_pu_Down", [=]() { return (double) 1.0; });
  }else{
    auto df2 = df.Define("PU_weights", PU_Reweighting, {"Pileup_nTrueInt"});
    return df2.Define("weight_pu_nominal", "PU_weights[0]")
              .Define("weight_pu_Up", "PU_weights[1]")
              .Define("weight_pu_Down", "PU_weights[2]");
  }
}
   

#endif
