#ifndef LEPTONSF_TRIG_H
#define LEPTONSF_TRIG_H

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include <ROOT/RDataFrame.hxx>
#include "helperFunctions.h"
#include "correction.h"

using correction::CorrectionSet;
using namespace ROOT::VecOps;



  // Load electron corrections
  //auto muonTrigcorrset = CorrectionSet::from_file("../commonFiles/MuonSF/2018UL/MuonIDISO_Z_v2_overflow.json");
  auto muonTrigcorrset = CorrectionSet::from_file("../commonFiles/MuonSF/2018UL/MuonSF_TRIG.json");
  auto electronTrigcorrset = CorrectionSet::from_file("../commonFiles/ElectronSF/2018UL/ElectronSF.json");
  //auto muonIDcorrset = CorrectionSet::from_file("../commonFiles/MuonSF/2018UL/MuonIDv1.json");
  auto muonTrigcorr = muonTrigcorrset->at("NUM_IsoMu24_or_Mu50_DEN_L1SingleMu22_and_MediumPrompt_and_TightMiniIso"); 
  auto electronTrigcorr = electronTrigcorrset->at("EGamma_SF2D"); 


// From Stephanie Kwan
// Given a reco object with recoPhi and recoEta, of type
// objType (e.g. muon: 13, tau: 15), return the index into trigObj if the
// reco object is within deltaR of a valid trigger object,
// or -99 if not found.

int getTrigObjIdx(int type, float recoEta, float recoPhi,
		  RVec<int> TrigObj_id,	RVec<float> TrigObj_eta, RVec<float> TrigObj_phi) {

  int idx = -99;
  for (int i = 0; i < (int) TrigObj_id.size(); i++) {
    idx = -95;  // inside loop
    if (TrigObj_id[i] == type) {
      idx = -90; // trig object found
      const auto deltar = Helper::DeltaR(recoEta, TrigObj_eta[i], recoPhi, TrigObj_phi[i]);
      if (deltar < 0.5) {
        idx = i;
        //	std::cout << "Matching trigObj found, index " << idx << std::endl;
        break;
      }
    }
  }

  return idx;
}



/**********************************************************/
auto ElectronTrig_SF_elel (const float &pt1, const float &eta1, const int &trigIdx1, const float &pt2, const float &eta2, const int &trigIdx2){
    RVec<double> SF_val;
    float pt;
    float eta;
    
    if (trigIdx1 > -1){
      pt = pt1;
      eta = eta1;
    }else if (trigIdx2 > -1 && pt2 > 35){
      pt = pt2;
      eta = eta2;
    }else{
      SF_val = {-99.0, -99.0, -99.0};
      //SF_val = {1.0, 1.0, 1.0};
      return SF_val;
    }

    SF_val = {electronTrigcorr->evaluate({"2018UL", "sf", "trigger", eta, pt}), 
  	          (electronTrigcorr->evaluate({"2018UL", "sf", "trigger", eta, pt}) + electronTrigcorr->evaluate({"2018UL", "sf_err", "trigger", eta, pt})),
  	          (electronTrigcorr->evaluate({"2018UL", "sf", "trigger", eta, pt}) - electronTrigcorr->evaluate({"2018UL", "sf_err", "trigger", eta, pt}))};
    return SF_val;
  }; 
/**********************************************************/
auto MuonTrig_SF_mumu (const float &pt1, const float &eta1, const int &trigIdx1, const float &pt2, const float &eta2, const int &trigIdx2){
    RVec<double> SF_val;
    float pt;
    float eta;
    
    if (trigIdx1 > -1){
      pt = pt1;
      eta = eta1;
    }else if (trigIdx2 > -1 && pt2 > 26){
      pt = pt2;
      eta = eta2;
    }else{
      SF_val = {-99.0, -99.0, -99.0};
      //SF_val = {1.0, 1.0, 1.0};
      return SF_val;
    }

    SF_val = {muonTrigcorr->evaluate({abs(eta), pt,"nominal"}), 
  	          muonTrigcorr->evaluate({abs(eta), pt,"systup"}), 
	            muonTrigcorr->evaluate({abs(eta), pt,"systdown"})};
    return SF_val;
  }; 
/**********************************************************/

auto ElectronTrig_SF_el (const float &pt1, const float &eta1, const int &trigIdx1){
    RVec<double> SF_val;
    float pt;
    float eta;
    
    if (trigIdx1 > -1){
      pt = pt1;
      eta = eta1;
    }else{
      //SF_val = {1.0, 1.0, 1.0};
      SF_val = {-99.0, -99.0, -99.0};
      return SF_val;
    }

    SF_val = {electronTrigcorr->evaluate({"2018UL", "sf", "trigger", eta, pt}), 
  	          (electronTrigcorr->evaluate({"2018UL", "sf", "trigger", eta, pt}) + electronTrigcorr->evaluate({"2018UL", "sf_err", "trigger", eta, pt})),
  	          (electronTrigcorr->evaluate({"2018UL", "sf", "trigger", eta, pt}) - electronTrigcorr->evaluate({"2018UL", "sf_err", "trigger", eta, pt}))};
    return SF_val;
  }; 
/**********************************************************/
auto MuonTrig_SF_mu (const float &pt1, const float &eta1, const int &trigIdx1){
    RVec<double> SF_val;
    float pt;
    float eta;
    
    if (trigIdx1 > -1){
      pt = pt1;
      eta = eta1;
    }else{
      //SF_val = {1.0, 1.0, 1.0};
      SF_val = {-99.0, -99.0, -99.0};
      return SF_val;
    }

    SF_val = {muonTrigcorr->evaluate({abs(eta), pt,"nominal"}), 
  	          muonTrigcorr->evaluate({abs(eta), pt,"systup"}), 
	            muonTrigcorr->evaluate({abs(eta), pt,"systdown"})};
    return SF_val;
  }; 

/**********************************************************/
template <typename T>
auto ApplyLeptonSF_Trig_2LZ(T &df, std::string channel, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("MuonSF_Trig_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_Trig_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_Trig_Down", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_nominal", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_Up", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_Down", [=]() { return (double) 1.0; })
             .Define("weight_trigger_nominal", []() { return (double) 1.0; })
             .Define("weight_trigger_Up", []() { return (double) 1.0; })
             .Define("weight_trigger_Down", []() { return (double) 1.0; });
  }else{
    if (channel == "mu"){
      auto df2 = df.Define("trigger_type", []() { return (int) 13; }); // muon trigger
      return df2.Define("trigObj_idx1", getTrigObjIdx, {"trigger_type", "eta_l1", "phi_l1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("trigObj_idx2", getTrigObjIdx, {"trigger_type", "eta_l2", "phi_l2", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("MuonTrig_SF", MuonTrig_SF_mumu, {"pt_l1", "eta_l1", "trigObj_idx1", "pt_l2", "eta_l2", "trigObj_idx2" })
                .Define("MuonSF_Trig_nominal", "MuonTrig_SF[0]")
                .Define("MuonSF_Trig_Up", "MuonTrig_SF[1]")
                .Define("MuonSF_Trig_Down", "MuonTrig_SF[2]")
                .Define("ElectronSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Up", []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "MuonSF_Trig_nominal")
                .Define("weight_trigger_Up", "MuonSF_Trig_Up")
                .Define("weight_trigger_Down", "MuonSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_2LZ: Discard events where leptons are not matched to trigger");
                

    }else{
      auto df2 = df.Define("trigger_type", []() { return (int) 11; }); // electron trigger
      return df2.Define("trigObj_idx1", getTrigObjIdx, {"trigger_type", "eta_l1", "phi_l1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("trigObj_idx2", getTrigObjIdx, {"trigger_type", "eta_l2", "phi_l2", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("ElectronTrig_SF", ElectronTrig_SF_elel, {"pt_l1", "eta_l1", "trigObj_idx1", "pt_l2", "eta_l2", "trigObj_idx2" })
                .Define("ElectronSF_Trig_nominal", "ElectronTrig_SF[0]")
                .Define("ElectronSF_Trig_Up", "ElectronTrig_SF[1]")
                .Define("ElectronSF_Trig_Down", "ElectronTrig_SF[2]")
                .Define("MuonSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Up", []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "ElectronSF_Trig_nominal")
                .Define("weight_trigger_Up", "ElectronSF_Trig_Up")
                .Define("weight_trigger_Down", "ElectronSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_2LZ: Discard events where leptons are not matched to trigger");
    }
  }
}
   
/**********************************************************/

template <typename T>
auto ApplyLeptonSF_Trig_1L(T &df, std::string channel, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("MuonSF_Trig_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_Trig_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_Trig_Down", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_nominal", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_Up", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_Down", [=]() { return (double) 1.0; })
             .Define("weight_trigger_nominal", []() { return (double) 1.0; })
             .Define("weight_trigger_Up", []() { return (double) 1.0; })
             .Define("weight_trigger_Down", []() { return (double) 1.0; });
  }else{
    if (channel == "mu"){
      auto df2 = df.Define("trigger_type", []() { return (int) 13; }); // muon trigger
      return df2.Define("trigObj_idx", getTrigObjIdx, {"trigger_type", "eta_lep", "phi_lep", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("MuonTrig_SF", MuonTrig_SF_mu, {"pt_lep", "eta_lep", "trigObj_idx"})
                .Define("MuonSF_Trig_nominal", "MuonTrig_SF[0]")
                .Define("MuonSF_Trig_Up", "MuonTrig_SF[1]")
                .Define("MuonSF_Trig_Down", "MuonTrig_SF[2]")
                .Define("ElectronSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Up", []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "MuonSF_Trig_nominal")
                .Define("weight_trigger_Up", "MuonSF_Trig_Up")
                .Define("weight_trigger_Down", "MuonSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_1L: Discard events where leptons are not matched to trigger");

    }else{
      auto df2 = df.Define("trigger_type", []() { return (int) 11; }); // electron trigger
      return df2.Define("trigObj_idx", getTrigObjIdx, {"trigger_type", "eta_lep", "phi_lep", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("ElectronTrig_SF", ElectronTrig_SF_el, {"pt_lep", "eta_lep", "trigObj_idx"})
                .Define("ElectronSF_Trig_nominal", "ElectronTrig_SF[0]")
                .Define("ElectronSF_Trig_Up", "ElectronTrig_SF[1]")
                .Define("ElectronSF_Trig_Down", "ElectronTrig_SF[2]")
                .Define("MuonSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Up", []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "ElectronSF_Trig_nominal")
                .Define("weight_trigger_Up", "ElectronSF_Trig_Up")
                .Define("weight_trigger_Down", "ElectronSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_1L: Discard events where leptons are not matched to trigger");
    }
  }
}

/**********************************************************/
template <typename T>
auto ApplyLeptonSF_Trig_2Ltt(T &df, std::string chan1, std::string chan2, std::string sample) { 
  // Don't apply lepton SF to data

  if (Helper::containsSubstring(sample, "data")){ 
    return df.Define("MuonSF_Trig_nominal", [=]() { return (double) 1.0; })
             .Define("MuonSF_Trig_Up", [=]() { return (double) 1.0; })
             .Define("MuonSF_Trig_Down", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_nominal", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_Up", [=]() { return (double) 1.0; })
             .Define("ElectronSF_Trig_Down", [=]() { return (double) 1.0; })
             .Define("weight_trigger_nominal", []() { return (double) 1.0; })
             .Define("weight_trigger_Up", []() { return (double) 1.0; })
             .Define("weight_trigger_Down", []() { return (double) 1.0; });
  }else{
    if (chan1 == "mu" && chan2 == "mu"){
      auto df2 = df.Define("trigger_type", []() { return (int) 13; }); // muon trigger
      return df2.Define("trigObj_idx1", getTrigObjIdx, {"trigger_type", "eta_l1", "phi_l1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("trigObj_idx2", getTrigObjIdx, {"trigger_type", "eta_l2", "phi_l2", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("MuonTrig_SF", MuonTrig_SF_mumu, {"pt_l1", "eta_l1", "trigObj_idx1", "pt_l2", "eta_l2", "trigObj_idx2" })
                .Define("MuonSF_Trig_nominal", "MuonTrig_SF[0]")
                .Define("MuonSF_Trig_Up", "MuonTrig_SF[1]")
                .Define("MuonSF_Trig_Down", "MuonTrig_SF[2]")
                .Define("ElectronSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Up", []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "MuonSF_Trig_nominal")
                .Define("weight_trigger_Up", "MuonSF_Trig_Up")
                .Define("weight_trigger_Down", "MuonSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_2L_tt: Discard events where leptons are not matched to trigger");
    }else if (chan1 == "el" && chan2 == "el"){
      auto df2 = df.Define("trigger_type", []() { return (int) 11; }); // electron trigger
      return df2.Define("trigObj_idx1", getTrigObjIdx, {"trigger_type", "eta_l1", "phi_l1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("trigObj_idx2", getTrigObjIdx, {"trigger_type", "eta_l2", "phi_l2", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("ElectronTrig_SF", ElectronTrig_SF_elel, {"pt_l1", "eta_l1", "trigObj_idx1", "pt_l2", "eta_l2", "trigObj_idx2" })
                .Define("ElectronSF_Trig_nominal", "ElectronTrig_SF[0]")
                .Define("ElectronSF_Trig_Up", "ElectronTrig_SF[1]")
                .Define("ElectronSF_Trig_Down", "ElectronTrig_SF[2]")
                .Define("MuonSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Up", []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "ElectronSF_Trig_nominal")
                .Define("weight_trigger_Up", "ElectronSF_Trig_Up")
                .Define("weight_trigger_Down", "ElectronSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_2L_tt: Discard events where leptons are not matched to trigger");
    }else if (chan1 == "mu" && chan2 == "el"){
      auto df2 = df.Define("trigger_type", []() { return (int) 13; }); // muon trigger
      return df2.Define("trigObj_idx1", getTrigObjIdx, {"trigger_type", "eta_l1", "phi_l1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("MuonTrig_SF", MuonTrig_SF_mu, {"pt_l1", "eta_l1", "trigObj_idx1"})
                .Define("MuonSF_Trig_nominal", "MuonTrig_SF[0]")
                .Define("MuonSF_Trig_Up", "MuonTrig_SF[1]")
                .Define("MuonSF_Trig_Down", "MuonTrig_SF[2]")
                .Define("ElectronSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Up", []() { return (double) 1.0; })
                .Define("ElectronSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "MuonSF_Trig_nominal")
                .Define("weight_trigger_Up", "MuonSF_Trig_Up")
                .Define("weight_trigger_Down", "MuonSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_2L_tt: Discard events where leptons are not matched to trigger");
    }else{ //(chan1 == "el" && chan2 == "mu")
      auto df2 = df.Define("trigger_type", []() { return (int) 11; }); // electron trigger
      return df2.Define("trigObj_idx1", getTrigObjIdx, {"trigger_type", "eta_l1", "phi_l1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
                .Define("ElectronTrig_SF", ElectronTrig_SF_el, {"pt_l1", "eta_l1", "trigObj_idx1"})
                .Define("ElectronSF_Trig_nominal", "ElectronTrig_SF[0]")
                .Define("ElectronSF_Trig_Up", "ElectronTrig_SF[1]")
                .Define("ElectronSF_Trig_Down", "ElectronTrig_SF[2]")
                .Define("MuonSF_Trig_nominal",  []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Up", []() { return (double) 1.0; })
                .Define("MuonSF_Trig_Down", []() { return (double) 1.0; })
                .Define("weight_trigger_nominal", "ElectronSF_Trig_nominal")
                .Define("weight_trigger_Up", "ElectronSF_Trig_Up")
                .Define("weight_trigger_Down", "ElectronSF_Trig_Down")
                .Filter("weight_trigger_nominal != -99.0", "ApplyLeptonSF_Trig_2L_tt: Discard events where leptons are not matched to trigger");
    }
  }
}
   
/**********************************************************/
#endif
