#ifndef ZHREWEIGHTING_H_INCL
#define ZHREWEIGHTING_H_INCL

#include <mutex>
#include "helperFunctions.h"
#include "sampleConfig_class.h"
#include "fileIO.h"


float ZHweight(float pt_genH, float pt_genZ, const TH2D* ZHptr) {

  float weight = 1.0;  // default value
  float ZH_X = log2(2*pt_genH/(pt_genH + pt_genZ));
  float ZH_Y = log2(pt_genH);
  int globalBin = ZHptr->FindFixBin(ZH_X, ZH_Y);
  weight = ZHptr->GetBinContent(globalBin);
  
  return weight;
}


/*******************************************************************/


template <typename T>
auto GetZHWeight(T &df, std::string sample){

  if (Helper::containsSubstring(sample,"SUSY_ZH")) { //ZH signal samples
    auto df2 = df.Define("weight_ZH_nominal", ZHweight, {"gen_pt_H", "gen_pt_Z", "ZHweight_nominal_ptr"});
    return df2.Define("weight_ZH_Down", [=]() { return 1.0f;})
              .Define("weight_ZH_Up", [](float weight_nom) { return (float) 1 + 2*(weight_nom -1); }, {"weight_ZH_nominal"});
  }else{ //everything else is not reweighted
    auto df2 = df.Define("weight_ZH_nominal", [=]() { return 1.0f; });
    return df2.Define("weight_ZH_Down", [=]() { return 1.0f;})
              .Define("weight_ZH_Up", [=]() { return 1.0f;});
  }
}

/*******************************************************************/

#endif
