# Usage:
# bash runPostprocess_2L_tt.sh mu/el mu/el

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi


#--------------------------------------------------------
# Compile
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Compiling postprocess_2L_tt.cxx executable ..."
TARGET_FILES="helpers/helperFunctions.cc helpers/sampleConfig_class.cc"
COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
FLAGSS=$(correction config --cflags --ldflags --rpath)
time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -lRooFitCore -lRooFit -o Postprocess_2L_tt postprocess_2L_tt.cxx ${TARGET_FILES} $FLAGS $FLAGSS 

if [[ $? -ne 0 ]]; then
    echo ">>> Compile failed, exit"
    exit 2
fi


#--------------------------------------------------------
# Execute
#--------------------------------------------------------
./Postprocess_2L_tt $1 $2 $3
