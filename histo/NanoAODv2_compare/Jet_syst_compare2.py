import ROOT
from ROOT import gROOT
gROOT.SetBatch(True)

# JEC
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_nom/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

events.Draw("FatJet_eta:FatJet_phi"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && (abs(FatJet_pt_nom-cleanFatJet_pt) > 1)","colz")
c.SaveAs("FatJet_pt_nom_etavsphi.pdf")
events.Draw("FatJet_eta:FatJet_phi"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && (abs(FatJet_mass_nom-cleanFatJet_mass) > 1)","colz")
c.SaveAs("FatJet_mass_nom_etavsphi.pdf")
events.Draw("FatJet_pt"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && (abs(FatJet_pt_nom-cleanFatJet_pt) > 1)")
c.SaveAs("FatJet_pt_nom_pt.pdf")
events.Draw("FatJet_pt"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && (abs(FatJet_mass_nom-cleanFatJet_mass) > 1)")
c.SaveAs("FatJet_mass_nom_pt.pdf")
events.Draw("FatJet_mass"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && (abs(FatJet_pt_nom-cleanFatJet_pt) > 1)")
c.SaveAs("FatJet_pt_nom_mass.pdf")
events.Draw("FatJet_mass"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && (abs(FatJet_mass_nom-cleanFatJet_mass) > 1)")
c.SaveAs("FatJet_mass_nom_mass.pdf")


events.Draw("Jet_eta:Jet_phi"," cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4) && (abs(Jet_pt_nom-cleanJet_pt) > 0.1)","colz")
c.SaveAs("Jet_pt_nom_etavsphi.pdf")
events.Draw("Jet_eta:Jet_phi"," cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4) && (abs(Jet_mass_nom-cleanJet_mass) > 0.1)","colz")
c.SaveAs("Jet_mass_nom_etavsphi.pdf")
events.Draw("Jet_pt"," cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4) && (abs(Jet_pt_nom-cleanJet_pt) > 0.1)")
c.SaveAs("Jet_pt_nom_pt.pdf")
events.Draw("Jet_pt"," cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4) && (abs(Jet_mass_nom-cleanJet_mass) > 0.1)")
c.SaveAs("Jet_mass_nom_pt.pdf")

