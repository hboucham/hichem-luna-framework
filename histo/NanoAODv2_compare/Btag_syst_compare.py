import ROOT
from ROOT import gROOT
gROOT.SetBatch(True)

# B-tag branches comparison
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_nom/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()


events.Draw("RDF_BtagSF_central:Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_central.pdf")
events.Draw("(RDF_BtagSF_Up - RDF_BtagSF_central):Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_Up.pdf")
events.Draw("(RDF_BtagSF_Down - RDF_BtagSF_central):Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_Down.pdf")
events.Draw("(RDF_BtagSF_cor_Up - RDF_BtagSF_central):Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_cor_Up.pdf")
events.Draw("(RDF_BtagSF_cor_Down - RDF_BtagSF_central):Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_cor_Down.pdf")
events.Draw("(RDF_BtagSF_uncor_Up - RDF_BtagSF_central):Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_uncor_Up.pdf")
events.Draw("(RDF_BtagSF_uncor_Down - RDF_BtagSF_central):Jet_pt"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("RDF_BtagSF_uncor_Down.pdf")


