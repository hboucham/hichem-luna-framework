import ROOT
from ROOT import gROOT, gPad
gROOT.SetBatch(True)

# B-tag branches comparison
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_nom/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

#gPad.Update()
events.Draw("Jet_btagSF_deepjet_M_up:sqrt(Jet_btagSF_deepjet_M_up_correlated*Jet_btagSF_deepjet_M_up_correlated + Jet_btagSF_deepjet_M_up_uncorrelated*Jet_btagSF_deepjet_M_up_uncorrelated)"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783", "COLZ")
#gPad.GetPrimitive("hframe").GetXaxis().SetTitleSize(0.04)i
hist = ROOT.gPad.GetPrimitive("htemp")
if hist:  # Ensure the histogram exists
    hist.SetTitle("Jet_btagSF_deepjet_M_up Comparison;sqrt(up_corr^2+up_uncorr^2); up")
    #hist.GetXaxis().SetTitleSize(0.018)  # Adjust the x-axis title size
    #hist.GetXaxis().SetTitleOffset(2)  # Adjust the x-axis title size
c.SaveAs("Jet_btagSF_deepjet_M_up_vs_cor_uncor_quadrature.pdf")

events.Draw("Jet_btagSF_deepjet_M_down:sqrt(Jet_btagSF_deepjet_M_down_correlated*Jet_btagSF_deepjet_M_down_correlated + Jet_btagSF_deepjet_M_down_uncorrelated*Jet_btagSF_deepjet_M_down_uncorrelated)"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783", "COLZ")
hist = ROOT.gPad.GetPrimitive("htemp")
if hist:  # Ensure the histogram exists
    hist.SetTitle("Jet_btagSF_deepjet_M_down Comparison;sqrt(up_corr^2+up_uncorr^2); down")
c.SaveAs("Jet_btagSF_deepjet_M_down_vs_cor_uncor_quadrature.pdf")

events.Draw("Jet_btagSF_deepjet_M_up:(Jet_btagSF_deepjet_M_up_correlated + Jet_btagSF_deepjet_M_up_uncorrelated)"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783", "COLZ")
hist = ROOT.gPad.GetPrimitive("htemp")
if hist:  # Ensure the histogram exists
    hist.SetTitle("Jet_btagSF_deepjet_M_up Comparison;(up_corr + up_uncorr); up")
c.SaveAs("Jet_btagSF_deepjet_M_up_vs_cor_uncor_sum.pdf")

events.Draw("Jet_btagSF_deepjet_M_down:(Jet_btagSF_deepjet_M_down_correlated + Jet_btagSF_deepjet_M_down_uncorrelated)"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783", "COLZ")
hist = ROOT.gPad.GetPrimitive("htemp")
if hist:  # Ensure the histogram exists
    hist.SetTitle("Jet_btagSF_deepjet_M_down Comparison;(up_corr + up_uncorr); down")
c.SaveAs("Jet_btagSF_deepjet_M_down_vs_cor_uncor_sum.pdf")
