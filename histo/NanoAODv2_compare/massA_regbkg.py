import ROOT
from ROOT import gROOT
gROOT.SetBatch(True)

# Open the file and get the tree
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/samples/Test_postprocessor_ZH_M-30_1114.root", "READ")
events = f.Get("Events")

# Create histograms
h1 = ROOT.TH1F("h1", "PNET regressed mass (30 GeV) Comparison", 50, 10, 60)  # Adjust bins and range as needed
h2 = ROOT.TH1F("h2", "PNET regressed mass b", 50, 10, 60)
h3 = ROOT.TH1F("h3", "PNET regressed mass c", 50, 10, 60)
h4 = ROOT.TH1F("h4", "PNET regressed mass d", 50, 10, 60)
h5 = ROOT.TH1F("h5", "PNET regressed mass avg", 50, 10, 60)

line = ROOT.TLine(30, 0, 30, 1400)
line.SetLineColor(ROOT.kViolet)
line.SetLineWidth(2)
line.SetLineStyle(2)

# Draw histograms
events.Draw("FatJet_PNet_massAa>>h1", "FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && FatJet_nBHadrons < 4")
events.Draw("FatJet_PNet_massAb>>h2", "FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && FatJet_nBHadrons < 4")
events.Draw("FatJet_PNet_massAc>>h3", "FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && FatJet_nBHadrons < 4")
events.Draw("FatJet_PNet_massAd>>h4", "FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && FatJet_nBHadrons < 4")
events.Draw("FatJet_PNet_massA_avg>>h5", "FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20 && FatJet_nBHadrons < 4")

h1.SetTitle("PNET regressed a mass Comparison (ZH_M-30, nGenB < 4); regressed m(a); # AK8")
# Set titles and styles
h1.SetLineColor(ROOT.kRed)
h2.SetLineColor(ROOT.kBlue)
h3.SetLineColor(ROOT.kGreen)
h4.SetLineColor(ROOT.kOrange)
h5.SetLineColor(ROOT.kBlack)

h1.SetLineWidth(2)
h2.SetLineWidth(2)
h3.SetLineWidth(2)
h4.SetLineWidth(2)
h5.SetLineWidth(2)

# Draw histograms on the same canvas
c = ROOT.TCanvas()
h1.Draw("HIST")  # Draw first histogram
h2.Draw("HIST SAME")  # Overlay the others
h3.Draw("HIST SAME")
h4.Draw("HIST SAME")
h5.Draw("HIST SAME")
line.Draw("same")

# Add a legend
legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.75)  # Position: (x1, y1, x2, y2)
legend.AddEntry(h1, "regressor a", "l")
legend.AddEntry(h2, "regressor b", "l")
legend.AddEntry(h3, "regressor c", "l")
legend.AddEntry(h4, "regressor d", "l")
legend.AddEntry(h5, "regressor avg", "l")
legend.AddEntry(line, "Mass(a) = 30 GeV", "l")
legend.SetBorderSize(0)
legend.SetTextSize(0.03)
legend.Draw()

# Save the canvas
c.SaveAs("massA_ZH-M30_comparison_regBkg.pdf")

