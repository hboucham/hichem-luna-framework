import ROOT
from ROOT import gROOT, gStyle

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
#f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_mu_nom/test_mu.root", "READ")
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_nom/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

# PU_nom
#events.Draw("weight_pu_nominal:puWeight>>h1", "", "colz")
events.Draw("PU_nominal:puWeight>>h1", "", "colz")
ROOT.h1.GetYaxis().SetLimits(0, 1.5)
ROOT.h1.GetXaxis().SetLimits(0, 1.5)
line = ROOT.TLine(0, 0, 1.5, 1.5)
line.SetLineColor(ROOT.kRed)
ROOT.h1.Draw("COLZ")
line.Draw("same")
c.SaveAs("puWeight_correctionlib.pdf")

# PU_down
#events.Draw("weight_pu_Down:puWeightDown>>h2", "", "colz")
events.Draw("PU_Down:puWeightDown>>h2", "", "colz")
ROOT.h2.GetYaxis().SetLimits(0, 1.5)
ROOT.h2.GetXaxis().SetLimits(0, 1.5)
line = ROOT.TLine(0, 0, 1.5, 1.5)
line.SetLineColor(ROOT.kRed)
ROOT.h2.Draw("COLZ")
line.Draw("same")
c.SaveAs("puWeightDown_correctionlib.pdf")

# PU_up
#events.Draw("weight_pu_Up:puWeightUp>>h3", "", "colz")
events.Draw("PU_Up:puWeightUp>>h3", "", "colz")
ROOT.h3.GetYaxis().SetLimits(0, 2)
ROOT.h3.GetXaxis().SetLimits(0, 2)
line = ROOT.TLine(0, 0, 2, 2)
line.SetLineColor(ROOT.kRed)
ROOT.h3.Draw("COLZ")
line.Draw("same")
c.SaveAs("puWeightUp_correctionlib.pdf")
