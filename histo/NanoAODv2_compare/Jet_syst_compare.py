import ROOT
from ROOT import gROOT
gROOT.SetBatch(True)

# JEC
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_nom/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

events.Draw("(FatJet_pt_nom-cleanFatJet_pt):FatJet_pt_nom"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_pt_nom.pdf")
events.Draw("(Jet_pt_nom-cleanJet_pt):Jet_pt_nom","cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_pt_nom.pdf")
events.Draw("(FatJet_mass_nom-cleanFatJet_mass):FatJet_mass_nom"," FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_mass_nom.pdf")
events.Draw("(Jet_mass_nom-cleanJet_mass):Jet_mass_nom","cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_mass_nom.pdf")


# JES up
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_JESup/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

events.Draw("(FatJet_pt_jesTotalUp-cleanFatJet_pt):(FatJet_pt_jesTotalUp-FatJet_pt_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_pt_jesTotalUp.pdf")
events.Draw("(Jet_pt_jesTotalUp-cleanJet_pt):(Jet_pt_jesTotalUp-Jet_pt_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_pt_jesTotalUp.pdf")
events.Draw("(FatJet_mass_jesTotalUp-cleanFatJet_mass):(FatJet_mass_jesTotalUp-FatJet_mass_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_mass_jesTotalUp.pdf")
events.Draw("(Jet_mass_jesTotalUp-cleanJet_mass):(Jet_mass_jesTotalUp-Jet_mass_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_mass_jesTotalUp.pdf")

# JES down
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_JESdn/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

events.Draw("(FatJet_pt_jesTotalDown-cleanFatJet_pt):(FatJet_pt_jesTotalDown-FatJet_pt_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_pt_jesTotalDown.pdf")
events.Draw("(Jet_pt_jesTotalDown-cleanJet_pt):(Jet_pt_jesTotalDown-Jet_pt_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_pt_jesTotalDown.pdf")
events.Draw("(FatJet_mass_jesTotalDown-cleanFatJet_mass):(FatJet_mass_jesTotalDown-FatJet_mass_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_mass_jesTotalDown.pdf")
events.Draw("(Jet_mass_jesTotalDown-cleanJet_mass):(Jet_mass_jesTotalDown-Jet_mass_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_mass_jesTotalDown.pdf")

# JER up
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_JERup/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

events.Draw("(FatJet_pt_jerUp-cleanFatJet_pt):(FatJet_pt_jerUp-FatJet_pt_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_pt_jerUp.pdf")
events.Draw("(Jet_pt_jerUp-cleanJet_pt):(Jet_pt_jerUp-Jet_pt_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_pt_jerUp.pdf")
events.Draw("(FatJet_mass_jerUp-cleanFatJet_mass):(FatJet_mass_jerUp-FatJet_mass_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_mass_jerUp.pdf")
events.Draw("(Jet_mass_jerUp-cleanJet_mass):(Jet_mass_jerUp-Jet_mass_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_mass_jerUp.pdf")

# JER down
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_JERdn/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()

events.Draw("(FatJet_pt_jerDown-cleanFatJet_pt):(FatJet_pt_jerDown-FatJet_pt_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_pt_jerDown.pdf")
events.Draw("(Jet_pt_jerDown-cleanJet_pt):(Jet_pt_jerDown-Jet_pt_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_pt_jerDown.pdf")
events.Draw("(FatJet_mass_jerDown-cleanFatJet_mass):(FatJet_mass_jerDown-FatJet_mass_nom)","FatJet_jetId >= 6 && abs(FatJet_eta) < 2.4 && FatJet_msoftdrop > 20","colz")
c.SaveAs("FatJet_mass_jerDown.pdf")
events.Draw("(Jet_mass_jerDown-cleanJet_mass):(Jet_mass_jerDown-Jet_mass_nom)", "cleanJet_pt > 30 && Jet_jetId >= 6 && (cleanJet_pt > 50 || Jet_puId >= 4)","colz")
c.SaveAs("Jet_mass_jerDown.pdf")
