import ROOT
from ROOT import gROOT
gROOT.SetBatch(True)

# B-tag branches comparison
f = ROOT.TFile("/eos/user/h/hboucham/Haa4b/skim/2L_Z_mu_nom/test_mu_skim.root", "READ")
events = f.Get("event_tree")
c = ROOT.TCanvas()


events.Draw("Jet_btagSF_deepjet_M:RDF_BtagSF_central"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M.pdf")
events.Draw("Jet_btagSF_deepjet_M_up:RDF_BtagSF_Up"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M_up.pdf")
events.Draw("Jet_btagSF_deepjet_M_down:RDF_BtagSF_Down"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M_down.pdf")

events.Draw("Jet_btagSF_deepjet_M_up_correlated:RDF_BtagSF_cor_Up"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M_up_correlated.pdf")
events.Draw("Jet_btagSF_deepjet_M_down_correlated:RDF_BtagSF_cor_Down"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M_down_correlated.pdf")

events.Draw("Jet_btagSF_deepjet_M_up_uncorrelated:RDF_BtagSF_uncor_Up"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M_up_uncorrelated.pdf")
events.Draw("Jet_btagSF_deepjet_M_down_uncorrelated:RDF_BtagSF_uncor_Down"," Jet_pt > 30 && Jet_jetId >= 6 && (Jet_pt > 50 || Jet_puId >= 4) && abs(Jet_eta) < 2.4 && Jet_btagDeepFlavB > 0.2783","colz")
c.SaveAs("Jet_btagSF_deepjet_M_down_uncorrelated.pdf")
