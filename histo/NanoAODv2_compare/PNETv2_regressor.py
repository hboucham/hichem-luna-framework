#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Functions
def createCanvasPads(): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.015)  # joins upper and lower plot
    pad1.SetGrid()
    pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def createRatio(h1, h2, x_axis): # Function to create ratio plot
     h3 = h1.Clone("h3")
     h3.SetLineColor(kBlack)
     h3.SetMarkerStyle(20)
     h3.SetTitle("")
     h3.SetMinimum(.1)
     h3.SetMaximum(2.1)
     # Set up plot for markers and errors
     #h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h2)
 
     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("Data/MC    ")
     y.SetNdivisions(505)
     y.SetTitleSize(25)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.25)
     y.SetLabelFont(43)
     y.SetLabelSize(20)
 
     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitle(x_axis)
     x.SetTitleSize(25)
     x.SetTitleFont(43)
     x.SetTitleOffset(1)
     x.SetLabelFont(43)
     x.SetLabelSize(20)

def FWHM (h):
   bin1 = h.FindFirstBinAbove(h.GetMaximum()/2)
   bin2 = h.FindLastBinAbove(h.GetMaximum()/2)
   fwhm = h.GetBinCenter(bin2) - h.GetBinCenter(bin1)
   return fwhm

def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


plot_type = "lin"
#weight2 = "ZHWeight"

baseline_cut = "FatJet_particleNetMD_Xbb/(FatJet_particleNetMD_Xbb + FatJet_particleNetMD_QCD) > 0.75 && FatJet_pt_nom > 170 && FatJet_jetId >= 6 && FatJet_msoftdrop_nom > 20 && abs(FatJet_eta) < 2.4 && "
additional_cutA = ["abs(FatJet_mass_nom - 120) < 20 && FatJet_nBHadrons > 3", "FatJet_nBHadrons == 3",  "abs(FatJet_mass_nom - 120) < 20 && FatJet_nBHadrons < 3", "FatJet_nBHadrons < 3"]
additional_cutH = ["FatJet_nBHadrons > 3", "FatJet_nBHadrons == 3",  "FatJet_nBHadrons < 3"]

mA = 55
if mA == 15: mf = "36"
if mA == 25: mf = "33"
if mA == 40: mf = "35"
if mA == 55: mf = "31"


# input dir
input_dir = "/eos/cms/store/user/abrinke1/NanoPost/hadd/"
events = "Events"
output_dir = "PNETv2_plots/"
tree_name = "Events"
files = ["GluGluH_M-" + str(mA) + "_Skim_" + mf + "_files.root"]
file_names = ["SUSY_ggH_M-" + str(mA)]


# Linear plots parameters
lin_vars = ["massA", "34massA", "massH_v2"]
lin_xmin = [0, 0, 50]
lin_xmax = [70, 70, 200]
lin_bins = [70, 70, 75]
lin_title = ["PNETv2 Mass a Regressor Comparison", "PNETv2 Mass a 34Regressor Comparison", "PNETv2 Mass H Regressor Comparison"]
lin_xtitle = ["m(a) reg", "m(a) 34reg", "m(H) reg"]


# Log2 plots parameters
log2_vars_1 = ["log2(m_fatjet)", "log2(msoftdrop_fatjet)", "log2(m_cor1_fatjet)", "log2(m_cor2_fatjet)"]
log2_vars_2 = ["log2(msoftdrop_fatjet)", "log2(msoftscaled_fatjet)","log2(msoftdrop_cor1_fatjet)", "log2(msoftdrop_cor2_fatjet)"]
log2_vars = [ log2_vars_1, log2_vars_2]
log2_bins = 80
log2_xmin = 4.5
log2_xmax = 8.5
log2_ytitle = "AK8 Jet log2(mass*) [GeV]"
log2_H_mass = math.log2(125.35)
log2_ymax = 350
log2_rounding = 3

if plot_type == "lin" :
    branches = lin_vars
    bins = lin_bins
    range_min = lin_xmin
    range_max = lin_xmax
    #ymax = lin_ymax
    titles = lin_title
    x_axis = lin_xtitle
    #H_mass = lin_H_mass
    #rounding = lin_rounding
else:
    variables = log2_vars
    bins = log2_bins
    xmin = log2_xmin
    xmax = log2_xmax
    ymax = log2_ymax
    ytitle = log2_ytitle
    H_mass = log2_H_mass
    rounding = log2_rounding

ROOT.gROOT.SetBatch(True) # disable canvas display
ROOT.gStyle.SetOptStat(0) # disable stat box


for i in range(len(branches)):

    if branches[i] == "massH_v2":
        additional_cut = additional_cutH
    else:
        additional_cut = additional_cutA

    for j in range(len(additional_cut)):
        cut = baseline_cut + additional_cut[j]
    
        c = TCanvas("c", "canvas", 800, 700)
        # Upper histogram plot is pad1
        pad1 = TPad("pad1", "pad1", 0, 0.01, 1, 1.0)
        pad1.SetTopMargin(0.12)  
        pad1.SetBottomMargin(0.12)  # joins upper and lower plot
        pad1.SetGrid()
        #pad1.SetLogx() 
        pad1.Draw()
        pad1.cd()

        # Sample 0
        f = TFile(input_dir + files[0], "READ")
        events_S = f.Get(tree_name)

        # reg a
        S0 = TH1D("S0", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw("FatJet_PNet_" + branches[i]+"a>>S0", f"({cut})")
        S0.SetLineColor(kGreen)
        S0.SetLineWidth(2)
        S0.SetLineStyle(1)

        # reg b
        S1 = TH1D("S1", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw("FatJet_PNet_" + branches[i]+"b>>S1", f"({cut})")
        S1.SetLineColor(kRed)
        S1.SetLineWidth(2)
        S1.SetLineStyle(1)
        
        # reg c
        S2 = TH1D("S2", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw("FatJet_PNet_" + branches[i]+"c>>S2", f"({cut})")
        S2.SetLineColor(kBlue)
        S2.SetLineWidth(2)
        S2.SetLineStyle(1)

        # reg d
        S3 = TH1D("S3", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw("FatJet_PNet_" + branches[i]+"d>>S3", f"({cut})")
        S3.SetLineColor(kViolet)
        S3.SetLineWidth(2)
        S3.SetLineStyle(1)

        # reg avg
        if branches[i] == 0:
            S4 = TH1D("S4", titles[i], bins[i], range_min[i], range_max[i])
            events_S.Draw("FatJet_PNet_" + branches[i]+"_avg>>S4", f"({cut})")
            S4.SetLineColor(kBlack)
            S4.SetLineWidth(2)
            S4.SetLineStyle(1)
        else:
            avg_branch = "((FatJet_PNet_" + branches[i]+"a + FatJet_PNet_" + branches[i]+"b + FatJet_PNet_" + branches[i]+"c + FatJet_PNet_" + branches[i]+"d)/4)"
            #print(avg_branch)
            S4 = TH1D("S4", titles[i], bins[i], range_min[i], range_max[i])
            events_S.Draw(avg_branch + ">>S4", f"({cut})")
            S4.SetLineColor(kBlack)
            S4.SetLineWidth(2)
            S4.SetLineStyle(1)
       
        if branches[i] == "massH_v2":
            line = ROOT.TLine(125, 0, 125, S4.GetMaximum())
        else:
            line = ROOT.TLine(mA, 0, mA, S4.GetMaximum())
        line.SetLineColor(kOrange)
        line.SetLineWidth(2)
        line.SetLineStyle(2)
        
        # Stylistic adjustments of regular plot through SD histo
        SD_y = S0.GetYaxis()
        SD_x = S0.GetXaxis()

        # Adjusting y axis range as needed for each variable (especially log)
        # Manually adjust both max and min
        #SD_y.SetRangeUser(1, 100000)
        
        # Set Minumum for Iso plots specifically
        #if (i > 34 and i < 47): # Isolation plots
        #    SD.SetMinimum(10)
        
        # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
        # This is mainly done to make space for the legend and other text
        S0.SetMaximum(S0.GetMaximum() + 0.5* (S0.GetMaximum()-S0.GetMinimum())) 
        gStyle.SetTitleFontSize(0.05) # Title size

        # x-axis (not necessary since hidden by ratio plot) 
        SD_x.SetTitle(x_axis[i])
        SD_x.SetTitleSize(30) 
        SD_x.SetTitleFont(43) 
        SD_x.SetTitleOffset(1.25)
        SD_x.SetLabelFont(43) # font and size of numbers
        SD_x.SetLabelSize(25)
        #SD_x.SetLabelOffset(0.3)

        # y-axis
        SD_y.SetTitle("                 Events") # space used for offset
        SD_y.SetNdivisions(505) #make divisions nice
        SD_y.SetTitleFont(43) 
        SD_y.SetTitleSize(25)
        SD_y.SetTitleOffset(1.35)
        SD_y.SetLabelFont(43) # font and size of numbers
        SD_y.SetLabelSize(20)

        gPad.Modified() # Update gpad (things might break depending on where this line is)
            
        # Drawing everything
        S0.Draw("HISTO")
        S1.Draw("SAME HISTO") 
        S2.Draw("SAME HISTO") 
        S3.Draw("SAME HISTO") 
        S4.Draw("SAME HISTO") 
        line.Draw("SAME") 
        
        # draw line for one of the plots 
        if (branches[i] == "ZH_dphi"):
            line = TLine(2.75, 0.,2.75, S0.GetMaximum())
            line.SetLineColor(kBlack)
            line.SetLineWidth(1)
            line.SetLineStyle(2)    
            line.Draw("SAME")
            t2 = TLatex(0.77,0.09,"7#pi/8")
            MinorTextFormat(t2)
            #print(str(S0.GetBinContent(bins[i])) + ", " + str(S0.GetBinContent(bins[i]-1)) + ", " + str(S0.GetBinContent(bins[i]-2)) + ", " + str(S0.GetBinContent(bins[i]-3)) + ", " + str(S0.GetBinContent(bins[i]-4)))
            #print(str(S1.GetBinContent(bins[i])) + ", " + str(S1.GetBinContent(bins[i]-1)) + ", " + str(S1.GetBinContent(bins[i]-2)) + ", " + str(S1.GetBinContent(bins[i]-3)) + ", " + str(S1.GetBinContent(bins[i]-4)))
            y0 = round(100*(S0.GetBinContent(bins[i]) + S0.GetBinContent(bins[i]-1) + S0.GetBinContent(bins[i]-2) + S0.GetBinContent(bins[i]-3) + 0.6*S0.GetBinContent(bins[i]-4)),2)
            y1 = round(100*(S1.GetBinContent(bins[i]) + S1.GetBinContent(bins[i]-1) + S1.GetBinContent(bins[i]-2) + S1.GetBinContent(bins[i]-3) + 0.6*S1.GetBinContent(bins[i]-4)),2)
            y2 = round(100*(S2.GetBinContent(bins[i]) + S2.GetBinContent(bins[i]-1) + S2.GetBinContent(bins[i]-2) + S2.GetBinContent(bins[i]-3) + 0.6*S2.GetBinContent(bins[i]-4)),2) 
            y3 = round(100*(S3.GetBinContent(bins[i]) + S3.GetBinContent(bins[i]-1) + S3.GetBinContent(bins[i]-2) + S3.GetBinContent(bins[i]-3) + 0.6*S3.GetBinContent(bins[i]-4)),2)
            t3 = TLatex(0.83,0.23, "#color[1]{" +str(y0) + "%}") # black is color[1]
            MinorTextFormat(t3)
            t6 = TLatex(0.83,0.205, "#color[6]{" +str(y3) + "%}") # blue is color[4]
            MinorTextFormat(t6) 
            t4 = TLatex(0.83,0.18, "#color[2]{" +str(y1) + "%}") # red is color[2]
            MinorTextFormat(t4)
            t5 = TLatex(0.83,0.155, "#color[4]{" +str(y2) + "%}") # blue is color[4]
            MinorTextFormat(t5) 
             

        # Legend
        leg=TLegend(.13,.8,.89,.87) #(x_min, y_min, x_max, y_max)
        #leg=TLegend(.38,.75,.88,.87) #(x_min, y_min, x_max, y_max)
        #leg=TLegend() # automatic legend placement sucks
        leg.SetNColumns(3) # number of columns for legend
        leg.SetBorderSize(0)
        leg.SetFillColor(0)
        leg.SetFillStyle(0)
        leg.SetTextFont(22)
        leg.SetTextSize(0.03)
        S0_leg = "reg " + branches[i] + " a"
        S1_leg = "reg " + branches[i] + " b"
        S2_leg = "reg " + branches[i] + " c"
        S3_leg = "reg " + branches[i] + " d"
        S4_leg = "reg " + branches[i] + " avg"
        leg.AddEntry(S0, S0_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
        leg.AddEntry(S1, S1_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
        leg.AddEntry(S2, S2_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
        leg.AddEntry(S3, S3_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
        leg.AddEntry(S4, S4_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
        leg.Draw("SAME")

        # CMS stuff
        t1 = TLatex(0.5,0.885," #bf{CMS} #it{Simulation}                                                     13 TeV ")
        t1.SetNDC()
        t1.SetTextFont(42)
        t1.SetTextSize(0.04)
        t1.SetTextAlign(20)
        t1.Draw("SAME")
        
        sample_text = TLatex(0.55, 0.71, f"#bf{{{file_names[0]}}}") 
        sample_text.SetNDC()
        sample_text.SetTextFont(42)
        sample_text.SetTextSize(0.03)
        sample_text.Draw("SAME")
       
        cut_text = TLatex(0.15, 0.76, f"#bf{{{additional_cut[j]}}}") 
        cut_text.SetNDC()
        cut_text.SetTextFont(42)
        cut_text.SetTextSize(0.025)
        cut_text.Draw("SAME")

        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        #borderline = TLine(range_max[i], 1.0,range_max[i], S0.GetMaximum())
        #borderline.SetLineColor(kBlack)
        #borderline.SetLineWidth(1)
        #borderline.Draw("SAME")

        # Save individual pdf/png 
        #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
        #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")
        # Save one pdf with all canvas
        if i == 0 and j == 0:
            c.Print(output_dir+ "PNETv2reg_" + file_names[0] + "_plot.pdf(", "Title: " + branches[i])
        elif i == (len(branches)-1) and j == (len(additional_cut)-1):
            c.Print(output_dir+ "PNETv2reg_" + file_names[0] + "_plot.pdf)", "Title: " + branches[i])
        else:
            c.Print(output_dir+ "PNETv2reg_" + file_names[0] + "_plot.pdf", "Title: " + branches[i])
        # Delete all histos to avoid warning when initializing histos with same name
        #S0.SetDirectory(0)
        #S1.SetDirectory(0)
        c.Close() # Delete Canvas after saving it
