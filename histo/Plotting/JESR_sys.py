#!/usr/bin/env python

import sys
import math
import os
import os.path

import numpy as np
#import matplotlib.pyplot as plt
#import mplhep as hep
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex 
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite

from helpers.ratioStatError import getRatioStatError

# Functions
def createCanvasPads(logs): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.015)  # joins upper and lower plot
    pad1.SetGrid()
    if (logs):
        pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def createRatio(h1, h2, x_axis): # Function to create ratio plot
     h3 = h1.Clone("h3")
     h3.SetLineColor(kBlack)
     h3.SetMarkerStyle(20)
     h3.SetTitle("")
     h3.SetMinimum(.4)
     h3.SetMaximum(1.6)
     # Set up plot for markers and errors
     #h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h2)
 
     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("Var/Nom    ")
     y.SetNdivisions(505)
     y.SetTitleSize(25)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.25)
     y.SetLabelFont(43)
     y.SetLabelSize(20)
 
     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitle(x_axis)
     x.SetTitleSize(25)
     x.SetTitleFont(43)
     x.SetTitleOffset(1)
     x.SetLabelFont(43)
     x.SetLabelSize(20)
 
     return h3

# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")

def MinorTextFormat_ratio(t):
    t.SetNDC()
    t.SetTextFont(43)
    t.SetTextSize(15)
    t.Draw("SAME")

# CMS Official Colors
CMS_colors = {
    "blue1": ROOT.TColor.GetColor("#3f90da"),
    "orange1": ROOT.TColor.GetColor("#ffa90e"),
    "red": ROOT.TColor.GetColor("#bd1f01"),
    "grey1": ROOT.TColor.GetColor("#94a4a2"),
    "purple": ROOT.TColor.GetColor("#832db6"),
    "brown": ROOT.TColor.GetColor("#a96b59"),
    "orange2": ROOT.TColor.GetColor("#e76300"),
    "grey2": ROOT.TColor.GetColor("#b9ac70"),
    "grey3": ROOT.TColor.GetColor("#717581"),
    "blue2": ROOT.TColor.GetColor("#92dadd"),
}



# init cuts
h_blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
z_peak = "(m_ll > 88) && (m_ll < 94)" 
z_tail = "((m_ll < 88) && (m_ll > 80)) || ((m_ll < 100) && (m_ll > 94))" 

# command inputs
#  python3 Data_MC_plot_2L_Z.py mu/el log/lin scale/noscale 0/PNETcut date
channel = sys.argv[1]
syst = sys.argv[2]
massA = sys.argv[3]
PNET_score = sys.argv[4]
date = sys.argv[5]

sc = "lin"
if (sc == "log"):
    scale_log = True
else:
    scale_log = False

###################################################################################################
############### Manually adjust PNET_score ########################################################
#PNET_score = "0"
#PNET_score = "0.5"
################Manually adjust cut ###############################################################
#cut = ""
cut_add = ""
#cut_add = "dphi_MET_l1 < 0.2"
#cut = "score_Haa4b_vs_QCD > 0.5"
#cut = "dphi_MET_l1 < 0.2" pt_ll (150)   pt_fatjet (250)   dphi_l1_l2 (1.5) 
#cut = "score_Haa4b_vs_QCD > 0.5"
################Manually adjust date ###############################################################
#date = "081224cs"
###################################################################################################

dataScale = ""

if PNET_score == "0":
    cut = ""
else:
    cut = "score_Haa4b_vs_QCD > " + PNET_score

if cut_add:
    cut = cut + " && " + cut_add

# input/output directory and tree name
#input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_" + channel + "_" + syst + "/"
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/"
output_dir = "../plots"
tree_name = "event_tree"
weight = "weight_tot"  

if cut:
    draw_option = f"({cut})*({weight})"
else:
    draw_option = weight

sig_scaling = 1
    
# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1


# signal: [M-15, M-30, M-55]
Signal_xsec = [0.11352, 0.11352, 0.11352]
Signal_ev = [106743, 101445, 95365]
Signal_scale =[]
for i in range(len(Signal_xsec)):
    Signal_scale.append(lumi*Signal_xsec[i]/Signal_ev[i])
if massA == "15":
    massA_scale = Signal_scale[0]
if massA == "30":
    massA_scale = Signal_scale[1]
if massA == "55":
    massA_scale = Signal_scale[2]

# Histo variables, labels, range and bins
# Kinematic plots ordering: AK8 jet (including mA), lepton pair, leptons, MET, AK4

branches = [ "m_fatjet", "msoftdrop_fatjet", "msoftscaled_fatjet", "mtopo_fatjet", "msofttopo_fatjet", "mH_avg", "mA_avg", "pt_fatjet", "eta_fatjet", "phi_fatjet", "m_ll", "pt_ll", "eta_ll", "phi_ll", "deta_fatjet_ll", "dphi_fatjet_ll", "pt_l1", "eta_l1", "phi_l1", "pt_l2", "eta_l2", "phi_l2", "dphi_l1_l2", "dr_l1_l2", "ngoodMuons", "ngoodElectrons","pt_MET", "phi_MET", "dphi_MET_fatjet", "dphi_MET_ll", "dphi_MET_l1","dphi_MET_l2", "mt_MET_ll", "mt_MET_l1", "mt_MET_l2", "HT_LHE", "nBJetM_add", "nJet_add", "nJetCent_add", "pt_SSum", "pt_VSum", "pt_jet1", "eta_jet1", "phi_jet1",  "pt_jet2", "eta_jet2", "phi_jet2", "pt_bjet1", "eta_bjet1", "phi_bjet1",  "pt_bjet2", "eta_bjet2", "phi_bjet2", "dphi_l1_j1", "dphi_l1_b1", "log2(abs(dxy_l1))", "log2(abs(dz_l1))", "log2(abs(ip3d_l1))", "log2(jetPtRelv2_l1)", "log2(jetRelIso_l1)", "log2(abs(dxy_l2))", "log2(abs(dz_l2))", "log2(abs(ip3d_l2))", "log2(jetPtRelv2_l2)", "log2(jetRelIso_l2)"] 

x_axis = ["m(AK8) [GeV]", "m_{SoftDrop}(AK8) [GeV]", "m_{SoftScaled}(AK8) [GeV]", "m_{Topo}(AK8) [GeV]", "m_{SoftTopo}(AK8) [GeV]", "mH_{avg} [GeV]", "mA_{avg} [GeV]", "p_{T}(AK8) [GeV]", "#eta (AK8)", "#phi (AK8)", "m(ll) [GeV]", "p_{T}(ll) [GeV]", "#eta(ll)", "#phi(ll)", "|#Delta#eta(AK8, ll)|", "|#Delta#phi(AK8, ll)|", "p_{T}(l1) [GeV]", "#eta(l1)", "#phi(l1)", "p_{T}(l2) [GeV]", "#eta(l2)", "#phi(l2)", "|#Delta#phi(l1, l2)|","#DeltaR(l1, l2)", "Good Muons", "Good Electrons", "p_{T}(MET) [GeV]", "#phi(MET)", "|#Delta#phi(MET, AK8)|", "|#Delta#phi(MET, ll)|", "|#Delta#phi(MET, l1|", "|#Delta#phi(MET, l2|", "M_{T}(MET, ll) [GeV]", "M_{T}(MET, l1) [GeV]", "M_{T}(MET, l2) [GeV]", "H_{T}(LHE)", "Nadd AK4 b-Jet(M)", "N_{extra}(AK4)", "N_{extra}(central AK4) ", "#Sigma_{i}^{Nextra}(p_{T}(AK4))", "#Sigma_{i}^{Nextra}(p_{T}(AK4_vec))", "p_{T}(j1) [GeV]", "#eta(j1)", "#phi(j1)", "p_{T}(j2) [GeV]", "#eta(j2)", "#phi(j2)", "p_{T}(b1) [GeV]", "#eta(b1)", "#phi(b1)", "p_{T}(b2) [GeV]", "#eta(b2)", "#phi(b2)", "|#Delta#phi(l1, j1)|", "|#Delta#phi(l1, b1)|", "log2|dxy(l1)| [cm]", "log2|dz(l1)| [cm]", "log2|ip3d(l1)| [cm]", "log2(jetPtRelv2(l1))", "log2(jetRelIso(l1))", "log2|dxy(l2)| [cm]", "log2|dz(l2)| [cm]", "log2|ip3d(l2)| [cm]", "log2(jetPtRelv2(l2))", "log2(jetRelIso(l2))"]


range_min = [50, 50, 50, 50, 50, 50, 2.5, 150, -3, -3.2, 80, 0, -3, -3.2, 0, 0, 0, -3, -3.2, 0, -3, -3.2, 0, 0, -0.5, -0.5, 0, -3.2, 0, 0, 0, 0, 0, 0, 0, 0, -0.5, -0.5, -0.5, 0, 0, 30, -3, -3.2, 30, -3, -3.2, 30, -3, -3.2, 30, -3, -3.2, 0, 0, -15, -15, -13, -1, -14, -15, -15, -13, -1, -14]


range_max = [200, 200, 200, 200, 200, 200, 72.5, 650, 3, 3.2, 100, 600, 3, 3.2, 4, 3.2, 300, 3, 3.2, 300, 3, 3.2, 3.2, 3.2, 5.5, 5.5, 300, 3.2, 3.2, 3.2, 3.2, 3.2, 400, 300, 300, 3000, 3.5, 7.5, 7.5, 1000, 1000, 530, 3, 3.2, 530, 3, 3.2, 530, 3, 3.2, 530, 3, 3.2, 3.2, 3.2, 0, 5, -3, 9, 4, 0, 5, -3, 9, 4]


bins = [15, 15, 15, 15, 15, 15, 14, 25, 15, 16, 20, 30, 15, 16, 20, 16, 15, 20, 16, 15, 15, 16, 16, 16, 6, 6, 15, 16, 16, 16, 32, 16, 20, 15, 15, 30, 4, 8, 8, 20, 20, 25, 15, 16, 25, 15, 16, 25, 15, 16, 25, 15, 16, 16, 16, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50]

titles = ["Leading Fat Jet Mass", "Leading Fat Jet SoftDrop Mass", "Leading Fat Jet p_{T}-Scaled SoftDrop Mass", "Leading Fat Jet ZH-topology corrected Mass", "Leading Fat Jet ZH-topology corrected SoftDrop Mass", "PNET Average Regressed Mass H #rightarrow aa #rightarrow 4b", "PNET Average Regressed Mass a #rightarrow bb", "Leading Fat Jet p_{T}", "Leading Fat Jet #eta", "Leading Fat Jet #phi", "Lepton Pair Mass", "Lepton Pair p_{T}", "Lepton Pair #eta", "Lepton Pair #phi", "#Delta#eta(Leading Fat Jet, Lepton Pair)", "#Delta#phi(Leading Fat Jet, Lepton Pair)", "Lepton_1 p_{T}", "Lepton_1 #eta", "Lepton_1 #phi", "Lepton_2 p_{T}", "Lepton_2 #eta", "Lepton_2 #phi", "#Delta#phi(Lepton_1, Lepton_2)", "#DeltaR(Lepton_1, Lepton_2)", "Number of Muons passing Loose Selection", "Number of Electrons passing Loose Selection", "MET p_{T}", "MET #phi", "#Delta#phi(MET, Leading Fat Jet)", "#Delta#phi(MET, Lepton Pair)", "#Delta#phi(MET, Lepton_1)", "#Delta#phi(MET, Lepton_2)", "Transverse Mass (MET, Lepton Pair)", "Transverse Mass (MET, Lepton_1)", "Transverse Mass (MET, Lepton_2)", "Scalar Sum of Parton pTs (HT) at LHE Step", "Number of Additional B-tagged (M) AK4 Jets", "Number of Additional AK4 Jets", "Number of Additional Central AK4 Jets", "Scalar Sum of all Additional AK4 Jets", "Vector Sum of all Additional AK4 Jets", "Leading-p_{T} Additional AK4 Jet p_{T}", "Leading-p_{T} Additional AK4 Jet #eta", "Leading-p_{T} Additional AK4 Jet #phi", "Sub-leading-p_{T} Additional AK4 Jet p_{T}", "Sub-leading-p_{T} Additional AK4 Jet #eta", "Sub-leading-p_{T} Additional AK4 Jet #phi", "Leading-p_{T} Additional AK4 B-Jet p_{T}", "Leading-p_{T} Additional AK4 B-Jet #eta", "Leading-p_{T} Additional AK4 B-Jet #phi", "Sub-leading-p_{T} Additional AK4 B-Jet p_{T}", "Sub-leading-p_{T} Additional AK4 B-Jet #eta", "Sub-leading-p_{T} Additional AK4 B-Jet #phi", "#Delta#phi(Lepton_1, Leading-p_{T} Jet)", "#Delta#phi(Lepton_1, Leading-p_{T} B-Jet)", "Lepton 1 dxy (with sign) with respect to first PV", "Lepton 1 dz (with sign) with respect to First PV", "Lepton 1 3D Impact Parameter with respect to First PV", "Relative Momentum of Lepton 1 with respect to the Closest Jet after Subtracting the Lepton", "Lepton 1 Relative Isolation in Matched Jet (1/ptRatio-1, pfRelIso04_all if no Matched Jet)", "Lepton 2 dxy (with sign) with respect to first PV", "Lepton 2 dz (with sign) with respect to First PV", "Lepton 2 3D Impact Parameter with respect to First PV", "Relative Momentum of Lepton 2 with respect to the Closest Jet after Subtracting the Lepton", "Lepton 2 Relative Isolation in Matched Jet (1/ptRatio-1, pfRelIso04_all if no Matched Jet)"]


gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box



for i in range(len(branches)):
    # Cutting events with msoftdrop in Higgs range [100, 140] GeV for branches
    # other than Leading Fat Jet mass/ softdrop_mass / softscale mass / mH_avg,
    # bins in that range are cuted later in the plotting script
  # if (i != 0) and (i != 10) and (i != 12):
  #   blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
    
    
    # Creating Canvas
    c, pad1, pad2 = createCanvasPads(scale_log)
    pad1.cd() # pad1 is for regular plot
    

    # Creating Canvas
    #c = TCanvas("c", "canvas", 800, 800)
    #c.SetTopMargin(0.11)
    #c.SetRightMargin(0.13)
    #c.SetLeftMargin(0.13)
    #c.SetGrid()
    
    # Adding signal samples
    # ZH syst nom
    fS1 = TFile(input_dir+ "samples_2L_Z_" + channel + "/SUSY_ZH_M-" + massA + ".root", "READ")
    events_S1 = fS1.Get(tree_name)
    S1 = TH1D("S1", titles[i], bins[i], range_min[i], range_max[i])
    events_S1.Draw(branches[i]+">>S1", draw_option)
    #S1.Scale(1./S1.Integral())  # can be used to normalize area under histo to 1
    S1.Scale(massA_scale)
    #S1.SetLineColor(kMagenta)
    S1.SetLineColor(CMS_colors["orange1"])
    S1.SetLineWidth(2)
    S1.SetLineStyle(1)

    # ZH syst up
    fS2 = TFile(input_dir+ "samples_2L_Z_" + channel + "_" + syst + "up" + "/SUSY_ZH_M-" + massA + ".root", "READ")
    events_S2 = fS2.Get(tree_name)
    S2 = TH1D("S2", titles[i], bins[i], range_min[i], range_max[i])
    events_S2.Draw(branches[i]+">>S2", draw_option)
    S2.Scale(massA_scale)
    S2.SetLineColor(CMS_colors["purple"])
    #S2.SetLineColor(kOrange)
    S2.SetLineWidth(2)
    S2.SetLineStyle(1)

    # ZH syst dn
    fS3 = TFile(input_dir+ "samples_2L_Z_" + channel + "_" + syst + "dn" + "/SUSY_ZH_M-" + massA + ".root", "READ")
    events_S3 = fS3.Get(tree_name)
    S3 = TH1D("S3", titles[i], bins[i], range_min[i], range_max[i])
    events_S3.Draw(branches[i]+">>S3", draw_option)
    S3.Scale(massA_scale)
    S3.SetLineColor(CMS_colors["blue1"])
    S3.SetLineWidth(2)
    S3.SetLineStyle(1)
    

    # Stylistic adjustments of regular plot through SD histo
    SD_y = S1.GetYaxis()
    SD_x = S1.GetXaxis()

    # Adjusting y axis range as needed for each variable (especially log)
    # Manually adjust both max and min
    #SD_y.SetRangeUser(1, 100000)
    
    # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
    # This is mainly done to make space for the legend and other text
    S1.SetMinimum(0)
    S1.SetMaximum(1.5*S1.GetMaximum())
    
    gStyle.SetTitleFontSize(0.05) # Title size
    
    # x-axis (not necessary since hidden by ratio plot) 
    SD_x.SetTitle(x_axis[i])
    SD_x.SetTitleSize(0.05)
    SD_x.SetTitleOffset(1.5)
    SD_x.SetLabelOffset(0.3)

    # y-axis
    SD_y.SetTitle("Events  ") # space used for offset
    SD_y.SetNdivisions(505) #make divisions nice
    SD_y.SetTitleSize(25) 
    SD_y.SetTitleFont(43) 
    SD_y.SetTitleOffset(1.25)
    SD_y.SetLabelFont(43) # font and size of numbers
    SD_y.SetLabelSize(20)


    gPad.Modified() # Update gpad (things might break depending on where this line is)
    
    # Drawing everything
    S1.Draw("HIST")
    S2.Draw("SAME HIST")
    S3.Draw("SAME HIST")
   
    ############# RATIO TEXT SECTION: START ###########################################################################
    # Ratio plot
    pad2.cd()# Go to pad2 for ratio plot
    
    # Histo for ratio plot
    RATIO1 = createRatio(S2, S1, x_axis[i])
    RATIO1.SetLineColor(CMS_colors["purple"])
    RATIO2 = createRatio(S3, S1, x_axis[i])
    RATIO2.SetLineColor(CMS_colors["blue1"])
    # Histo for statistical errors accounting for denominator's statistical erorr
    #RATIO.Draw("ep") # draw with stat errors (but doesn't account for denominator statistical error)
    
    # Ratio histo Stylistic stuff
    R_x = RATIO1.GetXaxis()
    R_x.SetTitleSize(25) 
    R_x.SetTitleFont(43) 
    R_x.SetTitleOffset(1.15)
    R_x.SetLabelFont(43) # font and size of numbers
    R_x.SetLabelSize(20)
    R_x.SetLabelOffset(.02)

    # Draw line for y = 1 to help compare ratio
    line = TLine(range_min[i], 1.,range_max[i], 1.)
    line.SetLineColor(kBlack)
    line.SetLineWidth(1)
   
    # Drawing histos in ratio pad
    RATIO1.Draw("HIST")
    RATIO2.Draw("HIST SAME")
    line.Draw("SAME")

    # Redraw Axis in case they are covered
    pad2.RedrawAxis()

    ############# RATIO TEXT SECTION: END ###########################################################################

    # Legend and other text
    pad1.cd() # go back to pad1
    
    # Legend
    leg=TLegend(.13,.74,.89,.84) #(x_min, y_min, x_max, y_max)
    #leg=TLegend(.38,.75,.88,.87) #(x_min, y_min, x_max, y_max)
    #leg=TLegend() # automatic legend placement sucks
    leg.SetNColumns(1) # number of columns for legend
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(22)
    leg.SetTextSize(0.03)
    leg.AddEntry(S1,"ZH_Haa4b_M-" + massA + "_" + syst +"_nom","L")
    leg.AddEntry(S2,"ZH_Haa4b_M-" + massA + "_" + syst +"_up","L")
    leg.AddEntry(S3,"ZH_Haa4b_M-" + massA + "_" + syst +"_down","L")
    leg.Draw("SAME")

    ############# TEXT SECTION: START ###########################################################################
    
    # CMS stuff
    if (scale_log == False and PNET_score == "0"):
        tex = TLatex(0.53,0.88,"#bf{CMS} #it{Simulation}                                                                13 TeV")
    else:
        tex = TLatex(0.51,0.88,"#bf{CMS} #it{Simulation}                                                                           13 TeV  ")    
    tex.SetNDC()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetTextAlign(20)
    tex.Draw("SAME")
    
    # CMS stuff
    #tex = TLatex(0.53,0.88,"  #bf{CMS} #it{Work in Progress}                            59.83 fb^{-1} (13 TeV, 2018)")
    #tex.SetNDC()
    #tex.SetTextFont(42)
    #tex.SetTextSize(0.04)
    #tex.SetTextAlign(20)
    #tex.Draw("SAME")

    # Channel
    if (channel =="mu"):
        chan = TLatex(0.15,0.84,"  #bf{Di-muon}")
    else:
        chan = TLatex(0.15,0.84,"  #bf{Di-electron}")
    chan.SetNDC()
    chan.SetTextFont(42)
    chan.SetTextSize(0.04)
    chan.Draw("SAME")
    
    # PNET score "score_Haa4b_vs_QCD"
    if PNET_score == "0":
        t_score = TLatex(0.4,0.835,"No score_Haa4b_vs_QCD cut")
    elif PNET_score == "fail":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD < 0.5")
    elif PNET_score == "Fail_WP40-80":
        t_score = TLatex(0.4,0.835,"0.5 < score_Haa4b_vs_QCD < 0.992")
    elif PNET_score == "pass":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD > 0.8")
    else:    
        t_score = TLatex(0.4,0.845,"score_Haa4b_vs_QCD > "+ PNET_score)
    MinorTextFormat(t_score)
    
    if (cut_add):
        t_cat = TLatex(0.4,0.855, cut_add)
        MinorTextFormat(t_cat)



    ####################################### TEXT SECTION: END ###############################################

    # Redraw axis + border since they are hidden by histo fills
    ROOT.gPad.RedrawAxis() 
    borderline = TLine(range_max[i], 1.0,range_max[i], S1.GetMaximum())
    borderline.SetLineColor(kBlack)
    borderline.SetLineWidth(1)
    borderline.Draw("SAME")
    
    # Save individual pdf/png

    #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
    #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")

    # Save one pdf with all canvas
    if i == 0:
        c.Print(output_dir+ "/JESR_signal_" + channel + "_" + syst + "_" + massA + "_" + date + ".pdf(", "Title: " + branches[i])
    elif i == len(branches)-1:
    #elif i == 6:
        c.Print(output_dir+ "/JESR_signal_" + channel + "_" + syst + "_" + massA + "_" + date + ".pdf)", "Title: " + branches[i])
    else:
        c.Print(output_dir+ "/JESR_signal_" + channel + "_" + syst + "_" + massA + "_" + date + ".pdf", "Title: " + branches[i])
    #if i == 0:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf(", "Title: " + branches[i])
    #elif i == len(branches)-1:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf)", "Title: " + branches[i])
    #else:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf", "Title: " + branches[i])
    # Delete all histos to avoid warning when initializing histos with same name
    #B0.SetDirectory(0)
    c.Close() # Delete Canvas after saving it

# Loops back to the next variable to plot
