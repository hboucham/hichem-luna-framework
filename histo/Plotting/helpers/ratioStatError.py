import argparse
import numpy as np
import ROOT


def getRatioStatError(hNumerator, hDenominator):
    """
    Return the ratio of two TH1Fs as a TGraphAsymmError, where the numerator does NOT have uncertainties, and
    the denominator does.
    e.g. in ratio of observed/expected events.

    Originally by https://github.com/pallabidas/MetStudiesMiniAOD/blob/master/GammaJets/Plot/staterror.h.
    and adapted to python by https://gitlab.cern.ch/skkwan/lunaFramework/-/blob/main/dataMCPlots/helpers/ratioStatError.py
    """

    # Include overflow
    nBins = hNumerator.GetNbinsX() + 1

    # TGraphAsymmErrors by dividing two input TH1 histograms does not work because it assumes a pass/total situation
    # where number of passing entries is always less than the number of total entries.
    # So we build the TGraphAsymm by hand, with six arrays

    x = np.zeros(nBins)
    y = np.zeros(nBins)

    xErrLow = np.zeros(nBins)
    yErrLow = np.zeros(nBins)

    xErrHigh = np.zeros(nBins)
    yErrHigh = np.zeros(nBins)

    # Central ratio as TH1F
    hRatio = hNumerator.Clone("hRatio")
    hRatio.Divide(hDenominator)

    # Calculate ratio errors by bin
    for iBin in range(nBins):
        ratioNominal = hRatio.GetBinContent(iBin)

        x[iBin] = hRatio.GetBinCenter(iBin)
        y[iBin] = ratioNominal

        # Set x errors to zero because we only want to plot the y-axis errors
        xErrLow[iBin] = 0  # hRatio.GetBinWidth(iBin) / 2
        xErrHigh[iBin] = 0  # hRatio.GetBinWidth(iBin) / 2

        # By error propagation of ratio = numerator/denominator, assuming numerator does not have uncertainties:
        # sigma_ratio = ratio * (sigma_denominator / denominator)
        sigma_denominator = hDenominator.GetBinError(iBin)
        denominator = hDenominator.GetBinContent(iBin)

        if denominator != 0:
            sigma_ratio = ratioNominal * (sigma_denominator / denominator)
            yErrLow[iBin] = sigma_ratio
            yErrHigh[iBin] = sigma_ratio
        else:
            sigma_ratio = 0
            yErrLow[iBin] = 0
            yErrHigh[iBin] = 0
        '''
        print(
            iBin,
            x[iBin],
            y[iBin],
            yErrLow[iBin],
            yErrHigh[iBin],
            sigma_denominator,
            denominator,
        )
        '''
    hAsymmErr = ROOT.TGraphAsymmErrors(
        nBins, x, y, xErrLow, xErrHigh, yErrLow, yErrHigh
    )

    return hAsymmErr

