#!/bin/bash

# Define parameters
SUBCATEGORY="r3" # at least 1 bjet
YAXIS="lin" # linear y scale
ADDCUT="" # additional cuts
DATE_NO_PNET="012025" # date to keep track of version
DATE_PNET="012025c" # same but with PNET tagger cut and scaling MC to data
DATE_PNET_SCALE="012025cs" # same but with PNET tagger cut and scaling MC to data
CHANNELS=("mu mu" "mu el" "el el" "el mu") # channels

# Run for No PNET cut
#for CH in "${CHANNELS[@]}"; do
#  IFS=" " read -r CHANNEL_A CHANNEL_B <<< "$CH"
#  python3 Data_MC_plot_2L_tt.py --channelsA="$CHANNEL_A" --channelsB="$CHANNEL_B" --subcategory="$SUBCATEGORY" --yaxis="$YAXIS" --MCscale="noscale" --score="0" --date="$DATE_NO_PNET" --addcut="$ADDCUT"
#done

# Run for PNET cut > 0.5
for CH in "${CHANNELS[@]}"; do
  IFS=" " read -r CHANNEL_A CHANNEL_B <<< "$CH"
  python3 Data_MC_plot_2L_tt.py --channelsA="$CHANNEL_A" --channelsB="$CHANNEL_B" --subcategory="$SUBCATEGORY" --yaxis="$YAXIS" --MCscale="noscale" --score="0.5" --date="$DATE_PNET" --addcut="$ADDCUT"
done

# Run for PNET cut > 0.5 and scale MC to data
#for CH in "${CHANNELS[@]}"; do
#  IFS=" " read -r CHANNEL_A CHANNEL_B <<< "$CH"
#  python3 Data_MC_plot_2L_tt.py --channelsA="$CHANNEL_A" --channelsB="$CHANNEL_B" --subcategory="$SUBCATEGORY" --yaxis="$YAXIS" --MCscale="scale" --score="0.5" --date="$DATE_PNET_SCALE" --addcut="$ADDCUT"
#done

# No PNET cut
#python3 Data_MC_plot_2L_tt.py --channelsA="mu" --channelsB="mu" --subcategory="r3" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_2L_tt.py --channelsA="mu" --channelsB="el" --subcategory="r3" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_2L_tt.py --channelsA="el" --channelsB="el" --subcategory="r3" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_2L_tt.py --channelsA="el" --channelsB="mu" --subcategory="r3" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""

# PNET cut > 0.5
#python3 Data_MC_plot_2L_tt.py --channelsA="mu" --channelsB="mu" --subcategory="r3" --yaxis="lin" --MCscale="scale" --score="0.5" --date="112424cs" --addcut=""
#python3 Data_MC_plot_2L_tt.py --channelsA="mu" --channelsB="el" --subcategory="r3" --yaxis="lin" --MCscale="scale" --score="0.5" --date="112424cs" --addcut=""
#python3 Data_MC_plot_2L_tt.py --channelsA="el" --channelsB="el" --subcategory="r3" --yaxis="lin" --MCscale="scale" --score="0.5" --date="112424cs" --addcut=""
#python3 Data_MC_plot_2L_tt.py --channelsA="el" --channelsB="mu" --subcategory="r3" --yaxis="lin" --MCscale="scale" --score="0.5" --date="112424cs" --addcut=""

