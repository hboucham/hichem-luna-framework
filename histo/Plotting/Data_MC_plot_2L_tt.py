#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array
import argparse
import os.path

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex 
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite

from helpers.ratioStatError import getRatioStatError

# Functions
def createCanvasPads(logs): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.015)  # joins upper and lower plot
    pad1.SetGrid()
    if (logs):
        pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def createRatio(h1, h2, x_axis): # Function to create ratio plot
     h3 = h1.Clone("h3")
     h3.SetLineColor(kBlack)
     h3.SetMarkerStyle(20)
     h3.SetTitle("")
     h3.SetMinimum(.1)
     h3.SetMaximum(2.1)
     # Set up plot for markers and errors
     #h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h2)
 
     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("Data/MC    ")
     y.SetNdivisions(505)
     y.SetTitleSize(25)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.25)
     y.SetLabelFont(43)
     y.SetLabelSize(20)
 
     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitle(x_axis)
     x.SetTitleSize(25)
     x.SetTitleFont(43)
     x.SetTitleOffset(1)
     x.SetLabelFont(43)
     x.SetLabelSize(20)
 
     return h3

# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")

def MinorTextFormat_ratio(t):
    t.SetNDC()
    t.SetTextFont(43)
    t.SetTextSize(15)
    t.Draw("SAME")

def HistoFill(path, name, scale):
    #h = TH1D(hname, htitle, hbins, hmin, hmax)
    if (os.path.isfile(path)):
        print(path + " exist!!!")
        f_h = TFile(path, "READ")
        events_h = f_h.Get(tree_name)
        h = TH1D(name, titles[i], bins[i], range_min[i], range_max[i])
        events_h.Draw(branches[i] + ">>B1" , draw_option)
        h.Scale(scale)
        return h
    else:
        h = TH1D(name, titles[i], bins[i], range_min[i], range_max[i])
        return h

# CMS Official Colors
CMS_colors = {
    "blue1": ROOT.TColor.GetColor("#3f90da"),
    "orange1": ROOT.TColor.GetColor("#ffa90e"),
    "red": ROOT.TColor.GetColor("#bd1f01"),
    "grey1": ROOT.TColor.GetColor("#94a4a2"),
    "purple": ROOT.TColor.GetColor("#832db6"),
    "brown": ROOT.TColor.GetColor("#a96b59"),
    "orange2": ROOT.TColor.GetColor("#e76300"),
    "grey2": ROOT.TColor.GetColor("#b9ac70"),
    "grey3": ROOT.TColor.GetColor("#717581"),
    "blue2": ROOT.TColor.GetColor("#92dadd"),
}

# init cuts
h_blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
z_peak = "(m_ll > 88) && (m_ll < 94)" 
z_tail = "((m_ll < 88) && (m_ll > 80)) || ((m_ll < 100) && (m_ll > 94))" 

# Sub-category
region1 = "nBJetM_add == 1"
region2 = "nBJetM_add > 1"
region3 = "nBJetM_add > 0"


# Create an argument parser
parser = argparse.ArgumentParser(description="Plotting argument parser.")

# Define the arguments that the script will accept
#  python3 Data_MC_plot_2L_tt.py --channelsA="mu/el" --channelsB="mu/el" --subcategory="r1/r2/r3" --yaxis="lin/log" --MCscale="scale/noscale" --score="0/0.5" --date="092824/test" --addcut="pt_fatjet > 250"
parser.add_argument('--channelsA', type=str, help='Specify lepton 1 is Muon (mu) or Electron (el)')
parser.add_argument('--channelsB', type=str, help='Specify letpon 2 is Muon (mu) or Electron (el)')
parser.add_argument('--subcategory', type=str, help='Specify 1b (r1), 2b+ (r2) or 1b+ (r3) sub-category')
parser.add_argument('--yaxis', type=str, help='Specify y axis scale linear (lin) or logarithmic (log)')
parser.add_argument('--MCscale', type=str, help='Specify if MC should be scaled to Data (scale) or not (noscale)')
parser.add_argument('--score', type=str, help='Specify PNET score cut (between 0 and 1)')
parser.add_argument('--datetext', type=str, help='Specify date or text to be added at the end of the pdf file name (e.g: 092824 or test)')
parser.add_argument('--addcut', type=str, help='Specify additional cuts (e.g: dphi_fatjet_ll > 2.9 && pt_ll > 100)')

# Parse the arguments from the command line
args = parser.parse_args()
channel = args.channelsA
channel2 = args.channelsB
reg = args.subcategory
sc = args.yaxis
dataScale = args.MCscale
PNET_score = args.score
date = args.datetext
cut_add = args.addcut

# input/output directory and tree name
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_tt_" + channel + channel2 + "_nom/"
output_dir = "../plots_2L_tt"
tree_name = "event_tree"
weight = "weight_tot" 
#weight = "weight_zPt_nominal" 

# Setting up arguments
# Sub-category
if (reg == "r1"):
    region = region1
    category = True
    regb = "1b"
elif (reg == "r2"):
    region = region2
    category = True
    regb = "2b+"
else:
    region = region3
    category = True
    regb = "1b+"

# y axis scale
if (sc == "log"):
    scale_log = True
else:
    scale_log = False

# PNET score cut and additional cuts
if PNET_score == "0":
    if cut_add:
        cut = region + " && " + cut_add
    else:
        cut = region
else:
    cut = "score_Haa4b_vs_QCD > " + PNET_score + " && " + region
    if cut_add:
        cut = cut + " && " + cut_add

# Draw options
if cut:
    draw_option = f"({cut})*({weight})"
else:
    draw_option = weight

# Signal MC scaling
if (PNET_score == "0"):
    sig_scaling = 100
else :
    sig_scaling = 2

# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1

## DYJets_M-50 background (HT bins)
DY_50_path = "DY_50_HT"
DYJets_files = ["70-100", "100-200","200-400", "400-600", "600-800", "800-1200","1200-2500", "2500-inf"]
DYJets_xsec = [158.7445, 159.1984, 43.5384, 5.9141, 1.4377, 0.6443, 0.1511, 0.00339]
DYJets_ev = [17004433, 26202328, 18455718, 8908406, 7035971, 6678036, 6166852, 1978203]
DYJets_scale = []
for i in range(len(DYJets_xsec)):
    DYJets_scale.append(lumi*DYJets_xsec[i]/DYJets_ev[i])

## DYJets_M1-10 background (HT bins)
DY_1_path = "DY_1-10_HT"
DYJets1_files = ["70-100", "100-200","200-400", "400-600", "600-inf"]
DYJets1_xsec = [13.45, 4.803, 0.3321, 0.01447, 0.002186]
DYJets1_ev = [5000000, 2491000, 250000, 25000, 25000]
DYJets1_scale = []
for i in range(len(DYJets1_xsec)):
    DYJets1_scale.append(lumi*DYJets1_xsec[i]/DYJets1_ev[i])

## Gjets background (HT bins)
GJets_path = "GJets_HT"
GJets_files = ["40-100", "100-200","200-400", "400-600", "600-inf"]
GJets_xsec = [18540, 8644, 2183, 260.2, 86.29]
GJets_ev = [30564375, 31722998, 62557132, 16896943, 16669251]
GJets_scale = []
for i in range(len(GJets_xsec)):
    GJets_scale.append(lumi*GJets_xsec[i]/GJets_ev[i])

# QCD
QCD_files = ["100-200","200-300", "300-500", "500-700", "700-1000","1000-1500", "1500-2000", "2000-inf"]
# PSWeight
QCD_inclusive_path = "QCD_PSWeights_HT"
QCD_inclusive_xsec = [23640000, 1546000, 321600, 30310, 6364, 1117, 108.4, 22.36]
QCD_inclusive_ev = [84461486, 57336623, 61705174, 49184771, 48506751, 13026372, 10871473, 5374711]
# bEnriched
QCD_bEnriched_path = "QCD_bEnriched_HT" 
QCD_bEnriched_xsec = [1122000, 79760, 16600, 1503, 297.4, 48.08, 3.951, 0.6957]
QCD_bEnriched_ev = [36118282, 18462183, 11197722, 9246898, 1844165, 1330829, 1431254, 1429280]
# bGenFilter
QCD_bGenFilter_path = "QCD_bGenFilter_HT" 
QCD_bGenFilter_xsec = [1266000, 109900, 27360, 2991, 731.8, 139.3, 14.74, 3.09]
QCD_bGenFilter_ev = [35884072, 14830551, 14144826, 8015859, 4642245, 1537452, 1263157, 1300672]
QCD_inclusive_scale = []
QCD_bGenFilter_scale = []
QCD_bEnriched_scale = []
for i in range(len(QCD_files)):
    QCD_inclusive_scale.append(lumi*QCD_inclusive_xsec[i]/QCD_inclusive_ev[i])
    QCD_bGenFilter_scale.append(lumi*QCD_bGenFilter_xsec[i]/QCD_bGenFilter_ev[i])
    QCD_bEnriched_scale.append(lumi*QCD_bEnriched_xsec[i]/QCD_bEnriched_ev[i])

# Other backgrounds: 
Background_files = ["TT_2L2Nu", "TTW", "ST_TW_Top_NoFullyHadronic", "ST_TW_AntiTop_NoFullyHadronic", "WW", "WZ", "ZZ", "DY_10-50"]
Background_xsec = [87.339, 0.6008, 21.63, 21.63, 118.7, 47.13, 16.523, 18610]
Background_ev = [143887383, 27686862, 11269836, 11012326, 15679000, 7940000, 3526000, 99288125]
Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])

# TTH Signal: [M-15, M-30, M-55]
Signal1_files = ["SUSY_TTH_M-15", "SUSY_TTH_M-30", "SUSY_TTH_M-55"]
Signal1_xsec = [0.1445, 0.1445, 0.1445]
Signal1_ev = [188898, 196711, 198645]
Signal1_scale =[]
for i in range(len(Signal1_xsec)):
    Signal1_scale.append(lumi*Signal1_xsec[i]/Signal1_ev[i])

# Data 
data_mu = ["data_SingleMuonA", "data_SingleMuonB", "data_SingleMuonC", "data_SingleMuonD"]
data_el = ["data_EGammaA", "data_EGammaB", "data_EGammaC", "data_EGammaD"]

# Histo variables, labels, range and bins
# Kinematic plots ordering: AK8 jet (including mA), lepton pair, leptons, MET, AK4

branches = [ 
        "m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet", "pt_fatjet", "eta_fatjet", "phi_fatjet",
        # PNET branches
        "mH_avg", "mA_avg",  "score_Haa4b_vs_QCD", "score_Haa4b_vs_QCD_v1", "score_Haa4b_vs_QCD_34",
        "score_Haa4b_vs_QCD_v2a", "score_Haa4b_vs_QCD_v2b","mA_34a", "mA_34b", "mA_34d",
        # other
        "pt_l1", "eta_l1", "phi_l1","pt_l2", "eta_l2", "phi_l2",
        "ngoodMuons", "ngoodElectrons", "pt_MET", "phi_MET", "pt_PUPPIMET", "phi_PUPPIMET",
        "nJet_add", "nJetCent_add", "pt_SSum", "pt_VSum",
        "pt_jet1", "eta_jet1", "phi_jet1",  "pt_jet2", "eta_jet2", "phi_jet2",
        "pt_bjet1", "eta_bjet1", "phi_bjet1",  "pt_bjet2", "eta_bjet2", "phi_bjet2", 
        "HT_LHE"] 

branches_mumu = [
        "Muon_pfRelIso04_all_l1", "Muon_pfIsoId_l1", "Muon_miniPFRelIso_all_l1", "Muon_miniIsoId_l1",
        "Muon_pfRelIso04_all_l2", "Muon_pfIsoId_l2", "Muon_miniPFRelIso_all_l2", "Muon_miniIsoId_l2", 
        "Muon_pfRelIso04_all_max", "Muon_pfIsoId_min", "Muon_miniPFRelIso_all_max", "Muon_miniIsoId_min", 
        "Idbit_l1", "Idbit_l2"]
branches_elel = ["Idbit_l1_mva", "Idbit_l2_mva", "Idbit_l1_cut", "Idbit_l2_cut", "trigger_IDbit"]
branches_muel = [
        "Muon_pfRelIso04_all_l1", "Muon_pfIsoId_l1", "Muon_miniPFRelIso_all_l1", "Muon_miniIsoId_l1", 
        "Idbit_l1", "Idbit_l2_mva", "Idbit_l2_cut"]
branches_elmu = [
        "Idbit_l1_mva", "Idbit_l1_cut", 
        "Muon_pfRelIso04_all_l2", "Muon_pfIsoId_l2", "Muon_miniPFRelIso_all_l2", "Muon_miniIsoId_l2", "Idbit_l2", "trigger_IDbit"]

x_axis = [
        "m(AK8) [GeV]", "m_{SoftDrop}(AK8) [GeV]", "m_{SoftScaled}(AK8) [GeV]", "p_{T}(AK8) [GeV]", "#eta (AK8)", "#phi (AK8)",
        # PNET branches
        "mH_{PNET} [GeV]", "mA_{PNET} [GeV]", "score_{PNET} v2", "score_{PNET} v1", "score_{PNET} 34v2", 
        "score_{PNET} v2a", "score_{PNET} v2b", "mA_{PNET} 34a [GeV]", "mA_{PNET} 34b [GeV]",  "mA_{PNET} 34d [GeV]",
        # AK8 H cand branches
        "p_{T}(lep1) [GeV]", "#eta(lep1)", "#phi(lep1)", "p_{T}(lep2) [GeV]", "#eta(lep2)", "#phi(lep2)",
        "Good Muons", "Good Electrons", "p_{T}(MET) [GeV]", "#phi(MET)", "p_{T}(PUPPI MET) [GeV]", "#phi(PUPPI MET)",
        "N_{extra}(AK4)", "N_{extra}(central AK4) ", "#Sigma_{i}^{Nextra}(p_{T}(AK4))", "#Sigma_{i}^{Nextra}(p_{T}(AK4_vec))", 
        "p_{T}(lead-AK4) [GeV]", "#eta(lead-AK4)", "#phi(lead-AK4)", "p_{T}(sublead-AK4) [GeV]", "#eta(sublead-AK4)", "#phi(sublead-AK4)",
        "p_{T}(lead-B-AK4) [GeV]", "#eta(lead-B-AK4)", "#phi(lead-B-AK4)", "p_{T}(sublead-B-AK4) [GeV]", "#eta(sublead-B-AK4)", "#phi(sublead-B-AK4)", "H_{T}(LHE)"]

x_axis_mumu = [
        "Muon_pfRelIso04_all(lep1)", "Muon_pfIsoId(lep1)", "Muon_miniPFRelIso_all(lep1)", "Muon_miniIsoId(lep1)",
        "Muon_pfRelIso04_all(lep2)", "Muon_pfIsoId(lep2)", "Muon_miniPFRelIso_all(lep2)", "Muon_miniIsoId(lep2)",
        "Muon_pfRelIso04_all max(lep1,lep2)", "Muon_pfIsoId  min(lep1,lep2)", "Muon_miniPFRelIso_all  max(lep1,lep2)", "Muon_miniIsoId  min(lep1,lep2)", 
        "ID bit(lep1)", "ID bit(lep2)"]
x_axis_elel = ["MVA ID bit(lep1)", "MVA ID bit(lep2)", "Cut ID bit(lep1)", "Cut ID bit(lep2)", "Trigger bit"]
x_axis_muel = [
        "Muon_pfRelIso04_all(lep1)", "Muon_pfIsoId(lep1)", "Muon_miniPFRelIso_all(lep1)", "Muon_miniIsoId(lep1)", 
        "ID bit(lep1)", "MVA ID bit(lep2)", "Cut ID bit(lep2)"]
x_axis_elmu = [
        "MVA ID bit(lep1)", "Cut ID bit(lep1)", 
        "Muon_pfRelIso04_all(lep2)", "Muon_pfIsoId(lep2)", "Muon_miniPFRelIso_all(lep2)", "Muon_miniIsoId(lep2)", 
        "ID bit(lep2)", "Trigger bit"]

range_min = [
        20, 20, 20, 150, -3, -3.2,
        # PNET branches
        20, 2.5, 0, 0, 0,
        0, 0,  2.5,  2.5,  2.5,
        # other
        10, -3, -3.2, 10, -3, -3.2, -0.5, -0.5, 0, -3.2, 0, -3.2, -0.5, -0.5, 0, 0, 30, -3, -3.2, 30, -3, -3.2, 30, -3, -3.2, 30, -3, -3.2, 0]

range_min_mumu = [ 0, -0.5, 0, -0.5, 0, -0.5, 0, -0.5, 0, -0.5, 0, -0.5, -0.5, -0.5] 
range_min_elel = [ -0.5, -0.5, -0.5, -0.5, -0.5]
range_min_muel = [ 0, -0.5, 0, -0.5, -0.5, -0.5, -0.5]
range_min_elmu = [ -0.5, -0.5, 0, -0.5, 0, -0.5, -0.5, -0.5]

range_max = [
        200, 200, 200, 650, 3, 3.2, 
        # PNET branches
        200, 72.5, 1, 1, 1, 
        1, 1, 72.5, 72.5, 72.5,
        # AK8 H cand branches
        610, 3, 3.2, 610, 3, 3.2, 5.5, 5.5, 500, 3.2, 500, 3.2, 7.5, 7.5, 1000, 1000, 530, 3, 3.2, 530, 3, 3.2, 530, 3, 3.2, 530, 3, 3.2, 3000]

range_max_mumu = [1, 6.5, 1, 4.5, 1, 6.5, 1, 4.5, 1, 6.5, 1, 4.5, 15.5, 15.5] 
range_max_elel = [15.5, 15.5, 15.5, 15.5, 15.5]
range_max_muel = [1, 6.5, 1, 4.5, 15.5, 15.5, 15.5] 
range_max_elmu = [15.5, 15.5, 1, 6.5, 1, 4.5, 15.5, 15.5]

bins = [
        18, 18, 18, 25, 15, 16, 
        # PNET branches
        18, 14, 20, 20, 20,
        20, 20, 14, 14, 14,
        # AK8 H cand branches
        15, 15, 16, 15, 15, 16, 6, 6, 25, 16, 25, 16, 8, 8, 20, 20, 25, 15, 16, 25, 15, 16, 25, 15, 16, 25, 15, 16, 30]

bins_mumu = [20, 7, 20, 5, 20, 7, 20, 5, 20, 7, 20, 5, 16, 16]
bins_elel = [16, 16, 16, 16, 16]
bins_muel = [20, 7, 20, 5, 16, 16, 16]
bins_elmu = [16, 16, 20, 7, 20, 5, 16, 16]

titles = [
        "Leading Fat Jet Mass", "Leading Fat Jet SoftDrop Mass", "Leading Fat Jet p_{T}-Scaled SoftDrop Mass", "Leading Fat Jet p_{T}", "Leading Fat Jet #eta", "Leading Fat Jet #phi", 
        # PNET branches
        "PNET Regressed Mass H #rightarrow aa #rightarrow 4b", "PNET Regressed Mass a #rightarrow bb", "PNET Haa4b v2 Tagger Score", "PNET Haa4b v1 Tagger Score", "PNET Haa34b v2 Tagger Score",
        "PNET Haa4b v2a Tagger Score", "PNET Haa4b v2b Tagger Score", "PNET Regressed Mass a (34a) #rightarrow bb", "PNET Regressed Mass a (34b) #rightarrow bb", "PNET Regressed Mass a (34d) #rightarrow bb",
        # AK8 H cand branches
        "Lepton 1 p_{T}", "Lepton 1 #eta", "Lepton 1 #phi", "Lepton 2 p_{T}", "Lepton 2 #eta", "Lepton 2 #phi", "Number of Muons passing Loose Selection", "Number of Electrons passing Loose Selection","MET p_{T}", "MET #phi", "PUPPI MET p_{T}", "PUPPI MET #phi", "Number of Additional AK4 Jets", "Number of Additional Central AK4 Jets", "Scalar Sum of all Additional AK4 Jets", "Vector Sum of all Additional AK4 Jets", "Leading-p_{T} Additional AK4 Jet p_{T}", "Leading-p_{T} Additional AK4 Jet #eta", "Leading-p_{T} Additional AK4 Jet #phi", "Sub-leading-p_{T} Additional AK4 Jet p_{T}", "Sub-leading-p_{T} Additional AK4 Jet #eta", "Sub-leading-p_{T} Additional AK4 Jet #phi", "Leading-p_{T} Additional AK4 B-Jet p_{T}", "Leading-p_{T} Additional AK4 B-Jet #eta", "Leading-p_{T} Additional AK4 B-Jet #phi", "Sub-leading-p_{T} Additional AK4 B-Jet p_{T}", "Sub-leading-p_{T} Additional AK4 B-Jet #eta", "Sub-leading-p_{T} Additional AK4 B-Jet #phi", "Scalar Sum of Parton pTs (HT) at LHE Step"]

titles_mumu = ["Lepton 1 PF relative isolation dR=0.4, total (deltaBeta corrections)", "Lepton 1 PFIso ID from miniAOD selector", "Lepton 1 mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Lepton 1 MiniIso ID from miniAOD selector", "Lepton 2 PF relative isolation dR=0.4, total (deltaBeta corrections)", "Lepton 2 PFIso ID from miniAOD selector", "Lepton 2 mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Lepton 2 MiniIso ID from miniAOD selector", "Maximum PF relative isolation dR=0.4, total (deltaBeta corrections)", "Minimum PFIso ID from miniAOD selector", "Maximum mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Minimum MiniIso ID from miniAOD selector", "Lepton 1 ID bit [1*medium + 2*mediumprompt + 4*tight + 8*highPt]", "Lepton 2 ID bit [1*medium + 2*mediumprompt + 4*tight + 8*highPt]"]
titles_elel = ["Lepton 1 MVA ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 2 MVA ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 1 Cut-based ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 2 Cut-based ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Trigger ID bit [1*HLT_Ele32_WPTight_Gsf + 2*HLT_Ele35_WPTight_Gsf_L1EGMT +4*HLT_Ele115_CaloIdVT_GsfTrkIdT + 8*HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165]"]
titles_muel = ["Lepton 1 PF relative isolation dR=0.4, total (deltaBeta corrections)", "Lepton 1 PFIso ID from miniAOD selector", "Lepton 1 mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Lepton 1 MiniIso ID from miniAOD selector","Lepton 1 ID bit [1*medium + 2*mediumprompt + 4*tight + 8*highPt]", "Lepton 2 MVA ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]","Lepton 2 Cut-based ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]"]
titles_elmu = ["Lepton 1 MVA ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 1 Cut-based ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 2 PF relative isolation dR=0.4, total (deltaBeta corrections)", "Lepton 2 PFIso ID from miniAOD selector", "Lepton 2 mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Lepton 2 MiniIso ID from miniAOD selector", "Lepton 2 ID bit [1*medium + 2*mediumprompt + 4*tight + 8*highPt]", "Trigger ID bit [1*HLT_Ele32_WPTight_Gsf + 2*HLT_Ele35_WPTight_Gsf_L1EGMT +4*HLT_Ele115_CaloIdVT_GsfTrkIdT + 8*HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165]"]

#ymax = [10000, 10000, 10000, 10000, 10000, 5]
#ymin = [0, 0, 0, 0, 0]

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box


if channel =="mu":
    data_files = data_mu
    if channel2 == "mu":
        branches = branches + branches_mumu
        x_axis = x_axis + x_axis_mumu
        range_min = range_min + range_min_mumu
        range_max = range_max + range_max_mumu
        bins = bins + bins_mumu
        titles = titles + titles_mumu
    else:
        branches = branches + branches_muel
        x_axis = x_axis + x_axis_muel
        range_min = range_min + range_min_muel
        range_max = range_max + range_max_muel
        bins = bins + bins_muel
        titles = titles + titles_muel
else:
    data_files = data_el
    if channel2 == "mu":
        branches = branches + branches_elmu
        x_axis = x_axis + x_axis_elmu
        range_min = range_min + range_min_elmu
        range_max = range_max + range_max_elmu
        bins = bins + bins_elmu
        titles = titles + titles_elmu
    else:
        branches = branches + branches_elel
        x_axis = x_axis + x_axis_elel
        range_min = range_min + range_min_elel
        range_max = range_max + range_max_elel
        bins = bins + bins_elel
        titles = titles + titles_elel


for i in range(len(branches)):
    #print(branches[i], titles[i], bins[i], range_min[i], range_max[i])
#for i in range(7):
    # Cutting events with msoftdrop in Higgs range [100, 140] GeV for branches
    # other than Leading Fat Jet mass/ softdrop_mass / softscale mass / mH_avg,
    # bins in that range are cuted later in the plotting script
  # if (i != 0) and (i != 10) and (i != 12):
  #   blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
    
    
    # Creating Canvas
    c, pad1, pad2 = createCanvasPads(scale_log)
    pad1.cd() # pad1 is for regular plot

    # GJets Background for every HT 
    # G Jets 40-100
    p_GB0 = input_dir + GJets_path + GJets_files[0] + ".root"
    if (os.path.isfile(p_GB0)):
        f_GB0 = TFile(p_GB0, "READ") 
        events_GB0 = f_GB0.Get(tree_name)
        GB0 = TH1D("GB0", titles[i], bins[i], range_min[i], range_max[i])
        events_GB0.Draw(branches[i]+">>GB0", draw_option)
        GB0.Scale(GJets_scale[0])
    else:
        GB0 = TH1D("GB0", titles[i], bins[i], range_min[i], range_max[i])
    
    # G Jets 100-200
    p_GB1 = input_dir + GJets_path + GJets_files[1] + ".root"
    if (os.path.isfile(p_GB1)):
        f_GB1 = TFile(p_GB1, "READ") 
        events_GB1 = f_GB1.Get(tree_name)
        GB1 = TH1D("GB1", titles[i], bins[i], range_min[i], range_max[i])
        events_GB1.Draw(branches[i]+">>GB1", draw_option)
        GB1.Scale(GJets_scale[1])
    else:
        GB1 = TH1D("GB1", titles[i], bins[i], range_min[i], range_max[i])
   
    # G Jets 200-400
    p_GB2 = input_dir + GJets_path + GJets_files[2] + ".root"
    if (os.path.isfile(p_GB2)):
        f_GB2 = TFile(p_GB2, "READ") 
        events_GB2 = f_GB2.Get(tree_name)
        GB2 = TH1D("GB2", titles[i], bins[i], range_min[i], range_max[i])
        events_GB2.Draw(branches[i]+">>GB2", draw_option)
        GB2.Scale(GJets_scale[2])
    else:
        GB2 = TH1D("GB2", titles[i], bins[i], range_min[i], range_max[i])

    # G Jets 400-600
    p_GB3 = input_dir + GJets_path + GJets_files[3] + ".root"
    if (os.path.isfile(p_GB3)):
        f_GB3 = TFile(p_GB3, "READ") 
        events_GB3 = f_GB3.Get(tree_name)
        GB3 = TH1D("GB3", titles[i], bins[i], range_min[i], range_max[i])
        events_GB3.Draw(branches[i]+">>GB3", draw_option)
        GB3.Scale(GJets_scale[3])
    else:
        GB3 = TH1D("GB3", titles[i], bins[i], range_min[i], range_max[i])

    # G Jets 600-inf
    p_GB4 = input_dir + GJets_path + GJets_files[4] + ".root"
    if (os.path.isfile(p_GB4)):
        f_GB4 = TFile(p_GB4, "READ") 
        events_GB4 = f_GB4.Get(tree_name)
        GB4 = TH1D("GB4", titles[i], bins[i], range_min[i], range_max[i])
        events_GB4.Draw(branches[i]+">>GB4", draw_option)
        GB4.Scale(GJets_scale[4])
    else:
        GB4 = TH1D("GB4", titles[i], bins[i], range_min[i], range_max[i])

    # Adding HT histos to main histo
    GBX = TH1D("GBX", titles[i], bins[i], range_min[i], range_max[i])
    GBX.Add(GB0)
    GBX.Add(GB1)
    GBX.Add(GB2)
    GBX.Add(GB3)
    GBX.Add(GB4)
    GBX.SetFillColor(kSpring+10)

    # DY_1-10 Background for every HT 
    # DY Jets 70-100
    p_YB0 = input_dir + DY_1_path + DYJets1_files[0] + ".root"
    if (os.path.isfile(p_YB0)):
        f_YB0 = TFile(p_YB0, "READ") 
        events_YB0 = f_YB0.Get(tree_name)
        YB0 = TH1D("YB0", titles[i], bins[i], range_min[i], range_max[i])
        events_YB0.Draw(branches[i]+">>YB0", draw_option)
        YB0.Scale(DYJets1_scale[0])
    else:
        YB0 = TH1D("YB0", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY Jets 100-200
    p_YB1 = input_dir + DY_1_path + DYJets1_files[1] + ".root"
    if (os.path.isfile(p_YB1)):
        f_YB1 = TFile(p_YB1, "READ") 
        events_YB1 = f_YB1.Get(tree_name)
        YB1 = TH1D("YB1", titles[i], bins[i], range_min[i], range_max[i])
        events_YB1.Draw(branches[i]+">>YB1", draw_option)
        YB1.Scale(DYJets1_scale[1])
    else:
        YB1 = TH1D("YB1", titles[i], bins[i], range_min[i], range_max[i])
   
    # DY Jets 200-400
    p_YB2 = input_dir + DY_1_path + DYJets1_files[2] + ".root"
    if (os.path.isfile(p_YB2)):
        f_YB2 = TFile(p_YB2, "READ") 
        events_YB2 = f_YB2.Get(tree_name)
        YB2 = TH1D("YB2", titles[i], bins[i], range_min[i], range_max[i])
        events_YB2.Draw(branches[i]+">>YB2", draw_option)
        YB2.Scale(DYJets1_scale[2])
    else:
        YB2 = TH1D("YB2", titles[i], bins[i], range_min[i], range_max[i])

    # DY Jets 400-600
    p_YB3 = input_dir + DY_1_path + DYJets1_files[3] + ".root"
    if (os.path.isfile(p_YB3)):
        f_YB3 = TFile(p_YB3, "READ") 
        events_YB3 = f_YB3.Get(tree_name)
        YB3 = TH1D("YB3", titles[i], bins[i], range_min[i], range_max[i])
        events_YB3.Draw(branches[i]+">>YB3", draw_option)
        YB3.Scale(DYJets1_scale[3])
    else:
        YB3 = TH1D("YB3", titles[i], bins[i], range_min[i], range_max[i])

    # DY Jets 600-inf
    p_YB4 = input_dir + DY_1_path + DYJets1_files[4] + ".root"
    if (os.path.isfile(p_YB4)):
        f_YB4 = TFile(p_YB4, "READ") 
        events_YB4 = f_YB4.Get(tree_name)
        YB4 = TH1D("YB4", titles[i], bins[i], range_min[i], range_max[i])
        events_YB4.Draw(branches[i]+">>YB4", draw_option)
        YB4.Scale(DYJets1_scale[4])
    else:
        YB4 = TH1D("YB4", titles[i], bins[i], range_min[i], range_max[i])

    # Adding HT histos to main histo
    YB = TH1D("YB", titles[i], bins[i], range_min[i], range_max[i])
    YB.Add(YB0)
    YB.Add(YB1)
    YB.Add(YB2)
    YB.Add(YB3)
    YB.Add(YB4)
    #YB.SetFillColor(kBlue)
   
   # DY_50 Background for every HT 
    # DY Jets 70-100
    p_ZB0 = input_dir + DY_50_path + DYJets_files[0] + ".root"
    if (os.path.isfile(p_ZB0)):
        f_ZB0 = TFile(p_ZB0, "READ") 
        events_ZB0 = f_ZB0.Get(tree_name)
        ZB0 = TH1D("ZB0", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB0.Draw(branches[i]+">>ZB0", draw_option)
        ZB0.Scale(DYJets_scale[0])
    else:
        ZB0 = TH1D("ZB0", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY Jets 100-200
    p_ZB1 = input_dir + DY_50_path + DYJets_files[1] + ".root"
    if (os.path.isfile(p_ZB1)):
        f_ZB1 = TFile(p_ZB1, "READ") 
        events_ZB1 = f_ZB1.Get(tree_name)
        ZB1 = TH1D("ZB1", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB1.Draw(branches[i]+">>ZB1", draw_option)
        ZB1.Scale(DYJets_scale[1])
    else:
        ZB1 = TH1D("ZB1", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY Jets 200-400
    p_ZB2 = input_dir + DY_50_path + DYJets_files[2] + ".root"
    if (os.path.isfile(p_ZB2)):
        f_ZB2 = TFile(p_ZB2, "READ") 
        events_ZB2 = f_ZB2.Get(tree_name)
        ZB2 = TH1D("ZB2", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB2.Draw(branches[i]+">>ZB2", draw_option)
        ZB2.Scale(DYJets_scale[2])
    else:
        ZB2 = TH1D("ZB2", titles[i], bins[i], range_min[i], range_max[i])

    # DY Jets 400-600
    p_ZB3 = input_dir + DY_50_path + DYJets_files[3] + ".root"
    if (os.path.isfile(p_ZB3)):
        f_ZB3 = TFile(p_ZB3, "READ") 
        events_ZB3 = f_ZB3.Get(tree_name)
        ZB3 = TH1D("ZB3", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB3.Draw(branches[i]+">>ZB3", draw_option)
        ZB3.Scale(DYJets_scale[3])
    else:
        ZB3 = TH1D("ZB3", titles[i], bins[i], range_min[i], range_max[i])

    # DY Jets 600-800
    p_ZB4 = input_dir + DY_50_path + DYJets_files[4] + ".root"
    if (os.path.isfile(p_ZB4)):
        f_ZB4 = TFile(p_ZB4, "READ") 
        events_ZB4 = f_ZB4.Get(tree_name)
        ZB4 = TH1D("ZB4", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB4.Draw(branches[i]+">>ZB4", draw_option)
        ZB4.Scale(DYJets_scale[4])
    else:
        ZB4 = TH1D("ZB4", titles[i], bins[i], range_min[i], range_max[i])

    # DY Jets 800-1200
    p_ZB5 = input_dir + DY_50_path + DYJets_files[5] + ".root"
    if (os.path.isfile(p_ZB5)):
        f_ZB5 = TFile(p_ZB5, "READ") 
        events_ZB5 = f_ZB5.Get(tree_name)
        ZB5 = TH1D("ZB5", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB5.Draw(branches[i]+">>ZB5", draw_option)
        ZB5.Scale(DYJets_scale[5])
    else:
        ZB5 = TH1D("ZB5", titles[i], bins[i], range_min[i], range_max[i])

    # DY Jets 1200-2500
    p_ZB6 = input_dir + DY_50_path + DYJets_files[6] + ".root"
    if (os.path.isfile(p_ZB6)):
        f_ZB6 = TFile(p_ZB6, "READ") 
        events_ZB6 = f_ZB6.Get(tree_name)
        ZB6 = TH1D("ZB6", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB6.Draw(branches[i]+">>ZB6", draw_option)
        ZB6.Scale(DYJets_scale[6])
    else:
        ZB6 = TH1D("ZB6", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY Jets 2500-inf
    p_ZB7 = input_dir + DY_50_path + DYJets_files[7] + ".root"
    if (os.path.isfile(p_ZB7)):
        f_ZB7 = TFile(p_ZB7, "READ") 
        events_ZB7 = f_ZB7.Get(tree_name)
        ZB7 = TH1D("ZB7", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB7.Draw(branches[i]+">>ZB7", draw_option)
        ZB7.Scale(DYJets_scale[7])
    else:
        ZB7 = TH1D("ZB7", titles[i], bins[i], range_min[i], range_max[i])
    
    # Adding HT histos to main histo
    ZB = TH1D("ZB", titles[i], bins[i], range_min[i], range_max[i])
    ZB.Add(ZB0)
    ZB.Add(ZB1)
    ZB.Add(ZB2)
    ZB.Add(ZB3)
    ZB.Add(ZB4)
    ZB.Add(ZB5)
    ZB.Add(ZB6)
    ZB.Add(ZB7)
    #ZB.SetFillColor(kGreen)

    # All the other backgrounds
    # TT_2l2v
    f_B0 = TFile(input_dir + Background_files[0] + ".root", "READ")
    events_B0 = f_B0.Get(tree_name)
    B0 = TH1D("B0", titles[i], bins[i], range_min[i], range_max[i])
    events_B0.Draw(branches[i]+">>B0", draw_option) 
    B0.Scale(Background_scale[0])
    B0.SetFillColor(CMS_colors["blue1"])

    ''' 
    # TTW
    p_B1 = input_dir + Background_files[1] + ".root"
    if (os.path.isfile(p_B1)):
        f_B1 = TFile(p_B1, "READ") 
        events_B1 = f_B1.Get(tree_name)
        B1 = TH1D("B1", titles[i], bins[i], range_min[i], range_max[i])
        events_B1.Draw(branches[i]+">>B1", draw_option)
        B1.Scale(Background_scale[1])
        B1.SetFillColor(kTeal)
    else:
        B1 = TH1D("B1", titles[i], bins[i], range_min[i], range_max[i])
    '''

    # Single Top TW Top, Not Fully Hadronic 
    f_B2 = TFile(input_dir + Background_files[2] + ".root", "READ")
    events_B2 = f_B2.Get(tree_name)
    B2 = TH1D("B2", titles[i], bins[i], range_min[i], range_max[i])
    events_B2.Draw(branches[i]+">>B2", draw_option)
    B2.Scale(Background_scale[2])

    # Single Top TW AntiTop, Not Fully Hadronic 
    f_B3 = TFile(input_dir + Background_files[3] + ".root", "READ")
    events_B3 = f_B3.Get(tree_name)
    B3 = TH1D("B3", titles[i], bins[i], range_min[i], range_max[i])
    events_B3.Draw(branches[i]+">>B3", draw_option)
    B3.Scale(Background_scale[3])

    # Consolidating ST_W samples B2 and B3 
    SB1 = TH1D("SB1", titles[i], bins[i], range_min[i], range_max[i]) 
    SB1.Add(B2)
    SB1.Add(B3)
    SB1.SetFillColor(CMS_colors["orange2"])
    
    '''
    # Diboson WW
    p_B4 = input_dir + Background_files[4] + ".root"
    if (os.path.isfile(p_B4)):
        f_B4 = TFile(p_B4, "READ") 
        events_B4 = f_B4.Get(tree_name)
        B4 = TH1D("B4", titles[i], bins[i], range_min[i], range_max[i])
        events_B4.Draw(branches[i]+">>B4", draw_option)
        B4.Scale(Background_scale[4])
    else:
        B4 = TH1D("B4", titles[i], bins[i], range_min[i], range_max[i])
    
    # Diboson WZ
    p_B5 = input_dir + Background_files[5] + ".root"
    if (os.path.isfile(p_B5)):
        f_B5 = TFile(p_B5, "READ") 
        events_B5 = f_B5.Get(tree_name)
        B5 = TH1D("B5", titles[i], bins[i], range_min[i], range_max[i])
        events_B5.Draw(branches[i]+">>B5", draw_option)
        B5.Scale(Background_scale[5])
    else:
        B5 = TH1D("B5", titles[i], bins[i], range_min[i], range_max[i])

    # Diboson ZZ
    p_B6 = input_dir + Background_files[6] + ".root"
    if (os.path.isfile(p_B6)):
        f_B6 = TFile(p_B6, "READ") 
        events_B6 = f_B6.Get(tree_name)
        B6 = TH1D("B6", titles[i], bins[i], range_min[i], range_max[i])
        events_B6.Draw(branches[i]+">>B6", draw_option)
        B6.Scale(Background_scale[6])
    else:
        B6 = TH1D("B6", titles[i], bins[i], range_min[i], range_max[i])

    # Consolidating samples B9, B10 and B11 into Diboson
    SB2 = TH1D("SB2", titles[i], bins[i], range_min[i], range_max[i]) 
    SB2.Add(B4)
    SB2.Add(B5)
    SB2.Add(B6)
    SB2.SetFillColor(kGreen)
    '''

   # DY_M10-50 
    p_B7 = input_dir + Background_files[7] + ".root"
    if (os.path.isfile(p_B7)):
        f_B7 = TFile(p_B7, "READ") 
        events_B7 = f_B7.Get(tree_name)
        B7 = TH1D("B7", titles[i], bins[i], range_min[i], range_max[i])
        events_B7.Draw(branches[i]+">>B7", draw_option)
        B7.Scale(Background_scale[7])
        #B7.SetFillColor(kRed-2)
    else:
        B7 = TH1D("B7", titles[i], bins[i], range_min[i], range_max[i])

    # Consolidating samples YB, ZB and B7 into DY_M1-50
    SB3 = TH1D("SB3", titles[i], bins[i], range_min[i], range_max[i]) 
    SB3.Add(ZB)
    SB3.Add(YB)
    SB3.Add(B7)
    SB3.SetFillColor(CMS_colors["purple"])
    
    ''' 
    # All QCD samples 
    # QCD Inclusive 100-200 
    p_QA0 = input_dir + QCD_inclusive_path + QCD_files[0] + ".root"
    if (os.path.isfile(p_QA0)):
        f_QA0 = TFile(p_QA0, "READ") 
        events_QA0 = f_QA0.Get(tree_name)
        QA0 = TH1D("QA0", titles[i], bins[i], range_min[i], range_max[i])
        events_QA0.Draw(branches[i]+">>QA0", draw_option)
        QA0.Scale(QCD_inclusive_scale[0])
    else:
        QA0 = TH1D("QA0", titles[i], bins[i], range_min[i], range_max[i])
    
    # QCD Inclusive 200-300 
    p_QA1 = input_dir + QCD_inclusive_path + QCD_files[1] + ".root"
    if (os.path.isfile(p_QA1)):
        f_QA1 = TFile(p_QA1, "READ") 
        events_QA1 = f_QA1.Get(tree_name)
        QA1 = TH1D("QA1", titles[i], bins[i], range_min[i], range_max[i])
        events_QA1.Draw(branches[i]+">>QA1", draw_option)
        QA1.Scale(QCD_inclusive_scale[1])
    else:
        QA1 = TH1D("QA1", titles[i], bins[i], range_min[i], range_max[i])

    # QCD Inclusive 300-500 
    p_QA2 = input_dir + QCD_inclusive_path + QCD_files[2] + ".root"
    if (os.path.isfile(p_QA2)):
        f_QA2 = TFile(p_QA2, "READ") 
        events_QA2 = f_QA2.Get(tree_name)
        QA2 = TH1D("QA2", titles[i], bins[i], range_min[i], range_max[i])
        events_QA2.Draw(branches[i]+">>QA2", draw_option)
        QA2.Scale(QCD_inclusive_scale[2])
    else:
        QA2 = TH1D("QA2", titles[i], bins[i], range_min[i], range_max[i])

    # QCD Inclusive 500-700 
    p_QA3 = input_dir + QCD_inclusive_path + QCD_files[3] + ".root"
    if (os.path.isfile(p_QA3)):
        f_QA3 = TFile(p_QA3, "READ") 
        events_QA3 = f_QA3.Get(tree_name)
        QA3 = TH1D("QA3", titles[i], bins[i], range_min[i], range_max[i])
        events_QA3.Draw(branches[i]+">>QA3", draw_option)
        QA3.Scale(QCD_inclusive_scale[3])
    else:
        QA3 = TH1D("QA3", titles[i], bins[i], range_min[i], range_max[i])
   
    # QCD Inclusive 700-1000 
    p_QA4 = input_dir + QCD_inclusive_path + QCD_files[4] + ".root"
    if (os.path.isfile(p_QA4)):
        f_QA4 = TFile(p_QA4, "READ") 
        events_QA4 = f_QA4.Get(tree_name)
        QA4 = TH1D("QA4", titles[i], bins[i], range_min[i], range_max[i])
        events_QA4.Draw(branches[i]+">>QA4", draw_option)
        QA4.Scale(QCD_inclusive_scale[4])
    else:
        QA4 = TH1D("QA4", titles[i], bins[i], range_min[i], range_max[i])
    
    # QCD Inclusive 1000-1500 
    p_QA5 = input_dir + QCD_inclusive_path + QCD_files[5] + ".root"
    if (os.path.isfile(p_QA5)):
        f_QA5 = TFile(p_QA5, "READ") 
        events_QA5 = f_QA5.Get(tree_name)
        QA5 = TH1D("QA5", titles[i], bins[i], range_min[i], range_max[i])
        events_QA5.Draw(branches[i]+">>QA5", draw_option)
        QA5.Scale(QCD_inclusive_scale[5])
    else:
        QA5 = TH1D("QA5", titles[i], bins[i], range_min[i], range_max[i])

    # QCD Inclusive 1500-2000 
    p_QA6 = input_dir + QCD_inclusive_path + QCD_files[6] + ".root"
    if (os.path.isfile(p_QA6)):
        f_QA6 = TFile(p_QA6, "READ") 
        events_QA6 = f_QA6.Get(tree_name)
        QA6 = TH1D("QA6", titles[i], bins[i], range_min[i], range_max[i])
        events_QA6.Draw(branches[i]+">>QA6", draw_option)
        QA6.Scale(QCD_inclusive_scale[6])
    else:
        QA6 = TH1D("QA6", titles[i], bins[i], range_min[i], range_max[i])

    # QCD Inclusive 2000-inf 
    p_QA7 = input_dir + QCD_inclusive_path + QCD_files[7] + ".root"
    if (os.path.isfile(p_QA7)):
        f_QA7 = TFile(p_QA7, "READ") 
        events_QA7 = f_QA7.Get(tree_name)
        QA7 = TH1D("QA7", titles[i], bins[i], range_min[i], range_max[i])
        events_QA7.Draw(branches[i]+">>QA7", draw_option)
        QA7.Scale(QCD_inclusive_scale[7])
    else:
        QA7 = TH1D("QA7", titles[i], bins[i], range_min[i], range_max[i])
   
    # Adding inclusive QCD sample HT bins
    QA = TH1D("QA", titles[i], bins[i], range_min[i], range_max[i])
    QA.Add(QA0)
    QA.Add(QA1)
    QA.Add(QA2)
    QA.Add(QA3)
    QA.Add(QA4)
    QA.Add(QA5)
    QA.Add(QA6)
    QA.Add(QA7)


    # QCD bEnriched 100-200 
    p_QB0 = input_dir + QCD_bEnriched_path + QCD_files[0] + ".root"
    if (os.path.isfile(p_QB0)):
        f_QB0 = TFile(p_QB0, "READ") 
        events_QB0 = f_QB0.Get(tree_name)
        QB0 = TH1D("QB0", titles[i], bins[i], range_min[i], range_max[i])
        events_QB0.Draw(branches[i]+">>QB0", draw_option)
        QB0.Scale(QCD_bEnriched_scale[0])
    else:
        QB0 = TH1D("QB0", titles[i], bins[i], range_min[i], range_max[i])
    
    # QCD bEnriched 200-300 
    p_QB1 = input_dir + QCD_bEnriched_path + QCD_files[1] + ".root"
    if (os.path.isfile(p_QB1)):
        f_QB1 = TFile(p_QB1, "READ") 
        events_QB1 = f_QB1.Get(tree_name)
        QB1 = TH1D("QB1", titles[i], bins[i], range_min[i], range_max[i])
        events_QB1.Draw(branches[i]+">>QB1", draw_option)
        QB1.Scale(QCD_bEnriched_scale[1])
    else:
        QB1 = TH1D("QB1", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bEnriched 300-500 
    p_QB2 = input_dir + QCD_bEnriched_path + QCD_files[2] + ".root"
    if (os.path.isfile(p_QB2)):
        f_QB2 = TFile(p_QB2, "READ") 
        events_QB2 = f_QB2.Get(tree_name)
        QB2 = TH1D("QB2", titles[i], bins[i], range_min[i], range_max[i])
        events_QB2.Draw(branches[i]+">>QB2", draw_option)
        QB2.Scale(QCD_bEnriched_scale[2])
    else:
        QB2 = TH1D("QB2", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bEnriched 500-700 
    p_QB3 = input_dir + QCD_bEnriched_path + QCD_files[3] + ".root"
    if (os.path.isfile(p_QB3)):
        f_QB3 = TFile(p_QB3, "READ") 
        events_QB3 = f_QB3.Get(tree_name)
        QB3 = TH1D("QB3", titles[i], bins[i], range_min[i], range_max[i])
        events_QB3.Draw(branches[i]+">>QB3", draw_option)
        QB3.Scale(QCD_bEnriched_scale[3])
    else:
        QB3 = TH1D("QB3", titles[i], bins[i], range_min[i], range_max[i])
   
    # QCD bEnriched 700-1000 
    p_QB4 = input_dir + QCD_bEnriched_path + QCD_files[4] + ".root"
    if (os.path.isfile(p_QB4)):
        f_QB4 = TFile(p_QB4, "READ") 
        events_QB4 = f_QB4.Get(tree_name)
        QB4 = TH1D("QB4", titles[i], bins[i], range_min[i], range_max[i])
        events_QB4.Draw(branches[i]+">>QB4", draw_option)
        QB4.Scale(QCD_bEnriched_scale[4])
    else:
        QB4 = TH1D("QB4", titles[i], bins[i], range_min[i], range_max[i])
    
    # QCD bEnriched 1000-1500 
    p_QB5 = input_dir + QCD_bEnriched_path + QCD_files[5] + ".root"
    if (os.path.isfile(p_QB5)):
        f_QB5 = TFile(p_QB5, "READ") 
        events_QB5 = f_QB5.Get(tree_name)
        QB5 = TH1D("QB5", titles[i], bins[i], range_min[i], range_max[i])
        events_QB5.Draw(branches[i]+">>QB5", draw_option)
        QB5.Scale(QCD_bEnriched_scale[5])
    else:
        QB5 = TH1D("QB5", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bEnriched 1500-2000 
    p_QB6 = input_dir + QCD_bEnriched_path + QCD_files[6] + ".root"
    if (os.path.isfile(p_QB6)):
        f_QB6 = TFile(p_QB6, "READ") 
        events_QB6 = f_QB6.Get(tree_name)
        QB6 = TH1D("QB6", titles[i], bins[i], range_min[i], range_max[i])
        events_QB6.Draw(branches[i]+">>QB6", draw_option)
        QB6.Scale(QCD_bEnriched_scale[6])
    else:
        QB6 = TH1D("QB6", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bEnriched 2000-inf 
    p_QB7 = input_dir + QCD_bEnriched_path + QCD_files[7] + ".root"
    if (os.path.isfile(p_QB7)):
        f_QB7 = TFile(p_QB7, "READ") 
        events_QB7 = f_QB7.Get(tree_name)
        QB7 = TH1D("QB7", titles[i], bins[i], range_min[i], range_max[i])
        events_QB7.Draw(branches[i]+">>QB7", draw_option)
        QB7.Scale(QCD_bEnriched_scale[7])
    else:
        QB7 = TH1D("QB7", titles[i], bins[i], range_min[i], range_max[i])
   
    # Adding bEnriched QCD sample HT bins
    QB = TH1D("QB", titles[i], bins[i], range_min[i], range_max[i])
    QB.Add(QB0)
    QB.Add(QB1)
    QB.Add(QB2)
    QB.Add(QB3)
    QB.Add(QB4)
    QB.Add(QB5)
    QB.Add(QB6)
    QB.Add(QB7)


    # QCD bGenFilter 100-200 
    p_QC0 = input_dir + QCD_bGenFilter_path + QCD_files[0] + ".root"
    if (os.path.isfile(p_QC0)):
        f_QC0 = TFile(p_QC0, "READ") 
        events_QC0 = f_QC0.Get(tree_name)
        QC0 = TH1D("QC0", titles[i], bins[i], range_min[i], range_max[i])
        events_QC0.Draw(branches[i]+">>QC0", draw_option)
        QC0.Scale(QCD_bGenFilter_scale[0])
    else:
        QC0 = TH1D("QC0", titles[i], bins[i], range_min[i], range_max[i])
    
    # QCD bGenFilter 200-300 
    p_QC1 = input_dir + QCD_bGenFilter_path + QCD_files[1] + ".root"
    if (os.path.isfile(p_QC1)):
        f_QC1 = TFile(p_QC1, "READ") 
        events_QC1 = f_QC1.Get(tree_name)
        QC1 = TH1D("QC1", titles[i], bins[i], range_min[i], range_max[i])
        events_QC1.Draw(branches[i]+">>QC1", draw_option)
        QC1.Scale(QCD_bGenFilter_scale[1])
    else:
        QC1 = TH1D("QC1", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bGenFilter 300-500 
    p_QC2 = input_dir + QCD_bGenFilter_path + QCD_files[2] + ".root"
    if (os.path.isfile(p_QC2)):
        f_QC2 = TFile(p_QC2, "READ") 
        events_QC2 = f_QC2.Get(tree_name)
        QC2 = TH1D("QC2", titles[i], bins[i], range_min[i], range_max[i])
        events_QC2.Draw(branches[i]+">>QC2", draw_option)
        QC2.Scale(QCD_bGenFilter_scale[2])
    else:
        QC2 = TH1D("QC2", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bGenFilter 500-700 
    p_QC3 = input_dir + QCD_bGenFilter_path + QCD_files[3] + ".root"
    if (os.path.isfile(p_QC3)):
        f_QC3 = TFile(p_QC3, "READ") 
        events_QC3 = f_QC3.Get(tree_name)
        QC3 = TH1D("QC3", titles[i], bins[i], range_min[i], range_max[i])
        events_QC3.Draw(branches[i]+">>QC3", draw_option)
        QC3.Scale(QCD_bGenFilter_scale[3])
    else:
        QC3 = TH1D("QC3", titles[i], bins[i], range_min[i], range_max[i])
   
    # QCD bGenFilter 700-1000 
    p_QC4 = input_dir + QCD_bGenFilter_path + QCD_files[4] + ".root"
    if (os.path.isfile(p_QC4)):
        f_QC4 = TFile(p_QC4, "READ") 
        events_QC4 = f_QC4.Get(tree_name)
        QC4 = TH1D("QC4", titles[i], bins[i], range_min[i], range_max[i])
        events_QC4.Draw(branches[i]+">>QC4", draw_option)
        QC4.Scale(QCD_bGenFilter_scale[4])
    else:
        QC4 = TH1D("QC4", titles[i], bins[i], range_min[i], range_max[i])
    
    # QCD bGenFilter 1000-1500 
    p_QC5 = input_dir + QCD_bGenFilter_path + QCD_files[5] + ".root"
    if (os.path.isfile(p_QC5)):
        f_QC5 = TFile(p_QC5, "READ") 
        events_QC5 = f_QC5.Get(tree_name)
        QC5 = TH1D("QC5", titles[i], bins[i], range_min[i], range_max[i])
        events_QC5.Draw(branches[i]+">>QC5", draw_option)
        QC5.Scale(QCD_bGenFilter_scale[5])
    else:
        QC5 = TH1D("QC5", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bGenFilter 1500-2000 
    p_QC6 = input_dir + QCD_bGenFilter_path + QCD_files[6] + ".root"
    if (os.path.isfile(p_QC6)):
        f_QC6 = TFile(p_QC6, "READ") 
        events_QC6 = f_QC6.Get(tree_name)
        QC6 = TH1D("QC6", titles[i], bins[i], range_min[i], range_max[i])
        events_QC6.Draw(branches[i]+">>QC6", draw_option)
        QC6.Scale(QCD_bGenFilter_scale[6])
    else:
        QC6 = TH1D("QC6", titles[i], bins[i], range_min[i], range_max[i])

    # QCD bGenFilter 2000-inf 
    p_QC7 = input_dir + QCD_bGenFilter_path + QCD_files[7] + ".root"
    if (os.path.isfile(p_QC7)):
        f_QC7 = TFile(p_QC7, "READ") 
        events_QC7 = f_QC7.Get(tree_name)
        QC7 = TH1D("QC7", titles[i], bins[i], range_min[i], range_max[i])
        events_QC7.Draw(branches[i]+">>QC7", draw_option)
        QC7.Scale(QCD_bGenFilter_scale[7])
    else:
        QC7 = TH1D("QC7", titles[i], bins[i], range_min[i], range_max[i])
   
    # Adding bGenFilter QCD sample HT bins
    QC = TH1D("QC", titles[i], bins[i], range_min[i], range_max[i])
    QC.Add(QC0)
    QC.Add(QC1)
    QC.Add(QC2)
    QC.Add(QC3)
    QC.Add(QC4)
    QC.Add(QC5)
    QC.Add(QC6)
    QC.Add(QC7)


    # Adding all QCD samples
    SQ = TH1D("SQ", titles[i], bins[i], range_min[i], range_max[i])
    SQ.Add(QA)
    SQ.Add(QB)
    SQ.Add(QC)
    SQ.SetFillColor(kGray+2)
    '''


    # Adding signal samples
    # TTH M-15
    fS1 = TFile(input_dir + Signal1_files[0] + ".root", "READ")
    events_S1 = fS1.Get(tree_name)
    S1 = TH1D("S1", titles[i], bins[i], range_min[i], range_max[i])
    events_S1.Draw(branches[i]+">>S1", draw_option)
    #S1.Scale(1./S1.Integral())  # can be used to normalize area under histo to 1
    S1.Scale(Signal1_scale[0]*sig_scaling)
    S1.SetLineColor(CMS_colors["orange1"])
    S1.SetLineWidth(2)
    S1.SetLineStyle(2)

    # TTH M-30
    fS2 = TFile(input_dir + Signal1_files[1] + ".root", "READ")
    events_S2 = fS2.Get(tree_name)
    S2 = TH1D("S2", titles[i], bins[i], range_min[i], range_max[i])
    events_S2.Draw(branches[i]+">>S2", draw_option)
    S2.Scale(Signal1_scale[1]*sig_scaling)
    S2.SetLineColor(CMS_colors["red"])
    S2.SetLineWidth(2)
    S2.SetLineStyle(2)

    # TTH M-55
    fS3 = TFile(input_dir + Signal1_files[2] + ".root", "READ")
    events_S3 = fS3.Get(tree_name)
    S3 = TH1D("S3", titles[i], bins[i], range_min[i], range_max[i])
    events_S3.Draw(branches[i]+">>S3", draw_option)
    S3.Scale(Signal1_scale[2]*sig_scaling) 
    S3.SetLineColor(CMS_colors["brown"])
    S3.SetLineWidth(2)
    S3.SetLineStyle(2)

    # Adding Data samples
    # Data 2018A
    fD0 = TFile(input_dir+ data_files[0] + ".root", "READ")
    events_D0 = fD0.Get(tree_name)
    D0 = TH1D("D0", titles[i], bins[i], range_min[i], range_max[i])
    events_D0.Draw(branches[i]+">>D0", cut) # no weight for data!

    # Data 2018B
    fD1 = TFile(input_dir+ data_files[1] + ".root", "READ")
    events_D1 = fD1.Get(tree_name)
    D1 = TH1D("D1", titles[i], bins[i], range_min[i], range_max[i])
    events_D1.Draw(branches[i]+">>D1", cut) # no weight for data!
    
    # Data 2018C
    fD2 = TFile(input_dir+ data_files[2] + ".root", "READ")
    events_D2 = fD2.Get(tree_name)
    D2 = TH1D("D2", titles[i], bins[i], range_min[i], range_max[i])
    events_D2.Draw(branches[i]+">>D2", cut) # no weight for data!

    # Data 2018D
    fD3 = TFile(input_dir+ data_files[3] + ".root", "READ")
    events_D3 = fD3.Get(tree_name)
    D3 = TH1D("D3", titles[i], bins[i], range_min[i], range_max[i])
    events_D3.Draw(branches[i]+">>D3", cut) # no weight for data!
 
    # Adding Data Histos to a single Data histo (SD)
    SD = TH1D("SD", titles[i], bins[i], range_min[i], range_max[i]) 
    SD.Add(D0)
    SD.Add(D1)
    SD.Add(D2)
    SD.Add(D3)
    
    # Scale MC to Data if 3rd argument is "scale"
    #if (channel== "mu" and channel2 == "mu"):
    #    histlist = [B0, B1, SB1, SB2, SB3] 
    #else:
    #    histlist = [B0, B1, SB1, SB2, SB3]
    
    histlist = [B0, SB1, SB3]
    prod = 1
    scale_status = False
    for hist in histlist:
        if hasattr(hist, "Integral"):
            prod = prod*1
        else: 
            prod = prod*0

    if (dataScale == "scale" and hasattr(SD, "Integral") and prod):
        if sum(hist.Integral() for hist in histlist) > 0 and SD.Integral() > 0:
            norm_scale = SD.Integral() / sum(hist.Integral() for hist in histlist)
            scale_status = True
            for hist in histlist:
                hist.Scale(norm_scale)
    
    


    # Scale MC to Data if 3rd argument is "scale"
    '''
    if (channel== "mu" and channel2 == "mu"):
        histlist = [B0, B1, SB1, SB2, SB3] 
    else:
        histlist = [B0, B1, SB1, SB2, SB3, GBX]
    if (dataScale == "scale" and hasattr(SD, "Integral") == True):
        if sum(hist.Integral() for hist in histlist) > 0 and SD.Integral() > 0:
            norm_scale = SD.Integral() / sum(hist.Integral() for hist in histlist)
            for hist in histlist:
                hist.Scale(norm_scale)
    '''

    # Stacking background histos in stack plot (ST)
    ST = THStack("ST","")
    ST.Add(B0)      # TT_2LNu
    ST.Add(SB1)     # Single Top W (B2 + B3)
    ST.Add(SB3)     # DY (B7 + ZB + YB)
    #ST.Add(B7)      # DY M10-50 amcnlo 
    #ST.Add(ZB)      # DY M-50 HT bins

    #if not(channel== "mu" and channel2== "mu"):
    #    ST.Add(GBX)      # G Jets HT bins
    #ST.Add(GBX)
    #ST.Add(B2)     # ST_TW_Top Not Fully Hadronic 
    #ST.Add(B3)     # ST_TW_antiTop Not Fully Hadronic
    #ST.Add(B4)     # WW
    #ST.Add(B5)     # WZ
    #ST.Add(B6)     # ZZ
    #ST.Add(SB2)     # Diboson (B4 + B5 + B6)
    #ST.Add(YB)      # DY M1-10 HT bins
    #ST.Add(SQ)     # QCD
    #ST.Add(B1)      # TTW
     
  
    # Blinding Higgs mass range (100-140 GeV) in Higgs Mass plots
    if (branches[i] == "m_fatjet" or branches[i] == "msoftdrop_fatjet" or branches[i] == "msoftscaled_fatjet" or branches[i] =="mH_avg"): 
        for j in range(9,13):
             SD.SetBinContent(j, 0)
    
    SD.Scale(1.0) # Doing this just to get data point instead of a histo (temporary fix)
    SD.SetMarkerStyle(20) 
    SD.SetLineColor(kBlack)
    SD.Draw()

    # Stylistic adjustments of regular plot through SD histo
    SD_y = SD.GetYaxis()
    SD_x = SD.GetXaxis()

    # Adjusting y axis range as needed for each variable (especially log)
    # Manually adjust both max and min
    #SD_y.SetRangeUser(1, 100000)
    
    # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
    # This is mainly done to make space for the legend and other text
    if (scale_log): # Isolation plots, adjust for linear plots
        if (branches[i] == "HT_LHE"):
            SD.SetMinimum(9)
            SD.SetMaximum(ST.GetMaximum() + 50* (ST.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 
        elif (branches[i] == "pt_bjet1" or branches[i] == "phi_bjet1" or branches[i] == "eta_bjet1"):
            if (region == region3 or region == region2):
                SD.SetMinimum(9)
                SD.SetMaximum(SD.GetMaximum() + 50* (SD.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 
        elif (branches[i] == "pt_bjet2" or branches[i] == "phi_bjet2" or branches[i] == "eta_bjet2"):
            if (region == region3):
                SD.SetMinimum(9)
                SD.SetMaximum(SD.GetMaximum() + 50* (SD.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 
        elif (branches[i] == "score_Haa4b_vs_QCD"):
            SD.SetMinimum(1)
            SD.SetMaximum(SD.GetMaximum() + 100* (SD.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 

        else:
            SD.SetMinimum(9)
            SD.SetMaximum(SD.GetMaximum() + 50* (SD.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 
        #SD.SetMaximum(11*SD.GetMaximum()) #1.2 for linear plots
        
    else:
        if (branches[i] == "HT_LHE"):
            SD.SetMinimum(0)
            SD.SetMaximum(1.5*ST.GetMaximum())
        else:
            SD.SetMinimum(0)
            SD.SetMaximum(2*SD.GetMaximum())
        
    gStyle.SetTitleFontSize(0.05) # Title size

    # x-axis (not necessary since hidden by ratio plot) 
    SD_x.SetTitle(x_axis[i])
    SD_x.SetTitleSize(0.05)
    SD_x.SetTitleOffset(1.5)
    SD_x.SetLabelOffset(0.3)

    # y-axis
    SD_y.SetTitle("Events  ") # space used for offset
    SD_y.SetNdivisions(505) #make divisions nice
    SD_y.SetTitleSize(25) 
    SD_y.SetTitleFont(43) 
    SD_y.SetTitleOffset(1.45)
    SD_y.SetLabelFont(43) # font and size of numbers
    SD_y.SetLabelSize(20)


    gPad.Modified() # Update gpad (things might break depending on where this line is)
    
    
    # Drawing everything
    SD.Draw()
    ST.Draw("SAME HIST")
    S1.Draw("SAME HIST")
    S2.Draw("SAME HIST")
    S3.Draw("SAME HIST")
    SD.Draw("SAME")
   

    # Ratio plot
    pad2.cd()# Go to pad2 for ratio plot
    
    # Need to convert stack plot to regular histo first
    nhist = ST.GetHists().GetSize()
    #print("Number of histos in THStack:",nhist)
    if nhist == 0:
        sys.exit()
    tmpHist = ST.GetHists().At(0).Clone()
    tmpHist.Reset()
    tmpHist.SetTitle("")
    for k in range(ST.GetHists().GetSize()):
        tmpHist.Add(ST.GetHists().At(k))
 
    # Histo for ratio plot
    RATIO = createRatio(SD, tmpHist, x_axis[i])
    # Histo for statistical errors accounting for denominator's statistical erorr
    hStatError = getRatioStatError(SD, tmpHist)
    #RATIO.Draw("ep") # draw with stat errors (but doesn't account for denominator statistical error)
    
    # Ratio histo Stylistic stuff
    R_x = RATIO.GetXaxis()
    R_x.SetTitleSize(25) 
    R_x.SetTitleFont(43) 
    R_x.SetTitleOffset(1.15)
    R_x.SetLabelFont(43) # font and size of numbers
    R_x.SetLabelSize(20)
    R_x.SetLabelOffset(.02)

    # Draw line for y = 1 to help compare ratio
    line = TLine(range_min[i], 1.,range_max[i], 1.)
    line.SetLineColor(kBlack)
    line.SetLineWidth(1)
    
    # Draw stacked plot uncertainty in grey
    h_uncertainty = tmpHist.Clone("h_uncertainty")
    h_uncertainty.SetFillColor(ROOT.kGray+1)
    h_uncertainty.SetFillStyle(3001)
    for l in range(1, h_uncertainty.GetNbinsX() + 1):
        h_uncertainty.SetBinError(l, h_uncertainty.GetBinError(l) / tmpHist.GetBinContent(l) if tmpHist.GetBinContent(l) != 0 else 0)
        h_uncertainty.SetBinContent(l, 1)

    # Drawing histos in ratio pad
    RATIO.Draw("HIST EP")
    #hStatError.Draw("SAME P")
    h_uncertainty.Draw("E2 SAME")
    line.Draw("SAME")

    # Redraw Axis in case they are covered
    pad2.RedrawAxis()

    ############# RATIO TEXT SECTION: START ###########################################################################
    # Muon channel ID bit maps
    if (branches[i] == "Idbit_l1" or branches[i] == "Idbit_l2"):
        bin_text = ["#bar{mpth}","m#bar{pth}","p#bar{mth}","mp#bar{th}", "t#bar{mph}","mt#bar{ph}","pt#bar{mh}","mpt#bar{h}","h#bar{mpt}","mh#bar{pt}","ph#bar{pt}","mph#bar{t}","th#bar{mp}","mth#bar{p}","pth#bar{m}","mpth"]
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            x_position.append( (0.97 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (0.25 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ bins[i])
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        t_bin0_text = TLatex(x_position[0], y_position, str(bin_text[0]))
        MinorTextFormat_ratio(t_bin0_text)
        t_bin1_text = TLatex(x_position[1], y_position, str(bin_text[1]))
        MinorTextFormat_ratio(t_bin1_text)
        t_bin2_text = TLatex(x_position[2], y_position, str(bin_text[2]))
        MinorTextFormat_ratio(t_bin2_text)
        t_bin3_text = TLatex(x_position[3], y_position, str(bin_text[3]))
        MinorTextFormat_ratio(t_bin3_text)
        t_bin4_text = TLatex(x_position[4], y_position, str(bin_text[4]))
        MinorTextFormat_ratio(t_bin4_text)
        t_bin5_text = TLatex(x_position[5], y_position, str(bin_text[5]))
        MinorTextFormat_ratio(t_bin5_text)
        t_bin6_text = TLatex(x_position[6], y_position, str(bin_text[6]))
        MinorTextFormat_ratio(t_bin6_text)
        t_bin7_text = TLatex(x_position[7], y_position, str(bin_text[7]))
        MinorTextFormat_ratio(t_bin7_text)
        t_bin8_text = TLatex(x_position[8], y_position, str(bin_text[8]))
        MinorTextFormat_ratio(t_bin8_text)
        t_bin9_text = TLatex(x_position[9], y_position, str(bin_text[9]))
        MinorTextFormat_ratio(t_bin9_text)
        t_bin10_text = TLatex(x_position[10], y_position, str(bin_text[10]))
        MinorTextFormat_ratio(t_bin10_text)
        t_bin11_text = TLatex(x_position[11], y_position, str(bin_text[11]))
        MinorTextFormat_ratio(t_bin11_text)
        t_bin12_text = TLatex(x_position[12], y_position, str(bin_text[12]))
        MinorTextFormat_ratio(t_bin12_text)
        t_bin13_text = TLatex(x_position[13], y_position, str(bin_text[13]))
        MinorTextFormat_ratio(t_bin13_text)
        t_bin14_text = TLatex(x_position[14], y_position, str(bin_text[14]))
        MinorTextFormat_ratio(t_bin14_text)
        t_bin15_text = TLatex(x_position[15], y_position, str(bin_text[15]))
        MinorTextFormat_ratio(t_bin15_text)

        pad1.cd() # go back to pad1


        ta_bin0_text = TLatex(x_position[0], ya_position, str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position, str(data_yield[5]))
        tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, str(data_yield[6]))
        tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        MinorTextFormat_ratio(tb_bin6_text)
        ta_bin7_text = TLatex(x_position[7], ya_position, str(data_yield[7]))
        tb_bin7_text = TLatex(x_position[7], ya_position - 0.02, str(S_yield[7]))
        MinorTextFormat_ratio(ta_bin7_text)
        MinorTextFormat_ratio(tb_bin7_text)
        ta_bin8_text = TLatex(x_position[8], ya_position, str(data_yield[8]))
        tb_bin8_text = TLatex(x_position[8], ya_position - 0.02, str(S_yield[8]))
        MinorTextFormat_ratio(ta_bin8_text)
        MinorTextFormat_ratio(tb_bin8_text)
        ta_bin9_text = TLatex(x_position[9], ya_position, str(data_yield[9]))
        tb_bin9_text = TLatex(x_position[9], ya_position - 0.02, str(S_yield[9]))
        MinorTextFormat_ratio(ta_bin9_text)
        MinorTextFormat_ratio(tb_bin9_text)
        ta_bin10_text = TLatex(x_position[10], ya_position, str(data_yield[10]))
        tb_bin10_text = TLatex(x_position[10], ya_position - 0.02, str(S_yield[10]))
        MinorTextFormat_ratio(ta_bin10_text)
        MinorTextFormat_ratio(tb_bin10_text)
        ta_bin11_text = TLatex(x_position[11], ya_position, str(data_yield[11]))
        tb_bin11_text = TLatex(x_position[11], ya_position - 0.02, str(S_yield[11]))
        MinorTextFormat_ratio(ta_bin11_text)
        MinorTextFormat_ratio(tb_bin11_text)
        ta_bin12_text = TLatex(x_position[12], ya_position, str(data_yield[12]))
        tb_bin12_text = TLatex(x_position[12], ya_position - 0.02, str(S_yield[12]))
        MinorTextFormat_ratio(ta_bin12_text)
        MinorTextFormat_ratio(tb_bin12_text)
        ta_bin13_text = TLatex(x_position[13], ya_position, str(data_yield[13]))
        tb_bin13_text = TLatex(x_position[13], ya_position - 0.02, str(S_yield[13]))
        MinorTextFormat_ratio(ta_bin13_text)
        MinorTextFormat_ratio(tb_bin13_text)
        ta_bin14_text = TLatex(x_position[14], ya_position, str(data_yield[14]))
        tb_bin14_text = TLatex(x_position[14], ya_position - 0.02, str(S_yield[14]))
        MinorTextFormat_ratio(ta_bin14_text)
        MinorTextFormat_ratio(tb_bin14_text)
        ta_bin15_text = TLatex(x_position[15], ya_position, str(data_yield[15]))
        tb_bin15_text = TLatex(x_position[15], ya_position - 0.02, str(S_yield[15]))
        MinorTextFormat_ratio(ta_bin15_text)
        MinorTextFormat_ratio(tb_bin15_text)
        
        pad2.cd()

    # Electron channel bit maps
    if (branches[i] == "Idbit_l1_mva" or branches[i] == "Idbit_l1_cut" or branches[i] == "Idbit_l2_mva" or branches[i] == "Idbit_l2_cut" or branches[i] == "trigger_IDbit"):
        if branches[i] == "trigger_IDbit" :
            bin_text = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""] 
        else:
            bin_text = ["#bar{lmth}","l#bar{mth}", "m#bar{lth}", "lm#bar{th}", "t#bar{lmh}", "lt#bar{mh}", "mt#bar{lh}", "lmt#bar{h}", "h#bar{lmt}", "lh#bar{mt}", "mh#bar{lt}", "lmh#bar{t}", "th#bar{lm}", "lth#bar{m}", "mth#bar{l}", "lmth"]
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.12
        for k in range(bins[i]):
            x_position.append( (0.97 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (0.25 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ bins[i])
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        t_bin0_text = TLatex(x_position[0], y_position, str(bin_text[0]))
        MinorTextFormat_ratio(t_bin0_text)
        t_bin1_text = TLatex(x_position[1], y_position, str(bin_text[1]))
        MinorTextFormat_ratio(t_bin1_text)
        t_bin2_text = TLatex(x_position[2], y_position, str(bin_text[2]))
        MinorTextFormat_ratio(t_bin2_text)
        t_bin3_text = TLatex(x_position[3], y_position, str(bin_text[3]))
        MinorTextFormat_ratio(t_bin3_text)
        t_bin4_text = TLatex(x_position[4], y_position, str(bin_text[4]))
        MinorTextFormat_ratio(t_bin4_text)
        t_bin5_text = TLatex(x_position[5], y_position, str(bin_text[5]))
        MinorTextFormat_ratio(t_bin5_text)
        t_bin6_text = TLatex(x_position[6], y_position, str(bin_text[6]))
        MinorTextFormat_ratio(t_bin6_text)
        t_bin7_text = TLatex(x_position[7], y_position, str(bin_text[7]))
        MinorTextFormat_ratio(t_bin7_text)
        t_bin8_text = TLatex(x_position[8], y_position, str(bin_text[8]))
        MinorTextFormat_ratio(t_bin8_text)
        t_bin9_text = TLatex(x_position[9], y_position, str(bin_text[9]))
        MinorTextFormat_ratio(t_bin9_text)
        t_bin10_text = TLatex(x_position[10], y_position, str(bin_text[10]))
        MinorTextFormat_ratio(t_bin10_text)
        t_bin11_text = TLatex(x_position[11], y_position, str(bin_text[11]))
        MinorTextFormat_ratio(t_bin11_text)
        t_bin12_text = TLatex(x_position[12], y_position, str(bin_text[12]))
        MinorTextFormat_ratio(t_bin12_text)
        t_bin13_text = TLatex(x_position[13], y_position, str(bin_text[13]))
        MinorTextFormat_ratio(t_bin13_text)
        t_bin14_text = TLatex(x_position[14], y_position, str(bin_text[14]))
        MinorTextFormat_ratio(t_bin14_text)
        t_bin15_text = TLatex(x_position[15], y_position, str(bin_text[15]))
        MinorTextFormat_ratio(t_bin15_text)

        pad1.cd() # go back to pad1

        ta_bin0_text = TLatex(x_position[0], ya_position, str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position, str(data_yield[5]))
        tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, str(data_yield[6]))
        tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        MinorTextFormat_ratio(tb_bin6_text)
        ta_bin7_text = TLatex(x_position[7], ya_position, str(data_yield[7]))
        tb_bin7_text = TLatex(x_position[7], ya_position - 0.02, str(S_yield[7]))
        MinorTextFormat_ratio(ta_bin7_text)
        MinorTextFormat_ratio(tb_bin7_text)
        ta_bin8_text = TLatex(x_position[8], ya_position, str(data_yield[8]))
        tb_bin8_text = TLatex(x_position[8], ya_position - 0.02, str(S_yield[8]))
        MinorTextFormat_ratio(ta_bin8_text)
        MinorTextFormat_ratio(tb_bin8_text)
        ta_bin9_text = TLatex(x_position[9], ya_position, str(data_yield[9]))
        tb_bin9_text = TLatex(x_position[9], ya_position - 0.02, str(S_yield[9]))
        MinorTextFormat_ratio(ta_bin9_text)
        MinorTextFormat_ratio(tb_bin9_text)
        ta_bin10_text = TLatex(x_position[10], ya_position, str(data_yield[10]))
        tb_bin10_text = TLatex(x_position[10], ya_position - 0.02, str(S_yield[10]))
        MinorTextFormat_ratio(ta_bin10_text)
        MinorTextFormat_ratio(tb_bin10_text)
        ta_bin11_text = TLatex(x_position[11], ya_position, str(data_yield[11]))
        tb_bin11_text = TLatex(x_position[11], ya_position - 0.02, str(S_yield[11]))
        MinorTextFormat_ratio(ta_bin11_text)
        MinorTextFormat_ratio(tb_bin11_text)
        ta_bin12_text = TLatex(x_position[12], ya_position, str(data_yield[12]))
        tb_bin12_text = TLatex(x_position[12], ya_position - 0.02, str(S_yield[12]))
        MinorTextFormat_ratio(ta_bin12_text)
        MinorTextFormat_ratio(tb_bin12_text)
        ta_bin13_text = TLatex(x_position[13], ya_position, str(data_yield[13]))
        tb_bin13_text = TLatex(x_position[13], ya_position - 0.02, str(S_yield[13]))
        MinorTextFormat_ratio(ta_bin13_text)
        MinorTextFormat_ratio(tb_bin13_text)
        ta_bin14_text = TLatex(x_position[14], ya_position, str(data_yield[14]))
        tb_bin14_text = TLatex(x_position[14], ya_position - 0.02, str(S_yield[14]))
        MinorTextFormat_ratio(ta_bin14_text)
        MinorTextFormat_ratio(tb_bin14_text)
        ta_bin15_text = TLatex(x_position[15], ya_position, str(data_yield[15]))
        tb_bin15_text = TLatex(x_position[15], ya_position - 0.02, str(S_yield[15]))
        MinorTextFormat_ratio(ta_bin15_text)
        MinorTextFormat_ratio(tb_bin15_text)

        pad2.cd()
    ############# RATIO TEXT SECTION: END ###########################################################################



    # Legend and other text
    pad1.cd() # go back to pad1

    # Legend
    #leg=TLegend(.38,.65,.88,.87) #(x_min, y_min, x_max, y_max)
    leg=TLegend(.13,.67,.89,.83) #(x_min, y_min, x_max, y_max)
    #leg=TLegend() # automatic legend placement sucks
    leg.SetNColumns(3) # use 2 columns for legend
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(22)
    leg.SetTextSize(0.03)
    leg.AddEntry(B0,Background_files[0],"f")
    #leg.AddEntry(B1,Background_files[1],"f")
    leg.AddEntry(SB1,"ST_tW","f")
    #leg.AddEntry(SB2,"Diboson (WW/WZ/ZZ)","f")
    leg.AddEntry(SB3,"DY_2L","f") # "f" for fills, "L" for linesi, "lep" for data points
    #leg.AddEntry(ZB,"DYJetsTo2LM-50HT","f") # "f" for fills, "L" for linesi, "lep" for data points
    #leg.AddEntry(B7,"DY_M10-50_amcnlo","f")
    #leg.AddEntry(YB,"DY1JetsTo2LM1-10HT","f")
    #if not(channel== "mu" and channel2== "mu"):
    #    leg.AddEntry(GBX,"GJetsHT","f")
    #leg.AddEntry(SQ,"QCD","f")
    leg.AddEntry(S1,"TTH_Haa4B_M-15*"+ str(sig_scaling),"L")
    leg.AddEntry(S2,"TTH_Haa4B_M-30*"+ str(sig_scaling),"L")
    leg.AddEntry(S3,"TTH_Haa4B_M-55*"+ str(sig_scaling),"L")
    leg.AddEntry(SD,"Data partially blind","lep")
    leg.Draw("SAME")

    ############# TEXT SECTION: START ###########################################################################

    # CMS stuff
    tex = TLatex(0.51,0.88,"#bf{CMS} #it{Work in Progress}                                     59.83 fb^{-1} (13 TeV, 2018)")    
    tex.SetNDC()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetTextAlign(20)
    tex.Draw("SAME")

    # Channel
    if (channel =="mu" and channel2 == "mu"):
        chan = TLatex(0.11,0.84,"  #bf{muon-muon}")
    elif (channel =="el" and channel2 == "el"):
        chan = TLatex(0.11,0.84,"  #bf{electron-electron}")
    elif (channel =="mu" and channel2 == "el"):
        chan = TLatex(0.11,0.84,"  #bf{muon-electron}")
    else:
        chan = TLatex(0.11,0.84,"  #bf{electron-muon}")
    chan.SetNDC()
    chan.SetTextFont(42)
    chan.SetTextSize(0.04)
    chan.Draw("SAME")
    
    
    # PNET score "score_Haa4b_vs_QCD"
    if PNET_score == "0":
        t_score = TLatex(0.45,0.855,"No score_Haa4b_vs_QCD cut")
    elif PNET_score == "fail":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD < 0.5")
    elif PNET_score == "Fail_WP40-80":
        t_score = TLatex(0.4,0.835,"0.5 < score_Haa4b_vs_QCD < 0.992")
    elif PNET_score == "pass":
        t_score = TLatex(0.23,0.81,"score_Haa4b_vs_QCD > 0.8")
    else:    
        t_score = TLatex(0.45,0.855,"score_Haa4b_vs_QCD > "+ PNET_score)
    MinorTextFormat(t_score)
    '''
    if PNET_score == "0":
        t_score = TLatex(0.4,0.835,"No score_Haa4b_vs_QCD cut")
    elif PNET_score == "fail":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD < 0.5")
    elif PNET_score == "Fail_WP40-80":
        t_score = TLatex(0.4,0.835,"0.5 < score_Haa4b_vs_QCD < 0.992")
    elif PNET_score == "pass":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD > 0.8")
    else:    
        t_score = TLatex(0.4,0.835,"score_Haa4b_vs_QCD > "+ PNET_score)
    MinorTextFormat(t_score)
    '''
    
    # Extra categories
    if (category):
        t_cat = TLatex(0.7,0.855, region)
        MinorTextFormat(t_cat)
    # Additional cuts
    if (cut_add):
        t_cut = TLatex(0.45,0.835, cut_add)
        MinorTextFormat(t_cut)
    # MC scaled to data
    if (dataScale == "scale" and scale_status):
        t_dscale = TLatex(0.7,0.835, "MC scaled to Data")
        MinorTextFormat(t_dscale)

    '''
    if (category):
        t_cat = TLatex(0.4,0.855, region)
        MinorTextFormat(t_cat)
    if (cut_add):
        t_cut = TLatex(0.4,0.875, cut_add)
        MinorTextFormat(t_cut)
    if (dataScale == "scale"):
        t_dscale = TLatex(0.7,0.835, "MC scaled to Data")
        MinorTextFormat(t_dscale)
    '''

    # Z purity regions
    if (cut == z_peak):
        Z_text = "High Z Purity"
        t_z = TLatex(0.2,0.78, Z_text)
        MinorTextFormat(t_z)
    if (cut == z_tail):
        Z_text = "Low Z Purity"
        t_z = TLatex(0.2,0.78, Z_text)
        MinorTextFormat(t_z)

    # Display bin text for specifc plots
    if (branches[i] == "Muon_pfIsoId_l1" or branches[i] == "Muon_pfIsoId_l2"):
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            #x_position.append(0.02 + 0.8*((k+1)/(bins[i])))
            x_position.append(0.81*((k+1)/(bins[i])))
            #x_position.append( (1 + k)/ (bins[i] - 1))
            #x_position.append( (1 + k)/ (bins[i] - 1))
            #bin_yield.append(int(SD.GetBinContent(k+1)))
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        ta_bin0_text = TLatex(x_position[0], ya_position, "d= " + str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, "s= " + str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, "d= " + str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, "s= " + str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, "d= " + str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, "s= " + str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, "d= " + str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, "s= " + str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, "d= " + str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, "s= " + str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position, "d= " + str(data_yield[5]))
        tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, "s= " + str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, "d= " + str(data_yield[6]))
        tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, "s= " + str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        MinorTextFormat_ratio(tb_bin6_text)
        
    if (branches[i] == "Muon_miniIsoId_l1" or branches[i] == "Muon_miniIsoId_l2"):
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            x_position.append(0.81*((k+1)/(bins[i])))
            #x_position.append( (1 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ (bins[i] - 1))
            #bin_yield.append(int(SD.GetBinContent(k+1)))
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        ta_bin0_text = TLatex(x_position[0], ya_position, "d= " + str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, "s= " + str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, "d= " + str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, "s= " + str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, "d= " + str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, "s= " + str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, "d= " + str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, "s= " + str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, "d= " + str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, "s= " + str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        
    # PNET SCORE S/sqrt(S+B)
    STH = ST.GetStack().Last()
    if (branches[i] == "score_Haa4b_vs_QCD" or branches[i] == "score_Haa4b_vs_QCD_v1" or branches[i] == "score_Haa4b_vs_QCD_34" or branches[i] == "score_Haa4b_vs_QCD_v2a" or branches[i] == "score_Haa4b_vs_QCD_v2b"):
        B_yield = []
        S_yield = []
        significance = []
        x_position = []
        y_position = 0.37
        ya_position = 0.05
        SGN_text = "#frac{S(TTH)}{#sqrt{S+B}}"
        significance_text = TLatex(0.01, ya_position, SGN_text)
        #tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, str(S_yield[0]))
        MinorTextFormat_ratio(significance_text)
        for k in range(bins[i]):
            x_position.append( (1.3 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (0.25 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ bins[i])
            Bkg_yield = STH.GetBinContent(k+1)
            B_yield.append(round(Bkg_yield, 4))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 4))
        #print(S_yield)
        #print(B_yield)
        for k in range(bins[i]):
            idx = bins[i]- k -1
            S = sum(S_yield[idx:])
            B = sum(B_yield[idx:])
            #print(idx, S, B)
            SGN = S/np.sqrt(S+B)
            significance.append(round(SGN, 2))
        #print(significance)
        significance.reverse()
        #print(significance)
        ta_bin0_text = TLatex(x_position[0], ya_position, str(significance[0]))
        #tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        #MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position -0.02, str(significance[1]))
        #tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        #MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, str(significance[2]))
        #tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        #MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position -0.02, str(significance[3]))
        #tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        #MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, str(significance[4]))
        #tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        #MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position -0.02, str(significance[5]))
        #tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        #MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, str(significance[6]))
        #tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        #MinorTextFormat_ratio(tb_bin6_text)
        ta_bin7_text = TLatex(x_position[7], ya_position -0.02, str(significance[7]))
        #tb_bin7_text = TLatex(x_position[7], ya_position - 0.02, str(S_yield[7]))
        MinorTextFormat_ratio(ta_bin7_text)
        #MinorTextFormat_ratio(tb_bin7_text)
        ta_bin8_text = TLatex(x_position[8], ya_position, str(significance[8]))
        #tb_bin8_text = TLatex(x_position[8], ya_position - 0.02, str(S_yield[8]))
        MinorTextFormat_ratio(ta_bin8_text)
        #MinorTextFormat_ratio(tb_bin8_text)
        ta_bin9_text = TLatex(x_position[9], ya_position -0.02, str(significance[9]))
        #tb_bin9_text = TLatex(x_position[9], ya_position - 0.02, str(S_yield[9]))
        MinorTextFormat_ratio(ta_bin9_text)
        #MinorTextFormat_ratio(tb_bin9_text)
        ta_bin10_text = TLatex(x_position[10], ya_position, str(significance[10]))
        #tb_bin10_text = TLatex(x_position[10], ya_position - 0.02, str(S_yield[10]))
        MinorTextFormat_ratio(ta_bin10_text)
        #MinorTextFormat_ratio(tb_bin10_text)
        ta_bin11_text = TLatex(x_position[11], ya_position -0.02, str(significance[11]))
        #tb_bin11_text = TLatex(x_position[11], ya_position - 0.02, str(S_yield[11]))
        MinorTextFormat_ratio(ta_bin11_text)
        #MinorTextFormat_ratio(tb_bin11_text)
        ta_bin12_text = TLatex(x_position[12], ya_position, str(significance[12]))
        #tb_bin12_text = TLatex(x_position[12], ya_position - 0.02, str(S_yield[12]))
        MinorTextFormat_ratio(ta_bin12_text)
        #MinorTextFormat_ratio(tb_bin12_text)
        ta_bin13_text = TLatex(x_position[13], ya_position -0.02, str(significance[13]))
        #tb_bin13_text = TLatex(x_position[13], ya_position - 0.02, str(S_yield[13]))
        MinorTextFormat_ratio(ta_bin13_text)
        #MinorTextFormat_ratio(tb_bin13_text)
        ta_bin14_text = TLatex(x_position[14], ya_position, str(significance[14]))
        #tb_bin14_text = TLatex(x_position[14], ya_position - 0.02, str(S_yield[14]))
        MinorTextFormat_ratio(ta_bin14_text)
        #MinorTextFormat_ratio(tb_bin14_text)
        ta_bin15_text = TLatex(x_position[15], ya_position -0.02, str(significance[15]))
        #tb_bin15_text = TLatex(x_position[15], ya_position - 0.02, str(S_yield[15]))
        MinorTextFormat_ratio(ta_bin15_text)
        #MinorTextFormat_ratio(tb_bin15_text)
        ta_bin16_text = TLatex(x_position[16], ya_position, str(significance[16]))
        #tb_bin16_text = TLatex(x_position[16], ya_position - 0.02, str(S_yield[16]))
        MinorTextFormat_ratio(ta_bin16_text)
        #MinorTextFormat_ratio(tb_bin16_text)
        ta_bin17_text = TLatex(x_position[17], ya_position -0.02, str(significance[17]))
        #tb_bin17_text = TLatex(x_position[17], ya_position - 0.02, str(S_yield[17]))
        MinorTextFormat_ratio(ta_bin17_text)
        #MinorTextFormat_ratio(tb_bin17_text)
        ta_bin18_text = TLatex(x_position[18], ya_position, str(significance[18]))
        #tb_bin18_text = TLatex(x_position[18], ya_position - 0.02, str(S_yield[18]))
        MinorTextFormat_ratio(ta_bin18_text)
        #MinorTextFormat_ratio(tb_bin18_text)
        ta_bin19_text = TLatex(x_position[19], ya_position - 0.02, str(significance[19]))
        #tb_bin19_text = TLatex(x_position[19], ya_position - 0.02, str(S_yield[19]))
        MinorTextFormat_ratio(ta_bin19_text)
        #MinorTextFormat_ratio(tb_bin19_text)
        
    ####################################### TEXT SECTION: END ###############################################

    # Redraw axis + border since they are hidden by histo fills
    ROOT.gPad.RedrawAxis() 
    borderline = TLine(range_max[i], 1.0,range_max[i], SD.GetMaximum())
    borderline.SetLineColor(kBlack)
    borderline.SetLineWidth(1)
    borderline.Draw("SAME")
    
    # Save individual pdf/png

    #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
    #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")

    # Save one pdf with all canvas
    if i == 0:
        #c.Print(output_dir+ "/Data-MC_2L_tt_" + regb + "_"+ channel + channel2 + "_" + sc + "_" + date + ".pdf(", "Title: " + branches[i])
        c.Print(output_dir+ "/Data-MC_2L_tt_" + regb + "_"+ channel + channel2 + "_" + sc + "_" + date + ".pdf(", "Title: " + branches[i])
    elif i == len(branches)-1:
        #c.Print(output_dir+ "/Data-MC_2L_tt_" + regb + "_"+ channel + channel2 + "_" + sc + "_" + date + ".pdf)", "Title: " + branches[i])
        c.Print(output_dir+ "/Data-MC_2L_tt_" + regb + "_"+ channel + channel2 + "_" + sc + "_" + date + ".pdf)", "Title: " + branches[i])
    else:
        #c.Print(output_dir+ "/Data-MC_2L_tt_" + regb + "_"+ channel + channel2 + "_" + sc + "_" + date + ".pdf", "Title: " + branches[i])
        c.Print(output_dir+ "/Data-MC_2L_tt_" + regb + "_"+ channel + channel2 + "_" + sc + "_" + date + ".pdf", "Title: " + branches[i])
    #if i == 0:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf(", "Title: " + branches[i])
    #elif i == len(branches)-1:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf)", "Title: " + branches[i])
    #else:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf", "Title: " + branches[i])
    # Delete all histos to avoid warning when initializing histos with same name
    #B0.SetDirectory(0)
    c.Close() # Delete Canvas after saving it

# Loops back to the next variable to plot
