#!/bin/bash

# No PNET cut
python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="noscale" --score="0" --date="SS020125" --addcut=""
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="noscale" --score="0" --date="SS020125" --addcut=""

#PNET cut > 0.5
python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="noscale" --score="0.5" --date="SS020125c" --addcut=""
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="noscale" --score="0.5" --date="SS020125c" --addcut=""

#PNET cut > 0.5 and scale MC to data
python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="SS020125cs" --addcut=""
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="SS020125cs" --addcut=""
