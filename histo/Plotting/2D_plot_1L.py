#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH2D, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


def Histo_style(h, x_title, y_title):

    h_y = h.GetYaxis()
    h_x = h.GetXaxis()

    # x-axis 
    h_x.SetTitle(x_title)
    h_x.SetTitleSize(30) 
    h_x.SetTitleFont(43) 
    h_x.SetTitleOffset(1.1)
    h_x.SetLabelFont(43) # font and size of numbers
    h_x.SetLabelSize(25)

    # y-axis
    h_y.SetTitle(y_title) # space used for offset
    h_y.SetTitleSize(30) 
    h_y.SetTitleFont(43) 
    h_y.SetTitleOffset(1.25)
    h_y.SetLabelFont(43) # font and size of numbers
    h_y.SetLabelSize(25)

# Definition of v2 WP
PFWP40 = ["(score_Haa4b_vs_QCD > 0.960)", "((score_Haa4b_vs_QCD < 0.960) && (score_Haa4b_vs_QCD > 0.84))"]
PFWP60 = ["(score_Haa4b_vs_QCD > 0.930)", "((score_Haa4b_vs_QCD < 0.930) && (score_Haa4b_vs_QCD > 0.660))"]
PFWP80 = ["(score_Haa4b_vs_QCD > 0.840)", "((score_Haa4b_vs_QCD < 0.840) && (score_Haa4b_vs_QCD > 0.40))"]
# Definition of v2a WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v2a > 0.968)", "((score_Haa4b_vs_QCD_v2a < 0.968) && (score_Haa4b_vs_QCD_v2a > 0.84))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v2a > 0.944)", "((score_Haa4b_vs_QCD_v2a < 0.944) && (score_Haa4b_vs_QCD_v2a > 0.660))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v2a > 0.868)", "((score_Haa4b_vs_QCD_v2a < 0.868) && (score_Haa4b_vs_QCD_v2a > 0.40))"]
# Definition of v2b WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v2b > 0.952)", "((score_Haa4b_vs_QCD_v2b < 0.952) && (score_Haa4b_vs_QCD_v2b > 0.84))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v2b > 0.916)", "((score_Haa4b_vs_QCD_v2b < 0.916) && (score_Haa4b_vs_QCD_v2b > 0.660))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v2b > 0.814)", "((score_Haa4b_vs_QCD_v2b < 0.814) && (score_Haa4b_vs_QCD_v2b > 0.40))"]
# Definition of v1 WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v1 > 0.992)", "((score_Haa4b_vs_QCD_v1 < 0.992) && (score_Haa4b_vs_QCD_v1 > 0.92))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v1 > 0.975)", "((score_Haa4b_vs_QCD_v1 < 0.975) && (score_Haa4b_vs_QCD_v1 > 0.80))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v1 > 0.920)", "((score_Haa4b_vs_QCD_v1 < 0.920) && (score_Haa4b_vs_QCD_v1 > 0.50))"]


PF_object_titles = ["Pass", "Fail"]

# command inputs
channel = sys.argv[1]
WP = sys.argv[2]
sub = sys.argv[3]
comb = sys.argv[4]

if (WP == "WP40"):
    PFWP = PFWP40
    PFWP_convention = "WP40"
elif (WP == "WP60"):
    PFWP = PFWP60
    PFWP_convention = "WP60"
elif (WP == "WP80"):
    PFWP = PFWP80
    PFWP_convention = "WP80"
else:
    exit

###################################################################################################
##############  Manually adjust CHANNEL  ##########################################################
#channel = "mu"
#channel = "el"
############### Manually adjust Pass/fail Working Point  ##########################################
#PFWP = PFWP40
#PFWP = PFWP60
#PFWP = PFWP80
#cut = " && nBJetM_add == 0 && dphi_fatjet_METlep > 3*pi/4"
###################################################################################################
###################################################################################################


# input/output directory and tree name
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_1L_" + channel + "_nom/"
output_dir = "../plots_1L"
tree_name = "event_tree"
weight = "weight_tot" #"weight_zPt_nominal"
#weight = "weight_HEM_nominal*weight_GEN_nominal"

# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1

# Scaling for MC histos

## WJets background (HT bins)
WJets_path = "WJets_LNu_HT"
WJets_files = ["70-100", "100-200","200-400", "400-600", "600-800", "800-1200","1200-2500", "2500-inf"]
WJets_xsec = [1440, 1431, 382.1, 51.54, 12.49, 5.619, 1.321, 0.02992]
WJets_ev = [66569448, 51541593, 58331446, 7444030, 7718765, 7296689, 6481518, 2097648]
WJets_scale = []
for i in range(len(WJets_xsec)):
    WJets_scale.append(lumi*WJets_xsec[i]/WJets_ev[i])


# signal WH (W->lv): [M-15, M-30, M-55]
Signal_WH_files = ["SUSY_WH_M-15", "SUSY_WH_M-30", "SUSY_WH_M-55"]
Signal_WH_xsec = [0.179256, 0.179256, 0.179256]
Signal_WH_ev = [103447, 104740, 104342]
Signal_WH_scale =[]
for i in range(len(Signal_WH_xsec)):
    Signal_WH_scale.append(lumi*Signal_WH_xsec[i]/Signal_WH_ev[i])

# TTH Signal: [M-15, M-30, M-55]
Signal_TTH_files = ["SUSY_TTH_M-15", "SUSY_TTH_M-30", "SUSY_TTH_M-55"]
Signal_TTH_xsec = [0.1445, 0.1445, 0.1445]
Signal_TTH_ev = [188898, 196711, 198645]
Signal_TTH_scale =[]
for i in range(len(Signal_TTH_xsec)):
    Signal_TTH_scale.append(lumi*Signal_TTH_xsec[i]/Signal_TTH_ev[i])


# Other backgrounds:
Background_files = ["TT_2L2Nu", "TT_SemiLeptonic", "TTW", "ST_TW_Top_NoFullyHadronic", "ST_TW_AntiTop_NoFullyHadronic", "ST-s_leptonic", "ST-s_hadronic", "ST-t_Top_inclusive", "ST-t_AntiTop_inclusive", "WW", "WZ", "ZZ", "WH_Hbb_Wpluslnu", "WH_Hbb_Wminuslnu", "DY_10-50"]
Background_xsec = [87.339, 364.328, 0.6008, 21.63, 21.63, 4.831, 5.041, 134.2, 80, 118.7, 47.13, 16.523, 0.1576, 0.0999, 18610]
Background_ev = [143887383, 471724160, 27686862, 11269836, 11012326, 12607741, 10793026, 197426478, 98724306, 15679000, 7940000, 3526000, 4562550, 4870968, 99288125]
Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])

# Data 
data_mu = ["data_SingleMuonA", "data_SingleMuonB", "data_SingleMuonC", "data_SingleMuonD"]
data_el = ["data_EGammaA", "data_EGammaB", "data_EGammaC", "data_EGammaD"]

# Histo variables, labels, range and binsi
# x information
x_root_titles = ["mass", "msoft", "msoftPtScaled", "pnet"]
xbranches = ["m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet","mH_avg"]
xrange_min = 0
xrange_max = 300
xbins = 30

# y information
y_root_titles = "mA"
ybranches = "mA_34a"
#ybranches = "mA_34d" # mA_avg, mA_34b, mA_34d
yrange_min = 0
yrange_max = 70
ybins = 35

# Naming convention
if channel =="mu":
    data_files = data_mu
    decay_channel = "mv"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "lv"
else:
    data_files = data_el
    decay_channel = "ev"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "lv"

if sub =="Hi":
    decay_channel = "W" + decay_channel + sub
    cut =  " && nBJetM_add == 0 && dphi_fatjet_METlep > 3*pi/4" + " && pt_METlep > 300"
    Signal_files = Signal_WH_files 
    Signal_scale = Signal_WH_scale
    sig_convention = "WH"
elif sub == "Lo":
    decay_channel = "W" + decay_channel + sub
    cut =  " && nBJetM_add == 0 && dphi_fatjet_METlep > 3*pi/4" + " && pt_METlep < 300"
    Signal_files = Signal_WH_files
    Signal_scale = Signal_WH_scale
    sig_convention = "WH"
elif sub == "ttb":
    decay_channel = sub + decay_channel 
    cut =  " && nBJetM_add == 1"
    Signal_files = Signal_TTH_files
    Signal_scale = Signal_TTH_scale
    sig_convention = "ttH"
elif sub == "ttbb":
    decay_channel = sub + decay_channel 
    cut =  " && nBJetM_add > 1"
    Signal_files = Signal_TTH_files
    Signal_scale = Signal_TTH_scale
    sig_convention = "ttH"
else:
    decay_channel = "W" + decay_channel
    cut =  " && nBJetM_add == 0 && dphi_fatjet_METlep > 3*pi/4" 
    Signal_files = Signal_WH_files
    Signal_scale = Signal_WH_scale
    sig_convention = "WH"


# samples info
samples_name = [Signal_files[0], Signal_files[1], Signal_files[2], "data_2018", "WJets_LNu_HT", "TT_SemiLeptonic"]
samples_convention = [sig_convention + "toaato4b_mA_15", sig_convention + "toaato4b_mA_30", sig_convention + "toaato4b_mA_55", "Data", "Wlv", "TT1l"]

syst = "Nom"
year = "2018"
date = "_012325"
#decay_channel = "Wlv"

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box

for q in range(len(samples_name)):
    output_file = TFile(output_dir + "/root_" + channel + "/" + PFWP_convention + "/" + decay_channel + "_" + samples_convention[q] + "_" + year + ".root","recreate")
    for k in range(len(PFWP)):
        draw_option = f"({PFWP[k] + cut})*({weight})"
        for i in range(len(xbranches)): 
            c = TCanvas("c", "canvas", 800, 800)
            c.SetTopMargin(0.11)
            c.SetRightMargin(0.13)
            c.SetLeftMargin(0.13)
            
            if samples_name[q] == "data_2018":
                # do that
                # Data 2018A
                fD0 = TFile(input_dir+ data_files[0] + ".root", "READ")
                events_D0 = fD0.Get(tree_name)
                D0 = TH2D("D0", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D0.Draw(ybranches + ":" + xbranches[i] + ">>D0",PFWP[k] + cut)
                
                # Data 2018B
                fD1 = TFile(input_dir+ data_files[1] + ".root", "READ")
                events_D1 = fD1.Get(tree_name)
                D1 = TH2D("D1", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D1.Draw(ybranches + ":" + xbranches[i] + ">>D1",PFWP[k] + cut)

                # Data 2018C
                fD2 = TFile(input_dir+ data_files[2] + ".root", "READ")
                events_D2 = fD2.Get(tree_name)
                D2 = TH2D("D2", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D2.Draw(ybranches + ":" + xbranches[i] + ">>D2",PFWP[k] + cut)
                
                # Data 2018D
                fD3 = TFile(input_dir+ data_files[3] + ".root", "READ")
                events_D3 = fD3.Get(tree_name)
                D3 = TH2D("D3", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D3.Draw(ybranches + ":" + xbranches[i] + ">>D3",PFWP[k] + cut)

                # Adding Histos together
                D = TH2D("D", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max) 
                D.Add(D0)
                D.Add(D1)
                D.Add(D2)
                D.Add(D3)

                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(D, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                D.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.15,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(D, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it
                
            elif samples_name[q] == "WJets_LNu_HT":
                
                # Load all HT bins 
                # W Jets HT70-100 
                p_WB0 = input_dir + WJets_path + WJets_files[0] + ".root"
                if (os.path.isfile(p_WB0)):
                    f_WB0 = TFile(p_WB0, "READ") 
                    events_WB0 = f_WB0.Get(tree_name)
                    WB0_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB0 = TH2D("WB0", WB0_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB0.Draw(ybranches + ":" + xbranches[i] + ">>WB0",draw_option)
                    WB0.Scale(WJets_scale[0])
                else:
                    WB0_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB0 = TH2D("WB0", WB0_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # W Jets HT100-200 
                p_WB1 = input_dir + WJets_path + WJets_files[1] + ".root"
                if (os.path.isfile(p_WB1)):
                    f_WB1 = TFile(p_WB1, "READ") 
                    events_WB1 = f_WB1.Get(tree_name)
                    WB1_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB1 = TH2D("WB1", WB1_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB1.Draw(ybranches + ":" + xbranches[i] + ">>WB1",draw_option)
                    WB1.Scale(WJets_scale[1])
                else:
                    WB1_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB1 = TH2D("WB1", WB1_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
            
                # W Jets HT200-400 
                p_WB2 = input_dir + WJets_path + WJets_files[2] + ".root"
                if (os.path.isfile(p_WB2)):
                    f_WB2 = TFile(p_WB2, "READ") 
                    events_WB2 = f_WB2.Get(tree_name)
                    WB2_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB2 = TH2D("WB2", WB2_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB2.Draw(ybranches + ":" + xbranches[i] + ">>WB2",draw_option)
                    WB2.Scale(WJets_scale[2])
                else:
                    WB2_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB2 = TH2D("WB2", WB2_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # W Jets HT400-600 
                p_WB3 = input_dir + WJets_path + WJets_files[3] + ".root"
                if (os.path.isfile(p_WB3)):
                    f_WB3 = TFile(p_WB3, "READ") 
                    events_WB3 = f_WB3.Get(tree_name)
                    WB3_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB3 = TH2D("WB3", WB3_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB3.Draw(ybranches + ":" + xbranches[i] + ">>WB3",draw_option)
                    WB3.Scale(WJets_scale[3])
                else:
                    WB3_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB3 = TH2D("WB3", WB3_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # W Jets HT600-800 
                p_WB4 = input_dir + WJets_path + WJets_files[4] + ".root"
                if (os.path.isfile(p_WB4)):
                    f_WB4 = TFile(p_WB4, "READ") 
                    events_WB4 = f_WB4.Get(tree_name)
                    WB4_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB4 = TH2D("WB4", WB4_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB4.Draw(ybranches + ":" + xbranches[i] + ">>WB4",draw_option)
                    WB4.Scale(WJets_scale[4])
                else:
                    WB4_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB4 = TH2D("WB4", WB4_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # W Jets HT800-1200 
                p_WB5 = input_dir + WJets_path + WJets_files[5] + ".root"
                if (os.path.isfile(p_WB5)):
                    f_WB5 = TFile(p_WB5, "READ") 
                    events_WB5 = f_WB5.Get(tree_name)
                    WB5_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB5 = TH2D("WB5", WB5_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB5.Draw(ybranches + ":" + xbranches[i] + ">>WB5",draw_option)
                    WB5.Scale(WJets_scale[5])
                else:
                    WB5_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB5 = TH2D("WB5", WB5_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # W Jets HT1200-2500 
                p_WB6 = input_dir + WJets_path + WJets_files[6] + ".root"
                if (os.path.isfile(p_WB6)):
                    f_WB6 = TFile(p_WB6, "READ") 
                    events_WB6 = f_WB6.Get(tree_name)
                    WB6_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB6 = TH2D("WB6", WB6_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB6.Draw(ybranches + ":" + xbranches[i] + ">>WB6",draw_option)
                    WB6.Scale(WJets_scale[6])
                else:
                    WB6_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB6 = TH2D("WB6", WB6_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # W Jets HT2500-inf
                p_WB7 = input_dir + WJets_path + WJets_files[7] + ".root"
                if (os.path.isfile(p_WB7)):
                    f_WB7 = TFile(p_WB7, "READ") 
                    events_WB7 = f_WB7.Get(tree_name)
                    WB7_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB7 = TH2D("WB7", WB7_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_WB7.Draw(ybranches + ":" + xbranches[i] + ">>WB7",draw_option)
                    WB7.Scale(WJets_scale[7])
                else:
                    WB7_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                    WB7 = TH2D("WB7", WB7_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # Adding Histos together
                WB_title = x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k]
                WB = TH2D("WB", WB_title, xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                if (os.path.isfile(p_WB0)):
                    WB.Add(WB0)
                WB.Add(WB1)
                WB.Add(WB2)
                WB.Add(WB3)
                WB.Add(WB4)
                WB.Add(WB5)
                WB.Add(WB6)
                WB.Add(WB7)

                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(WB, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                WB.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.15,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(WB, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it
                
            elif samples_name[q] == "TT_SemiLeptonic":
                
                # Loading file and filling 2D plot with pass/fail cut
                f_B = TFile(input_dir + samples_name[q] + ".root", "READ")
                events_B = f_B.Get(tree_name)
                B = TH2D("B", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_B.Draw(ybranches + ":" + xbranches[i] + ">>B",draw_option)
                B.Scale(Background_scale[1]) 
               
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(B, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                B.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.15,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(B, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it
            
            else : # all other samples that don't require adding histos
                # Loading file and filling 2D plot with pass/fail cut
                f_B = TFile(input_dir + samples_name[q] + ".root", "READ")
                events_B = f_B.Get(tree_name)
                B = TH2D("B", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_B.Draw(ybranches + ":" + xbranches[i] + ">>B",draw_option)
                B.Scale(Signal_scale[q]) 
               
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(B, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                B.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.15,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(B, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it

# Loops back to next sample
