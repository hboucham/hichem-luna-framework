#!/bin/bash
PNET="0.5"
DATE="082924"

# JER
python3 JESR_sys.py mu JER 15 $PNET $DATE
python3 JESR_sys.py el JER 15 $PNET $DATE
python3 JESR_sys.py mu JER 30 $PNET $DATE
python3 JESR_sys.py el JER 30 $PNET $DATE
python3 JESR_sys.py mu JER 55 $PNET $DATE
python3 JESR_sys.py el JER 55 $PNET $DATE

# JES
python3 JESR_sys.py mu JES 15 $PNET $DATE
python3 JESR_sys.py el JES 15 $PNET $DATE
python3 JESR_sys.py mu JES 30 $PNET $DATE
python3 JESR_sys.py el JES 30 $PNET $DATE
python3 JESR_sys.py mu JES 55 $PNET $DATE
python3 JESR_sys.py el JES 55 $PNET $DATE
