#!/bin/bash

#DIR="2D_2Ltt_test"
DIR="2D_2Ltt_013025_v2bfix"
DIRECTORY="/afs/cern.ch/user/h/hboucham/public/${DIR}"
#PDF_DIR="/eos/user/h/hboucham/www_hichem/Haa4b_plots/"
PDF_DIR="testpdf/"

# Making directory if it doesn't exist
if [ ! -d "$DIRECTORY" ]; then
  echo "$DIRECTORY does not exist, so I shall create it"
  mkdir "$DIRECTORY"
  mkdir "${DIRECTORY}/WP40"
  mkdir "${DIRECTORY}/WP60"
  mkdir "${DIRECTORY}/WP80"
fi

# Remove old 2D plots root files
echo ">>>>>>> Deleting old pdf and root files."
# Muon-Muon
rm -f ../plots_2L_tt/root_mumu/WP40/*
rm -f ../plots_2L_tt/root_mumu/WP60/*
rm -f ../plots_2L_tt/root_mumu/WP80/*
# Muon-Electron
rm -f ../plots_2l_tt/root_muel/WP40/*
rm -f ../plots_2l_tt/root_muel/WP60/*
rm -f ../plots_2l_tt/root_muel/WP80/*
# Electron-Electron
rm -f ../plots_2l_tt/root_elel/WP40/*
rm -f ../plots_2l_tt/root_elel/WP60/*
rm -f ../plots_2l_tt/root_elel/WP80/*
# Electron-Muon
rm -f ../plots_2L_tt/root_elmu/WP40/*
rm -f ../plots_2L_tt/root_elmu/WP60/*
rm -f ../plots_2L_tt/root_elmu/WP80/*
# Merged lepton-lepton
rm -f ../plots_2L_tt/root_ll/WP40/*
rm -f ../plots_2L_tt/root_ll/WP60/*
rm -f ../plots_2L_tt/root_ll/WP80/*

# Remove old pdf files
rm -f ../plots_2L_tt/2D_*

# Make new pdf and root files for individual channels (mu/el)
echo ">>>>>>> Making root files and pdfs of individual sub-categories (mm/me/ee/em) * 1b+ * (WP40/WP60/WP80)."
# WP40
python3 2D_plot_2L_tt.py mu mu 1b+ WP40 x
python3 2D_plot_2L_tt.py mu el 1b+ WP40 x
python3 2D_plot_2L_tt.py el el 1b+ WP40 x
python3 2D_plot_2L_tt.py el mu 1b+ WP40 x
# WP60
python3 2D_plot_2L_tt.py mu mu 1b+ WP60 x
python3 2D_plot_2L_tt.py mu el 1b+ WP60 x
python3 2D_plot_2L_tt.py el el 1b+ WP60 x
python3 2D_plot_2L_tt.py el mu 1b+ WP60 x
# WP80
python3 2D_plot_2L_tt.py mu mu 1b+ WP80 x
python3 2D_plot_2L_tt.py mu el 1b+ WP80 x
python3 2D_plot_2L_tt.py el el 1b+ WP80 x
python3 2D_plot_2L_tt.py el mu 1b+ WP80 x


# copy pdfs to website directory
cp ../plots_2L_tt/2D_* ${PDF_DIR}

# copy root files to public directory
# Muon-Muon
cp ../plots_2L_tt/root_mumu/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_tt/root_mumu/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_tt/root_mumu/WP80/* ${DIRECTORY}/WP80/
# Muon-Electron
cp ../plots_2L_tt/root_muel/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_tt/root_muel/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_tt/root_muel/WP80/* ${DIRECTORY}/WP80/
# Electron-Electron
cp ../plots_2L_tt/root_elel/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_tt/root_elel/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_tt/root_elel/WP80/* ${DIRECTORY}/WP80/
# Electron-Muon
cp ../plots_2L_tt/root_elmu/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_tt/root_elmu/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_tt/root_elmu/WP80/* ${DIRECTORY}/WP80/

# Make root files for combined channels, need to rerun 2D plots with slightly different naming convention
echo ">>>>>>> Making root files and pdfs of merged (ll) sub-categories (mm/me/ee/em) * 1b+ * (WP40/WP60/WP80)."

# WP40
python3 2D_plot_2L_tt.py mu mu 1b+ WP40 comb
python3 2D_plot_2L_tt.py mu el 1b+ WP40 comb
python3 2D_plot_2L_tt.py el el 1b+ WP40 comb
python3 2D_plot_2L_tt.py el mu 1b+ WP40 comb
# WP60
python3 2D_plot_2L_tt.py mu mu 1b+ WP60 comb
python3 2D_plot_2L_tt.py mu el 1b+ WP60 comb
python3 2D_plot_2L_tt.py el el 1b+ WP60 comb
python3 2D_plot_2L_tt.py el mu 1b+ WP60 comb
# WP80
python3 2D_plot_2L_tt.py mu mu 1b+ WP80 comb
python3 2D_plot_2L_tt.py mu el 1b+ WP80 comb
python3 2D_plot_2L_tt.py el el 1b+ WP80 comb
python3 2D_plot_2L_tt.py el mu 1b+ WP80 comb

# Merge mm/me/ee/em
echo ">>>>>>> Merging sub-categories root files."
# WP40  
hadd -f ../plots_2L_tt/root_ll/WP40/ttbll_Data_2018.root ../plots_2L_tt/root_mumu/WP40/ttbll_Data_2018.root ../plots_2L_tt/root_muel/WP40/ttbll_Data_2018.root ../plots_2L_tt/root_elel/WP40/ttbll_Data_2018.root ../plots_2L_tt/root_elmu/WP40/ttbll_Data_2018.root
hadd -f ../plots_2L_tt/root_ll/WP40/ttbll_TT2l_2018.root ../plots_2L_tt/root_mumu/WP40/ttbll_TT2l_2018.root ../plots_2L_tt/root_muel/WP40/ttbll_TT2l_2018.root ../plots_2L_tt/root_elel/WP40/ttbll_TT2l_2018.root ../plots_2L_tt/root_elmu/WP40/ttbll_TT2l_2018.root
hadd -f ../plots_2L_tt/root_ll/WP40/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_mumu/WP40/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_muel/WP40/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_elel/WP40/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_elmu/WP40/ttbll_ttHtoaato4b_mA_15_2018.root
hadd -f ../plots_2L_tt/root_ll/WP40/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_mumu/WP40/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_muel/WP40/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_elel/WP40/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_elmu/WP40/ttbll_ttHtoaato4b_mA_30_2018.root
hadd -f ../plots_2L_tt/root_ll/WP40/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_mumu/WP40/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_muel/WP40/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_elel/WP40/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_elmu/WP40/ttbll_ttHtoaato4b_mA_55_2018.root
# WP60
hadd -f ../plots_2L_tt/root_ll/WP60/ttbll_Data_2018.root ../plots_2L_tt/root_mumu/WP60/ttbll_Data_2018.root ../plots_2L_tt/root_muel/WP60/ttbll_Data_2018.root ../plots_2L_tt/root_elel/WP60/ttbll_Data_2018.root ../plots_2L_tt/root_elmu/WP60/ttbll_Data_2018.root
hadd -f ../plots_2L_tt/root_ll/WP60/ttbll_TT2l_2018.root ../plots_2L_tt/root_mumu/WP60/ttbll_TT2l_2018.root ../plots_2L_tt/root_muel/WP60/ttbll_TT2l_2018.root ../plots_2L_tt/root_elel/WP60/ttbll_TT2l_2018.root ../plots_2L_tt/root_elmu/WP60/ttbll_TT2l_2018.root
hadd -f ../plots_2L_tt/root_ll/WP60/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_mumu/WP60/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_muel/WP60/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_elel/WP60/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_elmu/WP60/ttbll_ttHtoaato4b_mA_15_2018.root
hadd -f ../plots_2L_tt/root_ll/WP60/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_mumu/WP60/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_muel/WP60/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_elel/WP60/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_elmu/WP60/ttbll_ttHtoaato4b_mA_30_2018.root
hadd -f ../plots_2L_tt/root_ll/WP60/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_mumu/WP60/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_muel/WP60/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_elel/WP60/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_elmu/WP60/ttbll_ttHtoaato4b_mA_55_2018.root
# WP80 
hadd -f ../plots_2L_tt/root_ll/WP80/ttbll_Data_2018.root ../plots_2L_tt/root_mumu/WP80/ttbll_Data_2018.root ../plots_2L_tt/root_muel/WP80/ttbll_Data_2018.root ../plots_2L_tt/root_elel/WP80/ttbll_Data_2018.root ../plots_2L_tt/root_elmu/WP80/ttbll_Data_2018.root
hadd -f ../plots_2L_tt/root_ll/WP80/ttbll_TT2l_2018.root ../plots_2L_tt/root_mumu/WP80/ttbll_TT2l_2018.root ../plots_2L_tt/root_muel/WP80/ttbll_TT2l_2018.root ../plots_2L_tt/root_elel/WP80/ttbll_TT2l_2018.root ../plots_2L_tt/root_elmu/WP80/ttbll_TT2l_2018.root
hadd -f ../plots_2L_tt/root_ll/WP80/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_mumu/WP80/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_muel/WP80/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_elel/WP80/ttbll_ttHtoaato4b_mA_15_2018.root ../plots_2L_tt/root_elmu/WP80/ttbll_ttHtoaato4b_mA_15_2018.root
hadd -f ../plots_2L_tt/root_ll/WP80/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_mumu/WP80/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_muel/WP80/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_elel/WP80/ttbll_ttHtoaato4b_mA_30_2018.root ../plots_2L_tt/root_elmu/WP80/ttbll_ttHtoaato4b_mA_30_2018.root
hadd -f ../plots_2L_tt/root_ll/WP80/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_mumu/WP80/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_muel/WP80/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_elel/WP80/ttbll_ttHtoaato4b_mA_55_2018.root ../plots_2L_tt/root_elmu/WP80/ttbll_ttHtoaato4b_mA_55_2018.root


# copy combined root files to public directory
# Lepton-Lepton
cp ../plots_2L_tt/root_ll/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_tt/root_ll/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_tt/root_ll/WP80/* ${DIRECTORY}/WP80/

echo ">>>>>>> DONE! All root files copied to ${DIRECTORY}, all pdf files copied to ${PDF_DIR}."















