#!/usr/bin/env python

import sys
import numpy 
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree, TLorentzVector
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite

from helpers.ratioStatError import getRatioStatError

# Functions
def createCanvasPads(logs): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.015)  # joins upper and lower plot
    pad1.SetGrid()
    if (logs):
        pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def createRatio(h1, h2, x_axis): # Function to create ratio plot
     h3 = h1.Clone("h3")
     h3.SetLineColor(kBlack)
     h3.SetMarkerStyle(20)
     h3.SetTitle("")
     h3.SetMinimum(.1)
     h3.SetMaximum(5.2)
     # Set up plot for markers and errors
     #h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h2)
 
     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("non-HT/HT    ")
     y.SetNdivisions(505)
     y.SetTitleSize(25)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.25)
     y.SetLabelFont(43)
     y.SetLabelSize(20)
 
     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitle(x_axis)
     x.SetTitleSize(25)
     x.SetTitleFont(43)
     
     return h3

#Returns abs(Dphi)
def DeltaPhi_abs(phi1, phi2):
    p = math.pi
    r = abs(phi1 - phi2)
    if r <= p:
        return r 
    else:
        return 2*p - r 


# input/output directory and tree name
input_dir = "file_list_WJets/"
output_dir = "../plots/"
tree_name = "Events"
weight = "(1 - 2*(genWeight < 0))"
cut = ""
logscale = [True, False]


# Scaling for MC histos
lumi = 58000 # pb-1 = 58 fb-1 (2018 lumi)

## WJets background (HT bins)
WJets_path = "WJets_LNu_HT"
WJets_files = ["70-100","100-200","200-400", "400-600", "600-800", "800-1200","1200-2500", "2500-inf"]
WJets_xsec = [1440, 1431, 382.1, 51.54, 12.49, 5.619, 1.321, 0.02992]
WJets_ev = [66220256, 51408967, 58225632, 7444030, 7718765, 7306187, 6481518, 2097648]
WJets_scale = []
for i in range(len(WJets_xsec)):
    WJets_scale.append(lumi*WJets_xsec[i]/WJets_ev[i])

# Other backgrounds: 
Background_files = ["WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8", "WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8"]
Background_xsec = [61526.7, 61526.7]
Background_ev = [19974968, 81051269]
Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])


branch = "LHE_HT"
x_axis = "H_{T}(LHE) [GeV]"
range_min = 0
range_max = 3500
bins = 35
title = "Scalar Sum of Parton pTs (HT) at LHE Step"


gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box

#load trees

# WJetsToLNu_HT-*To*_TuneCP5_13TeV-madgraphMLM-pythia8 
f_WB0 = open(input_dir+ WJets_path + WJets_files[0] + ".txt", "r")
events_WB0 = TChain(tree_name)
for line in f_WB0:
    #print("Adding file: " + line)
    events_WB0.Add(line[:-1])
print("Number of events:", events_WB0.GetEntries())
#WB0 = TH1D("WB0", title, bins, range_min, range_max)
#events_WB0.Draw(branch+">>WB0", cut, weight)
#WB0.Scale(WJets_scale[0])

f_WB1 = open(input_dir+ WJets_path + WJets_files[1] + ".txt", "r")
events_WB1 = TChain(tree_name)
for line in f_WB1:
    #print("Adding file: " + line)
    events_WB1.Add(line[:-1])
print("Number of events:", events_WB1.GetEntries())
#WB1 = TH1D("WB1", title, bins, range_min, range_max)
#events_WB1.Draw(branch+">>WB1", cut, weight)
#WB1.Scale(WJets_scale[1])

f_WB2 = open(input_dir+ WJets_path + WJets_files[2] + ".txt", "r")
events_WB2 = TChain(tree_name)
for line in f_WB2:
    #print("Adding file: " + line)
    events_WB2.Add(line[:-1])
print("Number of events:", events_WB2.GetEntries())
#WB2 = TH1D("WB2", title, bins, range_min, range_max)
#events_WB2.Draw(branch+">>WB2", cut, weight)
#WB2.Scale(WJets_scale[2])

f_WB3 = open(input_dir+ WJets_path + WJets_files[3] + ".txt", "r")
events_WB3 = TChain(tree_name)
for line in f_WB3:
    #print("Adding file: " + line)
    events_WB3.Add(line[:-1])
print("Number of events:", events_WB3.GetEntries())
#WB3 = TH1D("WB3", title, bins, range_min, range_max)
#events_WB3.Draw(branch+">>WB3", cut, weight)
#WB3.Scale(WJets_scale[3])

f_WB4 = open(input_dir+ WJets_path + WJets_files[4] + ".txt", "r")
events_WB4 = TChain(tree_name)
for line in f_WB4:
    #print("Adding file: " + line)
    events_WB4.Add(line[:-1])
print("Number of events:", events_WB4.GetEntries())
#WB4 = TH1D("WB4", title, bins, range_min, range_max)
#events_WB4.Draw(branch+">>WB4", cut, weight)
#WB4.Scale(WJets_scale[4])

f_WB5 = open(input_dir+ WJets_path + WJets_files[5] + ".txt", "r")
events_WB5 = TChain(tree_name)
for line in f_WB5:
    #print("Adding file: " + line)
    events_WB5.Add(line[:-1])
print("Number of events:", events_WB5.GetEntries())
#WB5 = TH1D("WB5", title, bins, range_min, range_max)
#events_WB5.Draw(branch+">>WB5", cut, weight)
#WB5.Scale(WJets_scale[5])

f_WB6 = open(input_dir+ WJets_path + WJets_files[6] + ".txt", "r")
events_WB6 = TChain(tree_name)
for line in f_WB6:
    #print("Adding file: " + line)
    events_WB6.Add(line[:-1])
print("Number of events:", events_WB6.GetEntries())
#WB6 = TH1D("WB6", title, bins, range_min, range_max)
#events_WB6.Draw(branch+">>WB6", cut, weight)
#WB6.Scale(WJets_scale[6])

f_WB7 = open(input_dir+ WJets_path + WJets_files[7] + ".txt", "r")
events_WB7 = TChain(tree_name)
for line in f_WB7:
    #print("Adding file: " + line)
    events_WB7.Add(line[:-1])
print("Number of events:", events_WB7.GetEntries())
#WB7 = TH1D("WB7", title, bins, range_min, range_max)
#events_WB7.Draw(branch+">>WB7", cut, weight)
#WB7.Scale(WJets_scale[7])
'''
# Scaling histos before adding them to main histo (WB)
WB = TH1D("WB", title, bins, range_min, range_max)
WB.Add(WB0)
WB.Add(WB1)
WB.Add(WB2)
WB.Add(WB3)
WB.Add(WB4)
WB.Add(WB5)
WB.Add(WB6)
WB.Add(WB7)
WB.SetFillColor(kGreen)
'''
# WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8
f_W0 = open(input_dir+ Background_files[0] + ".txt", "r")
events_W0 = TChain(tree_name)
for line in f_W0:
    #print("Adding file: " + line)
    events_W0.Add(line[:-1])
print("Number of events:", events_W0.GetEntries())
#W0 = TH1D("W0", title, bins, range_min, range_max)
#events_W0.Draw(branch+">>W0", cut, weight)
#W0.Scale(Background_scale[0])
#W0.SetLineColor(kBlue)
#W0.SetLineWidth(2)
#W0.SetLineStyle(2)

# WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8
f_W1 = open(input_dir+ Background_files[1] + ".txt", "r")
events_W1 = TChain(tree_name)
for line in f_W1:
    #print("Adding file: " + line)
    events_W1.Add(line[:-1])
print("Number of events:", events_W1.GetEntries())
#W1 = TH1D("W1", title, bins, range_min, range_max)
#events_W1.Draw(branch+">>W1", cut, weight)
#W1.Scale(Background_scale[1])
#W1.SetLineColor(kViolet)
#W1.SetLineWidth(2)
#W1.SetLineStyle(2)

for i in range(len(logscale)):

    # Creating Canvas
    c, pad1, pad2 = createCanvasPads(logscale[i])
    pad1.cd() # pad1 is for regular plot
    
    # Creating Canvas
    #c = TCanvas("c", "canvas", 800, 700)
    # Upper histogram plot is pad1
    #pad1 = TPad("pad1", "pad1", 0, 0.01, 1, 1.0)
    #pad1.SetTopMargin(0.12)  
    #pad1.SetBottomMargin(0.12)  # joins upper and lower plot
    #pad1.SetGrid()
    #if (logscale[i]):
    #    pad1.SetLogy() 
    #pad1.Draw()
    #pad1.cd()

   # drawing histo for already loaded trees
   
    WB0 = TH1D("WB0", title, bins, range_min, range_max)
    events_WB0.Draw(branch+">>WB0", cut, weight)
    WB0.Scale(WJets_scale[0])
    
    WB1 = TH1D("WB1", title, bins, range_min, range_max)
    events_WB1.Draw(branch+">>WB1", cut, weight)
    WB1.Scale(WJets_scale[1])

    WB2 = TH1D("WB2", title, bins, range_min, range_max)
    events_WB2.Draw(branch+">>WB2", cut, weight)
    WB2.Scale(WJets_scale[2])

    WB3 = TH1D("WB3", title, bins, range_min, range_max)
    events_WB3.Draw(branch+">>WB3", cut, weight)
    WB3.Scale(WJets_scale[3])

    WB4 = TH1D("WB4", title, bins, range_min, range_max)
    events_WB4.Draw(branch+">>WB4", cut, weight)
    WB4.Scale(WJets_scale[4])

    WB5 = TH1D("WB5", title, bins, range_min, range_max)
    events_WB5.Draw(branch+">>WB5", cut, weight)
    WB5.Scale(WJets_scale[5])

    WB6 = TH1D("WB6", title, bins, range_min, range_max)
    events_WB6.Draw(branch+">>WB6", cut, weight)
    WB6.Scale(WJets_scale[6])
    
    WB7 = TH1D("WB7", title, bins, range_min, range_max)
    events_WB7.Draw(branch+">>WB7", cut, weight)
    WB7.Scale(WJets_scale[7])
    
    # Adding HT bins histo
    WB = TH1D("WB", title, bins, range_min, range_max)
    WB.Add(WB0)
    WB.Add(WB1)
    WB.Add(WB2)
    WB.Add(WB3)
    WB.Add(WB4)
    WB.Add(WB5)
    WB.Add(WB6)
    WB.Add(WB7)
    WB.SetFillColor(kGreen)

    W0 = TH1D("W0", title, bins, range_min, range_max)
    events_W0.Draw(branch+">>W0", cut, weight)
    W0.Scale(Background_scale[0])
    W0.SetLineColor(kBlue)
    W0.SetLineWidth(2)
    W0.SetLineStyle(2)
    
    W1 = TH1D("W1", title, bins, range_min, range_max)
    events_W1.Draw(branch+">>W1", cut, weight)
    W1.Scale(Background_scale[1])
    W1.SetLineColor(kViolet)
    W1.SetLineWidth(2)
    W1.SetLineStyle(2)


    # Stylistic adjustments of regular plot through SD histo
    W_y = W0.GetYaxis()
    W_x = W0.GetXaxis()

    # Adjusting y axis range as needed for each variable (especially log)
    # Manually adjust both max and min
    #SD_y.SetRangeUser(1, 100000)

    # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
    # This is mainly done to make space for the legend and other text
    if (logscale[i]):
        W0.SetMinimum(9)
        W0.SetMaximum(5*W0.GetMaximum())
    else:
        W0.SetMinimum(0)
        W0.SetMaximum(1.2*W0.GetMaximum())
        
    gStyle.SetTitleFontSize(0.05) # Title size

    # x-axis (not necessary since hidden by ratio plot) 
    W_x.SetTitle(x_axis)
    W_x.SetTitleSize(0.05) #0.04
    W_x.SetTitleOffset(1.5) #1.1
    W_x.SetLabelOffset(0.3) #0.01

    # y-axis
    W_y.SetTitle("Events   ") # space used for offset
    W_y.SetNdivisions(505) #make divisions nice
    W_y.SetTitleSize(25) 
    W_y.SetTitleFont(43) 
    W_y.SetTitleOffset(1.25)
    W_y.SetLabelFont(43) # font and size of numbers
    W_y.SetLabelSize(20)


    gPad.Modified() # Update gpad (things might break depending on where this line is)


    # Drawing everything

    W0.Draw("HISTO")
    WB.Draw("SAME HIST")
    W0.Draw("SAME HISTO")
    W1.Draw("SAME HISTO")

    # Ratio plot
    pad2.cd()# Go to pad2 for ratio plot
    
 

    # Histo for ratio plot
    W0_RATIO = createRatio(W0, WB, x_axis)
    W0_RATIO.SetLineColor(kBlue)
    # Histo for statistical errors accounting for denominator's statistical erorr
    #W0_hStatError = getRatioStatError(W0, WB)
    #RATIO.Draw("ep") # draw with stat errors (but doesn't account for denominator statistical error)
    W1_RATIO = createRatio(W1, WB, x_axis)
    W1_RATIO.SetLineColor(kViolet)
    #W1_hStatError = getRatioStatError(W1, tmpHist)
    
    # Ratio histo Stylistic stuff
    R_x = W0_RATIO.GetXaxis()
    R_x.SetTitleSize(25) 
    R_x.SetTitleFont(43) 
    R_x.SetTitleOffset(1.15)
    R_x.SetLabelFont(43) # font and size of numbers
    R_x.SetLabelSize(20)
    R_x.SetLabelOffset(.02)

    # Draw line for y = 1 to help compare ratio
    line = TLine(range_min, 1.,range_max, 1.)
    line.SetLineColor(kBlack)
    line.SetLineWidth(1)
    
    # Drawing histos in ratio pad
    W0_RATIO.Draw("HIST")
    #W0_hStatError.Draw("SAME")
    W1_RATIO.Draw("HIST SAME")
    #W1_hStatError.Draw("SAME P")
    line.Draw("SAME")

    # Redraw Axis in case they are covered
    pad2.RedrawAxis()
    # Legend and other text

    pad1.cd() # pad1 is for regular plot
    # Legend
    #leg=TLegend(.38,.65,.88,.87) #(x_min, y_min, x_max, y_max)
    leg=TLegend(.19,.75,.88,.87) #(x_min, y_min, x_max, y_max)
    #leg=TLegend() # automatic legend placement sucks
    leg.SetNColumns(1) # use 2 columns for legend
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(22)
    leg.SetTextSize(0.028)
    leg.AddEntry(WB,"WJetsToLNu_HT-70ToInf_TuneCP5_13TeV-madgraphMLM-pythia8","f") # "f" for fills, "L" for linesi, "lep" for data points
    leg.AddEntry(W0,Background_files[0],"L")
    leg.AddEntry(W1,Background_files[1],"L")
    leg.Draw("SAME")

    ############# TEXT SECTION: START ###########################################################################

    # CMS stuff
    tex = TLatex(0.52,0.88,"   #bf{CMS} #it{Work in Progress}                                     58 fb^{-1} (13 TeV, 2018) ")
    tex.SetNDC()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetTextAlign(20)
    tex.Draw("SAME")


    # Redraw axis + border since they are hidden by histo fills
    ROOT.gPad.RedrawAxis() 
    borderline = TLine(range_max, W0.GetMinimum(), range_max, W0.GetMaximum())
    borderline.SetLineColor(kBlack)
    borderline.SetLineWidth(1)
    borderline.Draw("SAME")

    # Save individual pdf/png
    if i == 0:
        c.Print(output_dir+ "WJets_study.pdf(", "Title: Linear y-axis")
    else:
        c.Print(output_dir+ "WJets_study.pdf)", "Title: Log y-axis")
    #c.SaveAs(output_dir+ "WJets_study.pdf")
    # Delete all histos to avoid warning when initializing histos with same name
    WB0.SetDirectory(0)
    WB1.SetDirectory(0)
    WB2.SetDirectory(0)
    WB3.SetDirectory(0)
    WB4.SetDirectory(0)
    WB5.SetDirectory(0)
    WB6.SetDirectory(0)
    WB7.SetDirectory(0)
    WB.SetDirectory(0)
    W0.SetDirectory(0)
    W1.SetDirectory(0)
    c.Close() # Delete Canvas after saving it





