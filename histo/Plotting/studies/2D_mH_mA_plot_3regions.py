#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH2D, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


def Histo_style(h, x_title, y_title):

    h_y = h.GetYaxis()
    h_x = h.GetXaxis()

    # x-axis 
    h_x.SetTitle(x_title)
    h_x.SetTitleSize(30) 
    h_x.SetTitleFont(43) 
    h_x.SetTitleOffset(1.1)
    h_x.SetLabelFont(43) # font and size of numbers
    h_x.SetLabelSize(25)

    # y-axis
    h_y.SetTitle(y_title) # space used for offset
    h_y.SetTitleSize(30) 
    h_y.SetTitleFont(43) 
    h_y.SetTitleOffset(1.25)
    h_y.SetLabelFont(43) # font and size of numbers
    h_y.SetLabelSize(25)

# Pass faill different Working Points
PFWP80_root_titles = ["_SRWP40_central", "_SBWP80to40_central"] 
PFWP60_root_titles = ["_SRWP40_60", "_SBWP60to40_central"] 
PFWP40_root_titles = ["_SRWP40_40", "_SBWP40to40_central"] 
PFWP80 = ["(score_Haa4b_vs_QCD > 0.992)", "((score_Haa4b_vs_QCD < 0.992) && (score_Haa4b_vs_QCD > 0.92))"]
PFWP60 = ["(score_Haa4b_vs_QCD > 0.975)", "((score_Haa4b_vs_QCD < 0.975) && (score_Haa4b_vs_QCD > 0.80))"]
PFWP40 = ["(score_Haa4b_vs_QCD > 0.920)", "((score_Haa4b_vs_QCD < 0.920) && (score_Haa4b_vs_QCD > 0.50))"]
PF_object_titles_2R = ["Pass", "Fail"]

# Pass/loose/fail
PF_object_titles_3R = ["CR_fail", "CR_loose", "CR_pass"]
PFWP3R_root_titles = ["Pass", "Loose", "Fail"] #Control Region fail = signal region pass?
PFWP3R = ["(score_Haa4b_vs_QCD > 0.8)", "((score_Haa4b_vs_QCD <= 0.8) && (score_Haa4b_vs_QCD >= 0.5))", "(score_Haa4b_vs_QCD < 0.5)"]
###################################################################################################
##############  Manually adjust CHANNEL  ##########################################################
channel = "mu"
#channel = "el"
############### Manually adjust Pass/fail Working Point  ##########################################
PF_root_titles = PFWP3R_root_titles
PFWP = PFWP3R
PF_object_titles = PF_object_titles_3R
WP_text = "WP3R"
###################################################################################################
###################################################################################################

# input/output directory and tree name
input_dir = "../../postprocess/" + channel + "_postprocess/"
output_dir = "../plots_" + channel
tree_name = "event_tree"
weight = "weight_tot"

# Scaling for MC histos
## DYJets_M-50 background (HT bins)
DY_50_path = "DY_50_HT"
DYJets_files = ["200-400", "400-600", "600-800", "800-1200","1200-2500", "2500-inf"]
DYJets_xsec = [43.5384, 5.9141, 1.4377, 0.6443, 0.1511, 0.00339]
DYJets_ev = [18455718, 8682257, 7035971, 6554679, 5966661, 1978203]

# Other backgrounds: [TT_2L2Nu,ZZ, tZq_ll_4f_ckm_NLO, ZZTo2Q2L_mllmin4p0, ZH_HToBB_ZToLL_M-125, ttZJets]
Background_files = ["TT_2L2Nu", "ZZTo2Q2L","ZZ", "tZq_ll_4f_ckm_NLO", "ZH_HToBB_ZToLL_M-125", "ttZJets"]
Background_xsec = [87.339, 6.82, 10.32, 0.0942, 0.0519, 0.839]
Background_ev = [145020000, 29357938, 3526000, 11916000, 4885835, 32793815]

# signal: [M-15, M-30, M-55]
Signal_xsec = [0.11352, 0.11352, 0.11352]
Signal_ev = [106743, 101445, 95365]

# 2018 lumi
lumi = 58000 # pb-1 = 58 fb-1

DYJets_scale = []
for i in range(len(DYJets_xsec)):
    DYJets_scale.append(lumi*DYJets_xsec[i]/DYJets_ev[i])
#print(DYJets_scale)

Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])
#print(Background_scale)

Signal_scale =[]
for i in range(len(Signal_xsec)):
    Signal_scale.append(lumi*Signal_xsec[i]/Signal_ev[i])
#print(Signal_scale)


# samples info
background_signal_sample_list = ["TT_2L2Nu", "ZZTo2Q2L", "SUSY_ZH_M-15", "SUSY_ZH_M-30", "SUSY_ZH_M-55"]
background_signal_scale = [Background_scale[0], Background_scale[1], Signal_scale[0], Signal_scale[1], Signal_scale[2]]
samples_name = ["DY_50_HT", "data_2018", "TT_2L2Nu", "ZZTo2Q2L", "SUSY_ZH_M-15", "SUSY_ZH_M-30", "SUSY_ZH_M-55"]

# Histo variables, labels, range and binsi
# x information
x_root_titles = ["MH", "MSoftDropH", "MSoftScaledH", "MPnetH", "MTopoH", "MSoftTopoH"]
xbranches = ["m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet","mH_avg", "mtopo_fatjet", "msofttopo_fatjet"]
xrange_min = 0
xrange_max = 300
xbins = 30

# y information
y_root_titles = "MPnetA"
ybranches = "mA_avg"
yrange_min = 0
yrange_max = 70
ybins = 35

# init dir and tree
#input_dir = "../mu_skim"
#input_tree = "event_tree"
#output_dir = "../plots_mu"

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box


for q in range(len(samples_name)):
    output_file = TFile(output_dir + "/root/ZH_H4b_Zmumu_" + samples_name[q] + ".root","recreate")
    for k in range(len(PFWP)):
        for i in range(len(xbranches)): 
            c = TCanvas("c", "canvas", 800, 800)
            c.SetTopMargin(0.11)
            c.SetRightMargin(0.13)
            c.SetLeftMargin(0.13)
            
            if samples_name[q] == "DY_50_HT":
                 
                # DY_50_HT100-200 empty

                # DY_50_HT200-400 
                #f_SB2 = TFile( input_dir + DY_50_HT_sample_list[1], "READ")
                f_SB0 = TFile( input_dir+ DY_50_path + DYJets_files[0] + ".root", "READ")
                events_SB0 = f_SB0.Get(tree_name)
                SB0 = TH2D("SB0", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_SB0.Draw(ybranches + ":" + xbranches[i] + ">>SB0",PFWP[k], weight)
                SB0.Scale(DYJets_scale[0])

                # DY_50_HT400-600 
                f_SB1 = TFile( input_dir+ DY_50_path + DYJets_files[1] + ".root", "READ")
                events_SB1 = f_SB1.Get(tree_name)
                SB1 = TH2D("SB1", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_SB1.Draw(ybranches + ":" + xbranches[i] + ">>SB1",PFWP[k], weight)
                SB1.Scale(DYJets_scale[1])
                
                # DY_50_HT600-800
                f_SB2 = TFile( input_dir+ DY_50_path + DYJets_files[2] + ".root", "READ")
                events_SB2 = f_SB2.Get(tree_name)
                SB2 = TH2D("SB2", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_SB2.Draw(ybranches + ":" + xbranches[i] + ">>SB2",PFWP[k], weight)
                SB2.Scale(DYJets_scale[2])
                
                # DY_50_HT800-1200
                f_SB3 = TFile( input_dir+ DY_50_path + DYJets_files[3] + ".root", "READ")
                events_SB3 = f_SB3.Get(tree_name)
                SB3 = TH2D("SB3", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_SB3.Draw(ybranches + ":" + xbranches[i] + ">>SB3",PFWP[k], weight)
                SB3.Scale(DYJets_scale[3])
                
                # DY_50_HT1200-2500
                f_SB4 = TFile( input_dir+ DY_50_path + DYJets_files[4] + ".root", "READ")
                events_SB4 = f_SB4.Get(tree_name)
                SB4 = TH2D("SB4", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_SB4.Draw(ybranches + ":" + xbranches[i] + ">>SB4",PFWP[k], weight)
                SB4.Scale(DYJets_scale[4])

                # DY_50_HT2500-inf
                f_SB5 = TFile( input_dir+ DY_50_path + DYJets_files[5] + ".root", "READ")
                events_SB5 = f_SB5.Get(tree_name)
                SB5 = TH2D("SB5", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_SB5.Draw(ybranches + ":" + xbranches[i] + ">>SB5",PFWP[k], weight)
                SB5.Scale(DYJets_scale[5])
        
                # Adding Histos together
                SB = TH2D("SB", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                #SB.Add(SB1)
                SB.Add(SB0)
                SB.Add(SB1)
                SB.Add(SB2)
                SB.Add(SB3)
                SB.Add(SB4)
                SB.Add(SB5)
                
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(SB, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                SB.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89," #bf{CMS} #it{Work in Progress}                   58 fb^{-1} (13 TeV, 2018) ")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                #t3 = TLatex(0.3,0.8,"   Events " + PF_object_titles[k])
                t3 = TLatex(0.3,0.8, samples_name[q] + ": " + PF_root_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.04)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.3,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.3,0.85,"  #bf{Electon channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(SB, "h" + x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k])
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf(", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf)", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it

            elif samples_name[q] == "data_2018":
                # do that
                # Data SingleMuonA
                if (channel =="mu"):
                    fD1 = TFile(input_dir+"data_SingleMuonA.root", "READ")
                else:
                    fD1 = TFile(input_dir+"data_EGammaA.root", "READ")
                events_D1 = fD1.Get(tree_name)
                D1 = TH2D("D1", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D1.Draw(ybranches + ":" + xbranches[i] + ">>D1",PFWP[k], weight)

                # Data SingleMuonB
                if (channel =="mu"):
                    fD2 = TFile(input_dir+"data_SingleMuonB.root", "READ")
                else:
                    fD2 = TFile(input_dir+"data_EGammaB.root", "READ")
                events_D2 = fD2.Get(tree_name)
                D2 = TH2D("D2", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D2.Draw(ybranches + ":" + xbranches[i] + ">>D2",PFWP[k], weight)
                
                # Data SingleMuonC
                if (channel =="mu"):
                    fD3 = TFile(input_dir+"data_SingleMuonC.root", "READ")
                else:
                    fD3 = TFile(input_dir+"data_EGammaC.root", "READ")
                events_D3 = fD3.Get(tree_name)
                D3 = TH2D("D3", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D3.Draw(ybranches + ":" + xbranches[i] + ">>D3",PFWP[k], weight)

                # Data SingleMuonD
                if (channel =="mu"):
                    fD4 = TFile(input_dir+"data_SingleMuonD.root", "READ")
                else:
                    fD4 = TFile(input_dir+"data_EGammaD.root", "READ")
                events_D4 = fD4.Get(tree_name)
                D4 = TH2D("D4", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D4.Draw(ybranches + ":" + xbranches[i] + ">>D4",PFWP[k], weight)

                # Adding Histos together
                D = TH2D("D", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max) 
                D.Add(D1)
                D.Add(D2)
                D.Add(D3)
                D.Add(D4)

                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(D, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                D.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89," #bf{CMS} #it{Work in Progress}                   58 fb^{-1} (13 TeV, 2018) ")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                #t3 = TLatex(0.3,0.8,"   Events " + PF_object_titles[k])
                t3 = TLatex(0.3,0.8, samples_name[q] + ": " + PF_root_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.04)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.3,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.3,0.85,"  #bf{Electon channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(D, "h" + x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k])
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf(", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf)", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it


            else : # all other samples that don't require adding histos
                # Loading file and filling 2D plot with pass/fail cut
                f_B = TFile(input_dir + background_signal_sample_list[q-2] + ".root", "READ")
                events_B = f_B.Get(tree_name)
                B = TH2D("B", "h" + x_root_titles[i] + "_vs_" + y_root_titles + PF_root_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_B.Draw(ybranches + ":" + xbranches[i] + ">>B",PFWP[k], weight)
                B.Scale(background_signal_scale[q-2]) 
               
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(B, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                B.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89," #bf{CMS} #it{Work in Progress}                   58 fb^{-1} (13 TeV, 2018) ")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.2,0.8, samples_name[q] + ": " + PF_root_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.2,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.2,0.85,"  #bf{Electon channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(B, "h" + x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k])
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf(", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf)", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/pdf/2D_mH_mA_"+WP_text+".pdf", "Title: " +  samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it

# Loops back to next sample
