#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


def FWHM (h):
   bin1 = h.FindFirstBinAbove(h.GetMaximum()/2)
   bin2 = h.FindLastBinAbove(h.GetMaximum()/2)
   fwhm = h.GetBinCenter(bin2) - h.GetBinCenter(bin1)
   return fwhm

##############  Manually adjust CHANNEL & PLOT TYPE & SAMPLE TYPE   ###############################
###################################################################################################
channel = "mu"
#channel = "el"
###################################################################################################
plot_type = "lin"
#plot_type = "log2"
###################################################################################################
sample_type = "ZH"
#sample_type = "DY"
###################################################################################################

# Init vars
# init cut dummy
#cut = "(pt_MET > 200)"
cut = ""
# input/output directory and tree name

output_dir = "../plots_" + channel
file_single= "../../skim/test_mu_skim_M-all.root"
path_list = "../../skim/mu_skim/"
file_list = ["DY_50_HT200-400_skim.root", "DY_50_HT400-600_skim.root", "DY_50_HT600-800_skim.root", "DY_50_HT800-1200_skim.root", "DY_50_HT1200-2500_skim.root", "DY_50_HT2500-inf_skim.root"]
tree_name = "event_tree"


# Linear plots parameters
lin_vars_1 = ["m_fatjet", "msoftdrop_fatjet", "m_cor1_fatjet", "m_cor2_fatjet"]
lin_vars_2 = ["msoftdrop_fatjet", "msoftscaled_fatjet","msoftdrop_cor1_fatjet", "msoftdrop_cor2_fatjet"]
lin_vars = [ lin_vars_1, lin_vars_2]
lin_bins = 30
lin_xmin = 50
lin_xmax = 200
lin_ytitle = "AK8 Jet mass* [GeV]"
lin_H_mass = 125.35
lin_ymax = 350
lin_rounding = 2

# Log2 plots parameters
log2_vars_1 = ["log2(m_fatjet)", "log2(msoftdrop_fatjet)", "log2(m_cor1_fatjet)", "log2(m_cor2_fatjet)"]
log2_vars_2 = ["log2(msoftdrop_fatjet)", "log2(msoftscaled_fatjet)","log2(msoftdrop_cor1_fatjet)", "log2(msoftdrop_cor2_fatjet)"]
log2_vars = [ log2_vars_1, log2_vars_2]
log2_bins = 80
log2_xmin = 4.5
log2_xmax = 8.5
log2_ytitle = "AK8 Jet log2(mass*) [GeV]"
log2_H_mass = math.log2(125.35)
log2_ymax = 350
log2_rounding = 3

if plot_type == "lin" :
    variables = lin_vars
    bins = lin_bins
    xmin = lin_xmin
    xmax = lin_xmax
    ymax = lin_ymax
    ytitle = lin_ytitle
    H_mass = lin_H_mass
    rounding = lin_rounding
else:
    variables = log2_vars
    bins = log2_bins
    xmin = log2_xmin
    xmax = log2_xmax
    ymax = log2_ymax
    ytitle = log2_ytitle
    H_mass = log2_H_mass
    rounding = log2_rounding

title = ["AK8 Jet Candidate Mass Corrections", "AK8 Jet Candidate Softdrop Mass Corrections"]
ROOT.gROOT.SetBatch(True) # disable canvas display
ROOT.gStyle.SetOptStat(0) # disable stat box





for i in range(len(variables)):

    if (sample_type == "DY"):
        # Multiple root files
        ch = TChain(tree_name)
        for k in range(len(file_list)):
            ch.Add(path_list + file_list[k])
        events = ch
    else:
        # Single root file 
        f = ROOT.TFile(file_single, "READ")
        events = f.Get(tree_name)
    
    c = TCanvas("c", "canvas", 800, 700)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.01, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.12)  # joins upper and lower plot
    pad1.SetGrid()
    #pad1.SetLogx() 
    pad1.Draw()
    pad1.cd()

    S0 = TH1D("S0", title[i] , bins, xmin, xmax)
    events.Draw(variables[i][0]+">>S0", cut)
    #S1.Scale(1./S1.Integral())  # can be used to normalize area under histo to 1
    #S1.Scale(Signal_scale[0])
    S0.SetLineColor(kBlack)
    S0.SetLineWidth(2)
    #S0.SetLineStyle(2)
    #S0.Scale(1)

    S1 = TH1D("S1", title[i] , bins, xmin, xmax)
    events.Draw(variables[i][1]+">>S1", cut)
    S1.SetLineColor(kBlue)
    S1.SetLineWidth(2)
    #S1.SetLineStyle(2)
    #S1.Scale(1)

    S2 = TH1D("S2", title[i] , bins, xmin, xmax)
    events.Draw(variables[i][2]+">>S2", cut)
    S2.SetLineColor(kRed)
    S2.SetLineWidth(2)
    #S2.SetLineStyle(2)
    #S2.Scale(1)

    S3 = TH1D("S3", title[i] , bins, xmin, xmax)
    events.Draw(variables[i][3]+">>S3", cut)
    S3.SetLineColor(kViolet)
    S3.SetLineWidth(2)
    #S2.SetLineStyle(2)
    #S2.Scale(1)
    
    # draw line for y = 1 to help compare ratio
    line = TLine(H_mass, 0.,H_mass, ymax)
    line.SetLineColor(kBlack)
    line.SetLineWidth(1)
    line.SetLineStyle(2)    

    # Computing STDV and FWHM
    S0_st = str(round (S0.GetStdDev(), rounding))
    S0_fwhm = str(round( FWHM(S0), rounding))
    S0_leg = variables[i][0] + " (#sigma =" + S0_st + ", FWHM = " + S0_fwhm + ")"
    S1_st = str(round (S1.GetStdDev(), rounding))
    S1_fwhm = str(round( FWHM(S1), rounding)) 
    S1_leg = variables[i][1] + " (#sigma =" + S1_st + ", FWHM = " + S1_fwhm + ")"
    S2_st = str(round (S2.GetStdDev(), rounding))
    S2_fwhm = str(round( FWHM(S2), rounding)) 
    S2_leg = variables[i][2] + " (#sigma =" + S2_st + ", FWHM = " + S2_fwhm + ")"
    S3_st = str(round (S3.GetStdDev(), rounding))
    S3_fwhm = str(round( FWHM(S3), rounding)) 
    S3_leg = variables[i][3] + " (#sigma =" + S3_st + ", FWHM = " + S3_fwhm + ")"
    

    # Stylistic adjustments of regular plot through SD histo
    SD_y = S0.GetYaxis()
    SD_x = S0.GetXaxis()

    # Adjusting yaxis range as needed for each variable (especially log)
    #SD_y.SetRangeUser(ymin[i], ymax[i])

    gStyle.SetTitleFontSize(0.05) # Title size

    # x-axis (not necessary since hidden by ratio plot) 
    SD_x.SetTitle(ytitle)
    SD_x.SetTitleSize(30) 
    SD_x.SetTitleFont(43) 
    SD_x.SetTitleOffset(1.25)
    SD_x.SetLabelFont(43) # font and size of numbers
    SD_x.SetLabelSize(25)

    # y-axis
    SD_y.SetTitle("Events  ") # space used for offset
    SD_y.SetNdivisions(505) #make divisions nice
    SD_y.SetTitleSize(30) 
    SD_y.SetTitleFont(43) 
    SD_y.SetTitleOffset(1.25)
    SD_y.SetLabelFont(43) # font and size of numbers
    SD_y.SetLabelSize(25)
    SD_y.SetRangeUser(0, ymax)
    
    gPad.Modified() # Update gpad (things might break depending on where this line is)

    # Drawing everything
    S0.Draw()
    S1.Draw("SAME")
    S2.Draw("SAME")
    S3.Draw("SAME")
    line.Draw("SAME")
    
    gPad.RedrawAxis() # Redraw Axis

    
    # Legend
    leg=TLegend(.12,.65,.45,.81) #(x_min, y_min, x_max, y_max)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(22)
    leg.SetTextSize(0.02)
    leg.AddEntry(S0, S0_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
    leg.AddEntry(S1, S1_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
    leg.AddEntry(S2, S2_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
    leg.AddEntry(S3, S3_leg, "L") # "f" for fills, "L" for lines, "lep" for data points
    leg.Draw("SAME")

    # CMS stuff
    tex = TLatex(0.5,0.88," #bf{CMS} #it{Work in Progress}                                        13 TeV ")
    tex.SetNDC()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetTextAlign(20)
    tex.Draw("SAME")

    # Channel
    if (channel =="mu" and sample_type == "ZH"):
        chan = TLatex(0.12,0.82,"  #bf{SUSY_ZHaa4b_M-All} (Z#rightarrow #mu #mu)")
    elif (channel =="mu" and sample_type == "DY"):
        chan = TLatex(0.12,0.82,"  #bf{DY_50_HT200-inf} (Z#rightarrow #mu #mu)")
    else:
        chan = TLatex(0.37,0.82,"  #bf{Electon channel}")
    chan.SetNDC()
    chan.SetTextFont(42)
    chan.SetTextSize(0.03)
    chan.Draw("SAME")

    # Save individual pdf/png 
    #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
    #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")
    # Save one pdf with all canvas
    if i == 0:
        c.Print(output_dir+ "/pdf/Higgs_mass_correction_"+ channel + "_" + plot_type + "_" + sample_type + ".pdf(", "Title: " + title[i])
    elif i == (len(variables)-1):
        c.Print(output_dir+ "/pdf/Higgs_mass_correction_"+ channel + "_" + plot_type + "_" + sample_type + ".pdf)", "Title: " + title[i])
    else:
        c.Print(output_dir+ "/pdf/Higgs_mass_correction_"+ channel + "_" + plot_type + "_" + sample_type + ".pdf", "Title: " + title[i])
    # Delete all histos to avoid warning when initializing histos with same name
    #S0.SetDirectory(0)
    #S1.SetDirectory(0)
    c.Close() # Delete Canvas after saving it

