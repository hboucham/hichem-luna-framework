#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH2D, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


def Histo_style(h, x_title, y_title):

    h_y = h.GetYaxis()
    h_x = h.GetXaxis()

    # x-axis 
    h_x.SetTitle(x_title)
    h_x.SetTitleSize(30) 
    h_x.SetTitleFont(43) 
    h_x.SetTitleOffset(1.1)
    h_x.SetLabelFont(43) # font and size of numbers
    h_x.SetLabelSize(25)

    # y-axis
    h_y.SetTitle(y_title) # space used for offset
    h_y.SetTitleSize(30) 
    h_y.SetTitleFont(43) 
    h_y.SetTitleOffset(1.25)
    h_y.SetLabelFont(43) # font and size of numbers
    h_y.SetLabelSize(25)

###################################################################################################
##############  Manually adjust CHANNEL  ##########################################################
#channel = "mu"
channel = "el"
cut = ""

# input/output directory and tree name
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_" + channel + "/"
output_dir = "../plots/"
tree_name = "event_tree"
weight = "weight_tot" #"weight_zPt_nominal"

data_mu = ["data_SingleMuonA", "data_SingleMuonB", "data_SingleMuonC", "data_SingleMuonD"]
data_el = ["data_EGammaA", "data_EGammaB", "data_EGammaC", "data_EGammaD"]

# Histo variables, labels, range and binsi
# x information
xbranches = ["dphi_MET_l1"]
xrange_min = [0]
xrange_max = [3.2]
xbins = [16]

# y information
ybranches = ["score_Haa4b_vs_QCD"]
yrange_min = [0.1]
yrange_max = [1]
ybins = [9]


if channel =="mu":
    data_files = data_mu
else:
    data_files = data_el

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box

for i in range(len(xbranches)): 
    c = TCanvas("c", "canvas", 800, 800)
    c.SetTopMargin(0.11)
    c.SetRightMargin(0.13)
    c.SetLeftMargin(0.13)
            
    # Data 2018A
    fD0 = TFile(input_dir+ data_files[0] + ".root", "READ")
    events_D0 = fD0.Get(tree_name)
    D0 = TH2D("D0", xbranches[i] + "_vs_" + ybranches[i], xbins[i], xrange_min[i], xrange_max[i], ybins[i], yrange_min[i], yrange_max[i])
    events_D0.Draw(ybranches[i] + ":" + xbranches[i] + ">>D0", cut)

    # Data 2018B
    fD1 = TFile(input_dir+ data_files[1] + ".root", "READ")
    events_D1 = fD1.Get(tree_name)
    D1 = TH2D("D1", xbranches[i] + "_vs_" + ybranches[i], xbins[i], xrange_min[i], xrange_max[i], ybins[i], yrange_min[i], yrange_max[i])
    events_D1.Draw(ybranches[i] + ":" + xbranches[i] + ">>D1", cut)
    
    # Data 2018C
    fD2 = TFile(input_dir+ data_files[2] + ".root", "READ")
    events_D2 = fD2.Get(tree_name)
    D2 = TH2D("D2", xbranches[i] + "_vs_" + ybranches[i], xbins[i], xrange_min[i], xrange_max[i], ybins[i], yrange_min[i], yrange_max[i])
    events_D2.Draw(ybranches[i] + ":" + xbranches[i] + ">>D2", cut)

    # Data 2018D
    fD3 = TFile(input_dir+ data_files[3] + ".root", "READ")
    events_D3 = fD3.Get(tree_name)
    D3 = TH2D("D3", xbranches[i] + "_vs_" + ybranches[i], xbins[i], xrange_min[i], xrange_max[i], ybins[i], yrange_min[i], yrange_max[i])
    events_D3.Draw(ybranches[i] + ":" + xbranches[i] + ">>D3", cut)

    # Adding Histos together
    D = TH2D("D", xbranches[i] + "_vs_" + ybranches[i], xbins[i], xrange_min[i], xrange_max[i], ybins[i], yrange_min[i], yrange_max[i]) 
    D.Add(D0)
    D.Add(D1)
    D.Add(D2)
    D.Add(D3)
    D.GetZaxis().SetRangeUser(0, 25)

    # Histo style and drawing
    gStyle.SetTitleFontSize(0.05) # Title size
    Histo_style(D, xbranches[i], ybranches[i])
    D.SetTitle("dphi(MET, lep1) vs PNET_score")
    gPad.Modified() # Update gpad (things might break depending on where this line is) 
    #D.Draw("colz text") 
    D.Draw("COLZ") 
    D.SetMarkerStyle(20)  # Set marker style, 20 is a filled circle
    D.SetMarkerSize(1.5)  # Increase marker size
    D.SetMarkerColor(ROOT.kBlack)  # Set marker color to black
    D.Draw("P SAME") 
    #D.Draw("P") 
    ROOT.gPad.RedrawAxis() 

    # CMS Stuff
    t1 = TLatex(0.5,0.89," #bf{CMS} #it{Work in Progress}                   58 fb^{-1} (13 TeV, 2018) ")
    t1.SetNDC()
    t1.SetTextFont(42)
    t1.SetTextSize(0.032)
    t1.SetTextAlign(20)
    t1.Draw("SAME")


    # Channel
    if (channel =="mu"):
        chan = TLatex(0.3,0.85,"  #bf{Muon channel}")
    else:
        chan = TLatex(0.3,0.85,"  #bf{Electron channel}")
    chan.SetNDC()
    chan.SetTextFont(42)
    chan.SetTextSize(0.04)
    chan.Draw("SAME")

    # Save png and pdf of your canvas 
    c.Print(output_dir + "dphi_METl1_2Dplot.pdf")
    '''
    if i == 0:
        c.Print(output_dir + "dphi_METl1_2Dplot.pdf(", "Title: " + xbranches[i] + "_vs_" + ybranches[i])
    elif i == len(xbranches)-1 :
        c.Print(output_dir + "dphi_METl1_2Dplot.pdf)", "Title: " + xbranches[i] + "_vs_" + ybranches[i])
    else:
        c.Print(output_dir + "dphi_METl1_2Dplot.pdf", "Title: " +  xbranches[i] + "_vs_" + ybranches[i])
    '''    
    c.Close() # Delete Canvas after saving it
                

# Loops back to next sample
