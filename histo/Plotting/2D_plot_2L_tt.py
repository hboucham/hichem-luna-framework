#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH2D, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


def Histo_style(h, x_title, y_title):

    h_y = h.GetYaxis()
    h_x = h.GetXaxis()

    # x-axis 
    h_x.SetTitle(x_title)
    h_x.SetTitleSize(30) 
    h_x.SetTitleFont(43) 
    h_x.SetTitleOffset(1.1)
    h_x.SetLabelFont(43) # font and size of numbers
    h_x.SetLabelSize(25)

    # y-axis
    h_y.SetTitle(y_title) # space used for offset
    h_y.SetTitleSize(30) 
    h_y.SetTitleFont(43) 
    h_y.SetTitleOffset(1.25)
    h_y.SetLabelFont(43) # font and size of numbers
    h_y.SetLabelSize(25)

# Definition of v2 WP
PFWP40 = ["(score_Haa4b_vs_QCD > 0.960)", "((score_Haa4b_vs_QCD < 0.960) && (score_Haa4b_vs_QCD > 0.84))"]
PFWP60 = ["(score_Haa4b_vs_QCD > 0.930)", "((score_Haa4b_vs_QCD < 0.930) && (score_Haa4b_vs_QCD > 0.660))"]
PFWP80 = ["(score_Haa4b_vs_QCD > 0.840)", "((score_Haa4b_vs_QCD < 0.840) && (score_Haa4b_vs_QCD > 0.40))"]
# Definition of v2a WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v2a > 0.968)", "((score_Haa4b_vs_QCD_v2a < 0.968) && (score_Haa4b_vs_QCD_v2a > 0.84))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v2a > 0.944)", "((score_Haa4b_vs_QCD_v2a < 0.944) && (score_Haa4b_vs_QCD_v2a > 0.660))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v2a > 0.868)", "((score_Haa4b_vs_QCD_v2a < 0.868) && (score_Haa4b_vs_QCD_v2a > 0.40))"]
# Definition of v2b WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v2b > 0.952)", "((score_Haa4b_vs_QCD_v2b < 0.952) && (score_Haa4b_vs_QCD_v2b > 0.84))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v2b > 0.916)", "((score_Haa4b_vs_QCD_v2b < 0.916) && (score_Haa4b_vs_QCD_v2b > 0.660))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v2b > 0.814)", "((score_Haa4b_vs_QCD_v2b < 0.814) && (score_Haa4b_vs_QCD_v2b > 0.40))"]
# Definition of v1 WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v1 > 0.992)", "((score_Haa4b_vs_QCD_v1 < 0.992) && (score_Haa4b_vs_QCD_v1 > 0.92))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v1 > 0.975)", "((score_Haa4b_vs_QCD_v1 < 0.975) && (score_Haa4b_vs_QCD_v1 > 0.80))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v1 > 0.920)", "((score_Haa4b_vs_QCD_v1 < 0.920) && (score_Haa4b_vs_QCD_v1 > 0.50))"]

PF_object_titles = ["Pass", "Fail"]

# command inputs
channel = sys.argv[1]
channel2 = sys.argv[2]
sub = sys.argv[3]
WP = sys.argv[4]
comb = sys.argv[5]

if (WP == "WP40"):
    PFWP = PFWP40
    PFWP_convention = "WP40"
elif (WP == "WP60"):
    PFWP = PFWP60
    PFWP_convention = "WP60"
elif (WP == "WP80"):
    PFWP = PFWP80
    PFWP_convention = "WP80"
else:
    exit

###################################################################################################
##############  Manually adjust CHANNEL  ##########################################################
#channel = "mu"
#channel = "el"
############### Manually adjust Pass/fail Working Point  ##########################################
#PFWP = PFWP40
#PFWP = PFWP60
#PFWP = PFWP80
#cut = " && nBJetM_add == 0 && dphi_fatjet_METlep > 3*pi/4"
###################################################################################################
###################################################################################################


# input/output directory and tree name
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_tt_" + channel + channel2 + "_nom/"
output_dir = "../plots_2L_tt"
tree_name = "event_tree"
weight = "weight_tot" #"weight_zPt_nominal"
#weight = "weight_HEM_nominal*weight_GEN_nominal"

# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1

# Scaling for MC histos

# TTH Signal: [M-15, M-30, M-55]
Signal_TTH_files = ["SUSY_TTH_M-15", "SUSY_TTH_M-30", "SUSY_TTH_M-55"]
Signal_TTH_xsec = [0.1445, 0.1445, 0.1445]
Signal_TTH_ev = [188898, 196711, 198645]
Signal_TTH_scale =[]
for i in range(len(Signal_TTH_xsec)):
    Signal_TTH_scale.append(lumi*Signal_TTH_xsec[i]/Signal_TTH_ev[i])

Signal_files = Signal_TTH_files
Signal_scale = Signal_TTH_scale
sig_convention = "ttH"

# Other backgrounds:
Background_files = ["TT_2L2Nu", "TT_SemiLeptonic", "TTW", "ST_TW_Top_NoFullyHadronic", "ST_TW_AntiTop_NoFullyHadronic", "ST-s_leptonic", "ST-s_hadronic", "ST-t_Top_inclusive", "ST-t_AntiTop_inclusive", "WW", "WZ", "ZZ", "WH_Hbb_Wpluslnu", "WH_Hbb_Wminuslnu", "DY_10-50"]
Background_xsec = [87.339, 364.328, 0.6008, 21.63, 21.63, 4.831, 5.041, 134.2, 80, 118.7, 47.13, 16.523, 0.1576, 0.0999, 18610]
Background_ev = [143887383, 471724160, 27686862, 11269836, 11012326, 12607741, 10793026, 197426478, 98724306, 15679000, 7940000, 3526000, 4562550, 4870968, 99288125]
Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])

# Data 
data_mu = ["data_SingleMuonA", "data_SingleMuonB", "data_SingleMuonC", "data_SingleMuonD"]
data_el = ["data_EGammaA", "data_EGammaB", "data_EGammaC", "data_EGammaD"]

# Histo variables, labels, range and binsi
# x information
x_root_titles = ["mass", "msoft", "msoftPtScaled", "pnet"]
xbranches = ["m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet","mH_avg"]
xrange_min = 0
xrange_max = 300
xbins = 30

# y information
y_root_titles = "mA"
ybranches = "mA_34a"
#ybranches = "mA_34d" # mA_avg, mA_34b, mA_34d
yrange_min = 0
yrange_max = 70
ybins = 35

# Naming convention
if channel == "mu" and channel2 == "mu":
    data_files = data_mu
    decay_channel = "mm"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "ll"
if channel == "mu" and channel2 == "el":
    data_files = data_mu
    decay_channel = "me"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "ll"
if channel == "el" and channel2 == "el":
    data_files = data_el
    decay_channel = "ee"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "ll"
if channel == "el" and channel2 == "mu":
    data_files = data_el
    decay_channel = "em"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "ll"

if sub =="1b":
    decay_channel = "ttb" + decay_channel 
    cut =  " && nBJetM_add == 1"
if sub =="2b+":
    decay_channel = "ttbb" + decay_channel 
    cut =  " && nBJetM_add > 1"
if sub =="1b+":
    decay_channel = "ttb" + decay_channel 
    cut =  " && nBJetM_add > 0"

# samples info
samples_name = [Signal_files[0], Signal_files[1], Signal_files[2], "data_2018", "TT_2L2Nu"]
samples_convention = [sig_convention + "toaato4b_mA_15", sig_convention + "toaato4b_mA_30", sig_convention + "toaato4b_mA_55", "Data", "TT2l"]

syst = "Nom"
year = "2018"
date = "_012325"

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box

for q in range(len(samples_name)):
    output_file = TFile(output_dir + "/root_" + channel + channel2 + "/" + PFWP_convention + "/" + decay_channel + "_" + samples_convention[q] + "_" + year + ".root","recreate")
    for k in range(len(PFWP)):
        draw_option = f"({PFWP[k] + cut})*({weight})"
        #print(draw_option)
        for i in range(len(xbranches)): 
            c = TCanvas("c", "canvas", 800, 800)
            c.SetTopMargin(0.11)
            c.SetRightMargin(0.13)
            c.SetLeftMargin(0.13)
            
            if samples_name[q] == "data_2018":
                # do that
                # Data 2018A
                fD0 = TFile(input_dir+ data_files[0] + ".root", "READ")
                events_D0 = fD0.Get(tree_name)
                D0 = TH2D("D0", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D0.Draw(ybranches + ":" + xbranches[i] + ">>D0",PFWP[k] + cut)
                
                # Data 2018B
                fD1 = TFile(input_dir+ data_files[1] + ".root", "READ")
                events_D1 = fD1.Get(tree_name)
                D1 = TH2D("D1", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D1.Draw(ybranches + ":" + xbranches[i] + ">>D1",PFWP[k] + cut)

                # Data 2018C
                fD2 = TFile(input_dir+ data_files[2] + ".root", "READ")
                events_D2 = fD2.Get(tree_name)
                D2 = TH2D("D2", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D2.Draw(ybranches + ":" + xbranches[i] + ">>D2",PFWP[k] + cut)
                
                # Data 2018D
                fD3 = TFile(input_dir+ data_files[3] + ".root", "READ")
                events_D3 = fD3.Get(tree_name)
                D3 = TH2D("D3", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D3.Draw(ybranches + ":" + xbranches[i] + ">>D3",PFWP[k] + cut)

                # Adding Histos together
                D = TH2D("D", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max) 
                D.Add(D0)
                D.Add(D1)
                D.Add(D2)
                D.Add(D3)

                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(D, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                D.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu" and channel2 == "mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon-Muon}")
                if (channel =="mu" and channel2 == "el"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon-Electron}")
                if (channel =="el" and channel2 == "el"):
                    chan = TLatex(0.15,0.85,"  #bf{Electron-Electron}")
                if (channel =="el" and channel2 == "mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Electron-Muon}") 
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(D, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it
                
            elif samples_name[q] == "TT_2L2Nu":
                
                # Loading file and filling 2D plot with pass/fail cut
                f_B = TFile(input_dir + samples_name[q] + ".root", "READ")
                events_B = f_B.Get(tree_name)
                B = TH2D("B", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_B.Draw(ybranches + ":" + xbranches[i] + ">>B",draw_option)
                B.Scale(Background_scale[0]) 
               
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(B, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                B.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu" and channel2 == "mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon-Muon}")
                if (channel =="mu" and channel2 == "el"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon-Electron}")
                if (channel =="el" and channel2 == "el"):
                    chan = TLatex(0.15,0.85,"  #bf{Electron-Electron}")
                if (channel =="el" and channel2 == "mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Electron-Muon}") 
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(B, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it
            
            else : # all other samples that don't require adding histos
                # Loading file and filling 2D plot with pass/fail cut
                f_B = TFile(input_dir + samples_name[q] + ".root", "READ")
                events_B = f_B.Get(tree_name)
                B = TH2D("B", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_B.Draw(ybranches + ":" + xbranches[i] + ">>B",draw_option)
                B.Scale(Signal_scale[q]) 
               
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(B, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                B.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.5,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu" and channel2 == "mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon-Muon}")
                if (channel =="mu" and channel2 == "el"):
                    chan = TLatex(0.15,0.85,"  #bf{Muon-Electron}")
                if (channel =="el" and channel2 == "el"):
                    chan = TLatex(0.15,0.85,"  #bf{Electron-Electron}")
                if (channel =="el" and channel2 == "mu"):
                    chan = TLatex(0.15,0.85,"  #bf{Electron-Muon}") 
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(B, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/pdf/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it

# Loops back to next sample
