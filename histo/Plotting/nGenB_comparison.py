#!/usr/bin/env python

import sys
import math
import os
import os.path

import numpy as np
#import matplotlib.pyplot as plt
#import mplhep as hep
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex 
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite

from helpers.ratioStatError import getRatioStatError

# Functions
def createCanvasPads(logs): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.015)  # joins upper and lower plot
    pad1.SetGrid()
    if (logs):
        pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad1
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def createRatio(h1, h2, x_axis): # Function to create ratio plot
     h3 = h1.Clone("h3")
     h3.SetLineColor(kBlack)
     h3.SetMarkerStyle(20)
     h3.SetTitle("")
     h3.SetMinimum(0)
     h3.SetMaximum(1.1)
     # Set up plot for markers and errors
     #h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h2)
 
     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("4+ GenB/All    ")
     y.SetNdivisions(505)
     y.SetTitleSize(25)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.25)
     y.SetLabelFont(43)
     y.SetLabelSize(20)
 
     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitle(x_axis)
     x.SetTitleSize(25)
     x.SetTitleFont(43)
     x.SetTitleOffset(1)
     x.SetLabelFont(43)
     x.SetLabelSize(20)
 
     return h3

# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")

def MinorTextFormat_ratio(t):
    t.SetNDC()
    t.SetTextFont(43)
    t.SetTextSize(15)
    t.Draw("SAME")

# CMS Official Colors
CMS_colors = {
    "blue1": ROOT.TColor.GetColor("#3f90da"),
    "orange1": ROOT.TColor.GetColor("#ffa90e"),
    "red": ROOT.TColor.GetColor("#bd1f01"),
    "grey1": ROOT.TColor.GetColor("#94a4a2"),
    "purple": ROOT.TColor.GetColor("#832db6"),
    "brown": ROOT.TColor.GetColor("#a96b59"),
    "orange2": ROOT.TColor.GetColor("#e76300"),
    "grey2": ROOT.TColor.GetColor("#b9ac70"),
    "grey3": ROOT.TColor.GetColor("#717581"),
    "blue2": ROOT.TColor.GetColor("#92dadd"),
}



# init cuts
h_blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
z_peak = "(m_ll > 88) && (m_ll < 94)" 
z_tail = "((m_ll < 88) && (m_ll > 80)) || ((m_ll < 100) && (m_ll > 94))" 

# command inputs
#  python3 Data_MC_plot_2L_Z.py mu/el log/lin scale/noscale 0/PNETcut date
channel = sys.argv[1]
#massA = sys.argv[2]
date = sys.argv[2]

sc = "lin"
if (sc == "log"):
    scale_log = True
else:
    scale_log = False

###################################################################################################
############### Manually adjust PNET_score ########################################################
#PNET_score = "0"
#PNET_score = "0.5"
################Manually adjust cut ###############################################################
#cut = ""
cut_add = ""
#cut_add = "dphi_MET_l1 < 0.2"
#cut = "score_Haa4b_vs_QCD > 0.5"
#cut = "dphi_MET_l1 < 0.2" pt_ll (150)   pt_fatjet (250)   dphi_l1_l2 (1.5) 
#cut = "score_Haa4b_vs_QCD > 0.5"
################Manually adjust date ###############################################################
#date = "081224cs"
###################################################################################################

dataScale = ""
PNET_score = "0.5"


# input/output directory and tree name
#input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_" + channel + "_" + syst + "/"
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_"+ channel + "_nom/"
output_dir = "../plots"
tree_name = "event_tree"
weight = "weight_tot"

cut0 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet == 0" 
cut1 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet == 1" 
cut2 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet == 2" 
cut3 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet == 3" 
cut4 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet == 4" 
cut5 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet > 4" 
draw_S0 = f"({cut0})*({weight})"
draw_S1 = f"({cut1})*({weight})"
draw_S2 = f"({cut2})*({weight})"
draw_S3 = f"({cut3})*({weight})"
draw_S4 = f"({cut4})*({weight})"
draw_S5 = f"({cut5})*({weight})"

cut45 = "score_Haa4b_vs_QCD > 0.5 && GENnBhadron_fatjet > 3" 
cutX = "score_Haa4b_vs_QCD > 0.5" 
draw_S45 = f"({cut45})*({weight})"
draw_SX = f"({cutX})*({weight})"

sig_scaling = 1
    
# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1


# signal: [M-15, M-30, M-55]
Signal_xsec = [0.11352, 0.11352, 0.11352]
Signal_ev = [106743, 101445, 95365]
Signal_scale =[]
for i in range(len(Signal_xsec)):
    Signal_scale.append(lumi*Signal_xsec[i]/Signal_ev[i])

#if massA == "15":
#    massA_scale = Signal_scale[0]
#if massA == "30":
#    massA_scale = Signal_scale[1]
#if massA == "55":
#    massA_scale = Signal_scale[2]

# Histo variables, labels, range and bins
# Kinematic plots ordering: AK8 jet (including mA), lepton pair, leptons, MET, AK4

branches = [ "dphi_fatjet_ll", "pt_ll"]

x_axis = ["|#Delta#phi(AK8, ll)|", "p_{T}(ll) [GeV]"]

range_min = [0, 0]

range_max = [3.2, 600] 

bins = [16, 30]

titles = ["#Delta#phi(Leading Fat Jet, Lepton Pair)", "Lepton Pair p_{T}"]

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box

mass_A = ["15", "30", "55"]

for i in range(len(branches)):
    for ij in range(len(mass_A)):
        massA = mass_A[ij]
        massA_scale = Signal_scale[ij]
        input_file = "SUSY_ZH_M-" + massA + ".root" 
    # Cutting events with msoftdrop in Higgs range [100, 140] GeV for branches
    # other than Leading Fat Jet mass/ softdrop_mass / softscale mass / mH_avg,
    # bins in that range are cuted later in the plotting script
  # if (i != 0) and (i != 10) and (i != 12):
  #   blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
    
    
        # Creating Canvas
        c, pad1, pad2 = createCanvasPads(scale_log)
        pad1.cd() # pad1 is for regular plot
        

        # Creating Canvas
        #c = TCanvas("c", "canvas", 800, 800)
        #c.SetTopMargin(0.11)
        #c.SetRightMargin(0.13)
        #c.SetLeftMargin(0.13)
        #c.SetGrid()
        
        # Loadinh signal sample
        fS = TFile(input_dir + input_file, "READ")
        events_S = fS.Get(tree_name)
       
        # Plot 0 GenB
        S0 = TH1D("S0", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S0", draw_S0)
        #S1.Scale(1./S1.Integral())  # can be used to normalize area under histo to 1
        S0.Scale(massA_scale)
        S0.SetLineColor(kMagenta)
        S0.SetLineWidth(2)
        S0.SetLineStyle(1)

        # Plot 1 GenB
        S1 = TH1D("S1", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S1", draw_S1)
        S1.Scale(massA_scale)
        S1.SetLineColor(kRed)
        S1.SetLineWidth(2)
        S1.SetLineStyle(1)

        # Plot 2 GenB
        S2 = TH1D("S2", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S2", draw_S2)
        S2.Scale(massA_scale)
        S2.SetLineColor(kBlue)
        S2.SetLineWidth(2)
        S2.SetLineStyle(1)

        # Plot 3 GenB
        S3 = TH1D("S3", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S3", draw_S3)
        S3.Scale(massA_scale)
        S3.SetLineColor(kOrange)
        S3.SetLineWidth(2)
        S3.SetLineStyle(1)

        # Plot 4 GenB
        S4 = TH1D("S4", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S4", draw_S4)
        S4.Scale(massA_scale)
        S4.SetLineColor(kBlack)
        S4.SetLineWidth(2)
        S4.SetLineStyle(1)

        # Plot 5+ GenB
        S5 = TH1D("S5", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S5", draw_S5)
        S5.Scale(massA_scale)
        S5.SetLineColor(kSpring)
        S5.SetLineWidth(2)
        S5.SetLineStyle(1)

        # Plot 4+ GenB for ratio
        S45 = TH1D("S45", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>S45", draw_S45)
        S45.Scale(massA_scale)
        #S45.SetLineColor(kSpring)
        S45.SetLineWidth(2)
        S45.SetLineStyle(1)

        # Plot 4+ GenB for ratio
        SX = TH1D("SX", titles[i], bins[i], range_min[i], range_max[i])
        events_S.Draw(branches[i]+">>SX", draw_SX)
        SX.Scale(massA_scale)
        #S45.SetLineColor(kSpring)
        SX.SetLineWidth(2)
        SX.SetLineStyle(1)
        
        # Stylistic adjustments of regular plot through SD histo
        SD_y = S4.GetYaxis()
        SD_x = S4.GetXaxis()

        # Adjusting y axis range as needed for each variable (especially log)
        # Manually adjust both max and min
        #SD_y.SetRangeUser(1, 100000)
        
        # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
        # This is mainly done to make space for the legend and other text
        S4.SetMinimum(0)
        S4.SetMaximum(1.5*S4.GetMaximum())
        
        gStyle.SetTitleFontSize(0.05) # Title size
        
        # x-axis (not necessary since hidden by ratio plot) 
        SD_x.SetTitle(x_axis[i])
        SD_x.SetTitleSize(0.05)
        SD_x.SetTitleOffset(1.5)
        SD_x.SetLabelOffset(0.3)

        # y-axis
        SD_y.SetTitle("Events  ") # space used for offset
        SD_y.SetNdivisions(505) #make divisions nice
        SD_y.SetTitleSize(25) 
        SD_y.SetTitleFont(43) 
        SD_y.SetTitleOffset(1.25)
        SD_y.SetLabelFont(43) # font and size of numbers
        SD_y.SetLabelSize(20)


        gPad.Modified() # Update gpad (things might break depending on where this line is)
        
        # Drawing everything
        S4.Draw("HIST")
        S0.Draw("SAME HIST")
        S1.Draw("SAME HIST")
        S2.Draw("SAME HIST")
        S3.Draw("SAME HIST")
        S5.Draw("SAME HIST")
       
        ############# RATIO TEXT SECTION: START ###########################################################################
        # Ratio plot
        pad2.cd()# Go to pad2 for ratio plot
        
        # Histo for ratio plot
        #RATIO1 = createRatio(S2, S1, x_axis[i])
        #RATIO1.SetLineColor(CMS_colors["purple"])
        RATIO2 = createRatio(S45, SX, x_axis[i])
        RATIO2.SetLineColor(CMS_colors["blue1"])
        # Histo for statistical errors accounting for denominator's statistical erorr
        #RATIO.Draw("ep") # draw with stat errors (but doesn't account for denominator statistical error)
        
        # Ratio histo Stylistic stuff
        R_x = RATIO2.GetXaxis()
        R_x.SetTitleSize(25) 
        R_x.SetTitleFont(43) 
        R_x.SetTitleOffset(1.15)
        R_x.SetLabelFont(43) # font and size of numbers
        R_x.SetLabelSize(20)
        R_x.SetLabelOffset(.02)

        # Draw line for y = 1 to help compare ratio
        line = TLine(range_min[i], 0.5,range_max[i], 0.5)
        line.SetLineColor(kBlack)
        line.SetLineWidth(1)
       
        # Drawing histos in ratio pad
        #RATIO1.Draw("HIST")
        RATIO2.Draw("HIST")
        line.Draw("SAME")

        # Redraw Axis in case they are covered
        pad2.RedrawAxis()

        ############# RATIO TEXT SECTION: END ###########################################################################

        # Legend and other text
        pad1.cd() # go back to pad1
        
        # Legend
        leg=TLegend(.14,.62,.86,.84) #(x_min, y_min, x_max, y_max)
        #leg=TLegend(.38,.75,.88,.87) #(x_min, y_min, x_max, y_max)
        #leg=TLegend() # automatic legend placement sucks
        leg.SetNColumns(1) # number of columns for legend
        leg.SetBorderSize(0)
        leg.SetFillColor(0)
        leg.SetFillStyle(0)
        leg.SetTextFont(22)
        leg.SetTextSize(0.03)
        leg.AddEntry(S0,"ZH_Haa4b_M-" + massA + "_0genB","L")
        leg.AddEntry(S1,"ZH_Haa4b_M-" + massA + "_1genB","L")
        leg.AddEntry(S2,"ZH_Haa4b_M-" + massA + "_2genB","L")
        leg.AddEntry(S3,"ZH_Haa4b_M-" + massA + "_3genB","L")
        leg.AddEntry(S4,"ZH_Haa4b_M-" + massA + "_4genB","L")
        leg.AddEntry(S5,"ZH_Haa4b_M-" + massA + "_5+genB","L")
        leg.Draw("SAME")

        ############# TEXT SECTION: START ###########################################################################
        
        # CMS stuff
        if (scale_log == False and PNET_score == "0"):
            tex = TLatex(0.53,0.88,"#bf{CMS} #it{Simulation}                                                                13 TeV")
        else:
            tex = TLatex(0.51,0.88,"#bf{CMS} #it{Simulation}                                                                           13 TeV  ")    
        tex.SetNDC()
        tex.SetTextFont(42)
        tex.SetTextSize(0.04)
        tex.SetTextAlign(20)
        tex.Draw("SAME")
        
        # CMS stuff
        #tex = TLatex(0.53,0.88,"  #bf{CMS} #it{Work in Progress}                            59.83 fb^{-1} (13 TeV, 2018)")
        #tex.SetNDC()
        #tex.SetTextFont(42)
        #tex.SetTextSize(0.04)
        #tex.SetTextAlign(20)
        #tex.Draw("SAME")

        # Channel
        if (channel =="mu"):
            chan = TLatex(0.15,0.84,"  #bf{Di-muon}")
        else:
            chan = TLatex(0.15,0.84,"  #bf{Di-electron}")
        chan.SetNDC()
        chan.SetTextFont(42)
        chan.SetTextSize(0.04)
        chan.Draw("SAME")
        
        # PNET score "score_Haa4b_vs_QCD"
        t_score = TLatex(0.4,0.845,"score_Haa4b_vs_QCD > "+ PNET_score)
        MinorTextFormat(t_score)
        
        if (cut_add):
            t_cat = TLatex(0.4,0.855, cut_add)
            MinorTextFormat(t_cat)



        ####################################### TEXT SECTION: END ###############################################

        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderline = TLine(range_max[i], 1.0,range_max[i], S1.GetMaximum())
        borderline.SetLineColor(kBlack)
        borderline.SetLineWidth(1)
        borderline.Draw("SAME")
    
        # Save individual pdf/png

        #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
        #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")

        # Save one pdf with all canvas
        if i == 0 and ij == 0:
            c.Print(output_dir+ "/ZH_signal_nGenB_" + channel + "_" + date + ".pdf(", "Title: " + branches[i])
        elif i == len(branches)-1 and ij == len(mass_A)-1 :
        #elif i == 6:
            c.Print(output_dir+ "/ZH_signal_nGenB_" + channel + "_" + date + ".pdf)", "Title: " + branches[i])
        else:
            c.Print(output_dir+ "/ZH_signal_nGenB_" + channel + "_" + date + ".pdf", "Title: " + branches[i])
        #if i == 0:
        #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf(", "Title: " + branches[i])
        #elif i == len(branches)-1:
        #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf)", "Title: " + branches[i])
        #else:
        #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf", "Title: " + branches[i])
        # Delete all histos to avoid warning when initializing histos with same name
        #B0.SetDirectory(0)
        c.Close() # Delete Canvas after saving it

# Loops back to the next variable to plot
