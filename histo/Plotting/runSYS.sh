#!/bin/bash
PNET="0.5"
DATE="012225"
SYS=("PU" "PDF" "ISR" "FSR" "VH2D" "LEPIDISO" "LEPTRIG")
MASS=("15" "30" "55")

for sys in "${SYS[@]}"; do
  for mass in "${MASS[@]}"; do
    python3 simple_sys.py mu $sys $mass $PNET $DATE
    python3 simple_sys.py el $sys $mass $PNET $DATE
  done
done
