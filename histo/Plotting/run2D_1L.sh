#!/bin/bash

#DIR="2D_1L_test"
DIR="2D_1L_013025_v2bfix"
DIRECTORY="/afs/cern.ch/user/h/hboucham/public/${DIR}"
#PDF_DIR="/eos/user/h/hboucham/www_hichem/Haa4b_plots/"
PDF_DIR="testpdf/"

# Making directory if it doesn't exist
if [ ! -d "$DIRECTORY" ]; then
  echo "$DIRECTORY does not exist, so I shall create it"
  mkdir "$DIRECTORY"
  mkdir "${DIRECTORY}/WP40"
  mkdir "${DIRECTORY}/WP60"
  mkdir "${DIRECTORY}/WP80"
fi

# Remove old 2D plots root files
echo ">>>>>>> Deleting old pdf and root files."
# Muon
rm -f ../plots_1L/root_mu/WP40/*
rm -f ../plots_1L/root_mu/WP60/*
rm -f ../plots_1L/root_mu/WP80/*
# Electron
rm -f ../plots_1L/root_el/WP40/*
rm -f ../plots_1L/root_el/WP60/*
rm -f ../plots_1L/root_el/WP80/*
# Merged lepton
rm -f ../plots_1L/root_l/WP40/*
rm -f ../plots_1L/root_l/WP60/*
rm -f ../plots_1L/root_l/WP80/*

# Remove old pdf files
rm -f ../plots_1L/2D_*

# Make new pdf and root files for individual channels (mu/el)
echo ">>>>>>> Making root files and pdfs of individual sub-categories (mu/el) * (WlvHi/WlvLo/ttbl/ttbbl) * (WP40/WP60/WP80)."
# WP80
# Muon
python3 2D_plot_1L.py mu WP80 Hi x
python3 2D_plot_1L.py mu WP80 Lo x
python3 2D_plot_1L.py mu WP80 ttb x
python3 2D_plot_1L.py mu WP80 ttbb x
# electron
python3 2D_plot_1L.py el WP80 Hi x
python3 2D_plot_1L.py el WP80 Lo x
python3 2D_plot_1L.py el WP80 ttb x
python3 2D_plot_1L.py el WP80 ttbb x

# WP60
# Muon
python3 2D_plot_1L.py mu WP60 Hi x
python3 2D_plot_1L.py mu WP60 Lo x
python3 2D_plot_1L.py mu WP60 ttb x
python3 2D_plot_1L.py mu WP60 ttbb x
# electron
python3 2D_plot_1L.py el WP60 Hi x
python3 2D_plot_1L.py el WP60 Lo x
python3 2D_plot_1L.py el WP60 ttb x
python3 2D_plot_1L.py el WP60 ttbb x

# WP40
# Muon
python3 2D_plot_1L.py mu WP40 Hi x
python3 2D_plot_1L.py mu WP40 Lo x
python3 2D_plot_1L.py mu WP40 ttb x
python3 2D_plot_1L.py mu WP40 ttbb x
# electron
python3 2D_plot_1L.py el WP40 Hi x
python3 2D_plot_1L.py el WP40 Lo x
python3 2D_plot_1L.py el WP40 ttb x
python3 2D_plot_1L.py el WP40 ttbb x

# copy pdfs to website directory
cp ../plots_1L/2D_* ${PDF_DIR}

# copy root files to public directory
# Muon
cp ../plots_1L/root_mu/WP40/* ${DIRECTORY}/WP40/
cp ../plots_1L/root_mu/WP60/* ${DIRECTORY}/WP60/
cp ../plots_1L/root_mu/WP80/* ${DIRECTORY}/WP80/
# Electron
cp ../plots_1L/root_el/WP40/* ${DIRECTORY}/WP40/
cp ../plots_1L/root_el/WP60/* ${DIRECTORY}/WP60/
cp ../plots_1L/root_el/WP80/* ${DIRECTORY}/WP80/

# Make root files for combined channels, need to rerun 2D plots with slightly different naming convention
echo ">>>>>>> Making root files and pdfs of merged sub-categories (mu/el) * (WlvHi/WlvLo/ttbl/ttbbl) * (WP40/WP60/WP80)."
# WP80
# Muon
python3 2D_plot_1L.py mu WP80 Hi comb
python3 2D_plot_1L.py mu WP80 Lo comb
python3 2D_plot_1L.py mu WP80 ttb comb
python3 2D_plot_1L.py mu WP80 ttbb comb
# electron
python3 2D_plot_1L.py el WP80 Hi comb
python3 2D_plot_1L.py el WP80 Lo comb
python3 2D_plot_1L.py el WP80 ttb comb
python3 2D_plot_1L.py el WP80 ttbb comb

# WP60
# Muon
python3 2D_plot_1L.py mu WP60 Hi comb
python3 2D_plot_1L.py mu WP60 Lo comb
python3 2D_plot_1L.py mu WP60 ttb comb
python3 2D_plot_1L.py mu WP60 ttbb comb
# electron
python3 2D_plot_1L.py el WP60 Hi comb
python3 2D_plot_1L.py el WP60 Lo comb
python3 2D_plot_1L.py el WP60 ttb comb
python3 2D_plot_1L.py el WP60 ttbb comb

# WP40
# Muon
python3 2D_plot_1L.py mu WP40 Hi comb
python3 2D_plot_1L.py mu WP40 Lo comb
python3 2D_plot_1L.py mu WP40 ttb comb
python3 2D_plot_1L.py mu WP40 ttbb comb
# electron
python3 2D_plot_1L.py el WP40 Hi comb
python3 2D_plot_1L.py el WP40 Lo comb
python3 2D_plot_1L.py el WP40 ttb comb
python3 2D_plot_1L.py el WP40 ttbb comb

# Merge mu/el
echo ">>>>>>> Merging sub-categories (mu/el) root files."
# WP40
# Data
hadd -f ../plots_1L/root_l/WP40/WlvHi_Data_2018.root ../plots_1L/root_el/WP40/WlvHi_Data_2018.root ../plots_1L/root_mu/WP40/WlvHi_Data_2018.root
hadd -f ../plots_1L/root_l/WP40/WlvLo_Data_2018.root ../plots_1L/root_el/WP40/WlvLo_Data_2018.root ../plots_1L/root_mu/WP40/WlvLo_Data_2018.root
hadd -f ../plots_1L/root_l/WP40/ttblv_Data_2018.root ../plots_1L/root_el/WP40/ttblv_Data_2018.root ../plots_1L/root_mu/WP40/ttblv_Data_2018.root
hadd -f ../plots_1L/root_l/WP40/ttbblv_Data_2018.root ../plots_1L/root_el/WP40/ttbblv_Data_2018.root ../plots_1L/root_mu/WP40/ttbblv_Data_2018.root
# W jets
hadd -f ../plots_1L/root_l/WP40/WlvHi_Wlv_2018.root ../plots_1L/root_el/WP40/WlvHi_Wlv_2018.root ../plots_1L/root_mu/WP40/WlvHi_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP40/WlvLo_Wlv_2018.root ../plots_1L/root_el/WP40/WlvLo_Wlv_2018.root ../plots_1L/root_mu/WP40/WlvLo_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP40/ttblv_Wlv_2018.root ../plots_1L/root_el/WP40/ttblv_Wlv_2018.root ../plots_1L/root_mu/WP40/ttblv_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP40/ttbblv_Wlv_2018.root ../plots_1L/root_el/WP40/ttbblv_Wlv_2018.root ../plots_1L/root_mu/WP40/ttbblv_Wlv_2018.root
# TT SemiLeptonic
hadd -f ../plots_1L/root_l/WP40/WlvHi_TT1l_2018.root ../plots_1L/root_el/WP40/WlvHi_TT1l_2018.root ../plots_1L/root_mu/WP40/WlvHi_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP40/WlvLo_TT1l_2018.root ../plots_1L/root_el/WP40/WlvLo_TT1l_2018.root ../plots_1L/root_mu/WP40/WlvLo_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP40/ttblv_TT1l_2018.root ../plots_1L/root_el/WP40/ttblv_TT1l_2018.root ../plots_1L/root_mu/WP40/ttblv_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP40/ttbblv_TT1l_2018.root ../plots_1L/root_el/WP40/ttbblv_TT1l_2018.root ../plots_1L/root_mu/WP40/ttbblv_TT1l_2018.root
# WH M15
hadd -f ../plots_1L/root_l/WP40/WlvHi_WHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP40/WlvHi_WHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP40/WlvHi_WHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP40/WlvLo_WHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP40/WlvLo_WHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP40/WlvLo_WHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP40/ttblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP40/ttblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP40/ttblv_ttHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP40/ttbblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP40/ttbblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP40/ttbblv_ttHtoaato4b_mA_15_2018.root
# WH M30
hadd -f ../plots_1L/root_l/WP40/WlvHi_WHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP40/WlvHi_WHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP40/WlvHi_WHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP40/WlvLo_WHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP40/WlvLo_WHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP40/WlvLo_WHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP40/ttblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP40/ttblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP40/ttblv_ttHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP40/ttbblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP40/ttbblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP40/ttbblv_ttHtoaato4b_mA_30_2018.root
# WH M55
hadd -f ../plots_1L/root_l/WP40/WlvHi_WHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP40/WlvHi_WHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP40/WlvHi_WHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP40/WlvLo_WHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP40/WlvLo_WHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP40/WlvLo_WHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP40/ttblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP40/ttblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP40/ttblv_ttHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP40/ttbblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP40/ttbblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP40/ttbblv_ttHtoaato4b_mA_55_2018.root


# WP60
# Data
hadd -f ../plots_1L/root_l/WP60/WlvHi_Data_2018.root ../plots_1L/root_el/WP60/WlvHi_Data_2018.root ../plots_1L/root_mu/WP60/WlvHi_Data_2018.root
hadd -f ../plots_1L/root_l/WP60/WlvLo_Data_2018.root ../plots_1L/root_el/WP60/WlvLo_Data_2018.root ../plots_1L/root_mu/WP60/WlvLo_Data_2018.root
hadd -f ../plots_1L/root_l/WP60/ttblv_Data_2018.root ../plots_1L/root_el/WP60/ttblv_Data_2018.root ../plots_1L/root_mu/WP60/ttblv_Data_2018.root
hadd -f ../plots_1L/root_l/WP60/ttbblv_Data_2018.root ../plots_1L/root_el/WP60/ttbblv_Data_2018.root ../plots_1L/root_mu/WP60/ttbblv_Data_2018.root
# W jets
hadd -f ../plots_1L/root_l/WP60/WlvHi_Wlv_2018.root ../plots_1L/root_el/WP60/WlvHi_Wlv_2018.root ../plots_1L/root_mu/WP60/WlvHi_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP60/WlvLo_Wlv_2018.root ../plots_1L/root_el/WP60/WlvLo_Wlv_2018.root ../plots_1L/root_mu/WP60/WlvLo_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP60/ttblv_Wlv_2018.root ../plots_1L/root_el/WP60/ttblv_Wlv_2018.root ../plots_1L/root_mu/WP60/ttblv_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP60/ttbblv_Wlv_2018.root ../plots_1L/root_el/WP60/ttbblv_Wlv_2018.root ../plots_1L/root_mu/WP60/ttbblv_Wlv_2018.root
# TT SemiLeptonic
hadd -f ../plots_1L/root_l/WP60/WlvHi_TT1l_2018.root ../plots_1L/root_el/WP60/WlvHi_TT1l_2018.root ../plots_1L/root_mu/WP60/WlvHi_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP60/WlvLo_TT1l_2018.root ../plots_1L/root_el/WP60/WlvLo_TT1l_2018.root ../plots_1L/root_mu/WP60/WlvLo_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP60/ttblv_TT1l_2018.root ../plots_1L/root_el/WP60/ttblv_TT1l_2018.root ../plots_1L/root_mu/WP60/ttblv_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP60/ttbblv_TT1l_2018.root ../plots_1L/root_el/WP60/ttbblv_TT1l_2018.root ../plots_1L/root_mu/WP60/ttbblv_TT1l_2018.root
# WH M15
hadd -f ../plots_1L/root_l/WP60/WlvHi_WHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP60/WlvHi_WHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP60/WlvHi_WHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP60/WlvLo_WHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP60/WlvLo_WHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP60/WlvLo_WHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP60/ttblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP60/ttblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP60/ttblv_ttHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP60/ttbblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP60/ttbblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP60/ttbblv_ttHtoaato4b_mA_15_2018.root
# WH M30
hadd -f ../plots_1L/root_l/WP60/WlvHi_WHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP60/WlvHi_WHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP60/WlvHi_WHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP60/WlvLo_WHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP60/WlvLo_WHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP60/WlvLo_WHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP60/ttblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP60/ttblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP60/ttblv_ttHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP60/ttbblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP60/ttbblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP60/ttbblv_ttHtoaato4b_mA_30_2018.root
# WH M55
hadd -f ../plots_1L/root_l/WP60/WlvHi_WHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP60/WlvHi_WHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP60/WlvHi_WHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP60/WlvLo_WHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP60/WlvLo_WHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP60/WlvLo_WHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP60/ttblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP60/ttblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP60/ttblv_ttHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP60/ttbblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP60/ttbblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP60/ttbblv_ttHtoaato4b_mA_55_2018.root

# WP80
# Data
hadd -f ../plots_1L/root_l/WP80/WlvHi_Data_2018.root ../plots_1L/root_el/WP80/WlvHi_Data_2018.root ../plots_1L/root_mu/WP80/WlvHi_Data_2018.root
hadd -f ../plots_1L/root_l/WP80/WlvLo_Data_2018.root ../plots_1L/root_el/WP80/WlvLo_Data_2018.root ../plots_1L/root_mu/WP80/WlvLo_Data_2018.root
hadd -f ../plots_1L/root_l/WP80/ttblv_Data_2018.root ../plots_1L/root_el/WP80/ttblv_Data_2018.root ../plots_1L/root_mu/WP80/ttblv_Data_2018.root
hadd -f ../plots_1L/root_l/WP80/ttbblv_Data_2018.root ../plots_1L/root_el/WP80/ttbblv_Data_2018.root ../plots_1L/root_mu/WP80/ttbblv_Data_2018.root
# W jets
hadd -f ../plots_1L/root_l/WP80/WlvHi_Wlv_2018.root ../plots_1L/root_el/WP80/WlvHi_Wlv_2018.root ../plots_1L/root_mu/WP80/WlvHi_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP80/WlvLo_Wlv_2018.root ../plots_1L/root_el/WP80/WlvLo_Wlv_2018.root ../plots_1L/root_mu/WP80/WlvLo_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP80/ttblv_Wlv_2018.root ../plots_1L/root_el/WP80/ttblv_Wlv_2018.root ../plots_1L/root_mu/WP80/ttblv_Wlv_2018.root
hadd -f ../plots_1L/root_l/WP80/ttbblv_Wlv_2018.root ../plots_1L/root_el/WP80/ttbblv_Wlv_2018.root ../plots_1L/root_mu/WP80/ttbblv_Wlv_2018.root
# TT SemiLeptonic
hadd -f ../plots_1L/root_l/WP80/WlvHi_TT1l_2018.root ../plots_1L/root_el/WP80/WlvHi_TT1l_2018.root ../plots_1L/root_mu/WP80/WlvHi_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP80/WlvLo_TT1l_2018.root ../plots_1L/root_el/WP80/WlvLo_TT1l_2018.root ../plots_1L/root_mu/WP80/WlvLo_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP80/ttblv_TT1l_2018.root ../plots_1L/root_el/WP80/ttblv_TT1l_2018.root ../plots_1L/root_mu/WP80/ttblv_TT1l_2018.root
hadd -f ../plots_1L/root_l/WP80/ttbblv_TT1l_2018.root ../plots_1L/root_el/WP80/ttbblv_TT1l_2018.root ../plots_1L/root_mu/WP80/ttbblv_TT1l_2018.root
# WH M15
hadd -f ../plots_1L/root_l/WP80/WlvHi_WHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP80/WlvHi_WHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP80/WlvHi_WHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP80/WlvLo_WHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP80/WlvLo_WHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP80/WlvLo_WHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP80/ttblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP80/ttblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP80/ttblv_ttHtoaato4b_mA_15_2018.root
hadd -f ../plots_1L/root_l/WP80/ttbblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_el/WP80/ttbblv_ttHtoaato4b_mA_15_2018.root ../plots_1L/root_mu/WP80/ttbblv_ttHtoaato4b_mA_15_2018.root
# WH M30
hadd -f ../plots_1L/root_l/WP80/WlvHi_WHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP80/WlvHi_WHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP80/WlvHi_WHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP80/WlvLo_WHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP80/WlvLo_WHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP80/WlvLo_WHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP80/ttblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP80/ttblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP80/ttblv_ttHtoaato4b_mA_30_2018.root
hadd -f ../plots_1L/root_l/WP80/ttbblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_el/WP80/ttbblv_ttHtoaato4b_mA_30_2018.root ../plots_1L/root_mu/WP80/ttbblv_ttHtoaato4b_mA_30_2018.root
# WH M55
hadd -f ../plots_1L/root_l/WP80/WlvHi_WHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP80/WlvHi_WHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP80/WlvHi_WHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP80/WlvLo_WHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP80/WlvLo_WHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP80/WlvLo_WHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP80/ttblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP80/ttblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP80/ttblv_ttHtoaato4b_mA_55_2018.root
hadd -f ../plots_1L/root_l/WP80/ttbblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_el/WP80/ttbblv_ttHtoaato4b_mA_55_2018.root ../plots_1L/root_mu/WP80/ttbblv_ttHtoaato4b_mA_55_2018.root


# copy combined root files to public directory
cp ../plots_1L/root_l/WP40/* ${DIRECTORY}/WP40/
cp ../plots_1L/root_l/WP60/* ${DIRECTORY}/WP60/
cp ../plots_1L/root_l/WP80/* ${DIRECTORY}/WP80/

echo ">>>>>>> DONE! All root files copied to ${DIRECTORY}, all pdf files copied to ${PDF_DIR}."















