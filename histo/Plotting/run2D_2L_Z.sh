#!/bin/bash

#DIR="2D_2LZ_070924"
DIR="2D_2LZ_SS020125"
DIRECTORY="/afs/cern.ch/user/h/hboucham/public/${DIR}"
PDF_DIR="/eos/user/h/hboucham/www_hichem/Haa4b_plots/"
#PDF_DIR="testpdf/"

# Making directory if it doesn't exist
if [ ! -d "$DIRECTORY" ]; then
  echo "$DIRECTORY does not exist, so I shall create it"
  mkdir "$DIRECTORY"
  mkdir "${DIRECTORY}/WP40"
  mkdir "${DIRECTORY}/WP60"
  mkdir "${DIRECTORY}/WP80"
fi

# Remove old 2D plots root files
echo ">>>>>>> Deleting old pdf and root files."
# Muon
rm -f ../plots_2L_Z/root_mu/WP40/*
rm -f ../plots_2L_Z/root_mu/WP60/*
rm -f ../plots_2L_Z/root_mu/WP80/*
# Electron
rm -f ../plots_2L_Z/root_el/WP40/*
rm -f ../plots_2L_Z/root_el/WP60/*
rm -f ../plots_2L_Z/root_el/WP80/*
# Merged lepton-lepton
rm -f ../plots_2L_Z/root_ll/WP40/*
rm -f ../plots_2L_Z/root_ll/WP60/*
rm -f ../plots_2L_Z/root_ll/WP80/*

# Remove old pdf files
rm -f ../plots_2L_Z/2D_*

# Make new pdf and root files for individual channels (mu/el)
echo ">>>>>>> Making root files and pdfs of individual sub-categories (mu/el) * (WP40/WP60/WP80)."
# WP40
python3 2D_plot_2L_Z.py mu WP40 x
python3 2D_plot_2L_Z.py el WP40 x
# WP60
python3 2D_plot_2L_Z.py mu WP60 x
python3 2D_plot_2L_Z.py el WP60 x
# WP80
python3 2D_plot_2L_Z.py mu WP80 x
python3 2D_plot_2L_Z.py el WP80 x

# copy pdfs to website directory
cp ../plots_2L_Z/2D_* ${PDF_DIR}

# copy root files to public directory
# Muon
cp ../plots_2L_Z/root_mu/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_Z/root_mu/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_Z/root_mu/WP80/* ${DIRECTORY}/WP80/
# Electron
cp ../plots_2L_Z/root_el/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_Z/root_el/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_Z/root_el/WP80/* ${DIRECTORY}/WP80/

# Make root files for combined channels, need to rerun 2D plots with slightly different naming convention
echo ">>>>>>> Making root files and pdfs of merged (ll) sub-categories (mu/el) * (WP40/WP60/WP80)."
# WP40
python3 2D_plot_2L_Z.py mu WP40 comb
python3 2D_plot_2L_Z.py el WP40 comb
# WP60
python3 2D_plot_2L_Z.py mu WP60 comb
python3 2D_plot_2L_Z.py el WP60 comb
# WP80
python3 2D_plot_2L_Z.py mu WP80 comb
python3 2D_plot_2L_Z.py el WP80 comb

# Merge mu/el
echo ">>>>>>> Merging sub-categories root files."
# WP40 
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_Data_2018.root ../plots_2L_Z/root_el/WP40/Zll_Data_2018.root ../plots_2L_Z/root_mu/WP40/Zll_Data_2018.root
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_TT2l_2018.root ../plots_2L_Z/root_el/WP40/Zll_TT2l_2018.root ../plots_2L_Z/root_mu/WP40/Zll_TT2l_2018.root
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_ZZ_2018.root ../plots_2L_Z/root_el/WP40/Zll_ZZ_2018.root ../plots_2L_Z/root_mu/WP40/Zll_ZZ_2018.root
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_Zll_2018.root ../plots_2L_Z/root_el/WP40/Zll_Zll_2018.root ../plots_2L_Z/root_mu/WP40/Zll_Zll_2018.root
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_ZHtoaato4b_mA_15_2018.root ../plots_2L_Z/root_el/WP40/Zll_ZHtoaato4b_mA_15_2018.root ../plots_2L_Z/root_mu/WP40/Zll_ZHtoaato4b_mA_15_2018.root
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_ZHtoaato4b_mA_30_2018.root ../plots_2L_Z/root_el/WP40/Zll_ZHtoaato4b_mA_30_2018.root ../plots_2L_Z/root_mu/WP40/Zll_ZHtoaato4b_mA_30_2018.root
hadd -f ../plots_2L_Z/root_ll/WP40/Zll_ZHtoaato4b_mA_55_2018.root ../plots_2L_Z/root_el/WP40/Zll_ZHtoaato4b_mA_55_2018.root ../plots_2L_Z/root_mu/WP40/Zll_ZHtoaato4b_mA_55_2018.root
# WP60 
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_Data_2018.root ../plots_2L_Z/root_el/WP60/Zll_Data_2018.root ../plots_2L_Z/root_mu/WP60/Zll_Data_2018.root
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_TT2l_2018.root ../plots_2L_Z/root_el/WP60/Zll_TT2l_2018.root ../plots_2L_Z/root_mu/WP60/Zll_TT2l_2018.root
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_ZZ_2018.root ../plots_2L_Z/root_el/WP60/Zll_ZZ_2018.root ../plots_2L_Z/root_mu/WP60/Zll_ZZ_2018.root
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_Zll_2018.root ../plots_2L_Z/root_el/WP60/Zll_Zll_2018.root ../plots_2L_Z/root_mu/WP60/Zll_Zll_2018.root
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_ZHtoaato4b_mA_15_2018.root ../plots_2L_Z/root_el/WP60/Zll_ZHtoaato4b_mA_15_2018.root ../plots_2L_Z/root_mu/WP60/Zll_ZHtoaato4b_mA_15_2018.root
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_ZHtoaato4b_mA_30_2018.root ../plots_2L_Z/root_el/WP60/Zll_ZHtoaato4b_mA_30_2018.root ../plots_2L_Z/root_mu/WP60/Zll_ZHtoaato4b_mA_30_2018.root
hadd -f ../plots_2L_Z/root_ll/WP60/Zll_ZHtoaato4b_mA_55_2018.root ../plots_2L_Z/root_el/WP60/Zll_ZHtoaato4b_mA_55_2018.root ../plots_2L_Z/root_mu/WP60/Zll_ZHtoaato4b_mA_55_2018.root
# WP80 
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_Data_2018.root ../plots_2L_Z/root_el/WP80/Zll_Data_2018.root ../plots_2L_Z/root_mu/WP80/Zll_Data_2018.root
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_TT2l_2018.root ../plots_2L_Z/root_el/WP80/Zll_TT2l_2018.root ../plots_2L_Z/root_mu/WP80/Zll_TT2l_2018.root
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_ZZ_2018.root ../plots_2L_Z/root_el/WP80/Zll_ZZ_2018.root ../plots_2L_Z/root_mu/WP80/Zll_ZZ_2018.root
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_Zll_2018.root ../plots_2L_Z/root_el/WP80/Zll_Zll_2018.root ../plots_2L_Z/root_mu/WP80/Zll_Zll_2018.root
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_ZHtoaato4b_mA_15_2018.root ../plots_2L_Z/root_el/WP80/Zll_ZHtoaato4b_mA_15_2018.root ../plots_2L_Z/root_mu/WP80/Zll_ZHtoaato4b_mA_15_2018.root
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_ZHtoaato4b_mA_30_2018.root ../plots_2L_Z/root_el/WP80/Zll_ZHtoaato4b_mA_30_2018.root ../plots_2L_Z/root_mu/WP80/Zll_ZHtoaato4b_mA_30_2018.root
hadd -f ../plots_2L_Z/root_ll/WP80/Zll_ZHtoaato4b_mA_55_2018.root ../plots_2L_Z/root_el/WP80/Zll_ZHtoaato4b_mA_55_2018.root ../plots_2L_Z/root_mu/WP80/Zll_ZHtoaato4b_mA_55_2018.root

# copy combined root files to public directory
# Lepton-Lepton
cp ../plots_2L_Z/root_ll/WP40/* ${DIRECTORY}/WP40/
cp ../plots_2L_Z/root_ll/WP60/* ${DIRECTORY}/WP60/
cp ../plots_2L_Z/root_ll/WP80/* ${DIRECTORY}/WP80/

echo ">>>>>>> DONE! All root files copied to ${DIRECTORY}, all pdf files copied to ${PDF_DIR}."















