#!/bin/bash

# Parameters
CHANNELS=("mu" "el")
SUBCATEGORIES=("r1" "r2" "r3")
DATE_NO_PNET="012025"
DATE_PNET="012025c"
DATE_PNET_SCALE="012025cs"

# No PNET cut
#for CHANNEL in "${CHANNELS[@]}"; do
#  for SUBCATEGORY in "${SUBCATEGORIES[@]}"; do
#    python3 Data_MC_plot_1L.py --channel="$CHANNEL" --subcategory="$SUBCATEGORY" --yaxis="lin" --MCscale="noscale" --score="0" --date="$DATE_NO_PNET" --addcut=""
#  done
#done

# PNET cut > 0.5
for CHANNEL in "${CHANNELS[@]}"; do
  for SUBCATEGORY in "${SUBCATEGORIES[@]}"; do
    python3 Data_MC_plot_1L.py --channel="$CHANNEL" --subcategory="$SUBCATEGORY" --yaxis="lin" --MCscale="noscale" --score="0.5" --date="$DATE_PNET" --addcut=""
  done
done


# PNET cut > 0.5 and scale MC to data
#for CHANNEL in "${CHANNELS[@]}"; do
#  for SUBCATEGORY in "${SUBCATEGORIES[@]}"; do
#    python3 Data_MC_plot_1L.py --channel="$CHANNEL" --subcategory="$SUBCATEGORY" --yaxis="lin" --MCscale="scale" --score="0.5" --date="$DATE_PNET_SCALE" --addcut=""
#  done
#done

# No PNET cut
#python3 Data_MC_plot_1L.py --channel="mu" --subcategory="r1" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_1L.py --channel="el" --subcategory="r1" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_1L.py --channel="mu" --subcategory="r2" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_1L.py --channel="el" --subcategory="r2" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_1L.py --channel="mu" --subcategory="r3" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""
#python3 Data_MC_plot_1L.py --channel="el" --subcategory="r3" --yaxis="lin" --MCscale="noscale" --score="0" --date="112424" --addcut=""

# PNET cut > 0.8
#python3 Data_MC_plot_1L.py --channel="mu" --subcategory="r1" --yaxis="lin" --MCscale="scale" --score="0.8" --date="112424cs" --addcut=""
#python3 Data_MC_plot_1L.py --channel="el" --subcategory="r1" --yaxis="lin" --MCscale="scale" --score="0.8" --date="112424cs" --addcut=""
#python3 Data_MC_plot_1L.py --channel="mu" --subcategory="r2" --yaxis="lin" --MCscale="scale" --score="0.8" --date="112424cs" --addcut=""
#python3 Data_MC_plot_1L.py --channel="el" --subcategory="r2" --yaxis="lin" --MCscale="scale" --score="0.8" --date="112424cs" --addcut=""
#python3 Data_MC_plot_1L.py --channel="mu" --subcategory="r3" --yaxis="lin" --MCscale="scale" --score="0.8" --date="112424cs" --addcut=""
#python3 Data_MC_plot_1L.py --channel="el" --subcategory="r3" --yaxis="lin" --MCscale="scale" --score="0.8" --date="112424cs" --addcut=""

