#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH2D, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


def Histo_style(h, x_title, y_title):

    h_y = h.GetYaxis()
    h_x = h.GetXaxis()

    # x-axis 
    h_x.SetTitle(x_title)
    h_x.SetTitleSize(30) 
    h_x.SetTitleFont(43) 
    h_x.SetTitleOffset(1.1)
    h_x.SetLabelFont(43) # font and size of numbers
    h_x.SetLabelSize(25)

    # y-axis
    h_y.SetTitle(y_title) # space used for offset
    h_y.SetTitleSize(30) 
    h_y.SetTitleFont(43) 
    h_y.SetTitleOffset(1.25)
    h_y.SetLabelFont(43) # font and size of numbers
    h_y.SetLabelSize(25)

# Definition of v2 WP
PFWP40 = ["(score_Haa4b_vs_QCD > 0.960)", "((score_Haa4b_vs_QCD < 0.960) && (score_Haa4b_vs_QCD > 0.84))"]
PFWP60 = ["(score_Haa4b_vs_QCD > 0.930)", "((score_Haa4b_vs_QCD < 0.930) && (score_Haa4b_vs_QCD > 0.660))"]
PFWP80 = ["(score_Haa4b_vs_QCD > 0.840)", "((score_Haa4b_vs_QCD < 0.840) && (score_Haa4b_vs_QCD > 0.40))"]
# Definition of v2a WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v2a > 0.968)", "((score_Haa4b_vs_QCD_v2a < 0.968) && (score_Haa4b_vs_QCD_v2a > 0.84))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v2a > 0.944)", "((score_Haa4b_vs_QCD_v2a < 0.944) && (score_Haa4b_vs_QCD_v2a > 0.660))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v2a > 0.868)", "((score_Haa4b_vs_QCD_v2a < 0.868) && (score_Haa4b_vs_QCD_v2a > 0.40))"]
# Definition of v2b WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v2b > 0.952)", "((score_Haa4b_vs_QCD_v2b < 0.952) && (score_Haa4b_vs_QCD_v2b > 0.84))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v2b > 0.916)", "((score_Haa4b_vs_QCD_v2b < 0.916) && (score_Haa4b_vs_QCD_v2b > 0.660))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v2b > 0.814)", "((score_Haa4b_vs_QCD_v2b < 0.814) && (score_Haa4b_vs_QCD_v2b > 0.40))"]
# Definition of v1 WP
#PFWP40 = ["(score_Haa4b_vs_QCD_v1 > 0.992)", "((score_Haa4b_vs_QCD_v1 < 0.992) && (score_Haa4b_vs_QCD_v1 > 0.92))"]
#PFWP60 = ["(score_Haa4b_vs_QCD_v1 > 0.975)", "((score_Haa4b_vs_QCD_v1 < 0.975) && (score_Haa4b_vs_QCD_v1 > 0.80))"]
#PFWP80 = ["(score_Haa4b_vs_QCD_v1 > 0.920)", "((score_Haa4b_vs_QCD_v1 < 0.920) && (score_Haa4b_vs_QCD_v1 > 0.50))"]

PF_object_titles = ["Pass", "Fail"]

# command inputs
channel = sys.argv[1]
WP = sys.argv[2]
comb = sys.argv[3]

if (WP == "WP40"):
    PFWP = PFWP40
    PFWP_convention = "WP40"
elif (WP == "WP60"):
    PFWP = PFWP60
    PFWP_convention = "WP60"
elif (WP == "WP80"):
    PFWP = PFWP80
    PFWP_convention = "WP80"
else:
    exit

###################################################################################################
##############  Manually adjust CHANNEL  ##########################################################
#channel = "mu"
#channel = "el"
############### Manually adjust Pass/fail Working Point  ##########################################
#PFWP = PFWP40
#PFWP = PFWP60
#PFWP = PFWP80
###################################################################################################
###################################################################################################


# input/output directory and tree name
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_" + channel + "_nom/"
output_dir = "../plots_2L_Z"
tree_name = "event_tree"
#weight = "weight_tot" #"weight_zPt_nominal"
#weight = "weight_HEM_nominal*weight_GEN_nominal"
#HSS4b weight
weight = "weight_HEM_nominal*weight_GEN_nominal*weight_zPt_nominal*weight_pu_nominal*weight_ISR_nominal*weight_FSR_nominal*weight_PDF_nominal*weight_LeptonIDISO_nominal*weight_trigger_nominal"


# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1

# Scaling for MC histos
## DYJets_M-50 background (HT bins)
DY_50_path = "DY_50_HT"
DYJets_files = ["70-100", "100-200","200-400", "400-600", "600-800", "800-1200","1200-2500", "2500-inf"]
DYJets_xsec = [158.7445, 159.1984, 43.5384, 5.9141, 1.4377, 0.6443, 0.1511, 0.00339]
DYJets_ev = [17004433, 26202328, 18455718, 8908406, 7035971, 6678036, 6166852, 1978203]
DYJets_scale = []
for i in range(len(DYJets_xsec)):
    DYJets_scale.append(lumi*DYJets_xsec[i]/DYJets_ev[i])

# Other backgrounds (only using 2 first ones): [TT_2L2Nu,ZZ, tZq_ll_4f_ckm_NLO, ZZTo2Q2L_mllmin4p0, ZH_HToBB_ZToLL_M-125, ttZJets]
Background_files = ["TT_2L2Nu", "ZZTo2Q2L", "DY_10-50", "ZZ", "tZq_ll_4f_ckm_NLO", "ZH_HToBB_ZToLL_M-125", "ttZJets"]
Background_xsec = [87.339, 3.93, 18610, 10.32, 0.0942, 0.0519, 0.839]
Background_ev = [143887383, 15840251, 74373706, 3526000, 11916000, 4885835, 32793815]
Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])

# signal: [M-15, M-30, M-55]
#Signal_xsec = [0.11352, 0.11352, 0.11352]
#Signal_ev = [106743, 101445, 95365]
# HSS4b signal: [M-15_0, M-15_0p01, M-15_0p1]
Signal_xsec = [0.07924, 0.07924, 0.07924]
Signal_ev = [50000, 50000, 49126]
Signal_scale =[]
for i in range(len(Signal_xsec)):
    Signal_scale.append(lumi*Signal_xsec[i]/Signal_ev[i])
DYJets_scale = []
for i in range(len(DYJets_xsec)):
    DYJets_scale.append(lumi*DYJets_xsec[i]/DYJets_ev[i])
#print(DYJets_scale)

# samples info
background_signal_sample_list = ["TT_2L2Nu", "ZZTo2Q2L", "SUSY_ZH_M-15", "SUSY_ZH_M-30", "SUSY_ZH_M-55"]
background_signal_scale = [Background_scale[0], Background_scale[1], Signal_scale[0], Signal_scale[1], Signal_scale[2]]
#samples_name = ["DY_50_HT", "data_2018", "TT_2L2Nu", "ZZTo2Q2L", "SUSY_ZH_M-15", "SUSY_ZH_M-30", "SUSY_ZH_M-55"]
# HSS4b sample naming
samples_name = ["DY_50_HT", "data_2018", "TT_2L2Nu", "ZZTo2Q2L", "SUSY_ZH_M-15_0", "SUSY_ZH_M-15_0p01", "SUSY_ZH_M-15_0p1"]
samples_convention = ["Zll", "Data", "TT2l", "ZZ", "ZHtoaato4b_mA_15", "ZHtoaato4b_mA_30", "ZHtoaato4b_mA_55"]

# Data 
data_mu = ["data_SingleMuonA", "data_SingleMuonB", "data_SingleMuonC", "data_SingleMuonD"]
data_el = ["data_EGammaA", "data_EGammaB", "data_EGammaC", "data_EGammaD"]

# Histo variables, labels, range and binsi
# x information
x_root_titles = ["mass", "msoft", "msoftPtScaled", "pnet", "massZPtScaled", "mSoftZPtScaled"]
xbranches = ["m_fatjet", "msoftdrop_fatjet","msoftscaled_fatjet","mH_avg", "mtopo_fatjet", "msofttopo_fatjet"]
xrange_min = 0
xrange_max = 300
xbins = 30

# y information
y_root_titles = "mA"
ybranches = "mA_34a"
#ybranches = "mA_34d" # mA_avg, mA_34b, mA_34d
yrange_min = 0
yrange_max = 70
ybins = 35

# Naming convention
if channel == "mu":
    data_files = data_mu
    decay_channel = "Zmm"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "Zll"
elif channel == "el":
    data_files = data_el
    decay_channel = "Zee"
    if (comb == "comb"): # Making root files to combine channel
        decay_channel = "Zll"
else:
    exit

#cut = ""
syst = "Nom"
year = "2018"
date = "_SS020125"

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box

for q in range(len(samples_name)):
    output_file = TFile(output_dir + "/root_" + channel + "/" + PFWP_convention + "/" + decay_channel + "_" + samples_convention[q] + "_" + year + ".root","recreate")
    for k in range(len(PFWP)):
        draw_option = f"({PFWP[k]})*({weight})"
        for i in range(len(xbranches)): 
            c = TCanvas("c", "canvas", 800, 800)
            c.SetTopMargin(0.11)
            c.SetRightMargin(0.13)
            c.SetLeftMargin(0.13)
            
            if samples_name[q] == "DY_50_HT":
                 
                # DY_50_HT70-100 
                p_SB0 = input_dir + DY_50_path + DYJets_files[0] + ".root"
                if (os.path.isfile(p_SB0)):
                    f_SB0 = TFile(p_SB0, "READ") 
                    events_SB0 = f_SB0.Get(tree_name)
                    SB0 = TH2D("SB0", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB0.Draw(ybranches + ":" + xbranches[i] + ">>SB0",draw_option)
                    SB0.Scale(DYJets_scale[0])
                else:
                    SB0 = TH2D("SB0", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # DY_50_HT100-200
                p_SB1 = input_dir + DY_50_path + DYJets_files[1] + ".root"
                if (os.path.isfile(p_SB1)):
                    f_SB1 = TFile(p_SB1, "READ") 
                    events_SB1 = f_SB1.Get(tree_name)
                    SB1 = TH2D("SB1", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB1.Draw(ybranches + ":" + xbranches[i] + ">>SB1",draw_option)
                    SB1.Scale(DYJets_scale[1])
                else:
                    SB1 = TH2D("SB1", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                
                # DY_50_HT200-400
                p_SB2 = input_dir + DY_50_path + DYJets_files[2] + ".root"
                if (os.path.isfile(p_SB2)):
                    f_SB2 = TFile(p_SB2, "READ") 
                    events_SB2 = f_SB2.Get(tree_name)
                    SB2 = TH2D("SB2", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB2.Draw(ybranches + ":" + xbranches[i] + ">>SB2",draw_option)
                    SB2.Scale(DYJets_scale[2])
                else:
                    SB2 = TH2D("SB2", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # DY_50_HT400-600
                p_SB3 = input_dir + DY_50_path + DYJets_files[3] + ".root"
                if (os.path.isfile(p_SB3)):
                    f_SB3 = TFile(p_SB3, "READ") 
                    events_SB3 = f_SB3.Get(tree_name)
                    SB3 = TH2D("SB3", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB3.Draw(ybranches + ":" + xbranches[i] + ">>SB3",draw_option)
                    SB3.Scale(DYJets_scale[3])
                else:
                    SB3 = TH2D("SB3", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # DY_50_HT600-800
                p_SB4 = input_dir + DY_50_path + DYJets_files[4] + ".root"
                if (os.path.isfile(p_SB4)):
                    f_SB4 = TFile(p_SB4, "READ") 
                    events_SB4 = f_SB4.Get(tree_name)
                    SB4 = TH2D("SB4", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB4.Draw(ybranches + ":" + xbranches[i] + ">>SB4",draw_option)
                    SB4.Scale(DYJets_scale[4])
                else:
                    SB4 = TH2D("SB4", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # DY_50_HT800-1200
                p_SB5 = input_dir + DY_50_path + DYJets_files[5] + ".root"
                if (os.path.isfile(p_SB5)):
                    f_SB5 = TFile(p_SB5, "READ") 
                    events_SB5 = f_SB5.Get(tree_name)
                    SB5 = TH2D("SB5", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB5.Draw(ybranches + ":" + xbranches[i] + ">>SB5",draw_option)
                    SB5.Scale(DYJets_scale[5])
                else:
                    SB5 = TH2D("SB5", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # DY_50_HT1200-2500
                p_SB6 = input_dir + DY_50_path + DYJets_files[6] + ".root"
                if (os.path.isfile(p_SB6)):
                    f_SB6 = TFile(p_SB6, "READ") 
                    events_SB6 = f_SB6.Get(tree_name)
                    SB6 = TH2D("SB6", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB6.Draw(ybranches + ":" + xbranches[i] + ">>SB6",draw_option)
                    SB6.Scale(DYJets_scale[6])
                else:
                    SB6 = TH2D("SB6", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)

                # DY_50_HT2500-Inf
                p_SB7 = input_dir + DY_50_path + DYJets_files[7] + ".root"
                if (os.path.isfile(p_SB7)):
                    f_SB7 = TFile(p_SB7, "READ") 
                    events_SB7 = f_SB7.Get(tree_name)
                    SB7 = TH2D("SB7", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                    events_SB7.Draw(ybranches + ":" + xbranches[i] + ">>SB7",draw_option)
                    SB7.Scale(DYJets_scale[7])
                else:
                    SB7 = TH2D("SB7", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                
                # Adding Histos together
                SB = TH2D("SB", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                SB.Add(SB4)
                #SB.Add(SB0) # I don't know why by even with conditional in case file does not exist, it give me an error, so I just exclude it
                SB.Add(SB1)
                SB.Add(SB2)
                SB.Add(SB3)
                SB.Add(SB5)
                SB.Add(SB6)
                SB.Add(SB7)
                
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(SB, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                SB.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.3,0.8, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.04)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.3,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.3,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(SB, decay_channel + "_" + samples_convention[q] + "_"  + year + "_" + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst )
                #c.SaveAs(output_dir + "/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" +PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it

            elif samples_name[q] == "data_2018":
                # do that
                # Data 2018A
                fD0 = TFile(input_dir+ data_files[0] + ".root", "READ")
                events_D0 = fD0.Get(tree_name)
                D0 = TH2D("D0", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D0.Draw(ybranches + ":" + xbranches[i] + ">>D0",PFWP[k])
                
                # Data 2018B
                fD1 = TFile(input_dir+ data_files[1] + ".root", "READ")
                events_D1 = fD1.Get(tree_name)
                D1 = TH2D("D1", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D1.Draw(ybranches + ":" + xbranches[i] + ">>D1",PFWP[k])

                # Data 2018C
                fD2 = TFile(input_dir+ data_files[2] + ".root", "READ")
                events_D2 = fD2.Get(tree_name)
                D2 = TH2D("D2", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D2.Draw(ybranches + ":" + xbranches[i] + ">>D2",PFWP[k])
                
                # Data 2018D
                fD3 = TFile(input_dir+ data_files[3] + ".root", "READ")
                events_D3 = fD3.Get(tree_name)
                D3 = TH2D("D3", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_D3.Draw(ybranches + ":" + xbranches[i] + ">>D3",PFWP[k])

                # Adding Histos together
                D = TH2D("D", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max) 
                D.Add(D0)
                D.Add(D1)
                D.Add(D2)
                D.Add(D3)

                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(D, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                D.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.65,0.85, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 
                
                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.3,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.3,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(D, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it
                

            else : # all other samples that don't require adding histos
                # Loading file and filling 2D plot with pass/fail cut
                f_B = TFile(input_dir + background_signal_sample_list[q-2] + ".root", "READ")
                events_B = f_B.Get(tree_name)
                B = TH2D("B", x_root_titles[i] + "_vs_" + y_root_titles + "_" + PF_object_titles[k], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
                events_B.Draw(ybranches + ":" + xbranches[i] + ">>B",draw_option)
                B.Scale(background_signal_scale[q-2]) 
               
                # Histo style and drawing
                gStyle.SetTitleFontSize(0.05) # Title size
                Histo_style(B, x_root_titles[i], y_root_titles)
                gPad.Modified() # Update gpad (things might break depending on where this line is) 
                B.Draw("colz") 
                ROOT.gPad.RedrawAxis() 

                # CMS Stuff
                t1 = TLatex(0.5,0.89,"#bf{CMS} #it{Work in Progress}                 59.83 fb^{-1} (13 TeV, 2018)")
                t1.SetNDC()
                t1.SetTextFont(42)
                t1.SetTextSize(0.032)
                t1.SetTextAlign(20)
                t1.Draw("SAME")

                # Region
                t3 = TLatex(0.2,0.8, samples_name[q] + ": " + PF_object_titles[k])
                t3.SetNDC()
                t3.SetTextFont(42)
                t3.SetTextSize(0.03)
                t3.Draw("SAME") 

                # Channel
                if (channel =="mu"):
                    chan = TLatex(0.2,0.85,"  #bf{Muon channel}")
                else:
                    chan = TLatex(0.2,0.85,"  #bf{Electron channel}")
                chan.SetNDC()
                chan.SetTextFont(42)
                chan.SetTextSize(0.04)
                chan.Draw("SAME")

                # Save png and pdf of your canvas 
                output_file.WriteObject(B, decay_channel + "_" + samples_convention[q] + "_"  + year + "_"  + x_root_titles[i] + "_" + PFWP_convention + "_" + PF_object_titles[k] + "_" + syst)
                #c.SaveAs(output_dir + "/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + date + ".pdf")
                #c.SaveAs(output_dir + "/png/" + samples_name[q] + "_" + xbranches[i] + "_vs_" + ybranches + "_" + PF_object_titles[k] + ".png")
                if i == 0 and q == 0 and k == 0:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf(", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                elif q == len(samples_name)-1 and i == len(xbranches)-1 and k  == len(PF_object_titles)-1:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf)", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                else:
                    c.Print(output_dir + "/2D_" + decay_channel +"_" + PFWP_convention + date + ".pdf", "Title: " +  samples_convention[q] + "_" + x_root_titles[i] + "_" + PF_object_titles[k])
                c.Close() # Delete Canvas after saving it

# Loops back to next sample
