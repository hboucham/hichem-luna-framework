#!/usr/bin/env python

import sys
import numpy 
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array
from array import array

import ROOT
from ROOT import TChain, TSelector, TTree, TLorentzVector
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


#Returns abs(Dphi)
def DeltaPhi_abs(phi1, phi2):
    p = math.pi
    r = abs(phi1 - phi2)
    if r <= p:
        return r 
    else:
        return 2*p - r 



input_file = sys.argv[1]
reweighting = sys.argv[2]
suffix = ".root"

# Loading WH weights (2D) histogram
if (reweighting == "weight"):
    weight_file_path = "plots/WH_2D_weight.root"
    file_weights = TFile.Open(weight_file_path)
    hist_name = "WH_weights_histo"
    hist_pointer = file_weights.Get(hist_name)
    suffix = "_new.root"

# Opening many root files
output_dir = "/eos/user/h/hboucham/Haa4b/samples/Ntuples/Topology_WH/" 
output_file =  output_dir + input_file[len("file_list/"):-4] + suffix
root_files = open(input_file, "r")
chain = TChain("Events")
#line = "root://cmsxrootd.fnal.gov///store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v1_2023_10_06/SUSY_WH_ZToAll_HToAATo4B_Pt150_M-15_TuneCP5_13TeV_madgraph_pythia8/r1/PNet_v1.root"


# Adding files to TChain
for line in root_files:
    print("Adding file: " + line)
    chain.Add(line[:-1])

tree = chain
N = tree.GetEntries()
print("Number of events:",N)

time = 0

# create the ntuple
file_new = TFile.Open(output_file, "RECREATE")
#file_new.cd()
tree_new = TTree("event_tree","event_tree")
W_pt = array('f', [0])
tree_new.Branch('W_pt', W_pt, 'W_pt/F')
H_pt = array('f', [0])
tree_new.Branch('H_pt', H_pt, 'H_pt/F')
WH_dphi = array('f', [0])
tree_new.Branch('WH_dphi', WH_dphi, 'WH_dphi/F')
WH_dptrel = array('f', [0])
tree_new.Branch('WH_dptrel', WH_dptrel, 'WH_dptrel/F')
WH_pt = array('f', [0])
tree_new.Branch('WH_pt', WH_pt, 'WH_pt/F')
genWeight = array('f', [0])
tree_new.Branch('genWeight', genWeight, 'genWeight/F')

if (reweighting == "weight"):
    WHWeight = array('f', [0])
    tree_new.Branch('WHWeight', WHWeight, 'WHWeight/F')



for event in tree:
    #if time == 1000: 
    #    break
    h = -1
    w = -1
    for i in range(event.nGenPart):
        if event.GenPart_pdgId[i]  == 25 and event.GenPart_status[i] == 62: 
            h = i
        if abs(event.GenPart_pdgId[i])  == 24: 
            w = i
        if h != -1 and w != -1:
            if (event.GenPart_pt[h] > 150):
                #print("starting vector stuf")
                vH = ROOT.Math.PtEtaPhiMVector()
                vH.SetCoordinates(event.GenPart_pt[h], event.GenPart_eta[h], event.GenPart_phi[h], 125.0)
                vW = ROOT.Math.PtEtaPhiMVector()
                vW.SetCoordinates(event.GenPart_pt[w], event.GenPart_eta[w], event.GenPart_phi[w], 80.5)
                H_pt[0] = event.GenPart_pt[h]
                W_pt[0] = event.GenPart_pt[w]
                WH_dphi[0] = DeltaPhi_abs(vW.Phi(), vH.Phi())
                WH_dptrel[0] = 2*(event.GenPart_pt[h]-event.GenPart_pt[w])/(event.GenPart_pt[h]+event.GenPart_pt[w])
                WH_pt[0] = (vH+vW).Pt()
                genWeight[0] = event.genWeight
                if (reweighting == "weight"):
                    WH_X = math.log2(2*H_pt[0]/(H_pt[0] + W_pt[0]))
                    WH_Y = math.log2(H_pt[0])
                    WH_weight_bin = hist_pointer.FindFixBin(WH_X, WH_Y)
                    if (hist_pointer.GetBinContent(WH_weight_bin) == 0):
                         WHWeight[0] = 1.0
                    else:
                         WHWeight[0] = hist_pointer.GetBinContent(WH_weight_bin)
                tree_new.Fill()
                break

    # Since it may take a while to run, a statement like this is useful to evaluate run time
    time= time+1
    if (time%10000 ==0):
        print(">>>>> Progress: " + str (round(100*time/N, 3)) + " %")


# write the tuple to the output file and close it

tree_new.Write()
file_new.Close()
if (reweighting == "weight"):
    file_weights.Close()








