#!/usr/bin/env python

import sys
import numpy 
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array
from array import array

import ROOT
from ROOT import TChain, TSelector, TTree, TLorentzVector
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


#Returns abs(Dphi)
def DeltaPhi_abs(phi1, phi2):
    p = math.pi
    r = abs(phi1 - phi2)
    if r <= p:
        return r 
    else:
        return 2*p - r 



# Opening many root files
#input_file = "TnP_list_ntuples.txt"
input_file = "TnP_list_ntuplesT.txt"
root_files = open(input_file, "r")
chain = TChain("tpTree/fitter_tree")
#line = "root://cmsxrootd.fnal.gov///store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v1_2023_10_06/SUSY_WH_ZToAll_HToAATo4B_Pt150_M-15_TuneCP5_13TeV_madgraph_pythia8/r1/PNet_v1.root"

# Adding files to TChain
for line in root_files:
    print("Adding file: " + line)
    chain.Add(line[:-1])

tree = chain
N = tree.GetEntries()
print("Number of events:",N)

cut = "tag_pt > 26"
   
# Linear plots parameters
branches = ["miniIsoCharged", "activity_miniIsoCharged", ]
range_min = [0, 0]
range_max = [3, 200]
bins = [20, 20]
titles = ["Probe Muon Mini PF Isolation", "Tag Muon Mini PF Isolation"]
x_axis = ["MiniIso(probe)", "MiniIso(tag)"]

#title = ["AK8 Jet Candidate Mass Corrections", "AK8 Jet Candidate Softdrop Mass Corrections"]
ROOT.gROOT.SetBatch(True) # disable canvas display
ROOT.gStyle.SetOptStat(0) # disable stat box


for i in range(len(branches)):
    c = TCanvas("c", "canvas", 800, 700)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.01, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.12)  # joins upper and lower plot
    pad1.SetGrid()
    #pad1.SetLogx() 
    pad1.Draw()
    pad1.cd()

    S0 = TH1D("S0", titles[i], bins[i], range_min[i], range_max[i])
    tree.Draw(branches[i]+">>S0", cut)
    #S0.Scale(1./S0.Integral())  # can be used to normalize area under histo to 1
    #S1.Scale(Signal_scale[0]*100)
    S0.SetLineColor(kBlack)
    S0.SetLineWidth(2)
    S0.SetLineStyle(1)
    
    # Stylistic adjustments of regular plot through SD histo
    SD_y = S0.GetYaxis()
    SD_x = S0.GetXaxis()

    # Adjusting y axis range as needed for each variable (especially log)
    # Manually adjust both max and min
    #SD_y.SetRangeUser(1, 100000)
    
    # Set Minumum for Iso plots specifically
    #if (i > 34 and i < 47): # Isolation plots
    #    SD.SetMinimum(10)
    
    # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
    # This is mainly done to make space for the legend and other text
    S0.SetMaximum(S0.GetMaximum() + 0.5* (S0.GetMaximum()-S0.GetMinimum())) 
    gStyle.SetTitleFontSize(0.05) # Title size

    # x-axis (not necessary since hidden by ratio plot) 
    SD_x.SetTitle(x_axis[i])
    SD_x.SetTitleSize(30) 
    SD_x.SetTitleFont(43) 
    SD_x.SetTitleOffset(1.25)
    SD_x.SetLabelFont(43) # font and size of numbers
    SD_x.SetLabelSize(25)
    #SD_x.SetLabelOffset(0.3)

    # y-axis
    SD_y.SetTitle("Normalized Events") # space used for offset
    SD_y.SetNdivisions(505) #make divisions nice
    SD_y.SetTitleFont(43) 
    SD_y.SetTitleSize(25)
    SD_y.SetTitleOffset(1.35)
    SD_y.SetLabelFont(43) # font and size of numbers
    SD_y.SetLabelSize(20)

    gPad.Modified() # Update gpad (things might break depending on where this line is)
        
    # Drawing everything
    S0.Draw("HISTO")
    
    # CMS stuff
    t1 = TLatex(0.5,0.885," #bf{CMS} #it{Work In Progress}                                                     13 TeV ")
    t1.SetNDC()
    t1.SetTextFont(42)
    t1.SetTextSize(0.04)
    t1.SetTextAlign(20)
    t1.Draw("SAME")

    # Redraw axis + border since they are hidden by histo fills
    ROOT.gPad.RedrawAxis() 
    #borderline = TLine(range_max[i], 1.0,range_max[i], S0.GetMaximum())
    #borderline.SetLineColor(kBlack)
    #borderline.SetLineWidth(1)
    #borderline.Draw("SAME")

    # Save individual pdf/png 
    #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
    #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")
    # Save one pdf with all canvas
    if i == 0:
        c.Print("TnP_plots.pdf(", "Title: " + branches[i])
    elif i == (len(branches)-1):
        c.Print("TnP_plots.pdf)", "Title: " + branches[i])
    else:
        c.Print("TnP_plots.pdf", "Title: " + branches[i])
    # Delete all histos to avoid warning when initializing histos with same name
    S0.SetDirectory(0)
    #S1.SetDirectory(0)
    c.Close() # Delete Canvas after saving it

