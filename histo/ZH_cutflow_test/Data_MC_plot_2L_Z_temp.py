#!/usr/bin/env python

import sys
import math
import os
import os.path

import numpy as np
#import matplotlib.pyplot as plt
#import mplhep as hep
import array
import argparse

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex 
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite

from helpers.ratioStatError import getRatioStatError

# Functions
def createCanvasPads(logs): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.12)  
    pad1.SetBottomMargin(0.015)  # joins upper and lower plot
    pad1.SetGrid()
    if (logs):
        pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def createRatio(h1, h2, x_axis): # Function to create ratio plot
     h3 = h1.Clone("h3")
     h3.SetLineColor(kBlack)
     h3.SetMarkerStyle(20)
     h3.SetTitle("")
     h3.SetMinimum(.1)
     h3.SetMaximum(2.1)
     # Set up plot for markers and errors
     #h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h2)
 
     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("Data/MC    ")
     y.SetNdivisions(505)
     y.SetTitleSize(25)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.25)
     y.SetLabelFont(43)
     y.SetLabelSize(20)
 
     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitle(x_axis)
     x.SetTitleSize(25)
     x.SetTitleFont(43)
     x.SetTitleOffset(1)
     x.SetLabelFont(43)
     x.SetLabelSize(20)
 
     return h3

# Formatting for minor text
def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")

def MinorTextFormat_ratio(t):
    t.SetNDC()
    t.SetTextFont(43)
    t.SetTextSize(15)
    t.Draw("SAME")

# CMS Official Colors
CMS_colors = {
    "blue1": ROOT.TColor.GetColor("#3f90da"),
    "orange1": ROOT.TColor.GetColor("#ffa90e"),
    "red": ROOT.TColor.GetColor("#bd1f01"),
    "grey1": ROOT.TColor.GetColor("#94a4a2"),
    "purple": ROOT.TColor.GetColor("#832db6"),
    "brown": ROOT.TColor.GetColor("#a96b59"),
    "orange2": ROOT.TColor.GetColor("#e76300"),
    "grey2": ROOT.TColor.GetColor("#b9ac70"),
    "grey3": ROOT.TColor.GetColor("#717581"),
    "blue2": ROOT.TColor.GetColor("#92dadd"),
}



# init cuts
h_blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
z_peak = "(m_ll > 88) && (m_ll < 94)" 
z_tail = "((m_ll < 88 && m_ll > 80) || (m_ll < 100 && m_ll > 94))" 


# Create an argument parser
parser = argparse.ArgumentParser(description="Plotting argument parser.")

# Define the arguments that the script will accept
#  python3 Data_MC_plot_2L_Z.py --channel="mu/el" --yaxis="lin/log" --MCscale="scale/noscale" --score="0/0.5" --date="092824/test"
#  --addcut="dphi_fatjet_ll > 2.9"
parser.add_argument('--channels', type=str, help='Specify Muon (mu) or Electron (el) channel')
parser.add_argument('--yaxis', type=str, help='Specify y axis scale linear (lin) or logarithmic (log)')
parser.add_argument('--MCscale', type=str, help='Specify if MC should be scaled to Data (scale) or not (noscale)')
parser.add_argument('--score', type=str, help='Specify PNET score cut (between 0 and 1)')
parser.add_argument('--datetext', type=str, help='Specify date or text to be added at the end of the pdf file name (e.g: 092824 or test)')
parser.add_argument('--addcut', type=str, help='Specify additional cuts (e.g: dphi_fatjet_ll > 2.9 && pt_ll > 100)')

# Parse the arguments from the command line
args = parser.parse_args()
channel = args.channels
sc = args.yaxis
dataScale = args.MCscale
PNET_score = args.score
date = args.datetext
cut_add = args.addcut


# input/output directory and tree name
input_dir = "/eos/user/h/hboucham/Haa4b/postprocess/samples_2L_Z_" + channel + "_nom/"
output_dir = "../plots_2L_Z"
tree_name = "event_tree"
weight = "weight_tot"  


# Setting up arguments
# y axis scale
if (sc == "log"):
    scale_log = True
else:
    scale_log = False

# PNET score cut and additional cuts
if PNET_score == "0":
    cut = ""
    if cut_add:
        cut = cut_add
    else:
        cut = ""
else:
    cut = "score_Haa4b_vs_QCD > " + PNET_score
    if cut_add:
        cut = cut + " && " + cut_add

# Draw options
if cut:
    draw_option = f"({cut})*({weight})"
else:
    draw_option = weight

#print(cut)
#print(draw_option)
# Signal MC scaling
if (PNET_score == "0"):
    sig_scaling = 100
else :
    sig_scaling = 5
    
# 2018UL lumi from https://github.com/siddhesh86/htoaa/blob/ana_SS/htoaa_Settings.py#L52
lumi = 59830 # pb-1 = 58 fb-1

# Scaling for MC histos
## DYJets_M-50 background (HT bins)
DY_50_path = "DY_50_HT"
DYJets_files = ["70-100", "100-200","200-400", "400-600", "600-800", "800-1200","1200-2500", "2500-inf"]
DYJets_xsec = [158.7445, 159.1984, 43.5384, 5.9141, 1.4377, 0.6443, 0.1511, 0.00339]
DYJets_ev = [17004433, 26202328, 18455718, 8682257, 7035971, 6554679, 5966661, 1978203]
DYJets_scale = []
for i in range(len(DYJets_xsec)):
    DYJets_scale.append(lumi*DYJets_xsec[i]/DYJets_ev[i])


# Other backgrounds (only using 2 first ones): [TT_2L2Nu,ZZ, tZq_ll_4f_ckm_NLO, ZZTo2Q2L_mllmin4p0, ZH_HToBB_ZToLL_M-125, ttZJets]
Background_files = ["TT_2L2Nu", "ZZTo2Q2L", "DY_10-50", "ZZ", "tZq_ll_4f_ckm_NLO", "ZH_HToBB_ZToLL_M-125", "ttZJets"]
Background_xsec = [87.339, 3.93, 18610, 10.32, 0.0942, 0.0519, 0.839]
Background_ev = [145020000, 19020772, 74373706, 3526000, 11916000, 4885835, 32793815]
Background_scale = []
for i in range(len(Background_xsec)):
    Background_scale.append(lumi*Background_xsec[i]/Background_ev[i])

# signal: [M-15, M-30, M-55]
Signal_xsec = [0.11352, 0.11352, 0.11352]
Signal_ev = [106743, 101445, 95365]
Signal_scale =[]
for i in range(len(Signal_xsec)):
    Signal_scale.append(lumi*Signal_xsec[i]/Signal_ev[i])

# Data 
data_mu = ["data_SingleMuonA", "data_SingleMuonB", "data_SingleMuonC", "data_SingleMuonD"]
data_el = ["data_EGammaA", "data_EGammaB", "data_EGammaC", "data_EGammaD"]

# Histo variables, labels, range and bins
# Kinematic plots ordering: AK8 jet (including mA), lepton pair, leptons, MET, AK4

branches = [ "m_fatjet", "msoftdrop_fatjet", "msoftscaled_fatjet", "mtopo_fatjet", "msofttopo_fatjet", "mH_avg", "mA_avg", "pt_fatjet", "eta_fatjet", "phi_fatjet", "m_ll", "pt_ll", "eta_ll", "phi_ll", "deta_fatjet_ll", "dphi_fatjet_ll", "pt_l1", "eta_l1", "phi_l1", "pt_l2", "eta_l2", "phi_l2", "dphi_l1_l2", "dr_l1_l2", "ngoodMuons", "ngoodElectrons","pt_MET", "phi_MET", "dphi_MET_fatjet", "dphi_MET_ll", "dphi_MET_l1","dphi_MET_l2", "mt_MET_ll", "mt_MET_l1", "mt_MET_l2", "HT_LHE", "nBJetM_add", "nJet_add", "nJet_add1", "nJet_add2", "nJet_add3", "nJet_add4", "mindR_fatjet_jet", "nJetCent_add", "pt_SSum", "pt_VSum", "pt_jet1", "eta_jet1", "phi_jet1",  "pt_jet2", "eta_jet2", "phi_jet2", "pt_bjet1", "eta_bjet1", "phi_bjet1",  "pt_bjet2", "eta_bjet2", "phi_bjet2", "dphi_l1_j1", "dphi_l1_b1", "log2(abs(dxy_l1))", "log2(abs(dz_l1))", "log2(abs(ip3d_l1))", "log2(jetPtRelv2_l1)", "log2(jetRelIso_l1)", "log2(abs(dxy_l2))", "log2(abs(dz_l2))", "log2(abs(ip3d_l2))", "log2(jetPtRelv2_l2)", "log2(jetRelIso_l2)", "Double_trigger_bit"] 
branches_mu = ["Muon_pfRelIso04_all_l1", "Muon_pfIsoId_l1", "Muon_miniPFRelIso_all_l1", "Muon_miniIsoId_l1", "Muon_pfRelIso04_all_l2", "Muon_pfIsoId_l2", "Muon_miniPFRelIso_all_l2", "Muon_miniIsoId_l2", "Muon_pfRelIso04_all_max", "Muon_pfIsoId_min", "Muon_miniPFRelIso_all_max", "Muon_miniIsoId_min", "Idbit_l1", "Idbit_l2", "highpTId_l1", "highpTId_l2"]
branches_el = ["Idbit_l1_mva", "Idbit_l2_mva", "Idbit_l1_cut", "Idbit_l2_cut", "trigger_IDbit"]

x_axis = ["m(AK8) [GeV]", "m_{SoftDrop}(AK8) [GeV]", "m_{SoftScaled}(AK8) [GeV]", "m_{Topo}(AK8) [GeV]", "m_{SoftTopo}(AK8) [GeV]", "mH_{avg} [GeV]", "mA_{avg} [GeV]", "p_{T}(AK8) [GeV]", "#eta (AK8)", "#phi (AK8)", "m(ll) [GeV]", "p_{T}(ll) [GeV]", "#eta(ll)", "#phi(ll)", "|#Delta#eta(AK8, ll)|", "|#Delta#phi(AK8, ll)|", "p_{T}(l1) [GeV]", "#eta(l1)", "#phi(l1)", "p_{T}(l2) [GeV]", "#eta(l2)", "#phi(l2)", "|#Delta#phi(l1, l2)|","#DeltaR(l1, l2)", "Good Muons", "Good Electrons", "p_{T}(MET) [GeV]", "#phi(MET)", "|#Delta#phi(MET, AK8)|", "|#Delta#phi(MET, ll)|", "|#Delta#phi(MET, l1|", "|#Delta#phi(MET, l2|", "M_{T}(MET, ll) [GeV]", "M_{T}(MET, l1) [GeV]", "M_{T}(MET, l2) [GeV]", "H_{T}(LHE)", "Nadd AK4 b-Jet(M)", "N_{extra}(AK4)", "N_{extra}(AK4)", "N_{extra}(AK4)", "N_{extra}(AK4)", "N_{extra}(AK4)", "min#DeltaR(AK8,AK4)", "N_{extra}(central AK4) ", "#Sigma_{i}^{Nextra}(p_{T}(AK4))", "#Sigma_{i}^{Nextra}(p_{T}(AK4_vec))", "p_{T}(j1) [GeV]", "#eta(j1)", "#phi(j1)", "p_{T}(j2) [GeV]", "#eta(j2)", "#phi(j2)", "p_{T}(b1) [GeV]", "#eta(b1)", "#phi(b1)", "p_{T}(b2) [GeV]", "#eta(b2)", "#phi(b2)", "|#Delta#phi(l1, j1)|", "|#Delta#phi(l1, b1)|", "log2|dxy(l1)| [cm]", "log2|dz(l1)| [cm]", "log2|ip3d(l1)| [cm]", "log2(jetPtRelv2(l1))", "log2(jetRelIso(l1))", "log2|dxy(l2)| [cm]", "log2|dz(l2)| [cm]", "log2|ip3d(l2)| [cm]", "log2(jetPtRelv2(l2))", "log2(jetRelIso(l2))", "trigger_singlevsdouble"]

x_axis_mu = ["Muon_pfRelIso04_all(l1)", "Muon_pfIsoId(l1)", "Muon_miniPFRelIso_all(l1)", "Muon_miniIsoId(l1)","Muon_pfRelIso04_all(l2)", "Muon_pfIsoId(l2)", "Muon_miniPFRelIso_all(l2)", "Muon_miniIsoId(l2)", "Muon_pfRelIso04_all max(l1,l2)", "Muon_pfIsoId  min(l1,l2)", "Muon_miniPFRelIso_all  max(l1,l2)", "Muon_miniIsoId  min(l1,l2)", "ID bit(l1)", "ID bit(l2)", "High p_{T} ID (l1)", "High p_{T} ID (l2)"]
x_axis_el = [" MVA ID bit(l1)", "MVA ID bit(l2)", "Cut ID bit(l1)", "Cut ID bit(l2)", "Trigger bit"]

range_min = [20, 20, 20, 20, 20, 20, 2.5, 150, -3, -3.2, 80, 0, -3, -3.2, 0, 0, 0, -3, -3.2, 0, -3, -3.2, 0, 0, -0.5, -0.5, 0, -3.2, 0, 0, 0, 0, 0, 0, 0, 0, -0.5, -0.5, -0.5, -0.5, -0.5, -0.5, 0.8, -0.5, 0, 0, 30, -3, -3.2, 30, -3, -3.2, 30, -3, -3.2, 30, -3, -3.2, 0, 0, -15, -15, -13, -1, -14, -15, -15, -13, -1, -14, -0.5]

range_min_mu = [ 0, -0.5, 0, -0.5, 0, -0.5, 0, -0.5, 0, -0.5, 0, -0.5, -0.5, -0.5, 0.5, 0.5] 
range_min_el = [ -0.5, -0.5, -0.5, -0.5, -0.5]

range_max = [200, 200, 200, 200, 200, 200, 72.5, 650, 3, 3.2, 100, 600, 3, 3.2, 4, 3.2, 300, 3, 3.2, 300, 3, 3.2, 3.2, 3.2, 5.5, 5.5, 300, 3.2, 3.2, 3.2, 3.2, 3.2, 400, 300, 300, 3000, 3.5, 7.5, 7.5, 7.5, 7.5, 7.5, 3, 7.5, 1000, 1000, 530, 3, 3.2, 530, 3, 3.2, 530, 3, 3.2, 530, 3, 3.2, 3.2, 3.2, 0, 5, -3, 9, 4, 0, 5, -3, 9, 4, 3.5]

range_max_mu = [1, 6.5, 1, 4.5, 1, 6.5, 1, 4.5, 1, 6.5, 1, 4.5, 15.5, 15.5, 2.5, 2.5] 
range_max_el = [15.5, 15.5, 15.5, 15.5, 15.5]

bins = [18, 18, 18, 18, 18, 18, 14, 25, 15, 16, 20, 30, 15, 16, 20, 16, 15, 20, 16, 15, 15, 16, 16, 16, 6, 6, 15, 16, 16, 16, 32, 16, 20, 15, 15, 30, 4, 8, 8, 8, 8, 8, 22, 8, 20, 20, 25, 15, 16, 25, 15, 16, 25, 15, 16, 25, 15, 16, 16, 16, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 4]
bins_mu = [20, 7, 20, 5, 20, 7, 20, 5, 20, 7, 20, 5, 16, 16, 2, 2]
bins_el = [16, 16, 16, 16, 16]

titles = ["Leading Fat Jet Mass", "Leading Fat Jet SoftDrop Mass", "Leading Fat Jet p_{T}-Scaled SoftDrop Mass", "Leading Fat Jet ZH-topology corrected Mass", "Leading Fat Jet ZH-topology corrected SoftDrop Mass", "PNET Average Regressed Mass H #rightarrow aa #rightarrow 4b", "PNET Average Regressed Mass a #rightarrow bb", "Leading Fat Jet p_{T}", "Leading Fat Jet #eta", "Leading Fat Jet #phi", "Lepton Pair Mass", "Lepton Pair p_{T}", "Lepton Pair #eta", "Lepton Pair #phi", "#Delta#eta(Leading Fat Jet, Lepton Pair)", "#Delta#phi(Leading Fat Jet, Lepton Pair)", "Lepton_1 p_{T}", "Lepton_1 #eta", "Lepton_1 #phi", "Lepton_2 p_{T}", "Lepton_2 #eta", "Lepton_2 #phi", "#Delta#phi(Lepton_1, Lepton_2)", "#DeltaR(Lepton_1, Lepton_2)", "Number of Muons passing Loose Selection", "Number of Electrons passing Loose Selection", "MET p_{T}", "MET #phi", "#Delta#phi(MET, Leading Fat Jet)", "#Delta#phi(MET, Lepton Pair)", "#Delta#phi(MET, Lepton_1)", "#Delta#phi(MET, Lepton_2)", "Transverse Mass (MET, Lepton Pair)", "Transverse Mass (MET, Lepton_1)", "Transverse Mass (MET, Lepton_2)", "Scalar Sum of Parton pTs (HT) at LHE Step", "Number of Additional B-tagged (M) AK4 Jets", "Number of Additional AK4 Jets [dR(AK8,AK4) > 0.8 and dR(lep,AK4) > 0.4)]", "Number of Additional AK4 Jets [dR(lep,AK4) > 0.4)]", "Number of Additional AK4 Jets [dR(AK8,AK4) > 0.8]", "Number of Additional AK4 Jets [no dR cut]", "Number of Additional AK4 Jets [dR(AK8,AK4) > 1.2 and dR(lep,AK4) > 0.4)]", "Closest AK4 jet to AK8 jet Candidate", "Number of Additional Central AK4 Jets", "Scalar Sum of all Additional AK4 Jets", "Vector Sum of all Additional AK4 Jets", "Leading-p_{T} Additional AK4 Jet p_{T}", "Leading-p_{T} Additional AK4 Jet #eta", "Leading-p_{T} Additional AK4 Jet #phi", "Sub-leading-p_{T} Additional AK4 Jet p_{T}", "Sub-leading-p_{T} Additional AK4 Jet #eta", "Sub-leading-p_{T} Additional AK4 Jet #phi", "Leading-p_{T} Additional AK4 B-Jet p_{T}", "Leading-p_{T} Additional AK4 B-Jet #eta", "Leading-p_{T} Additional AK4 B-Jet #phi", "Sub-leading-p_{T} Additional AK4 B-Jet p_{T}", "Sub-leading-p_{T} Additional AK4 B-Jet #eta", "Sub-leading-p_{T} Additional AK4 B-Jet #phi", "#Delta#phi(Lepton_1, Leading-p_{T} Jet)", "#Delta#phi(Lepton_1, Leading-p_{T} B-Jet)", "Lepton 1 dxy (with sign) with respect to first PV", "Lepton 1 dz (with sign) with respect to First PV", "Lepton 1 3D Impact Parameter with respect to First PV", "Relative Momentum of Lepton 1 with respect to the Closest Jet after Subtracting the Lepton", "Lepton 1 Relative Isolation in Matched Jet (1/ptRatio-1, pfRelIso04_all if no Matched Jet)", "Lepton 2 dxy (with sign) with respect to first PV", "Lepton 2 dz (with sign) with respect to First PV", "Lepton 2 3D Impact Parameter with respect to First PV", "Relative Momentum of Lepton 2 with respect to the Closest Jet after Subtracting the Lepton", "Lepton 2 Relative Isolation in Matched Jet (1/ptRatio-1, pfRelIso04_all if no Matched Jet)", "Trigger bit = 1*SingleTrigger + 2*DoubleTrigger"]

titles_mu = ["Lepton 1 PF relative isolation dR=0.4, total (deltaBeta corrections)", "Lepton 1 PFIso ID from miniAOD selector", "Lepton 1 mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Lepton 1 MiniIso ID from miniAOD selector", "Lepton 2 PF relative isolation dR=0.4, total (deltaBeta corrections)", "Lepton 2 PFIso ID from miniAOD selector", "Lepton 2 mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Lepton 2 MiniIso ID from miniAOD selector", "Maximum PF relative isolation dR=0.4, total (deltaBeta corrections)", "Minimum PFIso ID from miniAOD selector", "Maximum mini PF relative isolation, total (with scaled rho*EA PU corrections)", "Minimum MiniIso ID from miniAOD selector", "Lepton 1 ID bit [1*medium + 2*mediumprompt + 4*tight + 8*highPt]", "Lepton 2 ID bit [1*medium + 2*mediumprompt + 4*tight + 8*highPt]", "Lepton 1 High p_{T} ID (1 = Tracker Muon, 2 = Global muon)", "Lepton 2 High p_{T} ID (1 = Tracker Muon, 2 = Global muon)"]
titles_el = ["Lepton 1 MVA ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 2 MVA ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 1 Cut-based ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Lepton 2 Cut-based ID bit [1*loose + 2*medium + 4*tight + 8*HightPt]", "Trigger ID bit [1*HLT_Ele32_WPTight_Gsf + 2*HLT_Ele35_WPTight_Gsf_L1EGMT +4*HLT_Ele115_CaloIdVT_GsfTrkIdT + 8*HLT_Ele50_CaloIdVT_GsfTrkIdT_PFJet165]"]

#ymax = [10000, 10000, 10000, 10000, 10000, 5]
#ymin = [0, 0, 0, 0, 0]

gROOT.SetBatch(True) # disable canvas display
gStyle.SetOptStat(0) # disable stat box


if channel =="mu":
    data_files = data_mu
    branches = branches + branches_mu
    x_axis = x_axis + x_axis_mu
    range_min = range_min + range_min_mu
    range_max = range_max + range_max_mu
    bins = bins + bins_mu
    titles = titles + titles_mu
else:
    data_files = data_el
    branches = branches + branches_el
    x_axis = x_axis + x_axis_el
    range_min = range_min + range_min_el
    range_max = range_max + range_max_el
    bins = bins + bins_el
    titles = titles + titles_el


for i in range(len(branches)):
    # Cutting events with msoftdrop in Higgs range [100, 140] GeV for branches
    # other than Leading Fat Jet mass/ softdrop_mass / softscale mass / mH_avg,
    # bins in that range are cuted later in the plotting script
  # if (i != 0) and (i != 10) and (i != 12):
  #   blind = "(msoftdrop_fatjet > 140) || (msoftdrop_fatjet < 100)" 
    
    #if i > 4: break
    # Creating Canvas
    c, pad1, pad2 = createCanvasPads(scale_log)
    pad1.cd() # pad1 is for regular plot

    # Add backgrounds
    # TT_2l2v
    f_B0 = TFile(input_dir + Background_files[0] + ".root", "READ")
    events_B0 = f_B0.Get(tree_name)
    B0 = TH1D("B0", titles[i], bins[i], range_min[i], range_max[i])
    events_B0.Draw(branches[i]+">>B0", draw_option) 
    B0.Scale(Background_scale[0])
    #B0.SetFillColor(kGreen)
    B0.SetFillColor(CMS_colors["orange2"])
    
    # ZZ_2Q2L
    f_B1 = TFile(input_dir + Background_files[1] + ".root", "READ")
    events_B1 = f_B1.Get(tree_name)
    B1 = TH1D("B1", titles[i], bins[i], range_min[i], range_max[i])
    events_B1.Draw(branches[i]+">>B1", draw_option)
    B1.Scale(Background_scale[1])
    #B1.SetFillColor(kYellow+1)
    B1.SetFillColor(CMS_colors["purple"])
    
   # DY_M10-50 inclusive amcnlo 
    p_B2 = input_dir + Background_files[2] + ".root"
    if (os.path.isfile(p_B2)):
        f_B2 = TFile(p_B2, "READ") 
        events_B2 = f_B2.Get(tree_name)
        B2 = TH1D("B2", titles[i], bins[i], range_min[i], range_max[i])
        events_B2.Draw(branches[i]+">>B2", draw_option)
        B2.Scale(Background_scale[2])
        #B2.SetFillColor(kTeal+1)
    else:
        B2 = TH1D("B2", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY_50 Background for every HT 
    # DY Jets 70-100
    p_ZB0 = input_dir + DY_50_path + DYJets_files[0] + ".root"
    if (os.path.isfile(p_ZB0)):
        f_ZB0 = TFile(p_ZB0, "READ") 
        events_ZB0 = f_ZB0.Get(tree_name)
        ZB0 = TH1D("ZB0", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB0.Draw(branches[i]+">>ZB0", draw_option)
        ZB0.Scale(DYJets_scale[0])
    else:
        ZB0 = TH1D("ZB0", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY Jets 100-200
    p_ZB1 = input_dir + DY_50_path + DYJets_files[1] + ".root"
    if (os.path.isfile(p_ZB1)):
        f_ZB1 = TFile(p_ZB1, "READ") 
        events_ZB1 = f_ZB1.Get(tree_name)
        ZB1 = TH1D("ZB1", titles[i], bins[i], range_min[i], range_max[i])
        events_ZB1.Draw(branches[i]+">>ZB1", draw_option)
        ZB1.Scale(DYJets_scale[1])
    else:
        ZB1 = TH1D("ZB1", titles[i], bins[i], range_min[i], range_max[i])
    
    # DY Jets 200-400
    f_ZB2 = TFile(input_dir+ DY_50_path + DYJets_files[2] + ".root", "READ")
    events_ZB2 = f_ZB2.Get(tree_name)
    ZB2 = TH1D("ZB2", titles[i], bins[i], range_min[i], range_max[i])
    events_ZB2.Draw(branches[i]+">>ZB2", draw_option)
    ZB2.Scale(DYJets_scale[2])

    # DY Jets 400-600
    f_ZB3 = TFile(input_dir+ DY_50_path + DYJets_files[3] + ".root", "READ")
    events_ZB3 = f_ZB3.Get(tree_name)
    ZB3 = TH1D("ZB3", titles[i], bins[i], range_min[i], range_max[i])
    events_ZB3.Draw(branches[i]+">>ZB3", draw_option)
    ZB3.Scale(DYJets_scale[3])

    # DY Jets 600-800
    f_ZB4 = TFile(input_dir+ DY_50_path + DYJets_files[4] + ".root", "READ")
    events_ZB4 = f_ZB4.Get(tree_name)
    ZB4 = TH1D("ZB4", titles[i], bins[i], range_min[i], range_max[i])
    events_ZB4.Draw(branches[i]+">>ZB4", draw_option)
    ZB4.Scale(DYJets_scale[4])

    # DY Jets 800-1200
    f_ZB5 = TFile(input_dir+ DY_50_path + DYJets_files[5] + ".root", "READ")
    events_ZB5 = f_ZB5.Get(tree_name)
    ZB5 = TH1D("ZB5", titles[i], bins[i], range_min[i], range_max[i])
    events_ZB5.Draw(branches[i]+">>ZB5", draw_option)
    ZB5.Scale(DYJets_scale[5])

    # DY Jets 1200-2500
    f_ZB6 = TFile(input_dir+ DY_50_path + DYJets_files[6] + ".root", "READ")
    events_ZB6 = f_ZB6.Get(tree_name)
    ZB6 = TH1D("ZB6", titles[i], bins[i], range_min[i], range_max[i])
    events_ZB6.Draw(branches[i]+">>ZB6", draw_option)
    ZB6.Scale(DYJets_scale[6])
    
    # DY Jets 2500-inf
    f_ZB7 = TFile(input_dir+ DY_50_path + DYJets_files[7] + ".root", "READ")
    events_ZB7 = f_ZB7.Get(tree_name)
    ZB7 = TH1D("ZB7", titles[i], bins[i], range_min[i], range_max[i])
    events_ZB7.Draw(branches[i]+">>ZB7", draw_option)
    ZB7.Scale(DYJets_scale[7])
    
    # Adding HT histos to main histo
    ZB = TH1D("ZB", titles[i], bins[i], range_min[i], range_max[i])
    ZB.Add(ZB0)
    ZB.Add(ZB1)
    ZB.Add(ZB2)
    ZB.Add(ZB3)
    ZB.Add(ZB4)
    ZB.Add(ZB5)
    ZB.Add(ZB6)
    ZB.Add(ZB7)
    ZB.Add(B2) #Adding NLO M10-50 sample as well
    #ZB.SetFillColor(kAzure+1)
    ZB.SetFillColor(CMS_colors["blue1"])

    
    # Adding signal samples
    # ZH M-15
    fS1 = TFile(input_dir+"SUSY_ZH_M-15.root", "READ")
    events_S1 = fS1.Get(tree_name)
    S1 = TH1D("S1", titles[i], bins[i], range_min[i], range_max[i])
    events_S1.Draw(branches[i]+">>S1", draw_option)
    #S1.Scale(1./S1.Integral())  # can be used to normalize area under histo to 1
    S1.Scale(Signal_scale[0]*sig_scaling)
    #S1.SetLineColor(kMagenta)
    S1.SetLineColor(CMS_colors["orange1"])
    S1.SetLineWidth(2)
    S1.SetLineStyle(2)

    # ZH M-30
    fS2 = TFile(input_dir+"SUSY_ZH_M-30.root", "READ")
    events_S2 = fS2.Get(tree_name)
    S2 = TH1D("S2", titles[i], bins[i], range_min[i], range_max[i])
    events_S2.Draw(branches[i]+">>S2", draw_option)
    S2.Scale(Signal_scale[1]*sig_scaling)
    S2.SetLineColor(CMS_colors["red"])
    #S2.SetLineColor(kOrange)
    S2.SetLineWidth(2)
    S2.SetLineStyle(2)

    # ZH M-55
    fS3 = TFile(input_dir+"SUSY_ZH_M-55.root", "READ")
    events_S3 = fS3.Get(tree_name)
    S3 = TH1D("S3", titles[i], bins[i], range_min[i], range_max[i])
    events_S3.Draw(branches[i]+">>S3", draw_option)
    S3.Scale(Signal_scale[2]*sig_scaling) 
    #S3.SetLineColor(kOrange+10)
    S3.SetLineColor(CMS_colors["brown"])
    S3.SetLineWidth(2)
    S3.SetLineStyle(2)

    # Adding Data samples
    # Data 2018A
    fD0 = TFile(input_dir+ data_files[0] + ".root", "READ")
    events_D0 = fD0.Get(tree_name)
    D0 = TH1D("D0", titles[i], bins[i], range_min[i], range_max[i])
    events_D0.Draw(branches[i]+">>D0", cut) # no weight for data!

    # Data 2018B
    fD1 = TFile(input_dir+ data_files[1] + ".root", "READ")
    events_D1 = fD1.Get(tree_name)
    D1 = TH1D("D1", titles[i], bins[i], range_min[i], range_max[i])
    events_D1.Draw(branches[i]+">>D1", cut) # no weight for data!
    
    # Data 2018C
    fD2 = TFile(input_dir+ data_files[2] + ".root", "READ")
    events_D2 = fD2.Get(tree_name)
    D2 = TH1D("D2", titles[i], bins[i], range_min[i], range_max[i])
    events_D2.Draw(branches[i]+">>D2", cut) # no weight for data!

    # Data 2018D
    fD3 = TFile(input_dir+ data_files[3] + ".root", "READ")
    events_D3 = fD3.Get(tree_name)
    D3 = TH1D("D3", titles[i], bins[i], range_min[i], range_max[i])
    events_D3.Draw(branches[i]+">>D3", cut) # no weight for data!
 
    # Adding Data Histos to a single Data histo (SD)
    SD = TH1D("SD", titles[i], bins[i], range_min[i], range_max[i]) 
    SD.Add(D0)
    SD.Add(D1)
    SD.Add(D2)
    SD.Add(D3)
    
    # Scale MC to Data if 3rd argument is "scale"
    if (dataScale == "scale"):
        if sum(hist.Integral() for hist in [ZB, B0, B1]) > 0 and SD.Integral() > 0:
            norm_scale = SD.Integral() / sum(hist.Integral() for hist in [ZB, B0, B1])
            for hist in [ZB, B0, B1]:
                hist.Scale(norm_scale)
    
    # Stacking background histos in stack plot (ST)
    # Including only the the 3 largest background: DYJets_2L (ZB), TT_2L2v (B0) and ZZ_2Q2L (B1)
    ST = THStack("ST","")
    ST.Add(ZB)
    ST.Add(B0)
    ST.Add(B1) 

    # Blinding Higgs mass range (100-140 GeV) in Higgs Mass plots
    if (i < 6): # m, m_softdrop, m_softscaled, m_topo, m_softtopo, mH_avg
        for j in range(9,13):
             SD.SetBinContent(j, 0)
    
    SD.Scale(1.0) # Doing this just to get data point instead of a histo (temporary fix)
    SD.SetMarkerStyle(20) 
    SD.SetLineColor(kBlack)
    SD.Draw()

    # Stylistic adjustments of regular plot through SD histo
    SD_y = SD.GetYaxis()
    SD_x = SD.GetXaxis()

    # Adjusting y axis range as needed for each variable (especially log)
    # Manually adjust both max and min
    #SD_y.SetRangeUser(1, 100000)
    
    # Manually adjust max only, factor of 1.5 works best for lin while 5 works best for log
    # This is mainly done to make space for the legend and other text
    if (scale_log): # Isolation plots, adjust for linear plots
        if (branches[i] == "HT_LHE"):
            SD.SetMinimum(9)
            SD.SetMaximum(ST.GetMaximum() + 50* (ST.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 
        else:
            SD.SetMinimum(9)
            SD.SetMaximum(SD.GetMaximum() + 50* (SD.GetMaximum()-SD.GetMinimum())) #1.2 for linear plots 
        #SD.SetMaximum(11*SD.GetMaximum()) #1.2 for linear plots 
    else:
        if (branches[i] == "HT_LHE"):
            SD.SetMinimum(0)
            SD.SetMaximum(1.5*ST.GetMaximum())
        else:
            SD.SetMinimum(0)
            SD.SetMaximum(1.5*SD.GetMaximum())
    
    gStyle.SetTitleFontSize(0.05) # Title size
    
    # x-axis (not necessary since hidden by ratio plot) 
    SD_x.SetTitle(x_axis[i])
    SD_x.SetTitleSize(0.05)
    SD_x.SetTitleOffset(1.5)
    SD_x.SetLabelOffset(0.3)

    # y-axis
    SD_y.SetTitle("Events  ") # space used for offset
    SD_y.SetNdivisions(505) #make divisions nice
    SD_y.SetTitleSize(25) 
    SD_y.SetTitleFont(43) 
    SD_y.SetTitleOffset(1.25)
    SD_y.SetLabelFont(43) # font and size of numbers
    SD_y.SetLabelSize(20)


    gPad.Modified() # Update gpad (things might break depending on where this line is)
    
    
    # Drawing everything
    SD.Draw()
    ST.Draw("SAME HIST")
    S1.Draw("SAME HIST")
    S2.Draw("SAME HIST")
    S3.Draw("SAME HIST")
    SD.Draw("SAME")
   

    # Ratio plot
    pad2.cd()# Go to pad2 for ratio plot
    
    # Need to convert stack plot to regular histo first
    nhist = ST.GetHists().GetSize()
    #print("Number of histos in THStack:",nhist)
    if nhist == 0:
        sys.exit()
    tmpHist = ST.GetHists().At(0).Clone()
    tmpHist.Reset()
    tmpHist.SetTitle("")
    for k in range(ST.GetHists().GetSize()):
        tmpHist.Add(ST.GetHists().At(k))
 
    # Histo for ratio plot
    RATIO = createRatio(SD, tmpHist, x_axis[i])
    # Histo for statistical errors accounting for denominator's statistical erorr
    hStatError = getRatioStatError(SD, tmpHist)
    #RATIO.Draw("ep") # draw with stat errors (but doesn't account for denominator statistical error)
    
    # Ratio histo Stylistic stuff
    R_x = RATIO.GetXaxis()
    R_x.SetTitleSize(25) 
    R_x.SetTitleFont(43) 
    R_x.SetTitleOffset(1.15)
    R_x.SetLabelFont(43) # font and size of numbers
    R_x.SetLabelSize(20)
    R_x.SetLabelOffset(.02)

    # Draw line for y = 1 to help compare ratio
    line = TLine(range_min[i], 1.,range_max[i], 1.)
    line.SetLineColor(kBlack)
    line.SetLineWidth(1)
   
    # Draw stacked plot uncertainty in grey
    h_uncertainty = tmpHist.Clone("h_uncertainty")
    h_uncertainty.SetFillColor(ROOT.kGray+1)
    h_uncertainty.SetFillStyle(3001)
    for l in range(1, h_uncertainty.GetNbinsX() + 1):
        h_uncertainty.SetBinError(l, h_uncertainty.GetBinError(l) / tmpHist.GetBinContent(l) if tmpHist.GetBinContent(l) != 0 else 0)
        h_uncertainty.SetBinContent(l, 1)

    # Drawing histos in ratio pad
    RATIO.Draw("HIST EP")
    #hStatError.Draw("SAME P")
    h_uncertainty.Draw("E2 SAME")
    line.Draw("SAME")

    # Redraw Axis in case they are covered
    pad2.RedrawAxis()

    ############# RATIO TEXT SECTION: START ###########################################################################
    # Muon channel ID bit maps
    if (branches[i] == "Idbit_l1" or branches[i] == "Idbit_l2"):
        bin_text = ["#bar{mpth}","m#bar{pth}","p#bar{mth}","mp#bar{th}", "t#bar{mph}","mt#bar{ph}","pt#bar{mh}","mpt#bar{h}","h#bar{mpt}","mh#bar{pt}","ph#bar{pt}","mph#bar{t}","th#bar{mp}","mth#bar{p}","pth#bar{m}","mpth"]
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            x_position.append( (0.97 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (0.25 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ bins[i])
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        t_bin0_text = TLatex(x_position[0], y_position, str(bin_text[0]))
        MinorTextFormat_ratio(t_bin0_text)
        t_bin1_text = TLatex(x_position[1], y_position, str(bin_text[1]))
        MinorTextFormat_ratio(t_bin1_text)
        t_bin2_text = TLatex(x_position[2], y_position, str(bin_text[2]))
        MinorTextFormat_ratio(t_bin2_text)
        t_bin3_text = TLatex(x_position[3], y_position, str(bin_text[3]))
        MinorTextFormat_ratio(t_bin3_text)
        t_bin4_text = TLatex(x_position[4], y_position, str(bin_text[4]))
        MinorTextFormat_ratio(t_bin4_text)
        t_bin5_text = TLatex(x_position[5], y_position, str(bin_text[5]))
        MinorTextFormat_ratio(t_bin5_text)
        t_bin6_text = TLatex(x_position[6], y_position, str(bin_text[6]))
        MinorTextFormat_ratio(t_bin6_text)
        t_bin7_text = TLatex(x_position[7], y_position, str(bin_text[7]))
        MinorTextFormat_ratio(t_bin7_text)
        t_bin8_text = TLatex(x_position[8], y_position, str(bin_text[8]))
        MinorTextFormat_ratio(t_bin8_text)
        t_bin9_text = TLatex(x_position[9], y_position, str(bin_text[9]))
        MinorTextFormat_ratio(t_bin9_text)
        t_bin10_text = TLatex(x_position[10], y_position, str(bin_text[10]))
        MinorTextFormat_ratio(t_bin10_text)
        t_bin11_text = TLatex(x_position[11], y_position, str(bin_text[11]))
        MinorTextFormat_ratio(t_bin11_text)
        t_bin12_text = TLatex(x_position[12], y_position, str(bin_text[12]))
        MinorTextFormat_ratio(t_bin12_text)
        t_bin13_text = TLatex(x_position[13], y_position, str(bin_text[13]))
        MinorTextFormat_ratio(t_bin13_text)
        t_bin14_text = TLatex(x_position[14], y_position, str(bin_text[14]))
        MinorTextFormat_ratio(t_bin14_text)
        t_bin15_text = TLatex(x_position[15], y_position, str(bin_text[15]))
        MinorTextFormat_ratio(t_bin15_text)

        pad1.cd() # go back to pad1


        ta_bin0_text = TLatex(x_position[0], ya_position, str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position, str(data_yield[5]))
        tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, str(data_yield[6]))
        tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        MinorTextFormat_ratio(tb_bin6_text)
        ta_bin7_text = TLatex(x_position[7], ya_position, str(data_yield[7]))
        tb_bin7_text = TLatex(x_position[7], ya_position - 0.02, str(S_yield[7]))
        MinorTextFormat_ratio(ta_bin7_text)
        MinorTextFormat_ratio(tb_bin7_text)
        ta_bin8_text = TLatex(x_position[8], ya_position, str(data_yield[8]))
        tb_bin8_text = TLatex(x_position[8], ya_position - 0.02, str(S_yield[8]))
        MinorTextFormat_ratio(ta_bin8_text)
        MinorTextFormat_ratio(tb_bin8_text)
        ta_bin9_text = TLatex(x_position[9], ya_position, str(data_yield[9]))
        tb_bin9_text = TLatex(x_position[9], ya_position - 0.02, str(S_yield[9]))
        MinorTextFormat_ratio(ta_bin9_text)
        MinorTextFormat_ratio(tb_bin9_text)
        ta_bin10_text = TLatex(x_position[10], ya_position, str(data_yield[10]))
        tb_bin10_text = TLatex(x_position[10], ya_position - 0.02, str(S_yield[10]))
        MinorTextFormat_ratio(ta_bin10_text)
        MinorTextFormat_ratio(tb_bin10_text)
        ta_bin11_text = TLatex(x_position[11], ya_position, str(data_yield[11]))
        tb_bin11_text = TLatex(x_position[11], ya_position - 0.02, str(S_yield[11]))
        MinorTextFormat_ratio(ta_bin11_text)
        MinorTextFormat_ratio(tb_bin11_text)
        ta_bin12_text = TLatex(x_position[12], ya_position, str(data_yield[12]))
        tb_bin12_text = TLatex(x_position[12], ya_position - 0.02, str(S_yield[12]))
        MinorTextFormat_ratio(ta_bin12_text)
        MinorTextFormat_ratio(tb_bin12_text)
        ta_bin13_text = TLatex(x_position[13], ya_position, str(data_yield[13]))
        tb_bin13_text = TLatex(x_position[13], ya_position - 0.02, str(S_yield[13]))
        MinorTextFormat_ratio(ta_bin13_text)
        MinorTextFormat_ratio(tb_bin13_text)
        ta_bin14_text = TLatex(x_position[14], ya_position, str(data_yield[14]))
        tb_bin14_text = TLatex(x_position[14], ya_position - 0.02, str(S_yield[14]))
        MinorTextFormat_ratio(ta_bin14_text)
        MinorTextFormat_ratio(tb_bin14_text)
        ta_bin15_text = TLatex(x_position[15], ya_position, str(data_yield[15]))
        tb_bin15_text = TLatex(x_position[15], ya_position - 0.02, str(S_yield[15]))
        MinorTextFormat_ratio(ta_bin15_text)
        MinorTextFormat_ratio(tb_bin15_text)
        
        pad2.cd()

    # Electron channel bit maps
    if (branches[i] == "Idbit_l1_mva" or branches[i] == "Idbit_l1_cut" or branches[i] == "Idbit_l2_mva" or branches[i] == "Idbit_l2_cut" or branches[i] == "trigger_IDbit"):
        if branches[i] == "trigger_IDbit" :
            bin_text = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""] 
        else:
            bin_text = ["#bar{lmth}","l#bar{mth}", "m#bar{lth}", "lm#bar{th}", "t#bar{lmh}", "lt#bar{mh}", "mt#bar{lh}", "lmt#bar{h}", "h#bar{lmt}", "lh#bar{mt}", "mh#bar{lt}", "lmh#bar{t}", "th#bar{lm}", "lth#bar{m}", "mth#bar{l}", "lmth"]
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            x_position.append( (0.97 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (0.25 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ bins[i])
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        t_bin0_text = TLatex(x_position[0], y_position, str(bin_text[0]))
        MinorTextFormat_ratio(t_bin0_text)
        t_bin1_text = TLatex(x_position[1], y_position, str(bin_text[1]))
        MinorTextFormat_ratio(t_bin1_text)
        t_bin2_text = TLatex(x_position[2], y_position, str(bin_text[2]))
        MinorTextFormat_ratio(t_bin2_text)
        t_bin3_text = TLatex(x_position[3], y_position, str(bin_text[3]))
        MinorTextFormat_ratio(t_bin3_text)
        t_bin4_text = TLatex(x_position[4], y_position, str(bin_text[4]))
        MinorTextFormat_ratio(t_bin4_text)
        t_bin5_text = TLatex(x_position[5], y_position, str(bin_text[5]))
        MinorTextFormat_ratio(t_bin5_text)
        t_bin6_text = TLatex(x_position[6], y_position, str(bin_text[6]))
        MinorTextFormat_ratio(t_bin6_text)
        t_bin7_text = TLatex(x_position[7], y_position, str(bin_text[7]))
        MinorTextFormat_ratio(t_bin7_text)
        t_bin8_text = TLatex(x_position[8], y_position, str(bin_text[8]))
        MinorTextFormat_ratio(t_bin8_text)
        t_bin9_text = TLatex(x_position[9], y_position, str(bin_text[9]))
        MinorTextFormat_ratio(t_bin9_text)
        t_bin10_text = TLatex(x_position[10], y_position, str(bin_text[10]))
        MinorTextFormat_ratio(t_bin10_text)
        t_bin11_text = TLatex(x_position[11], y_position, str(bin_text[11]))
        MinorTextFormat_ratio(t_bin11_text)
        t_bin12_text = TLatex(x_position[12], y_position, str(bin_text[12]))
        MinorTextFormat_ratio(t_bin12_text)
        t_bin13_text = TLatex(x_position[13], y_position, str(bin_text[13]))
        MinorTextFormat_ratio(t_bin13_text)
        t_bin14_text = TLatex(x_position[14], y_position, str(bin_text[14]))
        MinorTextFormat_ratio(t_bin14_text)
        t_bin15_text = TLatex(x_position[15], y_position, str(bin_text[15]))
        MinorTextFormat_ratio(t_bin15_text)

        pad1.cd() # go back to pad1

        ta_bin0_text = TLatex(x_position[0], ya_position, str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position, str(data_yield[5]))
        tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, str(data_yield[6]))
        tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        MinorTextFormat_ratio(tb_bin6_text)
        ta_bin7_text = TLatex(x_position[7], ya_position, str(data_yield[7]))
        tb_bin7_text = TLatex(x_position[7], ya_position - 0.02, str(S_yield[7]))
        MinorTextFormat_ratio(ta_bin7_text)
        MinorTextFormat_ratio(tb_bin7_text)
        ta_bin8_text = TLatex(x_position[8], ya_position, str(data_yield[8]))
        tb_bin8_text = TLatex(x_position[8], ya_position - 0.02, str(S_yield[8]))
        MinorTextFormat_ratio(ta_bin8_text)
        MinorTextFormat_ratio(tb_bin8_text)
        ta_bin9_text = TLatex(x_position[9], ya_position, str(data_yield[9]))
        tb_bin9_text = TLatex(x_position[9], ya_position - 0.02, str(S_yield[9]))
        MinorTextFormat_ratio(ta_bin9_text)
        MinorTextFormat_ratio(tb_bin9_text)
        ta_bin10_text = TLatex(x_position[10], ya_position, str(data_yield[10]))
        tb_bin10_text = TLatex(x_position[10], ya_position - 0.02, str(S_yield[10]))
        MinorTextFormat_ratio(ta_bin10_text)
        MinorTextFormat_ratio(tb_bin10_text)
        ta_bin11_text = TLatex(x_position[11], ya_position, str(data_yield[11]))
        tb_bin11_text = TLatex(x_position[11], ya_position - 0.02, str(S_yield[11]))
        MinorTextFormat_ratio(ta_bin11_text)
        MinorTextFormat_ratio(tb_bin11_text)
        ta_bin12_text = TLatex(x_position[12], ya_position, str(data_yield[12]))
        tb_bin12_text = TLatex(x_position[12], ya_position - 0.02, str(S_yield[12]))
        MinorTextFormat_ratio(ta_bin12_text)
        MinorTextFormat_ratio(tb_bin12_text)
        ta_bin13_text = TLatex(x_position[13], ya_position, str(data_yield[13]))
        tb_bin13_text = TLatex(x_position[13], ya_position - 0.02, str(S_yield[13]))
        MinorTextFormat_ratio(ta_bin13_text)
        MinorTextFormat_ratio(tb_bin13_text)
        ta_bin14_text = TLatex(x_position[14], ya_position, str(data_yield[14]))
        tb_bin14_text = TLatex(x_position[14], ya_position - 0.02, str(S_yield[14]))
        MinorTextFormat_ratio(ta_bin14_text)
        MinorTextFormat_ratio(tb_bin14_text)
        ta_bin15_text = TLatex(x_position[15], ya_position, str(data_yield[15]))
        tb_bin15_text = TLatex(x_position[15], ya_position - 0.02, str(S_yield[15]))
        MinorTextFormat_ratio(ta_bin15_text)
        MinorTextFormat_ratio(tb_bin15_text)

        pad2.cd()
    ############# RATIO TEXT SECTION: END ###########################################################################

    # Legend and other text
    pad1.cd() # go back to pad1

    # Legend
    leg=TLegend(.13,.69,.89,.83) #(x_min, y_min, x_max, y_max)
    #leg=TLegend(.38,.75,.88,.87) #(x_min, y_min, x_max, y_max)
    #leg=TLegend() # automatic legend placement sucks
    leg.SetNColumns(3) # number of columns for legend
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(22)
    leg.SetTextSize(0.03)
    leg.AddEntry(ZB,"DYJetsTo2L","f") # "f" for fills, "L" for linesi, "lep" for data points
    leg.AddEntry(B0,Background_files[0],"f")
    leg.AddEntry(B1,Background_files[1],"f")
    leg.AddEntry(S1,"ZH_Haa4b_M-15*"+ str(sig_scaling),"L")
    leg.AddEntry(S2,"ZH_Haa4b_M-30*"+ str(sig_scaling),"L")
    leg.AddEntry(S3,"ZH_Haa4b_M-55*"+ str(sig_scaling),"L")
    leg.AddEntry(SD,"Data partially blind","lep")
    leg.Draw("SAME")

    ############# TEXT SECTION: START ###########################################################################
    
    # CMS stuff
    if (scale_log == False and PNET_score == "0"):
        tex = TLatex(0.53,0.88,"#bf{CMS} #it{Work in Progress}                                59.83 fb^{-1} (13 TeV, 2018)")
    else:
        tex = TLatex(0.51,0.88,"#bf{CMS} #it{Work in Progress}                                     59.83 fb^{-1} (13 TeV, 2018)")    
    tex.SetNDC()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetTextAlign(20)
    tex.Draw("SAME")
    
    # CMS stuff
    #tex = TLatex(0.53,0.88,"  #bf{CMS} #it{Work in Progress}                            59.83 fb^{-1} (13 TeV, 2018)")
    #tex.SetNDC()
    #tex.SetTextFont(42)
    #tex.SetTextSize(0.04)
    #tex.SetTextAlign(20)
    #tex.Draw("SAME")

    # Channel
    if (channel =="mu"):
        chan = TLatex(0.15,0.84,"  #bf{Di-muon}")
    else:
        chan = TLatex(0.15,0.84,"  #bf{Di-electron}")
    chan.SetNDC()
    chan.SetTextFont(42)
    chan.SetTextSize(0.04)
    chan.Draw("SAME")
    
    # PNET score "score_Haa4b_vs_QCD"
    if PNET_score == "0":
        t_score = TLatex(0.4,0.835,"No score_Haa4b_vs_QCD cut")
    elif PNET_score == "fail":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD < 0.5")
    elif PNET_score == "Fail_WP40-80":
        t_score = TLatex(0.4,0.835,"0.5 < score_Haa4b_vs_QCD < 0.992")
    elif PNET_score == "pass":
        t_score = TLatex(0.15,0.81,"score_Haa4b_vs_QCD > 0.8")
    else:    
        t_score = TLatex(0.4,0.835,"score_Haa4b_vs_QCD > "+ PNET_score)
    MinorTextFormat(t_score)
    
    if (cut_add):
        t_cat = TLatex(0.4,0.855, cut_add)
        MinorTextFormat(t_cat)
    if (dataScale == "scale"):
        t_dscale = TLatex(0.7,0.835, "MC scaled to Data")
        MinorTextFormat(t_dscale)

    # PNET SCORE S/sqrt(S+B)
    if (branches[i] == "m_ll"):
        STH = ST.GetStack().Last()
        B_yield = 0
        S_yield = 0
        SG = 0
        for k in range(bins[i]):
            B_yield = B_yield + STH.GetBinContent(k)
            S_yield = S_yield +  (S1.GetBinContent(k)+ S2.GetBinContent(k) + S3.GetBinContent(k))/(3*sig_scaling)
        SG = S_yield/np.sqrt(S_yield + B_yield) 
        SGN = round(SG, 4)
        SGN_text = TLatex(0.3,0.5, "#frac{S(ZH)}{#sqrt{S+B}} = " + str(SGN))
        MinorTextFormat(SGN_text)
    
    # Z purity regions
    if (cut == z_peak):
        Z_text = "High Z Purity"
        t_z = TLatex(0.2,0.78, Z_text)
        MinorTextFormat(t_z)
    if (cut == z_tail):
        Z_text = "Low Z Purity"
        t_z = TLatex(0.2,0.78, Z_text)
        MinorTextFormat(t_z)

    # Display bin text for specifc plots
    if (branches[i] == "Muon_pfIsoId_min" or branches[i] == "Muon_pfIsoId_l1" or branches[i] == "Muon_pfIsoId_l2"):
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            #x_position.append(0.02 + 0.8*((k+1)/(bins[i])))
            x_position.append(0.81*((k+1)/(bins[i])))
            #x_position.append( (1 + k)/ (bins[i] - 1))
            #x_position.append( (1 + k)/ (bins[i] - 1))
            #bin_yield.append(int(SD.GetBinContent(k+1)))
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        ta_bin0_text = TLatex(x_position[0], ya_position, "d= " + str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, "s= " + str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, "d= " + str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, "s= " + str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, "d= " + str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, "s= " + str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, "d= " + str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, "s= " + str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, "d= " + str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, "s= " + str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        ta_bin5_text = TLatex(x_position[5], ya_position, "d= " + str(data_yield[5]))
        tb_bin5_text = TLatex(x_position[5], ya_position - 0.02, "s= " + str(S_yield[5]))
        MinorTextFormat_ratio(ta_bin5_text)
        MinorTextFormat_ratio(tb_bin5_text)
        ta_bin6_text = TLatex(x_position[6], ya_position, "d= " + str(data_yield[6]))
        tb_bin6_text = TLatex(x_position[6], ya_position - 0.02, "s= " + str(S_yield[6]))
        MinorTextFormat_ratio(ta_bin6_text)
        MinorTextFormat_ratio(tb_bin6_text)
        
    if (branches[i] == "Muon_miniIsoId_min" or branches[i] == "Muon_miniIsoId_l1" or branches[i] == "Muon_miniIsoId_l2"):
        data_yield = []
        S_yield = []
        x_position = []
        y_position = 0.37
        ya_position = 0.07
        for k in range(bins[i]):
            x_position.append(0.81*((k+1)/(bins[i])))
            #x_position.append( (1 + 0.8*(k+1) )/(bins[i]))
            #x_position.append( (1 + k)/ (bins[i] - 1))
            #bin_yield.append(int(SD.GetBinContent(k+1)))
            data_yield.append(int(SD.GetBinContent(k+1)))
            sig_yield = (S1.GetBinContent(k+1)+ S2.GetBinContent(k+1) + S3.GetBinContent(k+1))/(3*sig_scaling)
            S_yield.append(round(sig_yield, 2))

        ta_bin0_text = TLatex(x_position[0], ya_position, "d= " + str(data_yield[0]))
        tb_bin0_text = TLatex(x_position[0], ya_position - 0.02, "s= " + str(S_yield[0]))
        MinorTextFormat_ratio(ta_bin0_text)
        MinorTextFormat_ratio(tb_bin0_text)
        ta_bin1_text = TLatex(x_position[1], ya_position, "d= " + str(data_yield[1]))
        tb_bin1_text = TLatex(x_position[1], ya_position - 0.02, "s= " + str(S_yield[1]))
        MinorTextFormat_ratio(ta_bin1_text)
        MinorTextFormat_ratio(tb_bin1_text)
        ta_bin2_text = TLatex(x_position[2], ya_position, "d= " + str(data_yield[2]))
        tb_bin2_text = TLatex(x_position[2], ya_position - 0.02, "s= " + str(S_yield[2]))
        MinorTextFormat_ratio(ta_bin2_text)
        MinorTextFormat_ratio(tb_bin2_text)
        ta_bin3_text = TLatex(x_position[3], ya_position, "d= " + str(data_yield[3]))
        tb_bin3_text = TLatex(x_position[3], ya_position - 0.02, "s= " + str(S_yield[3]))
        MinorTextFormat_ratio(ta_bin3_text)
        MinorTextFormat_ratio(tb_bin3_text)
        ta_bin4_text = TLatex(x_position[4], ya_position, "d= " + str(data_yield[4]))
        tb_bin4_text = TLatex(x_position[4], ya_position - 0.02, "s= " + str(S_yield[4]))
        MinorTextFormat_ratio(ta_bin4_text)
        MinorTextFormat_ratio(tb_bin4_text)
        

    ####################################### TEXT SECTION: END ###############################################

    # Redraw axis + border since they are hidden by histo fills
    ROOT.gPad.RedrawAxis() 
    borderline = TLine(range_max[i], 1.0,range_max[i], SD.GetMaximum())
    borderline.SetLineColor(kBlack)
    borderline.SetLineWidth(1)
    borderline.Draw("SAME")
    
    # Save individual pdf/png

    #c.SaveAs("../"+output_dir+"/pdf/Data-MC/Data-MC_mu_"+branches[i]+".pdf")
    #c.SaveAs(output_dir+"/png/Data-MC/Data-MC_mu_"+branches[i]+".png")

    # Save one pdf with all canvas
    if i == 0:
        c.Print(output_dir+ "/Data-MC_2L_Z_" + channel + "_" + sc + "_" + date + ".pdf(", "Title: " + branches[i])
    elif i == len(branches)-1:
    #elif i == 6:
        c.Print(output_dir+ "/Data-MC_2L_Z_" + channel + "_" + sc + "_" + date + ".pdf)", "Title: " + branches[i])
    else:
        c.Print(output_dir+ "/Data-MC_2L_Z_" + channel + "_" + sc + "_" + date + ".pdf", "Title: " + branches[i])
    #if i == 0:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf(", "Title: " + branches[i])
    #elif i == len(branches)-1:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf)", "Title: " + branches[i])
    #else:
    #    c.Print(output_dir+ "/pdf/Data-MC_"+ channel +".pdf", "Title: " + branches[i])
    # Delete all histos to avoid warning when initializing histos with same name
    #B0.SetDirectory(0)
    c.Close() # Delete Canvas after saving it

# Loops back to the next variable to plot
