#!/bin/bash
#YSCALE="lin"
#DATASCALE="noscale"
#PNET="0.5"
#DATE="081224c"

# no cut
python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs" --addcut=""
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs" --addcut=""

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt100" --addcut="pt_ll > 100"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt100" --addcut="pt_ll > 100"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt110" --addcut="pt_ll > 110"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt110" --addcut="pt_ll > 110"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt120" --addcut="pt_ll > 120"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt120" --addcut="pt_ll > 120"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt130" --addcut="pt_ll > 130"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt130" --addcut="pt_ll > 130"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt140" --addcut="pt_ll > 140"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt140" --addcut="pt_ll > 140"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt150" --addcut="pt_ll > 150"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt150" --addcut="pt_ll > 150"



python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt160" --addcut="pt_ll > 160"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt160" --addcut="pt_ll > 160"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt170" --addcut="pt_ll > 170"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt170" --addcut="pt_ll > 170"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt180" --addcut="pt_ll > 180"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt180" --addcut="pt_ll > 180"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt190" --addcut="pt_ll > 190"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt190" --addcut="pt_ll > 190"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt200" --addcut="pt_ll > 200"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt200" --addcut="pt_ll > 200"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt210" --addcut="pt_ll > 210"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt210" --addcut="pt_ll > 210"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt220" --addcut="pt_ll > 220"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt220" --addcut="pt_ll > 220"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt230" --addcut="pt_ll > 230"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt230" --addcut="pt_ll > 230"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt240" --addcut="pt_ll > 240"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt240" --addcut="pt_ll > 240"


python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt250" --addcut="pt_ll > 250"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt250" --addcut="pt_ll > 250"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt260" --addcut="pt_ll > 260"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_pt260" --addcut="pt_ll > 260"


# dphi dphi_fatjet_ll
python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2" --addcut="dphi_fatjet_ll > 2"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2" --addcut="dphi_fatjet_ll > 2"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.1" --addcut="dphi_fatjet_ll > 2.1"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.1" --addcut="dphi_fatjet_ll > 2.1"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.2" --addcut="dphi_fatjet_ll > 2.2"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.2" --addcut="dphi_fatjet_ll > 2.2"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.3" --addcut="dphi_fatjet_ll > 2.3"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.3" --addcut="dphi_fatjet_ll > 2.3"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.4" --addcut="dphi_fatjet_ll > 2.4"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.4" --addcut="dphi_fatjet_ll > 2.4"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.5" --addcut="dphi_fatjet_ll > 2.5"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.5" --addcut="dphi_fatjet_ll > 2.5"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.6" --addcut="dphi_fatjet_ll > 2.6"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.6" --addcut="dphi_fatjet_ll > 2.6"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.7" --addcut="dphi_fatjet_ll > 2.7"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.7" --addcut="dphi_fatjet_ll > 2.7"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.8" --addcut="dphi_fatjet_ll > 2.8"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.8" --addcut="dphi_fatjet_ll > 2.8"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.9" --addcut="dphi_fatjet_ll > 2.9"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi2.9" --addcut="dphi_fatjet_ll > 2.9"

python3 Data_MC_plot_2L_Z.py --channel="mu" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi3" --addcut="dphi_fatjet_ll > 3"
python3 Data_MC_plot_2L_Z.py --channel="el" --yaxis="lin" --MCscale="scale" --score="0.5" --date="101124cs_dphi3" --addcut="dphi_fatjet_ll > 3"


