#!/usr/bin/env python

import sys
import numpy as np
#import matplotlib
#import matplotlib.backends.backend_pdf
#from  matplotlib import pyplot as plt
#import pylab

#from scipy.stats.stats import pearsonr
import math
import os
import array

import ROOT
from ROOT import TChain, TSelector, TTree
from ROOT import TCanvas, TPad, TFile, TPaveText
from ROOT import gBenchmark, gStyle, gROOT, gPad
from ROOT import TH1, TH2, TH2D, TH1F, THStack, TH1D  # import histos
from ROOT import TColor, TGaxis, TLine, TLegend, TLatex
# Import colors
from ROOT import kBlack, kAzure, kGreen, kViolet, kRed, kMagenta, kPink, kBlue
from ROOT import kOrange, kYellow, kSpring, kTeal, kCyan, kGray, kWhite


# Functions
def createCanvasPads(logs): # Function to create canvas for plot + ratio plot
    c = TCanvas("c", "canvas", 1700, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0, 0.5, 1.0)
    pad1.SetTopMargin(0.11)
    pad1.SetRightMargin(0.13)
    pad1.SetLeftMargin(0.13)
    if (logs):
        pad1.SetLogz()
    #pad1.SetTopMargin(0.12)  
    #pad1.SetBottomMargin(0.015)  # joins upper and lower plot
   # pad1.SetGrid()
    #if (logs):
    #    pad1.SetLogy() 
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0.5, 0, 1, 1.0)
    pad2.SetTopMargin(0.11)
    pad2.SetRightMargin(0.13)
    pad2.SetLeftMargin(0.13)
    if (logs):
        pad2.SetLogz()
    #pad2.SetTopMargin(0)  # joins upper and lower plot
    #pad2.SetBottomMargin(0.3)
    #pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def MinorTextFormat(t):
    t.SetNDC()
    t.SetTextFont(42)
    t.SetTextSize(0.025)
    t.Draw("SAME")


def Histo_style(h, x_title, y_title):

    h_y = h.GetYaxis()
    h_x = h.GetXaxis()

    # x-axis 
    h_x.SetTitle(x_title)
    h_x.SetTitleSize(20) 
    h_x.SetTitleFont(43) 
    h_x.SetTitleOffset(1.5)
    h_x.SetLabelFont(43) # font and size of numbers
    h_x.SetLabelSize(25)

    # y-axis
    h_y.SetTitle(y_title) # space used for offset
    h_y.SetTitleSize(30) 
    h_y.SetTitleFont(43) 
    h_y.SetTitleOffset(1.25)
    h_y.SetLabelFont(43) # font and size of numbers
    h_y.SetLabelSize(25)


##############  Manually adjust CUT & PLOT TYPE  ##################################################
###################################################################################################
#cut = ""
cut = "H_pt > 150"
###################################################################################################
#weight = "genWeight"
weight = "1 - 2*(genWeight < 0)"
###################################################################################################
scale_log = True
##################################

# SD.GetBinContent(N+1)
# SD.GetBinContent(0)

# init vars
input_dir = "/eos/user/h/hboucham/Haa4b/samples/Ntuples/Topology_ZH/"
output_dir = "plots/"
tree_name = "event_tree"

# files
#files = ["SUSY_ZH_M-30.root",  "ZHToMuMuG_amcatnlo.root"]
files = ["SUSY_ZH_M-all.root",  "ZHToMuMuG_amcatnlo.root"]
file_names = ["ZH_Haa4b_M-all", "ZH_MuMuG_Dalitz"]

# x information
xbranches = "log2(2*H_pt/((H_pt + Z_pt)))"
xrange_min = -1.0
xrange_max = 1.0
xbins = 20
x_title = "log2[#frac{2p_{T}(H)}{p_{T}(H)+p_{T}(Z)}]    "

# y information
ybranches = "log2(H_pt)"
yrange_min = 7.2
yrange_max = 10
ybins = 14
y_title = "log2[p_{T}(H)]"


ROOT.gROOT.SetBatch(True) # disable canvas display
ROOT.gStyle.SetOptStat(0) # disable stat box

steps = ["regular", "binning", "ratio", "weight", "weight_empty", "weight_scale", "comparison"]

for i in range(len(steps)):
    if (steps[i] == "regular"):

        # Canvas
        #c = TCanvas("c", "canvas", 800, 800)
        #c.SetTopMargin(0.11)
        #c.SetRightMargin(0.13)
        #c.SetLeftMargin(0.13)
        #c.SetLogz()

    
        # Creating Canvas
        c, pad1, pad2 = createCanvasPads(scale_log)

        # Draw ZH haa4b sample
        pad1.cd() # pad1 for ZH Haa4b sample

        # Making 2D plot
        fA = TFile(input_dir+ files[0], "READ")
        events_A = fA.Get(tree_name)
        A = TH2D("A", file_names[0], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
        events_A.Draw(ybranches + ":" + xbranches + ">>A", f"({cut})*({weight})")
        A.Scale(1./A.Integral())  # can be used to normalize area under histo to 1
        
        # Histo style and drawing
        gStyle.SetTitleFontSize(0.05) # Title size
        Histo_style(A, x_title, y_title)
        A.Draw("colz") 
        gPad.Modified() # Update gpad (things might break depending on where this line is) 
        
        # Show under/overflow bins
        A.GetXaxis().SetRange(0, xbins+1)
        A.GetYaxis().SetRange(0, ybins+1)
        A.GetZaxis().SetRangeUser(0.000001, 0.1)
        gPad.Modified() # Update gpad (things might break depending on where this line is) 

        # CMS Stuff
        tA = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tA.SetNDC()
        tA.SetTextFont(42)
        tA.SetTextSize(0.032)
        tA.SetTextAlign(20)
        tA.Draw("SAME")
        
        # Bin Text
        tbinA = TLatex(0.16,0.84,"  #bf{Before Rebinning}")
        tbinA.SetNDC()
        tbinA.SetTextFont(42)
        tbinA.SetTextSize(0.035)
        tbinA.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineA = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineA.SetLineColor(kBlack)
        borderlineA.SetLineWidth(1)
        borderlineA.Draw("SAME")
       

        # Draw ZH_mumuG Dalitz sample
        pad2.cd() # pad2 for ZH_mumuG Dalitz sample

        # Making 2D plot
        fB = TFile(input_dir+ files[1], "READ")
        events_B = fB.Get(tree_name)
        B = TH2D("B", file_names[1], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
        events_B.Draw(ybranches + ":" + xbranches + ">>B", f"({cut})*({weight})")
        B.Scale(1./B.Integral())  # can be used to normalize area under histo to 1
        
        # Histo style and drawing
        gStyle.SetTitleFontSize(0.05) # Title size
        Histo_style(B, x_title, y_title)
        B.Draw("colz") 
        gPad.Modified() # Update gpad (things might break depending on where this line is) 
        
        # Show under/overflow bins
        B.GetXaxis().SetRange(0, xbins+1)
        B.GetYaxis().SetRange(0, ybins+1)
        B.GetZaxis().SetRangeUser(0.000001, 0.1)
        gPad.Modified() # Update gpad (things might break depending on where this line is) 

        # CMS Stuff
        tB = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tB.SetNDC()
        tB.SetTextFont(42)
        tB.SetTextSize(0.032)
        tB.SetTextAlign(20)
        tB.Draw("SAME")
        
        # Bin Text
        tbinB = TLatex(0.16,0.84,"  #bf{Before Rebinning}")
        tbinB.SetNDC()
        tbinB.SetTextFont(42)
        tbinB.SetTextSize(0.035)
        tbinB.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")

        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it
                
    if (steps[i] == "binning"):
        # Creating Canvas
        c, pad1, pad2 = createCanvasPads(scale_log)
        
        # Draw ZH haa4b sample
        pad1.cd() # pad1 for ZH Haa4b sample
        A.Draw("colz")
        
        # CMS Stuff
        tA = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tA.SetNDC()
        tA.SetTextFont(42)
        tA.SetTextSize(0.032)
        tA.SetTextAlign(20)
        tA.Draw("SAME")
        
        # Bin Text
        tbinA = TLatex(0.16,0.84,"  #bf{After Rebinning}")
        tbinA.SetNDC()
        tbinA.SetTextFont(42)
        tbinA.SetTextSize(0.035)
        tbinA.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineA = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineA.SetLineColor(kBlack)
        borderlineA.SetLineWidth(1)
        borderlineA.Draw("SAME")

        pad2.cd() # pad1 for ZH Haa4b sample
        B.Draw("colz")
        
        # CMS Stuff
        tB = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tB.SetNDC()
        tB.SetTextFont(42)
        tB.SetTextSize(0.032)
        tB.SetTextAlign(20)
        tB.Draw("SAME")
        
        # Bin Text
        tbinB = TLatex(0.16,0.84,"  #bf{After Rebinning}")
        tbinB.SetNDC()
        tbinB.SetTextFont(42)
        tbinB.SetTextSize(0.035)
        tbinB.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")

        #Rebinning
        for l in range(ybins+2):
            for k in range(int(xbins/2) ):
                if (A.GetBinContent(k,l) < 0.001):
                    A.SetBinContent(k+1,l, A.GetBinContent(k,l) + A.GetBinContent(k+1,l))
                    #A.SetBinContent(k,l, 0)
                    B.SetBinContent(k+1,l, B.GetBinContent(k,l) + B.GetBinContent(k+1,l))
                    #B.SetBinContent(k,l, 0)
                    if (A.GetBinContent(k,l) > 0):
                        A.SetBinContent(k,l, 0.000002)
                        B.SetBinContent(k,l, 0.000002)
                kp = xbins + 1 - k 
                if (A.GetBinContent(kp,l) < 0.001):
                    A.SetBinContent(kp-1,l, A.GetBinContent(kp,l) + A.GetBinContent(kp-1,l))
                    #A.SetBinContent(kp,l, 0)
                    B.SetBinContent(kp-1,l, B.GetBinContent(kp,l) + B.GetBinContent(kp-1,l))
                    #B.SetBinContent(kp,l, 0)
                    if (A.GetBinContent(kp,l) > 0):
                        A.SetBinContent(kp,l, 0.000002)
                        B.SetBinContent(kp,l, 0.000002)


        B.Draw("SAME")
        pad1.cd()
        A.Draw("SAME")
        
        
        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it

    if (steps[i] == "ratio"):
        # Creating Canvas
        c = TCanvas("c", "canvas", 800, 800)
        c.SetTopMargin(0.11)
        c.SetRightMargin(0.13)
        c.SetLeftMargin(0.13)
        #c.SetLogz()
 
        C = B.Clone("C")
        C.Divide(A)
        C.SetTitle("Ratio ZH Dalitz/Haa4b")
        C.SetMarkerSize(0.35)
        C.Draw("colz text")
        C.GetZaxis().SetRangeUser(0.5, 2)
        
        # CMS Stuff
        tC = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tC.SetNDC()
        tC.SetTextFont(42)
        tC.SetTextSize(0.032)
        tC.SetTextAlign(20)
        tC.Draw("SAME")
        
        # Bin Text
        tbinC = TLatex(0.16,0.84,"  #bf{Low yield bins ratio = 1}")
        tbinC.SetNDC()
        tbinC.SetTextFont(42)
        tbinC.SetTextSize(0.02)
        tbinC.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")

        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it
    
    if (steps[i] == "weight"):
        # Creating Canvas
        c = TCanvas("c", "canvas", 800, 800)
        c.SetTopMargin(0.11)
        c.SetRightMargin(0.13)
        c.SetLeftMargin(0.13)
        #c.SetLogz()
 
        #Rebinning
        for l in range(ybins+2):
            for k in range(int(xbins/2) ):
                km = int(xbins/2) - k
                kn = int(xbins/2) + k
                if (C.GetBinContent(km-1,l) == 1):
                    C.SetBinContent(km-1,l, C.GetBinContent(km,l)) 
                if (C.GetBinContent(kn+1,l) == 1):
                    C.SetBinContent(kn+1,l, C.GetBinContent(kn,l)) 
        
        C.SetTitle("Weights ZH Dalitz/Haa4b")
        C.Draw("colz text")
        C.GetZaxis().SetRangeUser(0.5, 2)
        
        # CMS Stuff
        tB = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tB.SetNDC()
        tB.SetTextFont(42)
        tB.SetTextSize(0.032)
        tB.SetTextAlign(20)
        tB.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")

        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it
    
    if (steps[i] == "weight_empty"):
        # open root file to save weights histo
        #output_file = TFile("../weights/ZH_weights.root","recreate")
        # Creating Canvas
        c = TCanvas("c", "canvas", 800, 800)
        c.SetTopMargin(0.11)
        c.SetRightMargin(0.13)
        c.SetLeftMargin(0.13)
        #c.SetLogz()
 
        #Rebinning
        for l in range(ybins+2):
            for k in range(int(xbins/2) ):
                km = int(xbins/2) - k
                kn = int(xbins/2) + k
                if (C.GetBinContent(km-1,l) == 0):
                    C.SetBinContent(km-1,l, C.GetBinContent(km,l)) 
                if (C.GetBinContent(kn+1,l) == 0):
                    C.SetBinContent(kn+1,l, C.GetBinContent(kn,l)) 
        
        C.SetTitle("All Weights ZH Dalitz/Haa4b ")
        C.Draw("colz text")
        
        # CMS Stuff
        tB = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tB.SetNDC()
        tB.SetTextFont(42)
        tB.SetTextSize(0.032)
        tB.SetTextAlign(20)
        tB.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")

        # Save root file
        #output_file.WriteObject(C, "ZH_weights_histo") 
        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it
    
    if (steps[i] == "weight_scale"):
        
        # open root file to save weights histo
        output_file = TFile(output_dir + "ZH_2D_weight.root","recreate")
        
        # Creating Canvas
        c, pad1, pad2 = createCanvasPads(scale_log)

        # Draw ZH haa4b sample
        pad1.cd() # pad1 for ZH Haa4b sample

        # Making 2D plot
        fA = TFile(input_dir+ files[0], "READ")
        events_A = fA.Get(tree_name)
        A = TH2D("A", file_names[0], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
        events_A.Draw(ybranches + ":" + xbranches + ">>A", f"({cut})*({weight})")
        A.Scale(1./A.Integral())  # can be used to normalize area under histo to 1
        
        # Histo style and drawing
        gStyle.SetTitleFontSize(0.05) # Title size
        Histo_style(A, x_title, y_title)
        A.Draw("colz") 
        gPad.Modified() # Update gpad (things might break depending on where this line is) 
        
        # Show under/overflow bins
        A.GetXaxis().SetRange(0, xbins+1)
        A.GetYaxis().SetRange(0, ybins+1)
        A.GetZaxis().SetRangeUser(0.000001, 0.1)
        gPad.Modified() # Update gpad (things might break depending on where this line is) 

        # CMS Stuff
        tA = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tA.SetNDC()
        tA.SetTextFont(42)
        tA.SetTextSize(0.032)
        tA.SetTextAlign(20)
        tA.Draw("SAME")
        
        # Bin Text
        tbinA = TLatex(0.16,0.84,"  #bf{Without weights}")
        tbinA.SetNDC()
        tbinA.SetTextFont(42)
        tbinA.SetTextSize(0.035)
        tbinA.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineA = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineA.SetLineColor(kBlack)
        borderlineA.SetLineWidth(1)
        borderlineA.Draw("SAME")
       

        # Draw ZH_Haa4b sample after reweighting
        pad2.cd() 
         
        # Multiply ZH_Haa4b 2D plot by weights 2D plot
        W = TH2D("W", file_names[0], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
        W.Multiply(A,C)
        W.SetTitle("Reweighted ZH_Haa4b_M-all")
        W.Draw("colz")
        K = 1./W.Integral()
        #print(K) 
        W.Scale(1./W.Integral())  # can be used to normalize area under histo to 1
        
        W.GetXaxis().SetRange(0, xbins+1)
        W.GetYaxis().SetRange(0, ybins+1)
        W.GetZaxis().SetRangeUser(0.000001, 0.1)
        gPad.Modified() # Update gpad (things might break depending on where this line is) 

        # CMS Stuff
        tB = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tB.SetNDC()
        tB.SetTextFont(42)
        tB.SetTextSize(0.032)
        tB.SetTextAlign(20)
        tB.Draw("SAME")
        
        # Bin Text
        tbinB = TLatex(0.16,0.84,"  #bf{After weights}")
        tbinB.SetNDC()
        tbinB.SetTextFont(42)
        tbinB.SetTextSize(0.035)
        tbinB.Draw("SAME")
        
        # Weight Text
        tbinC = TLatex(0.16,0.81," Constant =" + str(round(K, 4)))
        tbinC.SetNDC()
        tbinC.SetTextFont(42)
        tbinC.SetTextSize(0.03)
        tbinC.Draw("SAME")
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")
        
        # Save root file
        C.Scale(K)  # can be used to normalize area under histo to 1
        output_file.WriteObject(C, "ZH_weights_histo") 
        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it
    if (steps[i] == "comparison"):

        # Canvas
        #c = TCanvas("c", "canvas", 800, 800)
        #c.SetTopMargin(0.11)
        #c.SetRightMargin(0.13)
        #c.SetLeftMargin(0.13)
        #c.SetLogz()

    
        # Creating Canvas
        c, pad1, pad2 = createCanvasPads(scale_log)

        # Draw ZH haa4b sample
        pad1.cd() # pad1 for ZH Haa4b sample

        # Multiply ZH_Haa4b 2D plot by weights 2D plot
        W.SetTitle("Reweighted ZH_Haa4b_M-all")
        W.Draw("colz")

        # CMS Stuff
        tA = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tA.SetNDC()
        tA.SetTextFont(42)
        tA.SetTextSize(0.032)
        tA.SetTextAlign(20)
        tA.Draw("SAME")
         
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineA = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineA.SetLineColor(kBlack)
        borderlineA.SetLineWidth(1)
        borderlineA.Draw("SAME")
        
        # Draw ZH_mumuG Dalitz sample
        pad2.cd() # pad2 for ZH_mumuG Dalitz sample

        # Making 2D plot
        fB = TFile(input_dir+ files[1], "READ")
        events_B = fB.Get(tree_name)
        B = TH2D("B", file_names[1], xbins, xrange_min, xrange_max, ybins, yrange_min, yrange_max)
        events_B.Draw(ybranches + ":" + xbranches + ">>B", f"({cut})*({weight})")
        B.Scale(1./B.Integral())  # can be used to normalize area under histo to 1
        
        # Histo style and drawing
        gStyle.SetTitleFontSize(0.05) # Title size
        Histo_style(B, x_title, y_title)
        B.Draw("colz") 
        gPad.Modified() # Update gpad (things might break depending on where this line is) 
        
        # Show under/overflow bins
        B.GetXaxis().SetRange(0, xbins+1)
        B.GetYaxis().SetRange(0, ybins+1)
        B.GetZaxis().SetRangeUser(0.000001, 0.1)
        gPad.Modified() # Update gpad (things might break depending on where this line is) 

        # CMS Stuff
        tB = TLatex(0.49,0.89,"  #bf{CMS} #it{Work in Progress}                                            13 TeV")
        tB.SetNDC()
        tB.SetTextFont(42)
        tB.SetTextSize(0.032)
        tB.SetTextAlign(20)
        tB.Draw("SAME")
        
        
        # Redraw axis + border since they are hidden by histo fills
        ROOT.gPad.RedrawAxis() 
        borderlineB = TLine(xrange_max, yrange_min,xrange_max,  yrange_min)
        borderlineB.SetLineColor(kBlack)
        borderlineB.SetLineWidth(1)
        borderlineB.Draw("SAME")

        # Save png and pdf of your canvas 
        if i == 0:
            c.Print(output_dir + "ZH_2D_plot.pdf(", "Title: " + steps[i])
        elif i == len(steps)-1 :
            c.Print(output_dir + "ZH_2D_plot.pdf)", "Title: " + steps[i])
        else:
            c.Print(output_dir + "ZH_2D_plot.pdf", "Title: " +  steps[i])
        c.Close() # Delete Canvas after saving it
                
