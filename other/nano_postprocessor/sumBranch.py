import os
import ROOT

def sum_branch_in_directory(directory, branch_name):
    total_sum = 0

    # Iterate through all files in the directory
    for filename in os.listdir(directory):
        if filename.endswith(".root"):
            filepath = os.path.join(directory, filename)
            print(f"Processing file: {filepath}")

            # Open the ROOT file
            root_file = ROOT.TFile.Open(filepath)
            if not root_file or root_file.IsZombie():
                print(f"Error opening file: {filepath}")
                continue

            # Access the TTree (assuming the first tree in the file)
            tree = root_file.Get("Runs")
            if not tree:
                print(f"No tree found in file: {filepath}")
                root_file.Close()
                continue

            # Sum the branch values
            for entry in tree:
                total_sum += getattr(entry, branch_name, 0)

            # Close the ROOT file
            root_file.Close()

    print(f"Total sum of branch '{branch_name}': {total_sum}")
    return total_sum

if __name__ == "__main__":
    directory_path = input("Enter the directory containing ROOT files: ").strip()
    branch_name = "genEventCount"  # Change this if you have a different branch name

    if os.path.isdir(directory_path):
        sum_branch_in_directory(directory_path, branch_name)
    else:
        print("Invalid directory path.")

