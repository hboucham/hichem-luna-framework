#!/usr/bin/env python

##########################################################################################
# Script by Hichem Bouchamaoui to compare number of events and files between
# DAS and EOS after running postprocessor
#
# How to run: python3 postprocessor_count.py DAS_dataset_MiniAOD EOS_dataset_directory
#
# Example: python3 postprocessor_count.py /DYJetsToLL_M-50_HT-1200to2500_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM /eos/cms/store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v2_2024_11_22/DYJetsToLL_M-50_HT-1200to2500_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8/r2/241212_210806/0000/
#
# Don't forget to activate proxy beforehand: voms-proxy-init -voms cms -rfc -valid 192:00
#########################################################################################

# necessary imporyts
import sys
import os
import subprocess
import ROOT


# get sample information from DAS
dataset = sys.argv[1]  
command = 'dasgoclient -query="dataset dataset=' + dataset + '" -json | grep nevents' 
#command = 'dasgoclient -query="dataset dataset=/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8/RunIISummer16NanoAODv7-PUMoriond17_Nano02Apr2020_102X_mcRun2_asymptotic_v8-v1/NANOAODSIM" -json | grep nevents' 

# Load dasgo command output and convert it from byte to string
das_output_b = subprocess.check_output(command, shell = True)
das_output = das_output_b.decode('UTF-8')

# Parse dasgo ouput and find relevant information
pos_a = das_output.find("nevents")
pos_b = das_output.find("nfiles")
pos_c = das_output.find("nlumis")
if pos_a != -1 and pos_b != -1 and pos_c != -1:
    ev_start = pos_a + len("nevents") + 2
    ev_end = pos_b - 2
    f_start = pos_b + len("nfiles") + 2
    f_end = pos_c - 2
    #print(das_output[start:end])
    das_events = int(das_output[ev_start:ev_end])
    das_files = int(das_output[f_start:f_end])
else:
    print("Error with dasgo command output, did you forget to run $voms-proxy-init -voms cms -rfc -valid 192:00 ?")

# Pipe list of eos output files in a dummy text file that will be loaded by TChain
eos_dir = sys.argv[2]
command_b = "ls " + eos_dir + "PNet_v1_Skim_* > test_file.txt"
os.system(command_b)
#ls /eos/cms/store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v2_2024_11_22/DYJetsToLL_M-50_HT-1200to2500_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8/r2/241212_210806/0000/PNet_v1_Skim_* > test_file.txt

# Loading output root files
input_file = "test_file.txt"
root_files = open(input_file, "r")
# Runs tree has the information we need
chain = ROOT.TChain("Runs")
for line in root_files:
    #print("Adding file: " + line)
    chain.Add(line[:-1])

# Since there is one genEventCount entry per file, total number of entries is equivalent to number of files
tree = chain
N = tree.GetEntries()

# init total number of events and sum all genEventCount entries 
sumEvent = 0
for event in tree:
    sumEvent = sumEvent + event.genEventCount

# Helpful print outs
print(">>>>> Number of input events in DAS = " + str(das_events) + " vs number of output events in eos (GenEventCount) = " + str(sumEvent) + ", approximately " + str(round(100*sumEvent/das_events, 2)) + " % completion")
print(">>>>> Number of input files in DAS = " + str(das_files) + " vs number of output files in eos = " + str(N) + ", approximately " + str(round(100*N/das_files, 2))+ " % completion")
