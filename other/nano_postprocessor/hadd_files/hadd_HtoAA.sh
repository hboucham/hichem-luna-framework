#!/bin/bash

# Script to run hadd macro given a sample
TOP='/eos/cms/store/group/phys_susy/HToaaTo4b/NanoAOD/2018/MC/PNet_v2_2024_11_22/'
#TOP='/eos/cms/store/group/phys_susy/HToaaTo4b/NanoAOD/2018/data/PNet_v2_2024_11_22/'
MACRO='/afs/cern.ch/work/h/hboucham/Haa4b/NanoPostprocessorV2_r2/CMSSW_10_6_30/src/PhysicsTools/NanoAOD/macros/haddnano.py'
#DATA='r1_Run2018A'

#cd ${TOP}/DYJetsToLL_M-50_HT-1200to2500_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8/r2/
#cd ${TOP}/DYJetsToLL_M-50_HT-2500toInf_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8/r2/
#cd ${TOP}/DYJetsToLL_M-50_HT-200to400_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8/r1/
#cd ${TOP}/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/r1/
#cd ${TOP}/GJets_HT-600ToInf_TuneCP5_13TeV-madgraphMLM-pythia8/r3/
#cd ${TOP}/WplusH_HToBB_WToLNu_M-125_TuneCP5_13TeV-powheg-pythia8/r3/
#cd ${TOP}/ZH_HToSSTobbbb_ZToLL_MH-125_MS-15_ctauS-0_TuneCP5_13TeV-powheg-pythia8/r1/
#cd ${TOP}/ZH_HToSSTobbbb_ZToLL_MH-125_MS-15_ctauS-0p01_TuneCP5_13TeV-powheg-pythia8/r1/
cd ${TOP}/ZH_HToSSTobbbb_ZToLL_MH-125_MS-15_ctauS-0p1_TuneCP5_13TeV-powheg-pythia8/r1/
#cd ${TOP}/SingleMuon/${DATA}/
#cd ${TOP}/EGamma/${DATA}/
#cd ${TOP}/SUSY_ZH_ZToAll_HToAATo4B_Pt150_M-30_TuneCP5_13TeV_madgraph_pythia8/r1/
pwd

echo "${MACRO} PNet_v1_Skim.root */*/PNet_v1_Skim_*.root"
python ${MACRO} PNet_v1_Skim.root */*/PNet_v1_Skim_*.root

#for IDX in {0..9}; do
#  echo "python ${MACRO} PNet_v1_Skim_${IDX}.root */*/PNet_v1_Skim_*${IDX}.root"
#  python ${MACRO} PNet_v1_Skim_${IDX}.root */*/PNet_v1_Skim_*${IDX}.root
  #echo "python ${MACRO} PNet_v1_Skim_${DATA}_${IDX}.root */*/PNet_v1_Skim_*${IDX}.root"
  #python ${MACRO} PNet_v1_Skim_${DATA}_${IDX}.root */*/PNet_v1_Skim_*${IDX}.root
#done
   
