#!/bin/env python

## Accessed on 2023.10.07
## https://github.com/cms-nanoAOD/nanoAOD-tools/blob/master/scripts/haddnano.py
## wget https://raw.githubusercontent.com/cms-nanoAOD/nanoAOD-tools/master/scripts/haddnano.py
## Script by Andrew that correctly hadds postprocessor files
## updated by Hichem to handle empty Root files and print out fraction of events skipped when hadding 

import ROOT
import numpy
import sys

if len(sys.argv) < 3:
    print("Syntax: haddnano.py out.root input1.root input2.root ...")
ofname = sys.argv[1]
files = sys.argv[2:]

# Function added by Hichem to check if root file is empty
def check_root_file_events(file_path, tree_name):
    # Open the ROOT file
    root_file = ROOT.TFile.Open(file_path)
    if not root_file or root_file.IsZombie():
        #print(f"Error: Cannot open {file_path}")
        return False

    # Check for a tree in the file
    tree = root_file.Get(tree_name)  
    if not tree:
        #print(f"No TTree found in {file_path}")
        return False
    else:
        # Get the number of entries (events) in the tree
        num_events = tree.GetEntries()
        if num_events == 0:
            #print(f"The ROOT file {file_path} has zero events.")
            return False
        else:
            #print(f"The ROOT file {file_path} has {num_events} events.")
            return True

    # Close the file
    root_file.Close()


def zeroFill(tree, brName, brObj, allowNonBool=False):
    # typename: (numpy type code, root type code)
    branch_type_dict = {'Bool_t': ('?', 'O'), 'Float_t': ('f4', 'F'), 'UInt_t': (
        'u4', 'i'), 'Long64_t': ('i8', 'L'), 'Double_t': ('f8', 'D')}
    brType = brObj.GetLeaf(brName).GetTypeName()
    if (not allowNonBool) and (brType != "Bool_t"):
        print(("Did not expect to back fill non-boolean branches ", tree, brName, brObj.GetLeaf(br).GetTypeName()))
    else:
        if brType not in branch_type_dict:
            raise RuntimeError('Impossible to backfill branch of type %s' % brType)
        buff = numpy.zeros(1, dtype=numpy.dtype(branch_type_dict[brType][0]))
        b = tree.Branch(brName, buff, brName + "/" +
                        branch_type_dict[brType][1])
        # be sure we do not trigger flushing
        b.SetBasketSize(tree.GetEntries() * 2)
        for x in range(0, tree.GetEntries()):
            b.Fill()
        b.ResetAddress()


fileHandles = []
goFast = True
count_skip = 0
for fn in files:
  # check if files is empty
  if check_root_file_events(fn, "Events") == False:
    count_skip = count_skip + 1
    continue
  else :
    print("Adding file " + str(fn))
    fileHandles.append(ROOT.TFile.Open(fn))
    if fileHandles[-1].GetCompressionSettings() != fileHandles[0].GetCompressionSettings():
        goFast = False
        print("Disabling fast merging as inputs have different compressions")
        
print("Fraction of files skipped: " + str(count_skip) + "/" + str(len(files)))

of = ROOT.TFile(ofname, "recreate")
if goFast:
    of.SetCompressionSettings(fileHandles[0].GetCompressionSettings())
of.cd()

for e in fileHandles[0].GetListOfKeys():
    name = e.GetName()
    print("Merging" + str(name))
    obj = e.ReadObj()
    cl = ROOT.TClass.GetClass(e.GetClassName())
    inputs = ROOT.TList()
    isTree = obj.IsA().InheritsFrom(ROOT.TTree.Class())
    if isTree:
        obj = obj.CloneTree(-1, "fast" if goFast else "")
        branchNames = set([x.GetName() for x in obj.GetListOfBranches()])
    ifh = 0
    for fh in fileHandles[1:]:
        ifh += 1
        try:
            otherObj = fh.GetListOfKeys().FindObject(name).ReadObj()
        except:
            print('\n\nFAILURE!!! Trying to find %s! Did not work :(\n\n' % name)
            print('File #%d' % ifh)
            print(files[ifh])
            sys.exit()
        inputs.Add(otherObj)
        if isTree and obj.GetName() == 'Events':
            otherObj.SetAutoFlush(0)
            otherBranches = set([x.GetName()
                                 for x in otherObj.GetListOfBranches()])
            missingBranches = list(branchNames - otherBranches)
            additionalBranches = list(otherBranches - branchNames)
            print("missing: " + str(missingBranches) + "\n Additional: " + str(additionalBranches))
            for br in missingBranches:
                # fill "Other"
                zeroFill(otherObj, br, obj.GetListOfBranches().FindObject(br))
            for br in additionalBranches:
                # fill main
                branchNames.add(br)
                zeroFill(obj, br, otherObj.GetListOfBranches().FindObject(br))
            # merge immediately for trees
        if isTree and obj.GetName() == 'Runs':
            otherObj.SetAutoFlush(0)
            otherBranches = set([x.GetName()
                                 for x in otherObj.GetListOfBranches()])
            missingBranches = list(branchNames - otherBranches)
            additionalBranches = list(otherBranches - branchNames)
            print("missing: " + str(missingBranches) + "\n Additional: " + str(additionalBranches))
            for br in missingBranches:
                # fill "Other"
                zeroFill(otherObj, br, obj.GetListOfBranches(
                ).FindObject(br), allowNonBool=True)
            for br in additionalBranches:
                # fill main
                branchNames.add(br)
                zeroFill(obj, br, otherObj.GetListOfBranches(
                ).FindObject(br), allowNonBool=True)
            # merge immediately for trees
        if isTree:
            obj.Merge(inputs, "fast" if goFast else "")
            inputs.Clear()

    if isTree:
        obj.Write()
    elif obj.IsA().InheritsFrom(ROOT.TH1.Class()):
        obj.Merge(inputs)
        obj.Write()
    elif obj.IsA().InheritsFrom(ROOT.TObjString.Class()):
        for st in inputs:
            if st.GetString() != obj.GetString():
                print("Strings are not matching")
        obj.Write()
    elif obj.IsA().InheritsFrom(ROOT.THnSparse.Class()) :
        obj.Merge(inputs)
        obj.Write()
    else:
        print("Cannot handle " + str(obj.IsA().GetName()))
