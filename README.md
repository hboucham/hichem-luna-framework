# H -> aa -> 4b, ZH(ll) LUNA framework

## Introduction
This Framework is based on a [Skimmer demo](https://gitlab.cern.ch/skkwan/demo-luna-framework) by Stephanie Kwan. Some additional modules have been inspired from the following frameworks:
- Stephanie Kwan' [framework](https://gitlab.cern.ch/skkwan/lunaFramework) developed for the `h->aa->bbtautau` analysis 
- [vlq-BtoTW](https://github.com/jmhogan/vlq-BtoTW-RDF/blob/main/analyzer_RDF.cc)

## Current features

- Instructions to run in a CMSSW environment, tested on lxplus9. There is no `cmsRun` command here, we just use CMSSW for the environment.
- Each of the following scripts is available in 3 flavors: single lepton (WH/ttH), dilepton ZH and dilepton ttH. 
- Analyzer under skim directory: skimming code, which takes NanoAOD as input, computes kinematic variables, performs event selection and saves relevant branches as a TTree in eos directory. 
    - Can/should use condor script under Condor_skim to run analyzer
- Postprocessor under postprocess directory: takes Analyzer output files and computes scale factors and other corrections, then saves TTree with the new branches in eos directory
- Data-MC plotter under histo/Plotting: takes postprocessor output files and makes Data-MC plots.
- 2D Plotter under histo/Plotting: takes postprocessor output files and makes 2D plots that will be used as input to 2D_Alphabet.
- More details to follow..

## How to run (e.g: dilepton ZH)
- make sure you run cmsenv and activate your grid certificate
- edit ZH selection: skim/helpers/selection_2L_Z.H and skim/skim_2L_Z.cxx
- run ZH analyzer via condor under Condor_skim/2L_Z: bash runCondor_2L_Z.sh 
- Once jobs finish running, run postprocessor under postprocess: bash runAllPostprocess_2L_Z.sh
- Make Data-MC plots under histo/Plotting: bash runData-MC_2L_Z.sh
- Make 2D Plots for mu, el and mu+el, copy root files (and pdfs) to eos directory: bash run2D_2L_Z.sh